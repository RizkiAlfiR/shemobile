import React, { Component } from "react";
import { Platform, Image, TouchableOpacity } from "react-native";
import { Footer, FooterTab, Text, Button, Icon, View } from "native-base";
import colors from "../../styles/colors";

import styles from "../screens/styles/CustomFooter";

class CustomRadioButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      statRadio: []
    };
  }


  render() {
    // var statRadio=[]
    // for (let i=0;i<this.props.option.length;i++){
    //     statRadio[i]=false
    // }
    // this.setState({
    //     statRadio:statRadio
    // })
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={{flexDirection: "row",marginBottom:10 }}>
          <View
            style={{
              height: 24,
              width: 24,
              borderRadius: 12,
              borderWidth: 2,
              borderColor: colors.green0,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            {this.props.selected ? (
              <View
                style={{
                  height: 12,
                  width: 12,
                  borderRadius: 6,
                  backgroundColor: colors.green0
                }}
              />
            ) : null}
          </View>
          <Text> {this.props.name}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default CustomRadioButton;
