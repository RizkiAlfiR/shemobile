import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import colors from "../../styles/colors";

export default class ListViewReport extends Component {
    render() {
        return (
            <View
                style={{
                    flexDirection: "column", flex: 1 
                }}
            >
                <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ marginLeft: 20, marginTop: 10,marginBottom: 10, flex: 2 }}>
                        <Text style={{ fontSize: 10, fontWeight: "bold", color: colors.Orange }}>No. Dokumen : {this.props.document}</Text>
                        {/* <Text style={{ fontSize: 9, paddingTop: 5 }}>{this.props.inspector} [{this.props.shift}]</Text> */}
                        <Text style={{ fontSize: 9, paddingTop: 5 }}>Lokasi : {this.props.lokasi}</Text>
                        <Text style={{ fontSize: 9, paddingTop: 2 }}>Type : {this.props.type}</Text>
                        {/* <Text style={{ fontSize: 9, paddingTop: 8 }}>Updated By : {this.props.update_inspector}</Text>
                        <Text style={{ fontSize: 8, paddingTop: 2, fontStyle: "italic" }}>Updated At : {this.props.update_date}</Text> */}
                    </View>
                    <View style={{ marginTop: 10,marginBottom: 10, marginRight: 20, flex: 1 }}>
                        <View style={{flexDirection: "row",alignSelf: "flex-end", }}>
                        {this.props.status == 'OPEN' ? (
                            <Text
                                note
                                style={{
                                    alignSelf: "flex-end",
                                    fontSize: 10,
                                    fontWeight: 'bold',
                                    color: "white",
                                    backgroundColor: colors.green01,
                                    paddingRight: 4,
                                    paddingLeft: 4,
                                    paddingBottom: 2,
                                    paddingTop: 2,
                                    borderRadius: 6
                                }}
                            >OPEN
                            </Text>
                        ) : (
                            <Text
                                note
                                style={{
                                    alignSelf: "flex-end",
                                    fontSize: 10,
                                    fontWeight: 'bold',
                                    color: "white",
                                    backgroundColor: colors.red,
                                    paddingRight: 4,
                                    paddingLeft: 4,
                                    paddingBottom: 2,
                                    paddingTop: 2,
                                    borderRadius: 6
                                }}
                            >CLOSE
                            </Text>
                        )}
                        {this.props.priority == '1' && (
                            <Text
                                note
                                style={{
                                    alignSelf: "flex-end",
                                    fontSize: 10,
                                    fontWeight: 'bold',
                                    color: "white",
                                    backgroundColor: colors.red,
                                    paddingRight: 4,
                                    paddingLeft: 4,
                                    paddingBottom: 2,
                                    paddingTop: 2,
                                    borderRadius: 6,
                                    marginLeft:5
                                }}
                            >HIGH
                            </Text>
                        ) }
                        {this.props.priority == '2' && (    
                            <Text
                                note
                                style={{
                                    alignSelf: "flex-end",
                                    fontSize: 10,
                                    fontWeight: 'bold',
                                    color: "white",
                                    backgroundColor: colors.Orange,
                                    paddingRight: 4,
                                    paddingLeft: 4,
                                    paddingBottom: 2,
                                    paddingTop: 2,
                                    borderRadius: 6,
                                    marginLeft:5
                                }}
                            >MEDIUM
                            </Text>
                        )}
                        {this.props.priority == '3' && (    
                            <Text
                                note
                                style={{
                                    alignSelf: "flex-end",
                                    fontSize: 10,
                                    fontWeight: 'bold',
                                    color: "white",
                                    backgroundColor: colors.green0,
                                    paddingRight: 4,
                                    paddingLeft: 4,
                                    paddingBottom: 2,
                                    paddingTop: 2,
                                    borderRadius: 6,
                                    marginLeft:5
                                }}
                            >LOW
                            </Text>
                        )}
                        
                        </View>
                        
                        <Text style={{ fontSize: 8, marginTop: 15, fontStyle: "italic",alignSelf: "flex-end", }}>Tanggal : {this.props.date}</Text>
                    </View>
                </View>
            </View>
        );
    }
}
