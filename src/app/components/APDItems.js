import React, { Component } from "react";
import { View, Text, Image, TouchableNativeFeedback } from "react-native";
import Ripple from "react-native-material-ripple";
import colors from "../../styles/colors";
import PropTypes from "prop-types";

let styles = {
  container: {
    height: 60,
    width: 40,
    marginLeft: 7
  },
  b: {
    backgroundColor: colors.primary
  }
};

export default class APDItems extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { loading, disabled, handleOnPress } = this.props;
    return (
      <View style={[styles.container]}>
        <Ripple
          // style={this.props.clicked ? {
          //   flex: 2,
          //   justifyContent: "center",
          //   alignItems: "center",
          //   borderRadius: 10,
          //   elevation: 1,
          //   borderColor: "#27ae60", borderWidth: 5//j try
          // }: {
          //   flex: 2,
          //   justifyContent: "center",
          //   alignItems: "center",
          //   borderRadius: 10,
          //   elevation: 1,
          // }}
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center",
            borderRadius: 10,
            elevation: 1
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={handleOnPress}
          rippleColor={colors.accent}
        >
          <Image
            source={this.props.imageUri}
            style={{ width: 25, height: 25, resizeMode: "contain" }}
          />
        </Ripple>
        <View style={{ flex: 1, paddingTop: 3, alignItems: "center" }}>
          <Text style={this.props.clicked ? {
            fontSize: 9 ,
            color:"#27ae60",
            fontWeight:'bold',
          }: {
            fontSize: 9,
          }}>{this.props.name}</Text>
        </View>
      </View>
    );
  }
}

APDItems.propTypes = {
  handleOnPress: PropTypes.func,
  disabled: PropTypes.bool
};
