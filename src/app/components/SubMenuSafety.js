import React, { Component } from "react";
import { View, Text, Image, TouchableNativeFeedback } from "react-native";
import { Icon } from "native-base";
import Ripple from "react-native-material-ripple";
import colors from "../../styles/colors";
import PropTypes from "prop-types";

let styles = {
  container: {
    height: 100,
    flex: 1,

    alignItems: 'center'
  },
  b: {
    backgroundColor: colors.primary
  }
};

export default class SubMenuSafety extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { loading, disabled, handleOnPress } = this.props;
    return (
      <View style={[styles.container]}>
        <Ripple
          rippleSize={276}
          rippleDuration={600}
          rippleContainerBorderRadius={100}
          onPress={handleOnPress}
          rippleColor={colors.accent}
        >
          <Image
            source={this.props.imageUri}
            style={{ width: 50, height: 50, resizeMode: "contain", marginTop: 2, marginBottom: 2 }}
          />
        </Ripple>
        <View style={{ flex: 1, paddingTop: 5, alignItems: "center" }}>
          <Text style={{ fontSize: 11 }}>{this.props.name}</Text>
        </View>
      </View>
    );
  }
}


SubMenuSafety.propTypes = {
  handleOnPress: PropTypes.func,
  disabled: PropTypes.bool
};
