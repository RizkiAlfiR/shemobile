import React, { Component } from "react";
import { View, Text, Image, ImageBackground } from "react-native";
import { Thumbnail } from "native-base";
import colors from '../../styles/colors';

export default class ListViewReportDetail extends Component {
  render() {
    return (
      <View
        style={{
          height: 120,
          width: "100%",
        }}
      >
        <View style={{ flex: 1, flexDirection: "row" }}>
          <View style={{ marginLeft: 10, marginTop: 10, flex: 2 }}>
            <Text style={{ fontSize: 10, fontWeight: "bold", color: colors.green01 }}>No. Dokumen Detail Report : {this.props.document}</Text>
            <Text style={{ fontSize: 10, fontWeight: "bold", paddingTop: 5 }}>Prioritas : {this.props.type_priority}</Text>
            {this.props.type_found == 'UC' ? (
              <Text
                note
                style={{
                  fontSize: 9,
                  paddingTop: 0,
                }}
              >Jenis Temuan : Unsafe Condition
              </Text>
            ) : (
                <Text
                  note
                  style={{
                    fontSize: 9,
                    paddingTop: 0,
                  }}
                >Jenis Temuan : Unsafe Action
                </Text>
              )}
            <Text style={{ fontSize: 9, paddingTop: 0, fontWeight: 'bold' }}>Ditemukan Oleh : User ID {this.props.employee}</Text>
            <Text style={{ fontSize: 9, paddingTop: 0 }}>Temuan : {this.props.found}</Text>
            <Text style={{ fontSize: 9, paddingTop: 0 }}>Lokasi : {this.props.location}</Text>
            {/* <Image style={{marginTop:10, height: 200, width: 300, borderColor: colors.gray, borderWidth: 3}} source={{uri : this.props.image_bef}}/> */}
          </View>
          <View style={{ marginTop: 10, marginRight: 20, flex: 1 }}>
            {this.props.status == 'OPEN' ? (
              <Text
                note
                style={{
                  alignSelf: "flex-end",
                  fontSize: 10,
                  fontWeight: 'bold',
                  color: "white",
                  backgroundColor: colors.red,
                  paddingRight: 4,
                  paddingLeft: 4,
                  paddingBottom: 2,
                  paddingTop: 2,
                  borderRadius: 6
                }}
              >OPEN
              </Text>
            ) : (
                <Text
                  note
                  style={{
                    alignSelf: "flex-end",
                    fontSize: 10,
                    fontWeight: 'bold',
                    color: "white",
                    backgroundColor: colors.green01,
                    paddingRight: 4,
                    paddingLeft: 4,
                    paddingBottom: 2,
                    paddingTop: 2,
                    borderRadius: 6
                  }}
                >CLOSE
                </Text>
              )}
            {/* <Text style={{ fontSize: 8, fontStyle: "italic", textAlign: 'right', marginTop: 50 }}>Tanggal Comitment : {this.props.date}</Text> */}
            {this.props.date != null ? (
              <Text style={{ fontSize: 8, fontStyle: "italic", textAlign: 'right', marginTop: 50 }}>Tanggal Comitment : {this.props.date}</Text>
            ) : (
                <Text style={{ fontSize: 8, fontStyle: "italic", textAlign: 'right', marginTop: 50 }}>Tanggal Comitment : -</Text>
              )}
          </View>
        </View>
      </View>
    );
  }
}
