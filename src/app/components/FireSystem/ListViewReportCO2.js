import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import colors from "../../../styles/colors";
import GlobalConfig from "../GlobalConfig";
import Moment from 'moment';

export default class ListViewReportCO2 extends Component {
  render() {
    return (
      <View
        style={{
          height: "100%",
          width: "100%",
        }}
      >
        <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{marginLeft:5, marginTop:0, width:'20%'}}>
              <Image
                source={this.props.imageUri}
                style={{ width: '100%', height: '100%', resizeMode: "contain" }}
              />
            </View>
            <View style={{marginLeft:5, marginBottom:5, paddingRight:5, marginRight:5, width:'75%'}}>
              <View style={{flex:1, flexDirection:'row'}}>
                <View style={{marginTop:5, paddingRight:5, paddingLeft:10, marginRight:5, width:'80%'}}>
                    <Text style={{fontSize:12, fontWeight:'bold'}}>{this.props.pplantdesc}</Text>
                </View>
                <View style={{marginTop:5, paddingRight:5, marginRight:5, width:'20%'}}>
                    <Text style={{fontSize:10, fontWeight:"bold", textAlign:'right', color:colors.green01}}>Shift {this.props.shift}</Text>
                </View>
              </View>
              <View style={{marginLeft:5}}>
                <Text style={{fontSize:10, paddingLeft:5}}>{Moment(this.props.date).format('DD MMMM YYYY')}, {this.props.time}</Text>
                <View style={{flex:1, flexDirection:'row'}}>
                  <View style={{marginTop:5, paddingRight:5, paddingLeft:5, marginRight:5, width:'50%'}}>
                    <Text style={{fontSize:9, marginBottom:2}}>Residual Gas Cemetron</Text>
                    {this.props.residualCemetron <= 4 ? (
                      <View style={{backgroundColor:colors.red, height:20, width:'30%', borderRadius:5, alignItems:'center', justifyContent:'center'}}>
                        <Text style={{fontSize:11, color:colors.white, fontWeight:'bold'}}>{this.props.residualCemetron}</Text>
                      </View>
                    ):(
                      <View style={{backgroundColor:colors.green0, height:20, width:'30%', borderRadius:5, alignItems:'center', justifyContent:'center'}}>
                        <Text style={{fontSize:11, color:colors.white, fontWeight:'bold'}}>{this.props.residualCemetron}</Text>
                      </View>
                    )}
                  </View>
                  <View style={{marginTop:5, paddingRight:5, paddingLeft:5, marginRight:5, width:'50%'}}>
                    <Text style={{fontSize:9, marginBottom:2}}>Residual Gas Samator</Text>
                    {this.props.residualSamator <= 8 ? (
                      <View style={{backgroundColor:colors.red, height:20, width:'30%', borderRadius:5, alignItems:'center', justifyContent:'center'}}>
                        <Text style={{fontSize:11, color:colors.white, fontWeight:'bold'}}>{this.props.residualSamator}</Text>
                      </View>
                    ):(
                      <View style={{backgroundColor:colors.green0, height:20, width:'30%', borderRadius:5, alignItems:'center', justifyContent:'center'}}>
                        <Text style={{fontSize:11, color:colors.white, fontWeight:'bold'}}>{this.props.residualSamator}</Text>
                      </View>
                    )}
                  </View>
                </View>
              </View>
            </View>
        </View>
      </View>
    );
  }
}
