import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import colors from "../../../styles/colors";
import Moment from 'moment';

export default class ListViewLotto extends Component {
  render() {
    return (
      <View
        style={{
          height: '100%',
          width: "100%",
          paddingBottom:10,
        }}
      >
        <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{marginLeft:5, marginTop:5, width:'18%'}}>
              <Image
                source={this.props.imageUri}
                style={{ marginTop:5, width: 50, height: 50, resizeMode: "contain" }}
              />
            </View>

            <View style={{marginTop:5, marginRight:5, width:'59%'}}>
                <Text style={{fontSize:12, fontWeight:'bold'}}>Equipment Number : {this.props.equipmentNumber}</Text>
                <Text style={{fontSize:10, paddingTop: 0}}>{this.props.area}</Text>
                <Text style={{fontSize:11, paddingTop:0}}>No Era : {this.props.noER}</Text>
                <Text style={{fontSize:11, paddingTop:0}}>Activity : {this.props.kegiatan}</Text>
            </View>

            <View style={{marginTop:5, paddingRight: 5, marginRight:5, width:'20%'}}>
                <Text style={{fontSize:10, paddingTop:0}}>{Moment(this.props.date).format('DD MMMM YYYY')}</Text>
            </View>
        </View>
      </View>
    );
  }
}
