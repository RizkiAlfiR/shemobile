import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import colors from "../../../styles/colors";
import GlobalConfig from "../GlobalConfig";
import Moment from 'moment';

export default class NewsFire extends Component {

  render() {
    return (
      <View style={{ height: 110, width: 155, paddingLeft:5, paddingRight:5 }}>
          <View style={{paddingTop:8, paddingBottom:5}}>
            <Image
                source={this.props.imageUri}
                style={{ marginTop:0, width: '100%', height: 50, resizeMode: "contain", borderRadius:5}}
            />
          </View>
          <View style={{alignItems:'center'}}>
            <Text style={{fontSize:10, fontWeight:'bold', color:colors.green01}}>{this.props.number}</Text>
            <Text style={{fontSize:10, fontWeight:'bold'}}>{this.props.place}</Text>
            <Text style={{fontSize:9, marginTop:0}}>{Moment(this.props.date).format('DD MMMM YYYY')}</Text>
          </View>
      </View>

    );
  }
}
