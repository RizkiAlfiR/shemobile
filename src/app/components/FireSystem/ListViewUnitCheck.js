import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import colors from "../../../styles/colors";
import GlobalConfig from "../GlobalConfig";
import Moment from 'moment'

export default class ListViewUnitCheck extends Component {
    render() {
        var dateNow = Moment().format('YYYY-MM-DD');
        return (
            <View
                style={{
                    height: '100%',
                    width: "100%",
                }}
            >
                <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ marginLeft: 5, marginTop: 5, width: '18%' }}>
                        <Image
                            source={{uri:GlobalConfig.IMAGEHOST + 'MasterVehicle/' + this.props.imageUri}}
                            style={{ marginTop: 5, width: 50, height: 70, resizeMode: "contain" }}
                        />
                    </View>
                    <View style={{ marginTop: 5, paddingRight: 5, marginRight: 5, width: '50%' }}>
                        <Text style={{ fontSize: 12, fontWeight: 'bold' }}>{this.props.vehicleCode}</Text>
                        <Text style={{ fontSize: 10,}}>{this.props.vehicleMerk}</Text>
                        <Text style={{ fontSize: 9, }}>Tahun : {this.props.tahun}</Text>
                        <Text style={{ fontSize: 9, }}>No Rangka : {this.props.nomorRangka}</Text>
                        <Text style={{ fontSize: 9, }}>Last Inspection : {Moment(this.props.lastInspection).format('DD MMMM YYYY')}</Text>
                    </View>
                    <View style={{ marginTop: 5, paddingRight: 5, marginRight: 5, width: '27%' }}>
                        {this.props.expInspection <= dateNow || this.props.lastInspection==null ? (
                            <Text
                                note
                                style={{
                                    alignSelf: "flex-end",
                                    fontSize: 8,
                                    fontWeight: 'bold',
                                    color: "white",
                                    backgroundColor: colors.red,
                                    paddingRight: 4,
                                    paddingLeft: 4,
                                    paddingBottom: 2,
                                    paddingTop: 2,
                                    borderRadius: 6
                                }}
                            >UNCHECKED
                            </Text>
                        ): (
                          <View>
                            <Text
                                note
                                style={{
                                    alignSelf: "flex-end",
                                    fontSize: 8,
                                    fontWeight: 'bold',
                                    color: "white",
                                    backgroundColor: colors.green01,
                                    paddingRight: 4,
                                    paddingLeft: 4,
                                    paddingBottom: 2,
                                    paddingTop: 2,
                                    borderRadius: 6
                                }}
                              >CHECKED
                            </Text>
                          </View>
                        )}
                    </View>
                </View>
            </View>
        );
    }
}
