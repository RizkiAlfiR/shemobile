import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import colors from "../../../styles/colors";
import GlobalConfig from "../GlobalConfig";
import Moment from 'moment';

export default class ListViewFireReport extends Component {
  render() {
    return (
      <View
        style={{
          height: '100%',
          width: "100%",
          paddingBottom:10,
        }}
      >
        <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{marginLeft:5, marginTop:5, width:'18%'}}>
              <Image
                source={this.props.imageUri}
                style={{ marginTop:5, width: 50, height: 50, resizeMode: "contain" }}
              />
            </View>

            <View style={{marginTop:5, paddingRight:5, marginRight:5, width:'62%'}}>
                <Text style={{fontSize:12, fontWeight:'bold'}}>{this.props.fireNumber}</Text>
                <Text style={{fontSize:10, paddingTop:3}}>Place : {this.props.place}</Text>
                <Text style={{fontSize:11, fontWeight:"bold", color:colors.green01}}>PIC : {this.props.pic}</Text>
                <Text style={{fontSize:10}}>{Moment(this.props.fireDate).format('DD MMMM YYYY')} | {this.props.fireTime}</Text>
            </View>
            <View style={{marginTop:5, paddingRight:5, marginRight:5, width:'15%'}}>
                {this.props.status=='OPEN' ? (
                <Text
                  note
                  style={{
                    alignSelf: "flex-end",
                    fontSize: 11,
                    color: "white",
                    backgroundColor: colors.green01,
                    paddingRight: 4,
                    paddingLeft: 4,
                    paddingBottom: 2,
                    paddingTop: 2,
                    borderRadius: 6
                  }}
                >OPEN
                </Text>
              ):(
                <Text
                  note
                  style={{
                    alignSelf: "flex-end",
                    fontSize: 11,
                    color: "white",
                    backgroundColor: "#FF0101",
                    paddingRight: 4,
                    paddingLeft: 4,
                    paddingBottom: 2,
                    paddingTop: 2,
                    borderRadius: 6
                  }}
                >CLOSE
                </Text>)}
            </View>
        </View>
      </View>
    );
  }
}
