import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import colors from "../../../styles/colors";
import GlobalConfig from "../GlobalConfig";
import Moment from 'moment';

export default class NewsDaily extends Component {
  render() {
    return (
      <View style={{ height: 100, width: 100, paddingLeft:5, paddingRight:5 }}>
          <View style={{paddingTop:5, paddingBottom:5}}>
            <Image
                source={{uri:GlobalConfig.IMAGEHOST + 'DailySectionReport/' +this.props.imageUri}}
                style={{ marginTop:0, width: '100%', height: 50, alignSelf: "stretch", borderRadius:5}}
              />
          </View>
          <View style={{alignItems:'center'}}>
            <Text style={{fontSize:10, fontWeight:'bold', color:colors.green01}}>{this.props.aksi}</Text>
            <Text style={{fontSize:9, marginTop:4}}>{Moment(this.props.date).format('DD MMMM YYYY')}</Text>
          </View>
      </View>

    );
  }
}
