import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import colors from "../../../styles/colors";
import GlobalConfig from "../GlobalConfig";
import Moment from 'moment';

export default class ListViewReportAktifitas extends Component {
  render() {
    return (
      <View
        style={{
          height: "100%",
          width: "100%",
          paddingLeft:5,
          paddingRight:5,
        }}
      >
        <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{width:'30%', paddingTop:5, paddingBottom:5, paddingLeft:5}}>
              <Image
                source={{uri:GlobalConfig.IMAGEHOST + 'DailySectionReport/' +this.props.imageUri}}
                style={{ marginTop:0, width: '100%', height: '100%', alignSelf: "stretch", borderRadius:5}}
              />
            </View>
            <View style={{marginLeft:5, marginBottom:5, paddingRight:5, marginRight:5, width:'70%'}}>
              <View style={{flex:1, flexDirection:'row'}}>
                <View style={{width:'80%'}}>
                  <Text style={{fontSize:12, paddingTop:4, fontWeight:"bold", color:colors.green01}}>{this.props.aksi}</Text>
                  <Text style={{fontSize:10, fontWeight:'bold'}}>{Moment(this.props.date).format('DD MMMM YYYY')}</Text>
                </View>
                <View style={{marginTop:5, paddingRight:5, marginRight:5, width:'20%'}}>
                    {this.props.status=='OPEN' ? (
                    <Text
                      note
                      style={{
                        alignSelf: "flex-end",
                        fontSize: 11,
                        color: "white",
                        backgroundColor: colors.green01,
                        paddingRight: 4,
                        paddingLeft: 4,
                        paddingBottom: 2,
                        paddingTop: 2,
                        borderRadius: 6
                      }}
                    >OPEN
                    </Text>
                  ):(
                    <Text
                      note
                      style={{
                        alignSelf: "flex-end",
                        fontSize: 11,
                        color: "white",
                        backgroundColor: "#FF0101",
                        paddingRight: 4,
                        paddingLeft: 4,
                        paddingBottom: 2,
                        paddingTop: 2,
                        borderRadius: 6
                      }}
                    >CLOSE
                    </Text>)}
                </View>
              </View>
              <View>
                <Text style={{fontSize:10}}>{this.props.aktifitas}</Text>
                <Text style={{fontSize:9, paddingRight:5, textAlign:'right'}}>Time Activity :</Text>
                <Text style={{fontSize:9, paddingRight:5, textAlign:'right'}}>{this.props.timeStart} - {this.props.timeEnd}</Text>
              </View>
            </View>
        </View>
      </View>
    );
  }
}
