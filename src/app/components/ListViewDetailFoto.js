import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import colors from '../../styles/colors';

export default class ListViewDetailFoto extends Component {
  render() {
    return (
      <View
        style={{
          height: 50,
          width: "100%",
        }}
      >
        <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{marginLeft:10, marginTop:10, width:260}}>
                <Text style={{fontSize:10, fontWeight:"bold"}}>{this.props.unitCode}</Text>
                <Text style={{fontSize:9, paddingTop:5}}>{this.props.type}</Text>
            </View>
            <View style={{marginTop:10, width:70}}>
                <Text style={{fontSize:8, fontStyle:"italic"}}>{this.props.date}</Text>
                <Text style={{fontSize:8, paddingTop:5, fontWeight:"bold", color:colors.green01}}>{this.props.status}</Text>
            </View>
            <View>
            <Image style={{marginTop:10, height: 200, width: 300, borderColor: colors.gray, borderWidth: 3}} source={{uri : this.props.img}}/>
            </View>
        </View>
      </View>
    );
  }
}
