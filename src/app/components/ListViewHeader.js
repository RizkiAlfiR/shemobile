import React, { Component } from "react";
import { View, Text, Image } from "react-native";

export default class ListViewHeader extends Component {
  render() {
    return (
      <View
        style={{
          height: 85,
          width: "100%",
        }}
      >
        <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{marginLeft:10, marginTop:10, width:260}}>
                <Text style={{fontSize:10, fontWeight:"bold"}}>{this.props.area}</Text>
                <Text style={{fontSize:9, paddingTop:5}}>{this.props.subArea}</Text>
                <Text style={{fontSize:9, paddingTop:2}}>{this.props.plant}</Text>
                <Text style={{fontSize:9, fontWeight:"bold", paddingTop:2}}>{this.props.inspector}</Text>
            </View>
            <View style={{marginTop:10, width:70}}>
                <Text style={{fontSize:8, fontStyle:"italic"}}>{this.props.date}</Text>
                <Text style={{fontSize:8}}>Shift {this.props.shift}</Text>
            </View>
        </View>
      </View>
    );
  }
}
