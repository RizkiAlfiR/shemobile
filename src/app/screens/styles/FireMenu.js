import { StyleSheet } from "react-native";
import iPhoneSize from "../../../helpers/utils";
import colors from "../../../styles/colors";
import { Row } from "native-base";

let labelTextSize = 12;
let weatherTextSize = 18;
let weatherTextSmSize = 14;
let headingTextSize = 28;
if (iPhoneSize() === "small") {
  labelTextSize = 10;
  headingTextSize = 24;
  weatherTextSize = 16;
  weatherTextSmSize = 12;
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    display: "flex",
    backgroundColor:colors.gray
  },
  wrapper2: {
    flex: 1,
    display: "flex",
    backgroundColor:colors.white
  },
  homeWrapper: {
    flex: 1,
    display: "flex",
    borderRadius: 10,
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 15,
    paddingLeft: 20,
    paddingTop: 10,
    elevation: 2,
    height: 140,
  },
  newsWrapper: {
    flex: 1,
    display: "flex",
    borderRadius: 10,
    marginTop: 10,
    marginLeft: 15,
    marginRight: 15,
    paddingLeft: 5,
    paddingTop: 10,
    paddingRight: 5,
    height: 140,
  },
  weatherContent: {
    flexDirection: "row",
    marginBottom: 15,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
  },
  safetyimage: {
    alignItems: 'center',
    width: 130,
    height: 100,
    marginTop: 20,
    marginLeft: 90,
    marginRight: 0,
  },
  weatherText: {
    fontFamily: "Montserrat-Medium",
    color: colors.lightBlack,
    fontWeight: "300",
    fontSize: weatherTextSize,
    alignItems: 'center',
    marginBottom: 5,
    marginLeft: 50,
    marginRight: 50
  },
  textTable:{
    paddingLeft:10,
    fontSize:11,
    paddingTop:5
  },
  itemMenu: {
    flexDirection: "row",
    paddingBottom: 0,
    justifyContent: 'center',
  },
  tabfooter: {
    backgroundColor: colors.green01
  },
  viewKonfirmasiRight: {
    width: 150,
  },
  btnTambah:{
    borderRadius: 5,
    width:38,
    height:38,
    backgroundColor: colors.green01,
    marginTop: 5,
  },
  btnClose:{
    borderRadius: 5,
    width:30,
    height:30,
    backgroundColor: colors.gray,
  },
  btnQTYLeft:{
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    width:30,
    height:30,
    backgroundColor: colors.green01,
    marginTop: 10,
  },
  btnQTYRight:{
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    width:30,
    height:30,
    backgroundColor: colors.green01,
    marginTop: 10,
  },
  facebookButtonIconQTY: {
    color: colors.white,
    fontSize: 20,
    paddingTop:5,
    width:30,
    height:30,
  },
  iconPluss: {
    color: colors.white,
    fontSize: 20,
    fontWeight:'bold',
    paddingTop:5,
    width:30,
    height:30,
  },
  iconClose: {
    color: 'black',
    fontSize: 30,
    fontWeight:'bold',
    width:30,
    height:30,
    flex: 2,
    justifyContent: "center",
    alignItems: "center"
  },
  itemNews: {
    flexDirection: "row",
    paddingBottom: 0,
    justifyContent: 'center',
    paddingTop: 5,
  },
  search: {
    position: 'absolute',
    left: 5,
    right: 5,
    top: 1,
    height: 10,
    flexDirection: 'row',
  },
  searchItem: {
    borderWidth: 2,
    borderColor: colors.green01,
    paddingHorizontal: 5,
    borderRadius: 20,
    marginTop: 60,
  },
  viewLeftHeader: {
    width: 350,
    paddingRight: 0,
  },

  facebookButtonIcon: {
    color: colors.white,
    fontSize: 40
  },
  header: {
    backgroundColor: colors.green01,
  },
  textbody: {
    fontWeight: 'bold',
    color: colors.white,
    fontSize:15,
  },
  newsContent: {
    flexDirection: "row"
  },

  titleInput:{
      fontSize: 10,
  },

  weatherTextSm: {
    fontFamily: "Montserrat-Light",
    fontSize: weatherTextSmSize,
    color: colors.lightBlack,
    fontWeight: "300",
    marginBottom: 10
  },
  viewLeftBtnUpload: {
    // width: 255,
  },
  buttonText: {
    fontWeight: 'bold',
    color: colors.white
  },
  newsText: {
    fontFamily: "Montserrat-Regular",
    fontSize: weatherTextSmSize,
    color: colors.lightBlack,
    fontWeight: "300",
    marginBottom: 5,
    marginLeft: 20
  },
  news: {
    height: 160,
    marginTop: 20
  },

  placeholder: {
    backgroundColor: "#eee",
    width: "100%",
    height: 200,
    borderRadius:15,
  },
  previewImage: {
    width: "100%",
    height: "100%",
    borderRadius:15,
  },
  viewMerkFoto: {
    fontWeight: 'bold'
  },
  shadow:{
    shadowColor:'#000',
    shadowOffset:{
      width:0,
      height:2,
    },
    shadowOpacity:0.25,
    shadowRadius:3.84,
    elevation:5,
  },
  detailTitle:{
    fontSize: 12,
    color:colors.graydark,
    marginBottom:10,
  },
  detailContent:{
    fontSize: 15,
    fontWeight: "bold",
    // color:colors.black,
  },
  detailContent2:{
    fontSize: 13,
    fontWeight: "bold",
    // color:colors.black,
  }


});

export default styles;
