import { StyleSheet } from "react-native";
import iPhoneSize from "../../../helpers/utils";
import colors from "../../../styles/colors";
import { Row } from "native-base";

let labelTextSize = 12;
let weatherTextSize = 18;
let weatherTextSmSize = 14;
let headingTextSize = 28;
if (iPhoneSize() === "small") {
  labelTextSize = 10;
  headingTextSize = 24;
  weatherTextSize = 16;
  weatherTextSmSize = 12;
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    display: "flex",
    backgroundColor: colors.gray
  },
  header: {
    backgroundColor: colors.white
  },
  homeWrapper: {
    flex: 1,
    display: "flex"
  },
  weatherContent: {
    flexDirection: "row",
    // marginBottom: 15,
    borderBottomColor: colors.lightBlack,
    borderBottomWidth: 1,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20
  },
  newsContent: {
    flexDirection: "row"
  },
  weatherText: {
    fontFamily: "Montserrat-Medium",
    fontSize: weatherTextSize,
    color: colors.lightBlack,
    fontWeight: "300",
    marginBottom: 5
  },
  weatherTextTitle: {
    fontFamily: "Montserrat-Medium",
    fontSize: weatherTextSmSize,
    color: colors.lightBlack,
    fontWeight: "300",
    marginBottom: 5
  },
  weatherTextContent: {
    fontFamily: "Montserrat-Light",
    fontSize: labelTextSize,
    color: colors.lightBlack,
    fontWeight: "300",
    marginBottom: 5
  },
  weatherTextContentRight: {
    fontFamily: "Montserrat-Light",
    fontSize: labelTextSize,
    color: colors.lightBlack,
    fontWeight: "300",
    marginBottom: 5,
    textAlign:'right',
  },
  newsKatText: {
    fontFamily: "Montserrat-Medium",
    fontSize: 10,
    marginTop: 5,
    color: colors.lightBlack,
    fontWeight: "300",
  },
  newsTitleText: {
    // fontFamily: "Montserrat-Regular",
    fontSize: 24,
    marginTop: 5,
    color: colors.black,
    fontWeight: "bold",
  },
  newsTextDetail: {
    fontFamily: "Montserrat-Medium",
    fontSize: 14,
    marginTop: 5,
    color: colors.black,
    fontWeight: '300',
  },
  newsTanggalDetail: {
    fontFamily: "Montserrat-Medium",
    fontSize: 12,
    marginTop: 5,
    color: colors.lightBlack,
    fontWeight: '300',
  },
  newsKatDetail: {
    fontFamily: "Montserrat-Medium",
    fontSize: 12,
    marginTop: 5,
    color: colors.green0,
    fontWeight: '300',
  },
  newsKatContent: {
    borderTopColor: colors.lightBlack,
    borderTopWidth: 1,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 5,

  },
  facebookButtonIconOrder2: {
    color: colors.white,
    fontSize: 20
  },
  weatherTextSm: {
    fontFamily: "Montserrat-Light",
    fontSize: weatherTextSmSize,
    color: colors.lightBlack,
    fontWeight: "300",
    marginBottom: 10
  },
  newsText: {
    fontFamily: "Montserrat-Regular",
    fontSize: weatherTextSmSize,
    color: colors.lightBlack,
    fontWeight: "300",
    marginBottom: 5,
    marginLeft: 20
  },
  news: {
    height: 160,
    marginTop: 20
  },
  itemMenu: {
    flexDirection: "row",
    padding: 10,
    marginTop: 10,
    marginBottom: 10, //ediit
    justifyContent: 'center'
  },
});

export default styles;
