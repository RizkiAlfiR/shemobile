import { StyleSheet,Platform,Dimensions } from "react-native";
import iPhoneSize from "../../../helpers/utils";
import colors from "../../../styles/colors";
import { Row } from "native-base";

let labelTextSize = 12;
let weatherTextSize = 18;
let weatherTextSmSize = 14;
let headingTextSize = 28;
if (iPhoneSize() === "small") { 
  labelTextSize = 10;
  headingTextSize = 24;
  weatherTextSize = 16;
  weatherTextSmSize = 12;
}

const styles = StyleSheet.create({

  SnackBarContainter:
  {
    position: 'absolute',
    backgroundColor: colors.green01,
    flexDirection: 'row',
    alignItems: 'center',
    left: 0,
    bottom: (Dimensions.get("window").height===812 && Platform.OS==='ios')?'2%':'0%',
    right: 0,
    height: 50,
    paddingLeft: 10,
    paddingRight: 55
  },

  SnackBarMessage:
  {
    color: '#fff',
    fontSize: 9
  },

  SnackBarUndoText: {
    color: '#FFEB3B',
    fontSize: 9,
    position: 'absolute',
    right: 5,
    justifyContent: 'center',
    padding: 5
  }
});


export default styles;
