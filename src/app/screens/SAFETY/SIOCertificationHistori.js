import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Icon,
    Item,
    Input,
    Thumbnail
} from "native-base";

import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SIOcertification";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewSIOHistori from "../../components/ListViewSIOHistori";
import Ripple from "react-native-material-ripple";

class SIOCertificationHistori extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            dataSource: [],
            isLoading: true,
            noLisensi:[],
        };
    }

    static navigationOptions = {
        header: null
    };

    _renderItem = ({ item }) => (
        <ListItem data={item}></ListItem>
    )

    // componentDidMount() {
    //     this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
    //         this.loadData();
    //     });
    // }

    navigateToScreen(route, detailSIO) {
        AsyncStorage.setItem('listSIO', JSON.stringify(detailSIO)).then(() => {
            this.props.navigation.navigate(route);
        })
    }

    componentDidMount() {
      AsyncStorage.getItem('noLisensi').then((noLisensi) => {
          this.setState({ noLisensi: noLisensi });
          this.setState({ isLoading: false });
          this.loadData();
        //   console.log(this.state.detailTools);
      })
    //   this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
    //       this.loadData();
    //   });
    }


    loadData() {
        AsyncStorage.getItem('token').then((value) => {
            const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/sertification/history/sio';
            var formData = new FormData();
            formData.append("token", value)
            formData.append("NO_LISENSI", this.state.noLisensi)

            fetch(url, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                method: 'POST',
                body: formData
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    // alert(JSON.stringify(responseJson.data.list_temuan));
                    this.setState({
                        dataSource: responseJson.data,
                        isLoading: false
                    });
                })
                .catch((error) => {
                    console.log(error)
                })
        })
    }

    render() {
        var listSIO;
        if (this.state.isLoading) {
            listSIO = (
                <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <ActivityIndicator size="large" color="#330066" animating />
                </View>
            );
        } else {
            if (this.state.dataSource==null) {
                listSIO = (
                    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                        <Thumbnail square large source={require("../../../assets/images/empty.png")} />
                        <Text>No Data!</Text>
                    </View>
                )
            } else {
                listSIO = (
                    <ScrollView>
                    {this.state.dataSource.map((detailSIO) => (
                            <View key={detailSIO} style={{ backgroundColor: "#FEFEFE" }}>
                                <Card style={{ marginLeft: 20, marginRight: 20, borderRadius: 10 }}>
                                    <View style={{ marginTop: 5, marginLeft: 5, marginRight: 5 }}>
                                        <ListViewSIOHistori
                                            image={GlobalConfig.SERVERHOST + 'api' + detailSIO.FOTO}
                                            kelas={detailSIO.KELAS}
                                            noLisensi={detailSIO.NO_LISENSI}
                                            nama={detailSIO.NAMA}
                                            masaBerlaku={detailSIO.MASA_BERLAKU}
                                        />
                                    </View>
                                </Card>
                            </View>
                    ))}
                    </ScrollView>
                );
            }
        }
        return (
            <Container style={styles.wrapper}>
                <Header style={styles.header}>
                    <Left style={{flex:1}}>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate("SIOCertification")}
                        >
                            <Icon
                                name="ios-arrow-back"
                                size={20}
                                style={styles.facebookButtonIconOrder2}
                            />
                        </Button>
                    </Left>
                    <Body style={{flex:3,alignItems:'center'}}>
                        <Title style={styles.textbody}>Histori</Title>
                    </Body>
                    
                        <Right style={{flex:1}}></Right>
                    
                </Header>

                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                <View style={{ flex:1,flexDirection:"column", marginTop: 5 }}>
                {listSIO}

                </View>
            </Container>

        );
    }
}

export default SIOCertificationHistori;
