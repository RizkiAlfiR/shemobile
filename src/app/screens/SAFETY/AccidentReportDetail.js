import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    TextInput,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Text,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Icon,
    Item,
    Picker,
    Form
} from "native-base";

import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton
} from "react-native-popup-dialog";
import Ripple from "react-native-material-ripple";
import ImagePicker from "react-native-image-picker";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewReportDetail from "../../components/ListViewReportDetail";

class AccidentReportDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            dataSource: [],
            dataDetail: [],
            isLoading: true,
            headerAccidentReport: [],
            noPeg: '',
            visibleDialogSubmit: false,
            visibleAttach: false,
            pickedImage2: "",
            uri: "",
            fileName: "",
        };
    }

    static navigationOptions = {
        header: null
    };

    reset = () => {
        this.setState({
            pickedImage: null
        });
    };

    pickImageHandler2 = () => {
        ImagePicker.showImagePicker(
            { title: "Pick an Image", maxWidth: 800, maxHeight: 600 },
            res => {
                if (res.didCancel) {
                    console.log("User cancelled!");
                } else if (res.error) {
                    console.log("Error", res.error);
                } else {
                    this.setState({
                        pickedImage2: res.uri,
                        uri: res.uri,
                        fileName: res.fileName,
                        fileType: res.type
                    });
                    console.log(res.type);
                }
            }
        );
    };

    resetHandler = () => {
        this.reset();
    };

    _renderItem = ({ item }) => (
        <ListItem data={item}></ListItem>
    )

    navigateToScreen(route, id_Accident) {
        // alert(this.state.headerAccidentReport.ID);
        AsyncStorage.setItem('id', JSON.stringify(id_Accident)).then(() => {
            this.props.navigation.navigate(route);
        })
    }

    componentDidMount() {
        this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
            AsyncStorage.getItem('list').then((id_Accident) => {
                this.setState({ headerAccidentReport: JSON.parse(id_Accident) });
                this.setState({ isLoading: false });
                console.log(this.state.headerAccidentReport);
                var arrNoPeg = this.state.headerAccidentReport.badge_vict.split('');
                var noPeg = ''
                var zeroStop = false
                for (let i = 0; i < arrNoPeg.length; i++) {
                    if (arrNoPeg[i] != '0') {
                        zeroStop = true
                    }
                    if (zeroStop) {
                        noPeg += arrNoPeg[i]
                    }
                }
                this.setState({
                    noPeg: noPeg
                })
                console.log(this.state.noPeg)
            })
            //this.loadData(id_Unsafe);
        });
    }

    updateData() {
        this.setState({
            visibleDialogSubmit: true,
            visibleAttach: false
        })
        alert(JSON.stringify(this.state.statusReport))
        AsyncStorage.getItem("token").then(value => {
            const url = GlobalConfig.LOCALHOST + 'api/accidentreports/update';
            var formData = new FormData();
            formData.append("token", value);
            formData.append("id", this.state.headerAccidentReport.id);
            formData.append("status_report", this.state.statusReport);
            formData.append("follow_up", this.state.follow_up);
            formData.append("pict_after", {
                uri: this.state.uri,
                name: this.state.pickedImage2,
                type: this.state.fileType
            });
            console.log(formData)
            fetch(url, {
                headers: {
                'Content-Type': 'multipart/form-data'
                },
                method: 'POST',
                body: formData
            })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status == 200) {
                this.setState({
                    visibleDialogSubmit: false
                })
                    Alert.alert('Success', 'Attach Accident Update Success', [{
                        text: 'Okay'
                    }])
                this.props.navigation.navigate('AccidentReport')
                } else {
                    this.setState({
                    visibleDialogSubmit: false
                    })
                    Alert.alert('Error', 'Attach Accident Update Failed', [{
                    text: 'Okay'
                    }])
                }
            })
            .catch(error => {
                this.setState({
                visibleDialogSubmit: false
                })
                Alert.alert('Error', 'Attach Accident Update Failed', [{
                text: 'Okay'
                }])
                console.log(error)
            })
        })
    }

    konfirmasideleteHeader() {
        Alert.alert(
            'Apakah Anda Yakin Ingin Menghapus', this.state.headerAccidentReport.ID,
            [
                { text: 'Yes', onPress: () => this.deleteDetailAccident() },
                { text: 'Cancel' },
            ],
            { cancelable: false }
        )
    }

    deleteDetailAccident() {
        this.setState({
            visibleDialogSubmit: true
        })
        AsyncStorage.getItem('token').then((value) => {
            // alert(JSON.stringify(value));
            const url = GlobalConfig.LOCALHOST + 'api/accidentreports/' + this.state.headerAccidentReport.id + '?token=' + value;

            fetch(url, {
                // headers: {
                //     "Content-Type": "multipart/form-data"
                // },
                method: "DELETE",
                // body: formData
            })
                .then(response => response.json())
                .then(response => {
                    if (response.status == 200) {
                        this.setState({
                            visibleDialogSubmit: false
                        })
                        Alert.alert('Success', 'Delete Success', [{
                            text: 'Okay',
                        }])
                        this.props.navigation.navigate("AccidentReport")
                    } else {
                        this.setState({
                            visibleDialogSubmit: false
                        })
                        Alert.alert('Error', 'Delete Failed', [{
                            text: 'Okay'
                        }])
                    }
                })
                .catch((error) => {
                    this.setState({
                        visibleDialogSubmit: false
                    })
                    Alert.alert('Error', 'Delete Failed', [{
                        text: 'Okay'
                    }])
                    console.log(error)
                })
        })
    }

    render() {
        var listReportK3;
        var listDetailReportK3;

        return (
            <Container style={styles.wrapper}>
                <Header style={styles.header}>
                    <Left style={{ flex: 1 }}>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate("AccidentReport")}
                        >
                            <Icon
                                name="ios-arrow-back"
                                size={20}
                                style={{ color: colors.white }}
                            />
                        </Button>
                    </Left>
                    <Body style={{ flex: 3, alignItems: 'center' }}>
                        <Title style={styles.textbody}>Detail Report</Title>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        {(this.state.headerAccidentReport.status_report != "CLOSE") && (
                            <Button
                                transparent

                                // onPress={() => this.navigateToScreen('AccidentReportUpdate', this.state.headerAccidentReport)}
                                onPress={() => this.setState({ visibleAttach: true })}
                            >
                                <Icon
                                    name="ios-attach"
                                    size={20}
                                    style={{ color: colors.white }}
                                />
                            </Button>
                        )}
                        {(this.state.headerAccidentReport.status_report != "CLOSE") && (
                            <Button
                                transparent

                                onPress={() => this.navigateToScreen('AccidentReportUpdate', this.state.headerAccidentReport)}
                            >
                                <Icon
                                    name="ios-create"
                                    size={20}
                                    style={{ color: colors.white }}
                                />
                            </Button>
                        )}
                        {(this.state.headerAccidentReport.status_report != "CLOSE") && (

                            <Button
                                transparent
                                onPress={() => this.konfirmasideleteHeader()}
                            >
                                <Icon
                                    name="ios-trash"
                                    size={20}
                                    style={{ color: colors.white }}
                                />
                            </Button>
                        )}
                    </Right>
                </Header>
                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                <Content style={{ marginTop: 0, marginBottom: 20, backgroundColor: colors.gray01 }}>
                    <ScrollView>
                        <View style={{ justifyContent: "center", alignItems: "center", marginTop: 10 }}>
                            <Text style={{ fontSize: 14, fontWeight: 'bold', color: colors.green01 }}>Accident Detail {this.state.noPeg} - {this.state.headerAccidentReport.name_vict}</Text>
                        </View>
                        <CardItem style={{ borderRadius: 10, marginTop: 15, marginHorizontal: 5, marginBottom: 5, borderWidth: 2, borderColor: colors.graydar }}>
                            <View style={{ flex: 1, paddingVertical: 10 }}>
                                <Text style={{ fontSize: 12, paddingBottom: 5, fontWeight: 'bold' }}>Foto Kecelakaan:</Text>
                                <View style={styles.placeholder}>
                                    {this.state.headerAccidentReport.pict_before == null ? (
                                        <Image style={styles.previewImage} source={require("../../../assets/images/nopic.png")} />
                                    ) : (
                                            <Image style={styles.previewImage} source={{ uri: GlobalConfig.LOCALHOST + 'public/uploads/accident/' + this.state.headerAccidentReport.pict_before }} />
                                        )}
                                </View>
                            </View>
                        </CardItem>
                        {(this.state.headerAccidentReport.status_report != "OPEN") && (
                            <CardItem style={{ borderRadius: 10, marginTop: 15, marginHorizontal: 5, marginBottom: 5, borderWidth: 2, borderColor: colors.graydar }}>
                                <View style={{ flex: 1, paddingVertical: 10 }}>
                                    <Text style={{ fontSize: 12, paddingBottom: 5, fontWeight: 'bold' }}>Foto Follow Up:</Text>
                                    <View style={styles.placeholder}>
                                        {this.state.headerAccidentReport.pict_after == null ? (
                                            <Image style={styles.previewImage} source={require("../../../assets/images/nopic.png")} />
                                        ) : (
                                                <Image style={styles.previewImage} source={{ uri: GlobalConfig.LOCALHOST + 'public/uploads/accident/' + this.state.headerAccidentReport.pict_after }} />
                                            )}
                                    </View>
                                </View>
                            </CardItem>
                        )}
                        <CardItem style={{ borderRadius: 10, marginTop: 10, marginHorizontal: 5, marginBottom: 5, borderWidth: 2, borderColor: colors.graydar, backgroundColor: colors.green01 }}>
                            <View style={{ flex: 1, flexDirection: 'row', paddingVertical: 10, }}>
                                <View style={{ flex: 1 }}>
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white }}>Shift :</Text>
                                    <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>Shift {this.state.headerAccidentReport.shift}</Text>
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white, paddingTop: 10 }}>Jenis Karyawan Korban :</Text>
                                    <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.headerAccidentReport.state_vict_text}</Text>
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white, paddingTop: 10 }}>Unit Kerja :</Text>
                                    <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.headerAccidentReport.uk_vict} - {this.state.headerAccidentReport.uk_vict_text}</Text>
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white, paddingTop: 10 }}>Posisi :</Text>
                                    <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.headerAccidentReport.position_text} (Since {this.state.headerAccidentReport.date_in})</Text>
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white, paddingTop: 10 }}>Umur :</Text>
                                    <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.headerAccidentReport.age} Tahun</Text>
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white, paddingTop: 10 }}>Macam Pekerjaan Waktu Terjadi Kecelakaan :</Text>
                                    <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.headerAccidentReport.work_task}</Text>
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white, paddingTop: 10 }}>Pengawas Kerja :</Text>
                                    <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.headerAccidentReport.spv_name}</Text>
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white, paddingTop: 10 }}>Lokasi :</Text>
                                    <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.headerAccidentReport.location} - {this.state.headerAccidentReport.location_text}</Text>
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white, paddingTop: 10 }}>Penyebab Terjadinya Kecelakaan :</Text>
                                    <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.headerAccidentReport.acd_cause}</Text>
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white, paddingTop: 10 }}>Saksi Kecelakaan :</Text>
                                    <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.headerAccidentReport.s_name} - {this.state.headerAccidentReport.s_pos_text}</Text>
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white, paddingTop: 10 }}>Cidera Yang Dialami :</Text>
                                    <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.headerAccidentReport.inj_text} ({this.state.headerAccidentReport.inj_note})</Text>
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white, paddingTop: 10 }}>Pertolongan Pertama :</Text>
                                    <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.headerAccidentReport.first_aid}</Text>
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white, paddingTop: 10 }}>Pertolongan Selanjutnya :</Text>
                                    <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.headerAccidentReport.next_aid}</Text>
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white, paddingTop: 10 }}>Keadan Syarat Keselamatan Kerja :</Text>
                                    <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.headerAccidentReport.safety_req}</Text>
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white, paddingTop: 10 }}>Unsafe Action :</Text>
                                    <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.headerAccidentReport.unsafe_act}</Text>
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white, paddingTop: 10 }}>Unsafe Condition :</Text>
                                    <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.headerAccidentReport.unsafe_cond}</Text>
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white, paddingTop: 10 }}>Penjelasan Tentang Penyebab Terjadinya Kecelakaan :</Text>
                                    <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.headerAccidentReport.acd_rpt_text}</Text>
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white, paddingTop: 10 }}>Saran Agar Kejadian Tidak Terulang :</Text>
                                    <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.headerAccidentReport.note}</Text>
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white, paddingTop: 10 }}>Petugas Investigasi / PIC :</Text>
                                    <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.headerAccidentReport.inv_badge} - {this.state.headerAccidentReport.inv_name}</Text>
                                </View>
                                <View style={{ flex: 1, width: 50 }}>
                                    <View>
                                        <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white, textAlign: 'right' }}>Tanggal</Text>
                                        <Text style={{ fontSize: 12, textAlign: 'right', color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.headerAccidentReport.date_acd}</Text>
                                        <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white, paddingTop: 10, textAlign: 'right' }}>Pukul</Text>
                                        <Text style={{ fontSize: 12, textAlign: 'right', color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.headerAccidentReport.time_acd}</Text>
                                    </View>
                                    {(this.state.headerAccidentReport.status_report != "OPEN") && (
                                        <View style={{ marginTop: 0 }}>
                                            <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white, paddingTop: 10, textAlign: 'right' }}>Follow Up</Text>
                                            <Text style={{ fontSize: 12, textAlign: 'right', color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.headerAccidentReport.follow_up}</Text>
                                        </View>
                                    )}
                                </View>
                            </View>
                        </CardItem>
                    </ScrollView>
                    <View style={{ width: 270, position: "absolute" }}>
                        <Dialog
                            visible={this.state.visibleDialogSubmit}
                            dialogTitle={<DialogTitle title="Deleting Accident Report .." />}
                        >
                            <DialogContent>
                                {<ActivityIndicator size="large" color="#330066" animating />}
                            </DialogContent>
                        </Dialog>
                    </View>
                </Content>
                <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                        visible={this.state.visibleAttach}
                        dialogTitle={<DialogTitle textStyle={{ fontSize: 13 }} title="Attach Follow Up" />}

                        dialogAnimation={
                            new SlideAnimation({
                                slideFrom: "bottom"
                            })
                        }
                        onTouchOutside={() => {
                            this.setState({ visibleAttach: false });
                        }}
                        dialogStyle={{ position: "absolute", width: "80%" }}
                    >
                        <DialogContent
                            style={{
                                backgroundColor: colors.white
                            }}
                        >
                            <CardItem
                                style={{
                                    borderRadius: 0,
                                    marginTop: 0,
                                    backgroundColor: colors.white
                                }}
                            >
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.titleInput}>Status Report</Text>
                                        <Form
                                            style={{
                                                borderWidth: 1,
                                                borderRadius: 5,
                                                marginRight: 5,
                                                marginLeft: 5,
                                                borderColor: "#E6E6E6",
                                                height: 35
                                            }}
                                        >
                                            <Picker
                                                mode="dropdown"
                                                iosIcon={
                                                    <Icon
                                                        name={
                                                            Platform.OS
                                                                ? "ios-arrow-down"
                                                                : "ios-arrow-down-outline"
                                                        }
                                                    />
                                                }
                                                style={{ width: "100%", height: 35 }}
                                                placeholder="Quantity"
                                                placeholderStyle={{ color: "#bfc6ea" }}
                                                placeholderIconColor="#007aff"
                                                selectedValue={this.state.statusReport}
                                                onValueChange={itemValue =>
                                                    this.setState({ statusReport: itemValue })
                                                }
                                            >
                                                <Picker.Item label="OPEN" value="OPEN" />
                                                <Picker.Item label="CLOSE" value="CLOSE" />
                                            </Picker>
                                        </Form>
                                        <Text style={{ fontSize: 10, marginTop: 20, paddingBottom: 5 }}>Follow Up :</Text>
                                            <Form
                                            style={{
                                                borderWidth: 1,
                                                borderRadius: 5,
                                                marginRight: 5,
                                                marginLeft: 5,
                                                borderColor: "#E6E6E6",
                                                height: 35
                                            }}
                                            >
                                            <TextInput
                                                style={{
                                                width: "100%", height: 35,
                                                fontSize: 11
                                                }}
                                                ref={input => {
                                                this.fifthTextInput = input;
                                                }}

                                                rowSpan={3}
                                                bordered
                                                value={this.state.follow_up}
                                                placeholder="Type Something ..."
                                                onChangeText={text =>
                                                this.setState({ follow_up: text })
                                                }
                                            />
                                            </Form>
                                        <Text style={{ fontSize: 10, marginTop: 20, paddingBottom: 5 }}>Foto Follow Up :</Text>
                                        <View style={styles.placeholder}>
                                            <Ripple
                                                style={{
                                                    flex: 2,
                                                    justifyContent: "center",
                                                    alignItems: "center"
                                                }}
                                                rippleSize={176}
                                                rippleDuration={600}
                                                rippleContainerBorderRadius={15}
                                                onPress={this.pickImageHandler2}
                                                rippleColor={colors.accent}
                                            >
                                                <View style={styles.placeholder}>
                                                    <Image
                                                        source={{ uri: this.state.pickedImage2 }}
                                                        style={styles.previewImage}
                                                    />
                                                </View>
                                            </Ripple>
                                        </View>
                                    </View>
                                </View>
                            </CardItem>
                            <View style={{ flexDirection: "row" }}>
                                <View style={{ flex: 1 }}>
                                    <Button
                                        block
                                        style={{
                                            height: 45,
                                            marginLeft: 20,
                                            marginRight: 20,
                                            marginBottom: 20,
                                            borderWidth: 1,
                                            backgroundColor: colors.green0,
                                            borderColor: colors.green0,
                                            borderRadius: 4
                                        }}
                                        onPress={() => this.updateData()}
                                        // onPress={() => this.setState({ visibleAttach: false })}
                                    >
                                        <Text style={styles.buttonText}>Update</Text>
                                    </Button>
                                </View>
                            </View>
                        </DialogContent>
                    </Dialog>
                </View>
                <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                        visible={this.state.visibleDialogSubmit}
                        dialogTitle={<DialogTitle title='Updating Temuan Unsafe ..' />}
                    >
                        <DialogContent>
                            {<ActivityIndicator size="large" color="#330066" animating />}
                        </DialogContent>
                    </Dialog>
                </View>
                <CustomFooter navigation={this.props.navigation} menu='Safety' />
            </Container >

        );
    }
}

export default AccidentReportDetail;
