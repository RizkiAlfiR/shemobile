import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input
} from "native-base";

import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewHeader from "../../components/ListViewHeader";
import ListViewDetail from "../../components/ListViewDetail";

class SIOCertificationDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      dataDetail: [],
      isLoading: true,
      detailSIO: [],
      visibleDialogSubmit: false
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => (
    <ListItem data={item}></ListItem>
  )

  navigateToScreen(route, detailSIO) {
    AsyncStorage.setItem('listSIO', JSON.stringify(detailSIO)).then(() => {
      this.props.navigation.navigate(route);
    })
  }

  navigateToScreenHistori(route, noLisensi) {
    AsyncStorage.setItem('noLisensi', noLisensi).then(() => {
      this.props.navigation.navigate(route);
    })
  }

  componentDidMount() {
    AsyncStorage.getItem('listSIO').then((detailSIO) => {
      this.setState({ detailSIO: JSON.parse(detailSIO) });
      this.setState({ isLoading: false });
      console.log(this.state.detailSIO);
    })
    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
      //this.loadData(id_Unsafe);
    });

  }

  konfirmasideleteSIO() {
    Alert.alert(
      'Apakah Anda Yakin Ingin Menghapus Sertifikasi', this.state.detailSIO.NAMA,
      [
        { text: 'Yes', onPress: () => this.deleteSIO() },
        { text: 'Cancel' },
      ],
      { cancelable: false }
    )
  }

  deleteSIO() {
    this.setState({
      visibleDialogSubmit: true
    })
    AsyncStorage.getItem('token').then((value) => {
      // alert(JSON.stringify(value));
      const url = GlobalConfig.LOCALHOST + 'api/sio/' + this.state.detailSIO.id + '?token=' + value;
      // var formData = new FormData();
      // formData.append("token", value);
      // formData.append("ID", this.state.detailSIO.ID);

      fetch(url, {
        // headers: {
        //   "Content-Type": "multipart/form-data"
        // },
        method: "DELETE",
        // body: formData
      })
        .then(response => response.json())
        .then(response => {
          // alert(JSON.stringify(response))
          if (response.status == 200) {
            this.setState({
              visibleDialogSubmit: false
            })
            Alert.alert('Success', 'Delete Success', [{
              text: 'Oke',
            }])
            this.props.navigation.navigate("SIOCertification")
          } else {
            this.setState({
              visibleDialogSubmit: false
            })
            Alert.alert('Error', 'Delete Failed', [{
              text: 'Oke'
            }])
          }
        })
        .catch((error) => {
          console.log(error)
          this.setState({
            visibleDialogSubmit: false
          })
          Alert.alert('Error', 'Delete Failed', [{
            text: 'Oke'
          }])
        })
    })
  }


  render() {
    var listSIO;
    var listDetailSIO;
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("SIOCertification")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 1, alignItems: 'center' }}>
            <Title style={styles.textbody}>SIO Detail</Title>
          </Body>
          <Right style={{ flex: 1 }}>
            {/* <Button
              transparent
              onPress={() => this.navigateToScreenHistori('SIOCertificationHistori', this.state.detailSIO.NO_LISENSI)}
            >
              <Icon
                name="ios-stopwatch"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button> */}
            <Button
              transparent
              onPress={() => this.navigateToScreen('SIOCertificationUpdate', this.state.detailSIO)}
            >
              <Icon
                name="ios-create"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
            <Button
              transparent
              onPress={() => this.konfirmasideleteSIO()}
            >
              <Icon
                name="ios-trash"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <Content style={{ marginTop: 0, marginBottom: 20, backgroundColor: colors.gray01 }}>
          <ScrollView>
            <View style={{ justifyContent: "center", alignItems: "center", marginTop: 10 }}>
              <Text style={{ fontSize: 14, color: colors.green01 }}>SIO Certification Detail {this.state.detailSIO.nama}</Text>
            </View>
            <CardItem style={{ borderRadius: 10, marginTop: 15, marginHorizontal: 5, marginBottom: 5, borderWidth: 2, borderColor: colors.graydar }}>
              <View style={{ flex: 1, paddingVertical: 10 }}>
                <Text style={{ fontSize: 12, paddingBottom: 5, fontWeight: 'bold' }}>Foto :</Text>
                <View style={styles.placeholder}>
                  {this.state.detailSIO.foto == null ? (
                    <Image style={styles.previewImage} source={require("../../../assets/images/nopic.png")} />
                  ) : (
                      <Image style={styles.previewImage} source={{ uri: GlobalConfig.LOCALHOST + "public/uploads/sio/" + this.state.detailSIO.foto }} />
                    )}
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 10, marginTop: 10, marginHorizontal: 5, marginBottom: 5, borderWidth: 2, borderColor: colors.graydar, backgroundColor: colors.green01 }}>
              <View style={{ flex: 1, flexDirection: 'row', paddingVertical: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white }}>Pemegang :</Text>
                  <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.detailSIO.nama}</Text>
                  <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, color: colors.white }}>Kelas :</Text>
                  <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.detailSIO.kelas}</Text>
                  <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, color: colors.white }}>Unit Kerja :</Text>
                  <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.detailSIO.unit_kerja} - {this.state.detailSIO.unit_kerja_txt}</Text>
                  <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, color: colors.white }}>Jenis Lisensi :</Text>
                  <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.detailSIO.grup}</Text>
                  <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, color: colors.white }}>Nomor Lisensi :</Text>
                  <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.detailSIO.no_lisensi}</Text>
                  <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, color: colors.white }}>Waktu :</Text>
                  <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.detailSIO.time_certification} Tahun</Text>
                </View>
                <View style={{ flex: 0 }}>
                  <Text style={{ fontSize: 12, fontWeight: "bold", textAlign: 'right', color: colors.white }}>Tanggal Mulai Sertfikasi :</Text>
                  <Text style={{ fontSize: 12, textAlign: 'right', color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.detailSIO.masa_awal}</Text>
                  <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, textAlign: 'right', color: colors.white }}>Tanggal Berakhir Sertifikasi :</Text>
                  <Text style={{ fontSize: 12, textAlign: 'right', color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.detailSIO.masa_berlaku}</Text>
                </View>
              </View>
            </CardItem>
          </ScrollView>
          <View style={{ width: 270, position: "absolute" }}>
            <Dialog
              visible={this.state.visibleDialogSubmit}
              dialogTitle={<DialogTitle title="Deleting SIO Certification .." />}
            >
              <DialogContent>
                {<ActivityIndicator size="large" color="#330066" animating />}
              </DialogContent>
            </Dialog>
          </View>
        </Content>
      </Container>
    );
  }
}

export default SIOCertificationDetail;
