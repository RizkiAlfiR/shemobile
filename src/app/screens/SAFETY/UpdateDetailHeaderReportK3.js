import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Textarea,
  Icon,
  Picker,
  Form,
  Input,
  Label,
  Item,
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DateTimePicker from 'react-native-datepicker';
import ImagePicker from "react-native-image-picker";
import Ripple from "react-native-material-ripple";
import CheckBox from 'react-native-check-box';

class ListItem extends React.PureComponent {
  render() {
    return (
      <View style={{ backgroundColor: "#FEFEFE" }}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.props.setSelectedUnitKerjaLabel(this.props.data.muk_nama, this.props.data.muk_kode)
          }
          rippleColor={colors.accent}
        >
          <CardItem
            style={{
              borderRadius: 0,
              marginTop: 4,
              backgroundColor: colors.gray
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                marginTop: 4,
                backgroundColor: colors.gray
              }}
            >
              <View >
                <Text style={{ fontSize: 12 }}>{this.props.data.muk_kode} - {this.props.data.muk_nama}</Text>
              </View>
            </View>
          </CardItem>
        </Ripple>
      </View>
    );
  }
}

class ListItemVendor extends React.PureComponent {
  render() {
    return (
      <View style={{ backgroundColor: "#FEFEFE" }}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.props.setSelectedVendorLabel(this.props.data.NAME1, this.props.data.ID)
          }
          rippleColor={colors.accent}
        >
          <CardItem
            style={{
              borderRadius: 0,
              marginTop: 4,
              backgroundColor: colors.gray
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                marginTop: 4,
                backgroundColor: colors.gray
              }}
            >
              <View >
                <Text style={{ fontSize: 12 }}>{this.props.data.ID} - {this.props.data.NAME1}</Text>
              </View>
            </View>
          </CardItem>
        </Ripple>
      </View>
    );
  }
}

class ListItemCategory extends React.PureComponent {
  render() {
    return (
      <View style={{ backgroundColor: "#FEFEFE" }}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.props.setSelectedCategoryLabel(this.props.data.DESKRIPSI, this.props.data.id, this.props.data.KODE_EN)
          }
          rippleColor={colors.accent}
        >
          <CardItem
            style={{
              borderRadius: 0,
              marginTop: 4,
              backgroundColor: colors.gray
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                marginTop: 4,
                backgroundColor: colors.gray
              }}
            >
              <View >
                <Text style={{ fontSize: 12 }}>{this.props.data.KODE} - {this.props.data.DESKRIPSI}</Text>
              </View>
            </View>
          </CardItem>
        </Ripple>
      </View>
    );
  }
}

export default class UpdateDetailHeaderReportK3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      dataHeader: [],
      isLoading: true,
      listHeader: [],
      aktifitas: '',

      type: '',
      penyebab: '',
      potensi: '',
      rekomendasi: '',
      followup: '',
      unitCode: '',
      unitName: '',
      waktu: '',
      idCategori: '',
      status: '',
      fotoTemuan: '',
      fotoFollowup: '',
      //idReport:'',
      listUnitKerja: [],
      listVendor: [],
      listKategori: [],
      pickedImage: '',
      uri: '',
      fileType: '',
      fileName: '',
      pickedImage2: '',
      uri2: '',
      fileType2: '',
      fileName2: '',
      isChecked: false,
      value: '',
      visibleLoadingUnitKerja: false,
      visibleSearchListUnitKerja: false,
      selectedUnitKerjaLabel: '',
      selectedUnitKerjaKode: '',
      selectedVendorLabel: '',
      selectedVendorKode: '',
      searchWordUnitKerja: '',
      searchWordVendor: '',
      visibleLoadingCategory: false,
      visibleSearchListCategory: false,
      searchWordCategory: '',
      selectedCategoryLabel: '',
      listFileUpload: [],
      visibleDialogSubmit: false,
      enableScrollViewScroll: true
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('list').then((listHeader) => {
      // console.log(JSON.parse(listHeader))
      this.setState({
        listHeader: JSON.parse(listHeader),
        // pickedImage2 : GlobalConfig.SERVERHOST+'api'+ JSON.parse(listHeader).LIST_FILE_UPLOAD[1].UPLOADED_PATH,
      });
      this.setState({
        aktifitas: this.state.listHeader.activity_description,
        type: this.state.listHeader.unsafe_type,
        penyebab: this.state.listHeader.trouble_maker,
        potensi: this.state.listHeader.potential_hazard,
        rekomendasi: this.state.listHeader.recomendation,
        followup: this.state.listHeader.follow_up,

        value: this.state.listHeader.is_smig,
        waktu: this.state.listHeader.o_clock_incident,
        selectedCategoryId: this.state.listHeader.id_category,
        selectedCategoryLabel: this.state.listHeader.deskripsi,
        selectedCategoryEn: this.state.listHeader.kode_en,
        status: (this.state.listHeader.status).toUpperCase(),
        // listFileUpload: JSON.parse(listHeader).LIST_FILE_UPLOAD

      }, function () {
        if (this.state.value == 1) {
          this.setState({
            isChecked: false,
            selectedUnitKerjaKode: this.state.listHeader.unit_code,
            selectedUnitKerjaLabel: this.state.listHeader.unit_name,
          })
        } else {
          this.setState({
            isChecked: true,
            selectedVendorKode: this.state.listHeader.unit_code,
            selectedVendorLabel: this.state.listHeader.unit_name,
          })
        }
      })
      if (this.state.listHeader.list_file_upload[0] != undefined) {
        if (this.state.listHeader.list_file_upload[1] != undefined) {
          this.setState({
            pickedImage: GlobalConfig.LOCALHOST + 'uploads/unsafe/' + this.state.listHeader.list_file_upload[0].uploaded_path,
            pickedImage2: GlobalConfig.LOCALHOST + 'uploads/unsafe/' + this.state.listHeader.list_file_upload[1].uploaded_path,
          })
        } else {
          this.setState({
            pickedImage: GlobalConfig.LOCALHOST + 'uploads/unsafe/' + this.state.listHeader.list_file_upload[0].uploaded_path,
            // pickedImage2 :  GlobalConfig.SERVERHOST+'api'+ this.state.listHeader.LIST_FILE_UPLOAD[1].UPLOADED_PATH,
          })
        }
      }
    })

    this.loadUnitKerja();
    this.loadVendor();
    this.loadKategori();
    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
      this.loadData();
    });
  }


  pickImageHandler = () => {
    ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
      if (res.didCancel) {
        console.log("User cancelled!");
      } else if (res.error) {
        console.log("Error", res.error);
      } else {
        this.setState({
          pickedImage: res.uri,
          uri: res.uri,
          fileType: res.type
        });

      }
    });
  }

  pickImageHandler2 = () => {
    ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
      if (res.didCancel) {
        console.log("User cancelled!");
      } else if (res.error) {
        console.log("Error", res.error);
      } else {
        this.setState({
          pickedImage2: res.uri,
          uri2: res.uri,
          fileType2: res.type
        });

      }
    });
  }

  static navigationOptions = {
    header: null
  };

  loadData() {

  }

  loadUnitKerja() {
    this.setState({
      visibleLoadingUnitKerja: true
    })
    AsyncStorage.getItem('token').then((value) => {
      const url = GlobalConfig.LOCALHOST + 'api/master/unitkerja/search?keyword=' + this.state.searchWordUnitKerja;

      // fetch(url, {
      //     headers: {
      //         'Content-Type': 'multipart/form-data'
      //     },
      //     method: 'POST',
      //     body: formData
      // })
      fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({
            visibleLoadingUnitKerja: false
          })
          this.setState({
            listUnitKerja: responseJson.data,
            isloading: false
          });
        })
        .catch((error) => {
          this.setState({
            visibleLoadingUnitKerja: false
          })
          console.log(error)
        })
    })
  }

  loadVendor() {
    AsyncStorage.getItem('token').then((value) => {
      const url = GlobalConfig.LOCALHOST + 'api/master/vendor/search?keyword=' + this.state.searchWordVendor;

      // fetch(url, {
      //     headers: {
      //         'Content-Type': 'multipart/form-data'
      //     },
      //     method: 'POST',
      //     body: formData
      // })
      fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({
            listVendor: responseJson.data,
            isloading: false
          });
        })
        .catch((error) => {
          console.log(error)
        })
    })
  }

  loadKategori() {
    AsyncStorage.getItem('token').then((value) => {
      const url = GlobalConfig.LOCALHOST + 'api/master/reportcategory/search?keyword=' + this.state.searchWordCategory;

      // fetch(url, {
      //     headers: {
      //         'Content-Type': 'multipart/form-data'
      //     },
      //     method: 'POST',
      //     body: formData
      // })
      fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({
            listKategori: responseJson.data,
            isloading: false
          });
        })
        .catch((error) => {
          console.log(error)
        })
    })
  }

  UpdateDetailHeader() {
    if (this.state.waktu == null) {
      alert('Masukkan Waktu Report');
    }
    else if ((this.state.selectedUnitKerjaLabel == '') && (this.state.selectedVendorLabel == '')) {
      alert('Masukkan Unit Kerja atau Vendor');
    }

    else if (this.state.selectedKategori == '') {
      alert('Masukkan Kategori');
    }
    else if (this.state.aktifitas == null) {
      alert('Masukkan Aktifitas Temuan');
    }
    else if (this.state.potensi == null) {
      alert('Masukkan Potensi Bahaya');
    }
    else {
      this.setState({
        visibleDialogSubmit: true
      })
      if (this.state.isChecked == false) {
        this.setState({
          value: 1,
        });
      } else {
        this.setState({
          value: 0,
        });
      }
      AsyncStorage.getItem("token").then(value => {
        var url = GlobalConfig.LOCALHOST + "api/unsafek3detail/update";
        var formData = new FormData();
        formData.append("token", value);
        formData.append("id", this.state.listHeader.id);
        formData.append("id_unsafe_report", this.state.listHeader.id_unsafe_report);
        formData.append("activity_description", this.state.aktifitas);
        formData.append("unsafe_type", this.state.type);
        if (this.state.penyebab != '') {
          formData.append("trouble_maker", this.state.penyebab);
        }
        else {
          formData.append("trouble_maker", '');
        }
        formData.append("potential_hazard", this.state.potensi);
        if (this.state.rekomendasi != '') {
          formData.append("recomendation", this.state.rekomendasi);
        } else {
          formData.append("recomendation", '');
        }
        if (this.state.followup != '') {
          formData.append("follow_up", this.state.followup);
        } else {
          formData.append("follow_up", '');
        }
        formData.append("is_smig", this.state.value);
        if (this.state.isChecked) {
          formData.append("unit_code", this.state.selectedVendorKode);
          formData.append("unit_name", this.state.selectedVendorLabel);
        } else {
          formData.append("unit_code", this.state.selectedUnitKerjaKode);
          formData.append("unit_name", this.state.selectedUnitKerjaLabel);
        }
        formData.append("o_clock_incident", this.state.waktu);
        formData.append("id_category", this.state.idCategori);
        formData.append("deskripsi", this.state.deskripsi);
        formData.append("kode_en", this.state.kode_en);
        formData.append("status", this.state.status);
        if (this.state.uri != '') {
          formData.append("foto_temuan", {
            uri: this.state.uri,
            type: this.state.fileType,
            name: this.state.pickedImage
          });
        } else {
          formData.append("foto_temuan", {
            uri: null,
            type: null,
            name: 'image'
          });
        }
        if (this.state.uri2 != '') {
          formData.append("foto_follow_up", {
            uri: this.state.uri2,
            type: this.state.fileType2,
            name: this.state.pickedImage2
          });
        } else {
          formData.append("foto_follow_up", {
            uri: null,
            type: null,
            name: 'image'
          });
        }

        console.log(formData)

        fetch(url, {
          headers: {
            "Content-Type": "multipart/form-data"
          },
          method: "POST",
          body: formData
        })
          .then(response => response.json())
          .then(response => {
            if (response.status == 200) {
              this.setState({
                visibleDialogSubmit: false
              })
              Alert.alert('Success', 'Update Detail Success', [{
                text: 'Oke'
              }])
              this.props.navigation.navigate('DailyReportK3DetailList')
            } else {
              this.setState({
                visibleDialogSubmit: false
              })
              Alert.alert('Error', 'Update Detail Failed', [{
                text: 'Oke'
              }])
            }
          })
          .catch((error) => {
            console.log(error)
            this.setState({
              visibleDialogSubmit: false
            })
            Alert.alert('Error', 'Update Detail Failed', [{
              text: 'Oke'
            }])
          })
      })
    }
  }


  onChangeUnitKerja(text) {
    this.setState({
      searchWordUnitKerja: text
    })
    this.loadUnitKerja();
    // this.setState({
    //   listKaryawan:this.state.listKaryawanMaster.filter(x => x.mk_nama.includes(text)),
    // })
  }

  onChangeVendor(text) {
    this.setState({
      searchWordVendor: text
    })
    this.loadVendor();
    // this.setState({
    //   listKaryawan:this.state.listKaryawanMaster.filter(x => x.mk_nama.includes(text)),
    // })
  }

  onChangeCategory(text) {
    this.setState({
      searchWordCategory: text
    })
    this.loadKategori();
    // this.setState({
    //   listKaryawan:this.state.listKaryawanMaster.filter(x => x.mk_nama.includes(text)),
    // })
  }

  onClickSearchUnitKerja() {
    console.log('masuk')
    if (this.state.visibleSearchListUnitKerja) {
      this.setState({
        visibleSearchListUnitKerja: false
      })
    } else {
      this.setState({
        visibleSearchListUnitKerja: true
      })
    }
  }

  onClickSearchVendor() {
    console.log('masuk')
    if (this.state.visibleSearchListVendor) {
      this.setState({
        visibleSearchListVendor: false
      })
    } else {
      this.setState({
        visibleSearchListVendor: true
      })
    }
  }

  onClickSearchCategory() {
    console.log('masuk')
    if (this.state.visibleSearchListCategory) {
      this.setState({
        visibleSearchListCategory: false
      })
    } else {
      this.setState({
        visibleSearchListCategory: true
      })
    }
  }

  setSelectedUnitKerjaLabel(label, kode) {
    this.setState({
      selectedUnitKerjaLabel: label,
      selectedUnitKerjaKode: kode,
      visibleSearchListUnitKerja: false
    })
  }

  setSelectedVendorLabel(label, kode) {
    this.setState({
      selectedVendorLabel: label,
      selectedVendorKode: kode,
      visibleSearchListVendor: false
    })
  }

  setSelectedCategoryLabel(label, id, en) {
    this.setState({
      selectedCategoryLabel: label,
      selectedCategoryId: id,
      selectedCategoryEn: en,
      visibleSearchListCategory: false
    })
  }

  _renderItem = ({ item }) => <ListItem data={item} setSelectedUnitKerjaLabel={(text1, text2) => this.setSelectedUnitKerjaLabel(text1, text2)} />;

  _renderItem2 = ({ item }) => <ListItemVendor data={item} setSelectedVendorLabel={(text1, text2) => this.setSelectedVendorLabel(text1, text2)} />;

  _renderItem3 = ({ item }) => <ListItemCategory data={item} setSelectedCategoryLabel={(text1, text2, text3) => this.setSelectedCategoryLabel(text1, text2, text3)} />;

  render() {
    let listUnitKerja = this.state.listUnitKerja.map((s, i) => {
      return <Picker.Item key={i} value={s.muk_nama} label={s.muk_nama} />
    });

    let listVendor = this.state.listVendor.map((s, i) => {
      return <Picker.Item key={i} value={s.NAME1} label={s.NAME1} />
    });

    let listKategori = this.state.listKategori.map((s, i) => {
      return <Picker.Item key={i} value={s.ID} label={s.DESKRIPSI} />
    });

    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DailyReportK3DetailHeaderList")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3, alignItems: 'center' }}>
            <Title style={styles.textbody}>Update Daily Report K3</Title>
          </Body>
          <Right style={{ flex: 1 }} />
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ flex: 1 }}
          onStartShouldSetResponderCapture={() => {
            this.setState({ enableScrollViewScroll: true });
          }}>
          <Content style={{ marginTop: 0 }}
            scrollEnabled={this.state.enableScrollViewScroll}
            ref={myScroll => (this._myScroll = myScroll)}>
            <View style={{ backgroundColor: '#FEFEFE' }}>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                <View style={{ flex: 1 }}>
                  <View style={{ paddingBottom: 5 }}>
                    <Text style={styles.titleInput}>Upload Foto Temuan</Text>
                  </View>
                  <View>
                    <Ripple
                      style={{
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                      rippleSize={176}
                      rippleDuration={600}
                      rippleContainerBorderRadius={15}
                      onPress={this.pickImageHandler}
                      rippleColor={colors.accent}
                    >
                      <View style={styles.placeholder}>
                        <Image source={{ uri: this.state.pickedImage + '?' + new Date() }} style={styles.previewImage} />
                      </View>
                    </Ripple>
                  </View>
                </View>
              </CardItem>

              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                <View style={{ flex: 1 }}>
                  <View style={{ paddingBottom: 5 }}>
                    <Text style={styles.titleInput}>Upload Foto Follow Up</Text>
                  </View>
                  <View>
                    <Ripple
                      style={{
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                      rippleSize={176}
                      rippleDuration={600}
                      rippleContainerBorderRadius={15}
                      onPress={this.pickImageHandler2}
                      rippleColor={colors.accent}
                    >
                      <View style={styles.placeholder}>
                        <Image source={{ uri: this.state.pickedImage2 + '?' + new Date() }} style={styles.previewImage} />
                      </View>
                    </Ripple>
                  </View>
                </View>
              </CardItem>


              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Status (*)</Text>
                    <Form style={{ borderWidth: 1, borderRadius: 5, marginRight: 5, marginLeft: 5, borderColor: "#E6E6E6", height: 35 }}>
                      <Picker
                        mode="dropdown"
                        iosIcon={<Icon name={Platform.OS ? "ios-arrow-down" : 'ios-arrow-down-outline'} />}
                        style={{ width: '100%', height: 35 }}
                        placeholder="Select Status ..."
                        placeholderStyle={{ color: "#bfc6ea" }}
                        placeholderIconColor="#007aff"
                        selectedValue={this.state.status}
                        onValueChange={(itemValue) => this.setState({ status: itemValue })}>
                        <Picker.Item label="Open" value="OPEN" />
                        <Picker.Item label="Close" value="CLOSE" />
                      </Picker>
                    </Form>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Unsafe Type (*)</Text>
                    <Form style={{ borderWidth: 1, borderRadius: 5, marginRight: 5, marginLeft: 5, borderColor: "#E6E6E6", height: 35 }}>
                      <Picker
                        mode="dropdown"
                        iosIcon={<Icon name={Platform.OS ? "ios-arrow-down" : 'ios-arrow-down-outline'} />}
                        style={{ width: '100%', height: 35 }}
                        placeholder="Select Shift ..."
                        placeholderStyle={{ color: "#bfc6ea" }}
                        placeholderIconColor="#007aff"
                        selectedValue={this.state.type}
                        onValueChange={(itemValue) => this.setState({ type: itemValue })}>
                        <Picker.Item label="UA / Unsafe Action" value="Unsafe Action" Sele />
                        <Picker.Item label="UC / Unsafe Condition" value="Unsafe Condition" />
                      </Picker>
                    </Form>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Jam (*)</Text>
                    <DateTimePicker
                      style={{ width: '100%', fontSize: 10, borderRadius: 20 }}
                      date={this.state.waktu}
                      mode="time"
                      placeholder="Choose Time ..."
                      format="hh:mm"
                      minTime="00:00"
                      maxTime="23:59"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateInput: {
                          marginLeft: 5, marginRight: 5, height: 35, borderRadius: 5, fontSize: 10, borderWidth: 1, borderColor: "#E6E6E6"
                        },
                        dateIcon: {
                          position: 'absolute',
                          left: 0,
                          top: 5,
                        },
                      }}
                      onDateChange={(time) => { this.setState({ waktu: time }) }}
                    />
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Unit Kerja/Instansi/Vendor (*)</Text>
                    {this.state.isChecked == false
                      ? (
                        <View>
                          <View style={{ flex: 1, flexDirection: 'column' }}>
                            <Button
                              block
                              style={{ flex: 1, borderWidth: 1, borderRadius: 5, marginRight: 5, marginLeft: 5, backgroundColor: colors.gray, borderColor: "#E6E6E6", height: 35 }}
                              onPress={() => this.onClickSearchUnitKerja()}>
                              <Text style={{ fontSize: 12 }}>{this.state.selectedUnitKerjaLabel}</Text>
                            </Button>
                          </View>

                          {this.state.visibleSearchListUnitKerja &&
                            <View style={{ height: 300, flexDirection: 'column', borderWidth: 1, padding: 10, backgroundColor: colors.gray, margin: 5, borderColor: "#E6E6E6", }}>
                              <Form>
                                <Item stackedLabel style={{ marginLeft: 0 }}>
                                  <Input value={this.state.searchWordUnitKerja} style={{ borderRadius: 5, marginLeft: 5, marginRight: 5, fontSize: 10 }} bordered onChangeText={(text) => this.onChangeUnitKerja(text)} placeholder='Ketik Unit Kerja' />
                                </Item>
                              </Form>
                              <View style={{ flex: 1 }}
                                onStartShouldSetResponderCapture={() => {
                                  this.setState({ enableScrollViewScroll: false });
                                  if (this._myScroll.contentOffset === 0
                                    && this.state.enableScrollViewScroll === false) {
                                    this.setState({ enableScrollViewScroll: true });
                                  }
                                }}>
                                <FlatList
                                  data={this.state.listUnitKerja}
                                  renderItem={this._renderItem}
                                  keyExtractor={(item, index) => index.toString()}
                                />
                              </View>
                            </View>
                          }
                        </View>
                      ) : (
                        <View>
                          <View style={{ flex: 1, flexDirection: 'column' }}>
                            <Button
                              block
                              style={{ flex: 1, borderWidth: 1, borderRadius: 5, marginRight: 5, marginLeft: 5, backgroundColor: colors.gray, borderColor: "#E6E6E6", height: 35 }}
                              onPress={() => this.onClickSearchVendor()}>
                              <Text style={{ fontSize: 12 }}>{this.state.selectedVendorLabel}</Text>
                            </Button>
                          </View>

                          {this.state.visibleSearchListVendor &&
                            <View style={{ height: 300, flexDirection: 'column', borderWidth: 1, padding: 10, backgroundColor: colors.gray, margin: 5, borderColor: "#E6E6E6", }}>
                              <Form>
                                <Item stackedLabel style={{ marginLeft: 0 }}>
                                  <Input value={this.state.searchWordVendor} style={{ borderRadius: 5, marginLeft: 5, marginRight: 5, fontSize: 10 }} bordered onChangeText={(text) => this.onChangeVendor(text)} placeholder='Ketik Vendor' />
                                </Item>
                              </Form>
                              <View style={{ flex: 1 }}
                                onStartShouldSetResponderCapture={() => {
                                  this.setState({ enableScrollViewScroll: false });
                                  if (this._myScroll.contentOffset === 0
                                    && this.state.enableScrollViewScroll === false) {
                                    this.setState({ enableScrollViewScroll: true });
                                  }
                                }}>
                                <FlatList
                                  data={this.state.listVendor}
                                  renderItem={this._renderItem2}
                                  keyExtractor={(item, index) => index.toString()}
                                />
                              </View>
                            </View>
                          }
                        </View>
                      )}
                  </View>
                  <CheckBox
                    style={{ flex: 1, paddingTop: 5 }}
                    onClick={() => {
                      this.setState({
                        isChecked: !this.state.isChecked,
                      })
                    }}
                    isChecked={this.state.isChecked}
                    rightText={"Non SMIG"}
                  />
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Kategori (*)</Text>
                    {/* <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                      <Picker
                          mode="dropdown"
                          iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                          style={{ width: '100%', height:35 }}
                          placeholder="Kategori"
                          placeholderStyle={{ color: "#bfc6ea" }}
                          placeholderIconColor="#007aff"
                          selectedValue={this.state.idCategori}
                          onValueChange={(itemValue) => this.setState({idCategori:itemValue})}>
                          <Picker.Item label="Choose Kategori..."  value=""/>
                          {listKategori}
                      </Picker>
                  </Form> */}
                    <View>
                      <View style={{ flex: 1, flexDirection: 'column' }}>
                        <Button
                          block
                          style={{ flex: 1, borderWidth: 1, borderRadius: 5, marginRight: 5, marginLeft: 5, backgroundColor: colors.gray, borderColor: "#E6E6E6", height: 35 }}
                          onPress={() => this.onClickSearchCategory()}>
                          <Text style={{ fontSize: 12 }}>{this.state.selectedCategoryLabel}</Text>
                        </Button>
                      </View>
                      {this.state.visibleSearchListCategory &&
                        <View style={{ height: 300, flexDirection: 'column', borderWidth: 1, padding: 10, backgroundColor: colors.gray, margin: 5, borderColor: "#E6E6E6", }}>
                          <Form>
                            <Item stackedLabel style={{ marginLeft: 0 }}>
                              <Input value={this.state.searchWordCategory} style={{ borderRadius: 5, marginLeft: 5, marginRight: 5, fontSize: 10 }} bordered onChangeText={(text) => this.onChangeCategory(text)} placeholder='Ketik Kategori' />
                            </Item>
                          </Form>
                          <View style={{ flex: 1 }}
                            onStartShouldSetResponderCapture={() => {
                              this.setState({ enableScrollViewScroll: false });
                              if (this._myScroll.contentOffset === 0
                                && this.state.enableScrollViewScroll === false) {
                                this.setState({ enableScrollViewScroll: true });
                              }
                            }}>
                            <FlatList
                              data={this.state.listKategori}
                              renderItem={this._renderItem3}
                              keyExtractor={(item, index) => index.toString()}
                            />
                          </View>
                        </View>
                      }
                    </View>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Uraian Aktifitas (*)</Text>
                  </View>
                  <View>
                    <Textarea style={{ borderRadius: 5, marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={2} bordered value={this.state.aktifitas} placeholder='Type Aktifitas ...' onChangeText={(text) => this.setState({ aktifitas: text })} />
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Penyebab Masalah</Text>
                  </View>
                  <View>
                    <Textarea style={{ borderRadius: 5, marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={2} bordered value={this.state.penyebab} placeholder='Type Penyebab ...' onChangeText={(text) => this.setState({ penyebab: text })} />
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Potensi Bahaya (*)</Text>
                  </View>
                  <View>
                    <Textarea style={{ borderRadius: 5, marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={2} bordered value={this.state.potensi} placeholder='Type Potensi Bahaya ...' onChangeText={(text) => this.setState({ potensi: text })} />
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Rekomendasi Safety</Text>
                  </View>
                  <View>
                    <Textarea style={{ borderRadius: 5, marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={2} bordered value={this.state.rekomendasi} placeholder='Type Rekomen Safety ...' onChangeText={(text) => this.setState({ rekomendasi: text })} />
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Rencana Tindak Lanjut</Text>
                  </View>
                  <View>
                    <Textarea style={{ borderRadius: 5, marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={2} bordered value={this.state.followup} placeholder='Type Tindak Lanjut ...' onChangeText={(text) => this.setState({ followup: text })} />
                  </View>
                </View>
              </CardItem>
            </View>
            <View style={styles.Contentsave}>
              <Button
                block
                style={{
                  height: 45,
                  marginLeft: 20,
                  marginRight: 20,
                  marginBottom: 20,
                  borderWidth: 1,
                  backgroundColor: "#00b300",
                  borderColor: "#00b300",
                  borderRadius: 4
                }}
                onPress={() => this.UpdateDetailHeader()}
              >
                <Text style={{ color: colors.white }}>SUBMIT</Text>
              </Button>
            </View>
          </Content>


        </View>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog visible={this.state.visibleLoadingUnitKerja}>
            <DialogContent>
              {
                <ActivityIndicator size="large" color="#330066" animating />
              }
            </DialogContent>
          </Dialog>
        </View>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleDialogSubmit}
            dialogTitle={<DialogTitle title="Updating Detail Header Report K3 .." />}
          >
            <DialogContent>
              {<ActivityIndicator size="large" color="#330066" animating />}
            </DialogContent>
          </Dialog>
        </View>
        <CustomFooter navigation={this.props.navigation} menu='Safety' />
      </Container>

    );
  }
}
