import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Textarea,
  Icon,
  Picker,
  Form,
  Input
} from "native-base";

import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DatePicker from "react-native-datepicker";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";

export default class UpdateRecapitulations extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isLoading: true,
      listMaster: [],
      status: "OPEN",
      Rekapitulasi: [],
      evaluasi: "",
      batasWaktu: "",
      dasarHukum: "",
      dasarHukumText: "",
      note: "",
      status: "",
      pengecekan: "",
      visibleModalHukum: false,
      visibleDialogSubmit:false
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} />;

  componentDidMount() {
    AsyncStorage.getItem("list").then(itemRekapitulasi => {
      this.setState({
        Rekapitulasi: JSON.parse(itemRekapitulasi),
        evaluasi: JSON.parse(itemRekapitulasi).EVALUASI,
        batasWaktu: JSON.parse(itemRekapitulasi).DATE_COMMITMENT,
        dasarHukum: JSON.parse(itemRekapitulasi).DASAR_HUKUM,
        dasarHukumText: JSON.parse(itemRekapitulasi).DASAR_HUKUM_TEXT,
        note: JSON.parse(itemRekapitulasi).NOTE,
        status: JSON.parse(itemRekapitulasi).STATUS
      });
    });

    //this.loadData();
    this.loadMaster();
  }

  loadMaster() {
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/safety/unsafe/dasar_hukum";
      var formData = new FormData();
      formData.append("token", value);
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            listMaster: responseJson.data,
            isloading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  UpdateRecapitulations() {
    if (this.state.pengecekan == null) {
      alert("Masukkan Pengecekan");
    } else {
      this.setState({
        visibleDialogSubmit:true
      })
      AsyncStorage.getItem("token").then(value => {
        var url =
          GlobalConfig.SERVERHOST + "api/v_mobile/safety/unsafe/update_recap";
        var formData = new FormData();
        formData.append("token", value);
        formData.append("ID", this.state.Rekapitulasi.ID);
        // formData.append("EVALUASI", this.state.evaluasi);
        // formData.append("BATAS_WAKTU", this.state.batasWaktu);
        formData.append("DASAR_HUKUM", this.state.dasarHukum);
        formData.append("DASAR_HUKUM_TEXT", this.state.dasarHukumText);
        // formData.append("NOTE", this.state.note);
        // formData.append("STATUS", this.state.status);

        fetch(url, {
          headers: {
            "Content-Type": "multipart/form-data"
          },
          method: "POST",
          body: formData
        })
          .then(response => response.json())
          .then(response => {
            // console.log(response)
            if (response.status == 200) {
              this.inputInspeksi();
            } else {
              this.setState({
                visibleDialogSubmit:false
              })
              Alert.alert("Error", "Update Failed", [
                {
                  text: "Oke"
                }
              ]);
            }
          })
          .catch((error)=>{
            this.setState({
              visibleDialogSubmit:false
            })
            Alert.alert("Error", "Update Failed", [
              {
                text: "Oke"
              }
            ]);
            console.log(error)
          })
      });
    }
  }

  inputInspeksi() {
    AsyncStorage.getItem("token").then(value => {
      var url = GlobalConfig.SERVERHOST + "api/v_mobile/safety/unsafe/inspeksi";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("NO_REKAP", this.state.Rekapitulasi.ID);
      formData.append("NOTE", this.state.pengecekan);
      formData.append("STATUS", "OPEN");

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          // console.log(response)
          if (response.status == 200) {
            this.setState({
              visibleDialogSubmit:false
            })
            Alert.alert("Success", "Update Success", [
              {
                text: "Oke"
              }
            ]);
            this.props.navigation.navigate("Recapitulations");
          } else {
            this.setState({
              visibleDialogSubmit:false
            })
            Alert.alert("Error", "Update Failed", [
              {
                text: "Oke"
              }
            ]);
          }
        })
        .catch((error)=>{
          this.setState({
            visibleDialogSubmit:false
          })
          Alert.alert("Error", "Update Failed", [
            {
              text: "Oke"
            }
          ]);
          console.log(error)
        })
    });
  }

  onChangeHukum(id) {
    this.setState({
      visibleModalHukum: true
    });
    AsyncStorage.getItem("token").then(value => {
      var url =
        GlobalConfig.SERVERHOST + "api/v_mobile/safety/unsafe/dasar_hukum";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("ID", id);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          // console.log(response)
          if (response.status == 200) {
            this.setState({
              visibleModalHukum: false
            });
            Alert.alert("Success", "Pilih Dasar Hukum Success", [
              {
                text: "Oke"
              }
            ]);
            this.setState({
              dasarHukum: id,
              dasarHukumText: response.data[0].DESKRIPSI
            });
          } else {
            this.setState({
              visibleModalHukum: false
            });
            Alert.alert("Error", "Pilih Dasar Hukum Failed", [
              {
                text: "Oke"
              }
            ]);
          }
        });
    });
  }

  render() {
    let listMaster = this.state.listMaster.map((s, i) => {
      return (
        <Picker.Item
          key={i}
          value={s.ID}
          label={"Ayat" + " " + s.AYAT + " " + "Pasal" + " " + s.PASAL + " -> "+s.DESKRIPSI}
        />
      );
    });
    return (
      <Container style={styles.wrapper2}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() =>
                this.props.navigation.navigate("RecapitulationsDetail")
              }
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>
              Edit {this.state.Rekapitulasi.NO_REKAP}
            </Title>
          </Body>
          
            <Right style={{flex:1}}></Right>
          
          {/* <Right/> */}
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ flex: 1 }}>
          <Content style={{ marginTop: 0 }}>
            <View style={{ backgroundColor: colors.gray }}>
              <View style={{ flex: 1, paddingLeft: 15, paddingRight: 15 }}>
                <View style={{ paddingTop: 10 }}>
                  <Text style={styles.titleInput}>Dasar Hukum (*)</Text>
                  <Form
                    style={{
                      borderWidth: 1,
                      borderRadius: 5,
                      marginRight: 5,
                      marginLeft: 5,
                      borderColor: "#E6E6E6",
                      height: 35
                    }}
                  >
                    <Picker
                      mode="dropdown"
                      iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                      style={{ width: "100%", height: 35 }}
                      placeholder="Choose Dasar Hukum ..."
                      placeholderStyle={{ color: "#bfc6ea" }}
                      placeholderIconColor="#007aff"
                      selectedValue={this.state.dasarHukum}
                      onValueChange={itemValue => this.onChangeHukum(itemValue)}
                    >
                      <Picker.Item label="Choose Dasar Hukum..." value="" />
                      {listMaster}
                    </Picker>
                  </Form>
                </View>

                <View style={{ flex: 1,paddingTop:10 }}>
                  <View>
                    <Text style={styles.titleInput}>Pengecekan *</Text>
                  </View>
                  <View>
                    <Textarea
                      style={{
                        borderRadius: 5,
                        marginLeft: 5,
                        marginRight: 5,
                        fontSize: 10
                      }}
                      rowSpan={2}
                      bordered
                      value={this.state.pengecekan}
                      placeholder="Input pengecekan ..."
                      onChangeText={text => this.setState({ pengecekan: text })}
                    />
                  </View>
                </View>
              </View>
            </View>
          </Content>

          <View style={styles.Contentsave}>
            <Button
              block
              style={{
                height: 45,
                marginLeft: 20,
                marginRight: 20,
                marginBottom: 20,
                borderWidth: 1,
                backgroundColor: "#00b300",
                borderColor: "#00b300",
                borderRadius: 4
              }}
              onPress={() => this.UpdateRecapitulations()}
            >
              <Text style={{ color: colors.white }}>UPDATE</Text>
            </Button>
          </View>
        </View>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleModalHukum}
            dialogTitle={<DialogTitle title="Proses Mengambil Data Hukum .." />}
          >
            <DialogContent>
              {<ActivityIndicator size="large" color="#330066" animating />}
            </DialogContent>
          </Dialog>
        </View>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleDialogSubmit}
            dialogTitle={<DialogTitle title="Updating Recapitulation .." />}
          >
            <DialogContent>
              {<ActivityIndicator size="large" color="#330066" animating />}
            </DialogContent>
          </Dialog>
        </View>
        <CustomFooter navigation={this.props.navigation} menu="Safety" />
      </Container>
    );
  }
}
