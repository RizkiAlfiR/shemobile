import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator,
  RefreshControl
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Thumbnail
} from "native-base";

import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewReportDetail from "../../components/ListViewReportDetail";
import Ripple from "react-native-material-ripple";

class ListItem extends React.PureComponent {
  navigateToScreen(route, id_listReport) {
    // alert(id_listReport)
    AsyncStorage.setItem("list", JSON.stringify(id_listReport)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View style={{
        marginLeft: 10, marginRight: 10, borderRadius: 5, backgroundColor: colors.white, marginBottom: 10,
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 2,
      }}>
        <Ripple
          style={
            {
              // flex: 2,
              // justifyContent: "center",
              // alignItems: "center"
            }
          }
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={
            () =>
              this.navigateToScreen(
                "DailyReportDetailListFoto",
                this.props.data
              )
          }
          rippleColor={colors.accent}
        >
          <View style={{ marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
            <ListViewReportDetail
              document={this.props.data.no_dokumen}
              type_priority={this.props.data.priority}
              type_found={this.props.data.jenis_temuan}
              employee={this.props.data.user_id}
              found={this.props.data.temuan}
              location={this.props.data.lokasi_text}
              caused={this.props.data.penyebab}
              recomendation={this.props.data.rekomendasi}
              date={this.props.data.tanggal_real}
              status={this.props.data.status}
              image_bef={
                GlobalConfig.LOCALHOST + "uploads/accident_uploads/" + this.props.data.foto_bef
              }
              tlanjut={this.props.data.tindak_lanjut}
              image_aft={
                GlobalConfig.LOCALHOST + "uploads/accident_uploads/" + this.props.data.foto_aft
              }
            />
          </View>
        </Ripple>
      </View>
    );
  }
}

class DailyReportDetailList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isLoading: true,
      idUnsafe: "",
      isEmpty: false,
      noDokumen: ''
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} navigation={this.props.navigation} />;

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        AsyncStorage.getItem("noDokumen").then(noDokumen => {
          this.setState({
            noDokumen: noDokumen
          })
        });
        AsyncStorage.getItem("idAndShift").then(idAndShift => {
          var temp = idAndShift.split("+");
          console.log(temp[0]);
          console.log(temp[1]);
          var idUnsafe = temp[0];
          this.setState({ idUnsafe: idUnsafe });
          this.setState({ isLoading: true });
          this.loadData(idUnsafe);
        });
      }
    );
  }

  onRefresh() {
    console.log('refreshing')
    this.setState({ isLoading: true }, function () {
      AsyncStorage.getItem("idAndShift").then(idAndShift => {
        var temp = idAndShift.split("+");
        console.log(temp[0]);
        console.log(temp[1]);
        var idUnsafe = temp[0];
        this.setState({ idUnsafe: idUnsafe });
        this.loadData(idUnsafe);
      });
    });
  }

  navigateToScreen(route, id_listReport) {
    // alert(id_listReport)
    AsyncStorage.setItem("list", JSON.stringify(id_listReport)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  loadData(id_Unsafe) {
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.LOCALHOST + "api/unsafedetail/" + id_Unsafe + "?token=" + value;
      // var formData = new FormData();
      // formData.append("token", value);
      // formData.append("ID", id_Unsafe);

      // fetch(url, {
      //   headers: {
      //     "Content-Type": "multipart/form-data"
      //   },
      //   method: "POST",
      //   body: formData
      // })
      fetch(url)
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson))
          if (responseJson.status == 200) {
            this.setState({
              dataSource: responseJson.data.unsafe_detail_reports,
              isLoading: false,
              isEmpty: false
            });
          } else {
            this.setState({
              isEmpty: true,
              isLoading: false
            });
          }
          if (this.state.dataSource.length() == 0) {
            this.setState({
              isEmpty: true,
              isLoading: false
            });
          }
          console.log(this.state.dataSource);
        })
        .catch(error => {
          console.log(error);
          this.setState({
            isLoading: false
          });
        });
    });
  }

  render() {
    var list;
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.isEmpty) {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = (
          <FlatList
            data={this.state.dataSource}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index.toString()}
            refreshControl={
              <RefreshControl
                refreshing={this.state.isloading}
                onRefresh={this.onRefresh.bind(this)}
              />}
          />
        );
      }
    }

    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DailyReport")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3, alignItems: 'center' }}>
            <Title style={styles.textbody}>Unsafe Detail Report</Title>
          </Body>

          <Right style={{ flex: 1 }} />
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ flex: 1, flexDirection: "column", marginTop: 10 }}>
          {/* <ScrollView> */}
          <View style={{ justifyContent: "center", alignItems: "center", marginTop: 0 }}>
            <Text style={{ fontSize: 12, color: colors.green01, fontWeight: 'bold', marginBottom: 20 }}>{this.state.noDokumen}</Text>
          </View>
          {list}
          {/* </ScrollView> */}
        </View>

        <CustomFooter navigation={this.props.navigation} menu="Safety" />
      </Container>
    );
  }
}

export default DailyReportDetailList;
