import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator,
  RefreshControl,
  Dimensions
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Thumbnail
} from "native-base";

import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewDetail from "../../components/ListViewDetail";
import Ripple from "react-native-material-ripple";

class ListItem extends React.PureComponent {
  navigateToScreen(route, listReportK3) {
    AsyncStorage.setItem("list", JSON.stringify(listReportK3)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View style={{
        marginLeft: 10, marginRight: 10, borderRadius: 5, backgroundColor: colors.white, marginBottom: 10,
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 2,
        marginTop: 5,
        paddingVertical: 10
      }}>
        <Ripple
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.navigateToScreen("DailyReportK3DetailHeaderList", this.props.data)
          }
          rippleColor={colors.accent}
        >
          <View style={{ marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
            <ListViewDetail
              unit={this.props.data.unit_name}
              aktifitas={this.props.data.activity_description}
              type={this.props.data.unsafe_type}
              potensi={this.props.data.potential_hazard}
              followUp={this.props.data.follow_up}
              status={this.props.data.status}
              kode={this.props.data.kode_en}
              deskripsi={this.props.data.deskripsi}
              idKategori={this.props.data.id_category}
            />
          </View>
        </Ripple>
      </View>
    );
  }
}

class DailyReportK3DetailList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isLoading: true,
      idUnsafe: ""
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => (
    <ListItem data={item} navigation={this.props.navigation} />
  );

  onRefresh() {
    console.log("refreshing");
    this.setState({ isLoading: true }, function () {
      this.loadData();
    });
  }

  componentDidMount() {
    // AsyncStorage.getItem('id').then((idUnsafe) => {
    //     this.setState({ idUnsafe: idUnsafe });
    //     this.setState({ isLoading: true });
    //     this.loadData(idUnsafe);
    // })
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        AsyncStorage.getItem("id").then(idUnsafe => {
          this.setState({ idUnsafe: idUnsafe });
          this.setState({ isLoading: false });
          this.loadData(idUnsafe);
        });
      }
    );
  }



  loadData(idUnsafe) {
    this.setState({ isLoading: true });
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.LOCALHOST +
        "api/unsafek3header/" + idUnsafe + "?token=" + value;

      // fetch(url, {
      //   headers: {
      //     "Content-Type": "multipart/form-data"
      //   },
      //   method: "POST",
      //   body: formData
      // })
      fetch(url)
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson.data.unsafe_report_details_k3))
          this.setState({
            // dataSource: responseJson.data.filter(x => x.DELETE_FLAG == 0),
            dataSource: responseJson.data.unsafe_report_details_k3,
            isLoading: false
          });
        })
        .catch(error => {
          this.setState({ isLoading: false });
          console.log(error);
        });
    });
  }

  navigateToScreenCreate(route, id) {
    AsyncStorage.setItem("id", id).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  render() {
    var list;
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.dataSource.length == 0) {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list =
          <FlatList
            data={this.state.dataSource}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index.toString()}
            refreshControl={
              <RefreshControl
                refreshing={this.state.isLoading}
                onRefresh={this.onRefresh.bind(this)}
              />}
          />
      }
    }
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() =>
                this.props.navigation.navigate("DailyReportK3Detail")
              }
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3, alignItems: 'center' }}>
            <Title style={styles.textbody}>Unsafe Detail List</Title>
          </Body>
          <Right style={{ flex: 1 }} />
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ marginTop: 10, flex: 1, flexDirection: 'column' }}>{list}</View>
        <Fab
          active={this.state.active}
          direction="up"
          containerStyle={{}}
          style={{ backgroundColor: colors.green01, marginBottom: ((Dimensions.get("window").height === 812 || Dimensions.get("window").height === 896) && Platform.OS === 'ios') ? 80 : 50 }}
          position="bottomRight"
          onPress={() =>
            this.navigateToScreenCreate(
              "CreateDetailHeaderReportK3",
              this.state.idUnsafe
            )
          }
        >
          <Icon name="ios-add" style={{ fontSize: 50, fontWeight: "bold", paddingTop: Platform.OS === 'ios' ? 25 : null, }} />
        </Fab>
        <CustomFooter navigation={this.props.navigation} menu="Safety" />
      </Container>
    );
  }
}

export default DailyReportK3DetailList;
