import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator,
  RefreshControl
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Text,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Thumbnail
} from "native-base";

import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewUnsafe from "../../components/ListViewUnsafe";
import Ripple from "react-native-material-ripple";

class ListItem extends React.PureComponent {
    navigateToScreen(route, unsafe) {
        AsyncStorage.setItem("unsafe", JSON.stringify(unsafe)).then(() => {
          this.props.navigation.navigate(route);
        });
      }

  render() {
    return (
      <View>
        <Ripple

          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.navigateToScreen("DailyReportUnsafeDetail", this.props.data)
          }
          rippleColor={colors.accent}
        >
          <View  style={{ backgroundColor: "#FEFEFE" }}>
            <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
              <View style={{ marginTop: 5, marginLeft: 5, marginRight: 5 }}>
                <ListViewUnsafe
                  document={this.props.data.NO_DOKUMEN}
                  lokasi={this.props.data.LOKASI_TEXT}
                  date={this.props.data.CREATE_AT}
                  status={this.props.data.STATUS}
                  type={this.props.data.JENIS_TEMUAN=='UA' ? 'Unsafe Action':'Unsafe Condition' }
                  priority={this.props.data.PRIORITY}
                />
              </View>

            </Card>
          </View>
        </Ripple>
      </View>
    );
  }
}

class DailyReportUnsafe extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSourceUnsafe: [],
      isLoading: true,
      isEmpty: false,
      page: 0,
      posts: [],
      searchText:''
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} navigation={this.props.navigation} />;

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.setState({
          searchText:''
        })
        this.loadDataUnsafe();
      }
    );
  }

  navigateToScreen(route, id_Unsafe, shift) {
    var idAndShift = id_Unsafe + "+" + shift;
    console.log(idAndShift);
    AsyncStorage.setItem("idAndShift", idAndShift).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  onRefresh() {
    console.log('refreshing')
    this.setState({ isloading: true }, function() {
      this.setState({
        searchText:''
      })
      this.loadDataUnsafe();
    });
  }

  searchData(){
    this.setState({
      isloading: true,
      page: 0,
        posts: []
    });
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/safety/unsafe/list_view";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("search", this.state.searchText);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson.data.list_temuan));
          if (responseJson.data.my_unsafe.length != 0) {
            this.setState({
              dataSourceUnsafe: responseJson.data.my_unsafe,
              isloading: false,
              isEmpty: false
            }, function() {
              // call the function to pull initial 12 records
              this.addRecords(0);
            });
          } else {
            this.setState({
              isloading: false
            });
            this.setState({
              isEmpty: true
            });
          }
        })
        .catch(error => {
          console.log(error);
          this.setState({
            isloading: false
          });
        });
    });
  }

  loadDataUnsafe() {
    this.setState({
      isloading: true,
      page: 0,
        posts: []
    });
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/safety/unsafe/list_view";
      var formData = new FormData();
      formData.append("token", value);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson.data.list_temuan));
          if (responseJson.data.my_unsafe.length != 0) {
            this.setState({
              dataSourceUnsafe: responseJson.data.my_unsafe,
              isloading: false,
              isEmpty: false
            }, function() {
              // call the function to pull initial 12 records
              this.addRecords(0);
            });
          } else {
            this.setState({
              dataSourceUnsafe:[],
              isloading: false
            });
            this.setState({
              isEmpty: true
            });
          }
        })
        .catch(error => {
          console.log(error);
          this.setState({
            dataSourceUnsafe:[],
            isloading: false
          });
        });
    });
  }

  addRecords = (page) => {
    // assuming this.state.dataPosts hold all the records
    console.log('masuk add record')
    const newRecords = []
    for(var i = page * 12, il = i + 12; i < il && i <
      this.state.dataSourceUnsafe.length; i++){
      newRecords.push(this.state.dataSourceUnsafe[i]);
    }
    this.setState({
      posts: [...this.state.posts, ...newRecords]
    });
  }

  onScrollHandler = () => {
    this.setState({
      page: this.state.page + 1
    }, () => {
      this.addRecords(this.state.page);
    });
  }

  render() {
    var listUnsafe;
    if (this.state.isloading) {
      listUnsafe = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.isEmpty) {
        listUnsafe = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        listUnsafe =
        <FlatList
        data={this.state.posts}
        renderItem={this._renderItem}
        keyExtractor={(item, index) => index.toString()}
        onEndReached={this.onScrollHandler}
       onEndThreshold={1}
        refreshControl={
          <RefreshControl
            refreshing={this.state.isloading}
            onRefresh={this.onRefresh.bind(this)}
          />}
      />
      }
    }
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("SafetyMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body>
            <Title style={styles.textbody}>Daily Report</Title>
          </Body>
          <Right style={{flex:1}}></Right>
        </Header>
        <Footer style={styles.tabHeight}>
          <FooterTab style={styles.tabfooter}>
            <Button
              onPress={() => this.props.navigation.navigate("DailyReport")}
            >
              <Text style={{ color: "white", fontWeight: "bold" }}>List All Temuan</Text>
            </Button>
            <Button active style={styles.tabfooter}>
            <View style={{ height: "40%" }} />
                            <View style={{ height: "50%" }}>
                                <Text style={styles.textbody}>List My Unsafe</Text>
                            </View>
                            <View style={{ height: "20%" }} />
                            <View
                                style={{
                                borderWidth: 2,
                                marginTop: 2,
                                height: 0.5,
                                width: "100%",
                                borderColor: colors.white
                                }}
                            />
            </Button>
          </FooterTab>
        </Footer>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={styles.searchTab}>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={styles.viewLeftHeader}>
              <Item style={styles.searchItem}>
                <Input
                  style={{ fontSize: 15 }}
                  placeholder="Type something here"
                  value={this.state.searchText}
                  onChangeText={text => this.setState({ searchText: text })}
                />
                <Icon
                  name="ios-search"
                  style={{ fontSize: 30, paddingLeft: 0 }}
                  onPress={() => this.searchData()}
                />
              </Item>
            </View>
          </View>
        </View>
        <View style={{flex:1,flexDirection:'column', marginTop: 60 }}>
          {listUnsafe}
        </View>

        <CustomFooter navigation={this.props.navigation} menu="Safety" />
      </Container>
    );
  }
}

export default DailyReportUnsafe;
