import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Thumbnail
} from "native-base";

import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewHeader from "../../components/ListViewHeader";
import ListViewDetail from "../../components/ListViewDetail";

class ToolsCertificationDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      dataDetail: [],
      isLoading: true,
      detailTools: [],
      visibleDialogSubmit: false
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => (
    <ListItem data={item}></ListItem>
  )

  navigateToScreen(route, detailTools) {
    AsyncStorage.setItem('listAlat', JSON.stringify(detailTools)).then(() => {
      this.props.navigation.navigate(route);
    })
  }

  componentDidMount() {
    AsyncStorage.getItem('listAlat').then((listDataTools) => {
      this.setState({ detailTools: JSON.parse(listDataTools) });
      this.setState({ isLoading: false });
      console.log(this.state.detailTools);
    })
    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
      //this.loadData(id_Unsafe);
    });

  }

  konfirmasideleteTools() {
    Alert.alert(
      'Apakah Anda Yakin Ingin Menghapus', this.state.detailTools.NO_BUKU,
      [
        { text: 'Yes', onPress: () => this.deleteTools() },
        { text: 'Cancel' },
      ],
      { cancelable: false }
    )
  }

  deleteTools() {
    this.setState({
      visibleDialogSubmit: true
    })
    AsyncStorage.getItem('token').then((value) => {
      // alert(JSON.stringify(value));
      const url = GlobalConfig.LOCALHOST + 'api/tools/' + this.state.detailTools.id + '?token=' + value;
      // var formData = new FormData();
      // formData.append("token", value);
      // formData.append("ID", this.state.detailTools.ID);

      fetch(url, {
        // headers: {
        //   "Content-Type": "multipart/form-data"
        // },
        method: "DELETE",
        // body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            this.setState({
              visibleDialogSubmit: false
            })
            Alert.alert('Success', 'Delete Success', [{
              text: 'Oke',
            }])
            this.props.navigation.navigate("ToolsCertification")
          } else {
            this.setState({
              visibleDialogSubmit: false
            })
            Alert.alert('Error', 'Delete Failed', [{
              text: 'Oke'
            }])
          }
        })
        .catch((error) => {
          console.log(error)
          this.setState({
            visibleDialogSubmit: false
          })
          Alert.alert('Error', 'Delete Failed', [{
            text: 'Oke'
          }])
        })
    })
  }


  render() {
    var listTools;
    var listDetailTools;
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("ToolsCertification")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 1, alignItems: 'center' }}>
            <Title style={styles.textbody}>Tools Detail</Title>
          </Body>
          <Right style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.navigateToScreen('ToolsCertificationUpdate', this.state.detailTools)}
            >
              <Icon
                name="ios-create"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
            <Button
              transparent
              onPress={() => this.konfirmasideleteTools()}
            >
              <Icon
                name="ios-trash"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <Content style={{ marginTop: 0, marginBottom: 20, backgroundColor: colors.gray01 }}>
          <ScrollView>
            <View style={{ justifyContent: "center", alignItems: "center", marginTop: 10 }}>
              <Text style={{ fontSize: 14, color: colors.green01 }}>Tools Certification Detail {this.state.detailTools.no_buku}</Text>
            </View>
            <CardItem style={{ borderRadius: 10, marginTop: 15, marginHorizontal: 5, borderWidth: 2, borderColor: colors.graydar }}>
              <View style={{ flex: 1 }}>
                <View style={{ flex: 1, paddingVertical: 10 }}>
                  <Text style={{ fontSize: 12, paddingBottom: 5, fontWeight: 'bold' }}>Foto Buku :</Text>
                  <View style={styles.placeholder}>
                    {this.state.detailTools.file_buku == null ? (
                      <Image style={styles.previewImage} source={require("../../../assets/images/nopic.png")} />
                    ) : (
                        <Image style={styles.previewImage} source={{ uri: GlobalConfig.LOCALHOST + "public/uploads/tools/" + this.state.detailTools.file_buku }} />
                      )}
                  </View>
                </View>
                <View style={{ flex: 1, paddingVertical: 10 }}>
                  <Text style={{ fontSize: 12, paddingBottom: 5, fontWeight: 'bold' }}>Foto Alat :</Text>
                  <View style={styles.placeholder}>
                    {this.state.detailTools.file_aktual == null ? (
                      <Image style={styles.previewImage} source={require("../../../assets/images/nopic.png")} />
                    ) : (
                        <Image style={styles.previewImage} source={{ uri: GlobalConfig.LOCALHOST + "public/uploads/tools/" + this.state.detailTools.file_aktual }} />
                      )}
                  </View>
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 10, marginTop: 10, marginHorizontal: 5, marginBottom: 5, borderWidth: 2, borderColor: colors.graydar, backgroundColor: colors.green01 }}>
              <View style={{ flex: 1, flexDirection: 'row', paddingVertical: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white }}>Nomor Buku :</Text>
                  <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.detailTools.no_buku}</Text>
                  <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, color: colors.white }}>Peralatan :</Text>
                  <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.detailTools.equipment}</Text>
                  <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, color: colors.white }}>Data Teknis :</Text>
                  <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.detailTools.data_teknis}</Text>
                  <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, color: colors.white }}>Nomor Pengesahan :</Text>
                  <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.detailTools.nomor_pengesahan}</Text>
                  <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, color: colors.white }}>Unit Kerja :</Text>
                  <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.detailTools.uk_text}</Text>
                  <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, color: colors.white }}>Lokasi :</Text>
                  <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.detailTools.lokasi}</Text>
                </View>
                <View style={{ flex: 0 }}>
                  <Text style={{ fontSize: 12, fontWeight: "bold", textAlign: 'right', color: colors.white }}>Mulai Sertifikasi :</Text>
                  <Text style={{ fontSize: 12, textAlign: 'right', color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.detailTools.uji_awal}</Text>
                  <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, textAlign: 'right', color: colors.white }}>Tanggal Riksa :</Text>
                  <Text style={{ fontSize: 12, textAlign: 'right', color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.detailTools.uji_ulang}</Text>
                </View>
              </View>
            </CardItem>
          </ScrollView>
          <View style={{ width: 270, position: "absolute" }}>
            <Dialog
              visible={this.state.visibleDialogSubmit}
              dialogTitle={<DialogTitle title="Deleting Tools Certification .." />}
            >
              <DialogContent>
                {<ActivityIndicator size="large" color="#330066" animating />}
              </DialogContent>
            </Dialog>
          </View>
        </Content>
      </Container>
    );
  }
}

export default ToolsCertificationDetail;
