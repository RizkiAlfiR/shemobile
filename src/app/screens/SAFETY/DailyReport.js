import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    ActivityIndicator,
    RefreshControl,
    Dimensions
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Text,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Icon,
    Item,
    Input,
    Thumbnail
} from "native-base";

import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewReport from "../../components/ListViewReport";
import Ripple from "react-native-material-ripple";

class ListItem extends React.PureComponent {
    navigateToScreen(route, id_Unsafe, shift) {
        var idAndShift = id_Unsafe + '+' + shift;
        AsyncStorage.setItem('noDokumen', this.props.data.no_dokumen).then(() => {
            AsyncStorage.setItem('idAndShift', idAndShift).then(() => {
                this.props.navigation.navigate(route);
                // alert(this.props.data.id)
            })
        })

    }

    render() {
        return (
            <View style={{
                marginLeft: 10, marginRight: 10, borderRadius: 5, backgroundColor: colors.white, marginBottom: 10,
                shadowColor: '#000',
                shadowOffset: {
                    width: 0,
                    height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
                elevation: 2,
                marginTop: 5,
                paddingVertical: 10
            }}>
                <Ripple
                    rippleSize={176}
                    rippleDuration={600}
                    rippleContainerBorderRadius={15}
                    onPress={() =>
                        this.navigateToScreen(
                            "DailyReportDetailList",
                            this.props.data.id,
                            this.props.data.shift
                        )
                    }
                    rippleColor={colors.accent}
                >
                    <View style={{ marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
                        <ListViewReport
                            document={this.props.data.no_dokumen}
                            inspector={this.props.data.user_id}
                            shift={this.props.data.shift}
                            jumlah={this.props.data.unsafe_detail_reports.length}
                            date={this.props.data.tanggal}
                            status={this.props.data.status}
                        />
                    </View>
                </Ripple>
            </View>
        );
    }
}

class DailyReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            dataSource: [],
            isloading: true,
            isEmpty: false,
            searchText: ''
        };
    }

    static navigationOptions = {
        header: null
    };

    _renderItem = ({ item }) => (
        <ListItem data={item} navigation={this.props.navigation}></ListItem>
    )

    componentDidMount() {
        this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
            this.setState({
                searchText: ''
            })
            this.loadData();
        });
    }

    onRefresh() {
        console.log('refreshing')
        this.setState({ isloading: true }, function () {
            this.setState({
                searchText: ''
            })
            this.loadData();
        });
    }

    loadData() {
        this.setState({
            isloading: true
        });
        AsyncStorage.getItem('token').then((value) => {
            const url = GlobalConfig.LOCALHOST + 'api/unsafedetail/search?token=' + value;
            var formData = new FormData();
            formData.append("search", this.state.searchText)

            fetch(url, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                method: 'POST',
                body: formData
            })
                // fetch(url)
                .then((response) => response.json())
                .then((responseJson) => {
                    // alert(JSON.stringify(responseJson.data));
                    if (responseJson.status == 200) {
                        this.setState({
                            dataSource: responseJson.data,
                            isloading: false,
                            isEmpty: false
                        });
                    } else {
                        this.setState({
                            isloading: false
                        });
                        this.setState({
                            isEmpty: true
                        });
                    }
                })
                .catch((error) => {
                    console.log(error)
                    this.setState({
                        isloading: false
                    });
                })
        })
    }

    // searchData() {
    //     this.setState({
    //         isloading: true
    //     });
    //     AsyncStorage.getItem('token').then((value) => {
    //         const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/unsafe/list_view';
    //         var formData = new FormData();
    //         formData.append("token", value)
    //         formData.append("search", this.state.searchText)

    //         fetch(url, {
    //             headers: {
    //                 'Content-Type': 'multipart/form-data'
    //             },
    //             method: 'POST',
    //             body: formData
    //         })
    //             .then((response) => response.json())
    //             .then((responseJson) => {
    //                 // alert(JSON.stringify(responseJson.data.list_temuan));
    //                 if (responseJson.data.list_temuan != undefined) {
    //                     this.setState({
    //                         dataSource: responseJson.data.list_temuan,
    //                         isloading: false,
    //                         isEmpty: false
    //                     });
    //                 } else {
    //                     this.setState({
    //                         dataSource: [],
    //                         isloading: false
    //                     });
    //                     this.setState({
    //                         isEmpty: true
    //                     });
    //                 }
    //             })
    //             .catch((error) => {
    //                 console.log(error)
    //                 this.setState({
    //                     dataSource: [],
    //                     isloading: false
    //                 });
    //             })
    //     })
    // }

    render() {
        var listTemuan;
        if (this.state.isloading) {
            listTemuan = (
                <View
                    style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
                >
                    <ActivityIndicator size="large" color="#330066" animating />
                </View>
            );
        } else {
            if (this.state.isEmpty) {
                listTemuan = (
                    <View
                        style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
                    >
                        <Thumbnail square large source={require("../../../assets/images/empty.png")} />
                        <Text>No Data!</Text>
                    </View>
                )
            } else {
                listTemuan =
                    <FlatList
                        data={this.state.dataSource}
                        renderItem={this._renderItem}
                        keyExtractor={(item, index) => index.toString()}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.isloading}
                                onRefresh={this.onRefresh.bind(this)}
                            />}
                    />
            }
        }
        return (
            <Container style={styles.wrapper}>
                <Header style={styles.header}>
                    <Left style={{ flex: 1 }}>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate("SafetyMenu")}
                        >
                            <Icon
                                name="ios-arrow-back"
                                size={20}
                                style={styles.facebookButtonIconOrder2}
                            />
                        </Button>
                    </Left>
                    <Body style={{ flex: 3, alignItems: 'center' }}>
                        <Title style={styles.textbody}>Daily Report</Title>
                    </Body>
                    <Right style={{ flex: 1 }}></Right>
                </Header>
                {/* <Footer style={styles.tabHeight}>
                    <FooterTab style={styles.tabfooter}>
                        <Button active style={styles.tabfooter}>
                            <View style={{ height: "40%" }} />
                            <View style={{ height: "50%" }}>
                                <Text style={styles.textbody}>List All Temuan</Text>
                            </View>
                            <View style={{ height: "20%" }} />
                            <View
                                style={{
                                    borderWidth: 2,
                                    marginTop: 2,
                                    height: 0.5,
                                    width: "100%",
                                    borderColor: colors.white
                                }}
                            />
                        </Button>
                        <Button
                            onPress={() =>
                                // this.props.navigation.navigate("DailyReportUnsafe")
                                alert("My Unsafe Pressed")
                            }
                        >
                            <Text style={{ color: "white", fontWeight: "bold" }}>List My Unsafe</Text>
                        </Button>
                    </FooterTab>
                </Footer> */}
                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                {/* <View style={styles.searchTab}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={styles.viewLeftHeader}>
                            <Item style={styles.searchItem}>
                                <Input style={{ fontSize: 15 }} placeholder="Type something here" value={this.state.searchText} onChangeText={(text) => this.setState({ searchText: text })} />
                                <Icon name="ios-search" style={{ fontSize: 30, paddingLeft: 0 }} onPress={() => this.searchData()} />
                            </Item>
                        </View>
                    </View>
                </View> */}
                <View style={styles.search}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={styles.viewLeftHeader}>
                            <Item style={styles.searchItem}>
                                <Input
                                    style={{ fontSize: 15 }}
                                    placeholder="Type something here"
                                    value={this.state.searchText}
                                    onChangeText={text => this.setState({ searchText: text })}
                                />
                                <Icon
                                    name="ios-search"
                                    style={{ fontSize: 30, paddingLeft: 0 }}
                                    onPress={() => this.loadData()}
                                />
                            </Item>
                        </View>
                    </View>
                </View>
                <View style={{ marginTop: 60, flex: 1, flexDirection: 'column' }}>

                    {listTemuan}

                </View>
                <Fab
                    active={this.state.active}
                    direction="up"
                    containerStyle={{}}
                    style={{ backgroundColor: colors.green01, marginBottom: ((Dimensions.get("window").height === 812 || Dimensions.get("window").height === 896) && Platform.OS === 'ios') ? 80 : 50 }}
                    position="bottomRight"

                    onPress={() => this.props.navigation.navigate('CreateTemuanUnsafe')}>
                    <Icon name="ios-add" style={{ fontSize: 50, fontWeight: "bold", paddingTop: Platform.OS === 'ios' ? 25 : null, justifyContent: "center" }} />
                </Fab>
                <CustomFooter navigation={this.props.navigation} menu='Safety' />
            </Container>

        );
    }
}

export default DailyReport;
