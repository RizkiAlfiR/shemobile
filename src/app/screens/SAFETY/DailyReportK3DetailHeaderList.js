import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  RefreshControl,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input
} from "native-base";

import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewHeader from "../../components/ListViewHeader";
import ListViewDetail from "../../components/ListViewDetail";

class ListItem extends React.PureComponent {
  render() {
    return (
      <View style={{ flex: 1, paddingVertical: 10 }}>
        {/* {this.props.data.UPLOADED_PATH.includes('TEMUAN')? ( */}
        {this.props.data.type_file_upload == 0 ? (
          <Text style={{ fontSize: 12, paddingBottom: 5, fontWeight: 'bold' }}>Foto Temuan :</Text>
        ) : (
            <Text style={{ fontSize: 12, paddingBottom: 5, fontWeight: 'bold' }}>Foto Hasil Tindak Lanjut :</Text>
          )}
        <View style={styles.placeholder}>
          <Image style={styles.previewImage} source={{ uri: GlobalConfig.LOCALHOST + 'public/uploads/unsafe/' + this.props.data.uploaded_path + '?' + new Date() }} />
        </View>
      </View>
    )
  }
}

class DailyReportK3DetailHeaderList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      isLoading: true,
      listHeader: [],
      listFileUpload: [],
      visibleDialogSubmit: false
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => (
    <ListItem data={item}></ListItem>
  )

  onRefresh() {
    console.log('refreshing')
    this.setState({ isloading: true }, function () {

    });
  }


  navigateToScreen(route, listHeader) {
    AsyncStorage.setItem('list', JSON.stringify(listHeader)).then(() => {
      this.props.navigation.navigate(route);
    })
  }

  componentDidMount() {
    AsyncStorage.getItem('list').then((listReportK3) => {
      this.setState({ listHeader: JSON.parse(listReportK3) }, function () { console.log(this.state.listHeader) });
      this.setState({ listFileUpload: this.state.listHeader.list_file_upload });
      this.setState({ isLoading: false });
    })
    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
      //this.loadData(id_Unsafe);
    });
  }

  konfirmasideleteList() {
    Alert.alert(
      'Apakah Anda Yakin Ingin Menghapus Detail ID ', JSON.stringify(this.state.listHeader.id),
      [
        { text: 'Yes', onPress: () => this.deleteList() },
        { text: 'Cancel' },
      ],
      { cancelable: false }
    )
  }

  deleteList() {
    this.setState({
      visibleDialogSubmit: true
    })
    AsyncStorage.getItem('token').then((value) => {
      // alert(JSON.stringify(value));
      const url = GlobalConfig.LOCALHOST + 'api/unsafek3detail/' + this.state.listHeader.id + '?token=' + value;

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "DELETE"
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            this.setState({
              visibleDialogSubmit: false
            })
            Alert.alert('Success', 'Delete Success', [{
              text: 'Oke',
            }])
            this.props.navigation.navigate("DailyReportK3Detail")
          } else {
            this.setState({
              visibleDialogSubmit: false
            })
            Alert.alert('Error', 'Delete Failed', [{
              text: 'Oke'
            }])
          }
        })
        .catch((error) => {
          console.log(error)
          this.setState({
            visibleDialogSubmit: false
          })
          Alert.alert('Error', 'Delete Failed', [{
            text: 'Oke'
          }])
        })
    })
  }

  render() {
    var status = ""
    if (this.state.listHeader.status != undefined) {
      status = this.state.listHeader.status
    }
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DailyReportK3DetailList")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 1, alignItems: 'center' }}>
            <Title style={styles.textbody}>Unsafe Detail</Title>
          </Body>
          <Right style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.navigateToScreen('UpdateDetailHeaderReportK3', this.state.listHeader)}
            >
              <Icon
                name="ios-create"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
            <Button
              transparent
              onPress={() => this.konfirmasideleteList()}
            >
              <Icon
                name="ios-trash"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <Content style={{ marginTop: 0, marginBottom: 20, backgroundColor: colors.gray01 }}>
          <View style={{}}>
            <CardItem style={{ borderRadius: 10, marginTop: 15, marginHorizontal: 5, marginBottom: 5, borderWidth: 2, borderColor: colors.graydar }}>
              <View style={{ flex: 1, paddingVertical: 10 }}>
                <Text style={{ fontSize: 12, paddingBottom: 5, fontWeight: 'bold' }}>Foto Temuan :</Text>
                <View style={styles.placeholder}>
                  {this.state.listHeader.foto_bef == null ? (
                    <Image style={styles.previewImage} source={require("../../../assets/images/nopic.png")} />
                  ) : (
                      <Image style={styles.previewImage} source={{ uri: GlobalConfig.LOCALHOST + "public/uploads/unsafe/" + this.state.listHeader.foto_bef }} />
                    )}
                </View>
              </View>
            </CardItem>
            {(this.state.listHeader.status == "CLOSE") && (
              <CardItem style={{ borderRadius: 10, marginTop: 15, marginHorizontal: 5, marginBottom: 5, borderWidth: 2, borderColor: colors.graydar }}>
                <View style={{ flex: 1, paddingVertical: 10 }}>
                  <Text style={{ fontSize: 12, paddingBottom: 5, fontWeight: 'bold' }}>Foto Follow Up :</Text>
                  <View style={styles.placeholder}>
                    {this.state.listHeader.foto_aft == null ? (
                      <Image style={styles.previewImage} source={require("../../../assets/images/nopic.png")} />
                    ) : (
                        <Image style={styles.previewImage} source={{ uri: GlobalConfig.LOCALHOST + "public/uploads/unsafe/" + this.state.listHeader.foto_aft }} />
                      )}
                  </View>
                </View>
              </CardItem>
            )}
            <CardItem style={{ borderRadius: 10, marginTop: 10, marginHorizontal: 5, marginBottom: 5, borderWidth: 2, borderColor: colors.graydar, backgroundColor: colors.green01 }}>
              <View style={{ flex: 1, flexDirection: "row", paddingVertical: 10 }}>
                <View style={{ width: 260 }}>
                  <Text style={{ fontSize: 12, fontWeight: "bold", color: colors.white }}>Kode</Text>
                  <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.listHeader.kode_en}-00{this.state.listHeader.id_category}</Text>
                  <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, color: colors.white }}>Type</Text>
                  <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.listHeader.unsafe_type}</Text>
                  <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, color: colors.white }}>Aktifitas</Text>
                  <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.listHeader.activity_description}</Text>
                  <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, color: colors.white }}>Report Date</Text>
                  <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.listHeader.created_at}</Text>
                  <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, color: colors.white }}>Jam</Text>
                  <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.listHeader.o_clock_incident}</Text>
                  {((status).toUpperCase()) == "OPEN" && (
                    <View>
                      <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, color: colors.white }}>Status</Text>
                      <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular", color: colors.red }}>{this.state.listHeader.status}</Text>
                    </View>
                  )}
                  {((status).toUpperCase()) == "CLOSE" && (
                    <View>
                      <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, color: colors.white }}>Status</Text>
                      <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular", color: colors.green01 }}>{this.state.listHeader.status}</Text>
                    </View>
                  )}
                  {((((status).toUpperCase()) != "CLOSE") && (((status).toUpperCase()) != "OPEN")) && (
                    <View>
                      <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, color: colors.white }}>Status</Text>
                      <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.listHeader.status}</Text>
                    </View>
                  )}
                  <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, color: colors.white }}>Potensi</Text>
                  <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.listHeader.potential_hazard}</Text>
                  <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, color: colors.white }}>Unit</Text>
                  <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.listHeader.unit_name}</Text>
                  <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, color: colors.white }}>Inspector</Text>
                  <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.listHeader.user_id}</Text>
                  <Text style={{ fontSize: 12, fontWeight: "bold", paddingTop: 10, color: colors.white }}>Follow Up</Text>
                  <Text style={{ fontSize: 12, color: colors.white, fontFamily: "Montserrat-Regular" }}>{this.state.listHeader.follow_up}</Text>
                </View>
              </View>
            </CardItem>

          </View>
          <View style={{ width: 270, position: "absolute" }}>
            <Dialog
              visible={this.state.visibleDialogSubmit}
              dialogTitle={<DialogTitle title="Deleting Detail Header Report K3 .." />}
            >
              <DialogContent>
                {<ActivityIndicator size="large" color="#330066" animating />}
              </DialogContent>
            </Dialog>
          </View>
        </Content>
      </Container>
    );
  }
}

export default DailyReportK3DetailHeaderList;
