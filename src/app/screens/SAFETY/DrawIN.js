import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    TouchableOpacity,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Icon,
    Item,
    Input,
    Thumbnail
} from "native-base";

import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewSIO from "../../components/ListViewSIO";
import Ripple from "react-native-material-ripple";

class DrawIN extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            dataSource: [],
            isLoading: true,
            listLotto:[],
            idLotto:'',
        };
    }

    static navigationOptions = {
        header: null
    };

    _renderItem = ({ item }) => (
        <ListItem data={item}></ListItem>
    )

    componentDidMount() {
        AsyncStorage.getItem('idLotto').then((idLotto) => {
            this.setState({ idLotto: idLotto});
            this.setState({ isLoading: true });
        })
        this.loadData();
        this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
            this.loadData();
        });
    }

    loadData(){
      AsyncStorage.getItem('token').then((value) => {
          const url = GlobalConfig.SERVERHOST + 'api/v_mobile/firesystem/lotto/view';
          var formData = new FormData();
          formData.append("token", value)
          formData.append("ID", this.state.idLotto)

          fetch(url, {
              headers: {
                  'Content-Type': 'multipart/form-data'
              },
              method: 'POST',
              body: formData
          })
              .then((response) => response.json())
              .then((responseJson) => {
                  //alert(JSON.stringify(responseJson));
                  this.setState({
                      dataSource: responseJson.data,
                      isLoading: false
                  });
              })
              .catch((error) => {
                  console.log(error)
              })
      })
    }

    navigateToScreen(route, listLotto, idUpdate) {
        AsyncStorage.setItem('list', JSON.stringify(listLotto)).then(() => {
            AsyncStorage.setItem('idUpdate', (idUpdate)).then(() => {
                this.props.navigation.navigate(route);
            })
        })
    }

    render() {
      var list;
      if (this.state.isLoading) {
          list = (
              <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                  <ActivityIndicator size="large" color="#330066" animating />
              </View>
          );
      } else {
        list = (
          <ScrollView>
              {this.state.dataSource.map((listLotto) => (
              <View>
                <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10, height:50}}>
                    <Ripple
                      style={{
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                      rippleSize={176}
                      rippleDuration={600}
                      rippleContainerBorderRadius={15}
                      onPress={() => this.navigateToScreen('InputDrawIN', listLotto, 'k3DrawIN')}
                      rippleColor={colors.accent}
                    >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:220 }}>
                            {listLotto.K3_DRAWIN_NAMA == null ? (
                            <Text style={{fontSize: 10, fontWeight: "bold"}}>Data Belum Ada Record</Text>):(
                            <View>
                            <Text style={{fontSize: 10, fontWeight: "bold"}}>{listLotto.K3_DRAWIN_NAMA}</Text>
                            {listLotto.K3_DRAWIN_KET=='V' ?(
                              <Text style={{fontSize: 10}}>Datang & Mengamankan</Text>
                            ):listLotto.K3_DRAWIN_KET=='Y' ?(
                              <Text style={{fontSize: 10}}>Datang & Tidak Mengamankan</Text>):(
                                <Text style={{fontSize: 10}}>Tidak Datang & Tidak Mengamankan</Text>
                              )
                            }
                            </View>)}
                        </View>
                        <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:80 }}>
                            <Text style={{fontSize: 10, fontWeight: "bold", textAlign:'right', color:colors.green01}}>K3</Text>
                        </View>
                    </View>
                    </Ripple>
                </Card>

                <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10, height:50}}>
                  <Ripple
                    style={{
                      flex: 2,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    rippleSize={176}
                    rippleDuration={600}
                    rippleContainerBorderRadius={15}
                    onPress={() => this.navigateToScreen('InputDrawIN', listLotto, 'pmlListrikDrawIN')}
                    rippleColor={colors.accent}
                  >
                  <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:220 }}>
                          {listLotto.PML_LISTRIK_DRAWIN_NAMA == null ? (
                          <Text style={{fontSize: 10, fontWeight: "bold"}}>Data Belum Ada Record</Text>):(
                          <View>
                          <Text style={{fontSize: 10, fontWeight: "bold"}}>{listLotto.PML_LISTRIK_DRAWIN_NAMA}</Text>
                          {listLotto.PML_LISTRIK_DRAWIN_KET=='V' ?(
                            <Text style={{fontSize: 10}}>Datang & Mengamankan</Text>
                          ):listLotto.PML_LISTRIK_DRAWIN_KET=='Y' ?(
                            <Text style={{fontSize: 10}}>Datang & Tidak Mengamankan</Text>):(
                              <Text style={{fontSize: 10}}>Tidak Datang & Tidak Mengamankan</Text>
                            )
                          }
                          </View>)}
                      </View>
                      <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:80 }}>
                          <Text style={{fontSize: 10, fontWeight: "bold", textAlign:'right', color:colors.green01}}>PML LISTRIK</Text>
                      </View>
                  </View>
                  </Ripple>
                  </Card>

                  <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10, height:50}}>
                  <Ripple
                    style={{
                      flex: 2,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    rippleSize={176}
                    rippleDuration={600}
                    rippleContainerBorderRadius={15}
                    onPress={() => this.navigateToScreen('InputDrawIN', listLotto, 'operatorDrawIN')}
                    rippleColor={colors.accent}
                  >
                  <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:220 }}>
                          {listLotto.OPERATOR_DRAWIN_NAMA == null ? (
                          <Text style={{fontSize: 10, fontWeight: "bold"}}>Data Belum Ada Record</Text>):(
                          <View>
                          <Text style={{fontSize: 10, fontWeight: "bold"}}>{listLotto.OPERATOR_DRAWIN_NAMA}</Text>
                          {listLotto.OPERATOR_DRAWIN_KET=='V' ?(
                            <Text style={{fontSize: 10}}>Datang & Mengamankan</Text>
                          ):listLotto.OPERATOR_DRAWIN_KET=='Y' ?(
                            <Text style={{fontSize: 10}}>Datang & Tidak Mengamankan</Text>):(
                              <Text style={{fontSize: 10}}>Tidak Datang & Tidak Mengamankan</Text>
                            )
                          }
                          </View>)}
                      </View>
                      <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:80 }}>
                          <Text style={{fontSize: 10, fontWeight: "bold", textAlign:'right', color:colors.green01}}>OPERATOR</Text>
                      </View>
                  </View>
                  </Ripple>
                  </Card>

                  <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10, height:50}}>
                  <Ripple
                    style={{
                      flex: 2,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    rippleSize={176}
                    rippleDuration={600}
                    rippleContainerBorderRadius={15}
                    onPress={() => this.navigateToScreen('InputDrawIN', listLotto, 'pmlTerkaitDrawIN')}
                    rippleColor={colors.accent}
                  >
                  <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:220 }}>
                          {listLotto.PML_TERKAIT_DRAWIN_NAMA == null ? (
                          <Text style={{fontSize: 10, fontWeight: "bold"}}>Data Belum Ada Record</Text>):(
                          <View>
                          <Text style={{fontSize: 10, fontWeight: "bold"}}>{listLotto.PML_TERKAIT_DRAWIN_NAMA}</Text>
                          {listLotto.PML_TERKAIT_DRAWIN_KET=='V' ?(
                            <Text style={{fontSize: 10}}>Datang & Mengamankan</Text>
                          ):listLotto.PML_TERKAIT_DRAWIN_KET=='Y' ?(
                            <Text style={{fontSize: 10}}>Datang & Tidak Mengamankan</Text>):(
                              <Text style={{fontSize: 10}}>Tidak Datang & Tidak Mengamankan</Text>
                            )
                          }
                          </View>)}
                      </View>
                      <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:80 }}>
                          <Text style={{fontSize: 10, fontWeight: "bold", textAlign:'right', color:colors.green01}}>PML TERKAIT</Text>
                      </View>
                  </View>
                  </Ripple>
                  </Card>
                </View>
              ))}
          </ScrollView>
          );
        }
        return (
            <Container style={styles.wrapper}>
                <Header style={styles.header}>
                    <Left style={{flex:1}}>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate("LottoDetail")}
                        >
                            <Icon
                                name="ios-arrow-back"
                                size={20}
                                style={styles.facebookButtonIconOrder2}
                            />
                        </Button>
                    </Left>
                    <Body style={{flex:3,alignItems:'center'}}>
                        <Title style={styles.textbody}>DRAW IN </Title>
                    </Body>
                    <Right style={{flex:1}}></Right>
                </Header>
                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                  <Footer style={styles.tabHeight}>
                    <FooterTab style={styles.tabfooter}>
                    <Button active style={styles.tabfooter}>
                        <View style={{height:'40%'}}></View>
                        <View style={{height:'50%'}}>
                            <Text style={{color:'white', fontWeight:'bold'}}>Draw IN</Text>
                        </View>
                        <View style={{height:'20%'}}></View>
                        <View style={{borderWidth:2, marginTop:2, height:0.5, width:'100%', borderColor:colors.white}}></View>
                    </Button>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("DrawOUT")
                        }>
                        <Text style={{color:'white', fontWeight:'bold'}}>Draw OUT</Text>
                    </Button>
                    </FooterTab>
                </Footer>

                <Content style={{ marginTop: 10 }}>
                  {list}
                </Content>
            </Container>

        );
    }
}

export default DrawIN;
