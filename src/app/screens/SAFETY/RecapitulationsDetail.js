import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Textarea,
  Icon,
  Picker,
  Form,
  Input
} from "native-base";

import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DatePicker from 'react-native-datepicker';

class ListItem extends React.PureComponent {

  konfirmasideleteInspeksi(ID, NO_REKAP) {
    Alert.alert(
      'Apakah Anda Yakin Ingin Menghapus', this.props.data.ID,
      [
        { text: 'Yes', onPress: () => this.deleteInspeksi(ID, NO_REKAP) },
        { text: 'Cancel' },
      ],
      { cancelable: false }
    )
  }

  deleteInspeksi(ID, NO_REKAP) {
    console.log(ID + NO_REKAP);
    // alert(route);
    AsyncStorage.getItem('token').then((value) => {
      this.props.setVisibleModal(true, 'Deleting Inspeksi ..')
      // alert(JSON.stringify(value));
      const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/unsafe/delete_inspeksi';
      var formData = new FormData();
      formData.append("token", value);
      formData.append("ID", ID);
      formData.append("NO_REKAP", NO_REKAP);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            this.props.setVisibleModal(false, '')
            Alert.alert('Success', 'Delete Success', [{
              text: 'Oke',
            }])
            this.props.navigation.navigate('Recapitulations');
          } else {
            this.props.setVisibleModal(false, '')
            Alert.alert('Error', 'Delete Failed', [{
              text: 'Okay'
            }])
          }
        })
        .catch((error) => {
          console.log(error)
          this.props.setVisibleModal(false, '')
          Alert.alert('Error', 'Delete Failed', [{
            text: 'Okay'
          }])
        })
    })
  }

  render() {
    return (
      <View>
        <Card style={{ marginLeft: 10, marginRight: 10 }}>
          <CardItem
          >
            <View style={{ flexDirection: "column", flex: 1 }}>
              <View style={{ flexDirection: "row", flex: 1 }}>

                <View style={{ flexDirection: "column", flex: 1 }}>
                  <Text style={{ fontSize: 12, }}>
                    Nomor
                      </Text>
                  <Text style={{ fontSize: 12, }}>
                    Tanggal
                      </Text>
                  <Text style={{ fontSize: 12, }}>
                    Catatan
                      </Text>
                </View>

                <View style={{ flexDirection: "column", flex: 3 }}>
                  <Text style={{ fontSize: 12, }}>
                    : {this.props.index + 1}
                  </Text>
                  <Text style={{ fontSize: 12, }}>
                    : {this.props.data.CHECKLIST_DATE}
                  </Text>
                  <Text style={{ fontSize: 12, }}>
                    : {this.props.data.NOTE}
                  </Text>
                </View>


                <View style={{ flex: 1 }}>
                  <Button
                    style={{ alignSelf: "flex-end", flexWrap: 'wrap', backgroundColor: colors.red }}
                    onPress={() => this.konfirmasideleteInspeksi(this.props.data.ID, this.props.data.NO_REKAP)}
                  >
                    <Icon
                      name="ios-trash"
                      size={20}
                      style={{
                        color: colors.white,
                        fontSize: 20,
                        paddingTop: 5,
                        height: 30,
                      }}
                    />
                  </Button>
                </View>
              </View>
            </View>
          </CardItem>
        </Card>
      </View>
    );
  }
}

export default class RecapitulationsDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      isLoading: true,
      itemRekapitulasi: [],
      inspeksi_list: [],
      visibleDialogSubmit: false,
      visibleDialogSubmitText: ''
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    AsyncStorage.getItem('list').then((listRekapitulasi) => {
      this.setState({ itemRekapitulasi: JSON.parse(listRekapitulasi) });
      this.setState({ isLoading: false });
      this.setState({ inspeksi_list: this.state.itemRekapitulasi.INSPEKSI_LIST })
      console.log(this.state.itemRekapitulasi);
      console.log(this.state.inspeksi_list.length)
    })
    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {

    });
  }

  _renderItem = ({ item, index }) => <ListItem data={item} index={index} navigation={this.props.navigation} setVisibleModal={(status, text) => this.setVisibleModal(status, text)} />;

  setVisibleModal(status, text) {
    this.setState({
      visibleDialogSubmit: status,
      visibleDialogSubmitText: text
    })
  }

  navigateToScreen(route, itemRekapitulasi) {
    AsyncStorage.setItem('list', JSON.stringify(itemRekapitulasi)).then(() => {
      this.props.navigation.navigate(route);
    })
  }

  konfirmasideleteRekapitulasi() {
    Alert.alert(
      'Apakah Anda Yakin Ingin Menghapus', this.state.itemRekapitulasi.NO_REKAP,
      [
        { text: 'Yes', onPress: () => this.deleteRekapitulasi() },
        { text: 'Cancel' },
      ],
      { cancelable: false }
    )
  }

  deleteRekapitulasi() {
    this.setState({
      visibleDialogSubmit: true,
      visibleDialogSubmitText: 'Deleting Recapitulation ..'
    })
    AsyncStorage.getItem('token').then((value) => {
      // alert(JSON.stringify(value));
      const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/unsafe/delete_rekap';
      var formData = new FormData();
      formData.append("token", value);
      formData.append("ID", this.state.itemRekapitulasi.ID);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            this.setState({
              visibleDialogSubmit: false
            })
            Alert.alert('Success', 'Delete Success', [{
              text: 'Oke',
            }])
            this.props.navigation.navigate("Recapitulations");
          } else {
            this.setState({
              visibleDialogSubmit: false
            })
            Alert.alert('Error', 'Delete Failed', [{
              text: 'Okay'
            }])
          }
        })
        .catch((error) => {
          console.log(error)
          this.setState({
            visibleDialogSubmit: false
          })
          Alert.alert('Error', 'Delete Failed', [{
            text: 'Okay'
          }])
        })
    })
  }

  render() {
    var status = ""
    if (this.state.itemRekapitulasi.status != undefined) {
      status = this.state.itemRekapitulasi.status
    }
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("Recapitulations")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 1, alignItems: 'center' }}>
            <Title style={styles.textbody}>Activity Detail</Title>
          </Body>
          <Right style={{ flex: 1 }}></Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <Content style={{ marginTop: 0, marginBottom: 20, backgroundColor: colors.gray01 }}>
          <View style={{}}>
            <CardItem style={{ borderRadius: 10, marginTop: 15, marginHorizontal: 5, marginBottom: 5, borderWidth: 2, borderColor: colors.graydar }}>
              <View style={{ flex: 1, paddingVertical: 10 }}>
                <Text style={{ fontSize: 12, paddingBottom: 5, fontWeight: 'bold' }}>Foto Temuan :</Text>
                <View style={styles.placeholder}>
                  {this.state.itemRekapitulasi.foto_bef == null ? (
                    <Image style={styles.previewImage} source={require("../../../assets/images/nopic.png")} />
                  ) : (
                      <Image style={styles.previewImage} source={{ uri: GlobalConfig.LOCALHOST + "public/uploads/unsafe/" + this.state.itemRekapitulasi.foto_bef }} />
                    )}
                </View>
              </View>
            </CardItem>
            {(this.state.itemRekapitulasi.status == "CLOSE") && (
              <CardItem style={{ borderRadius: 10, marginTop: 15, marginHorizontal: 5, marginBottom: 5, borderWidth: 2, borderColor: colors.graydar }}>
                <View style={{ flex: 1, paddingVertical: 10 }}>
                  <Text style={{ fontSize: 12, paddingBottom: 5, fontWeight: 'bold' }}>Foto Follow Up :</Text>
                  <View style={styles.placeholder}>
                    {this.state.itemRekapitulasi.foto_aft == null ? (
                      <Image style={styles.previewImage} source={require("../../../assets/images/nopic.png")} />
                    ) : (
                        <Image style={styles.previewImage} source={{ uri: GlobalConfig.LOCALHOST + "public/uploads/unsafe/" + this.state.itemRekapitulasi.foto_aft }} />
                      )}
                  </View>
                </View>
              </CardItem>
            )}
            <CardItem style={{ borderRadius: 10, marginTop: 10, marginHorizontal: 5, marginBottom: 5, borderWidth: 2, borderColor: colors.red, backgroundColor: colors.white }}>
              <View style={{ flex: 1, flexDirection: "row", paddingVertical: 10 }}>
                <View style={{ width: '100%' }}>
                  <View style={{ paddingVertical: 10, alignItems: 'center', alignSelf: 'center', justifyContent: 'center' }}>
                    <Image style={{ width: 80, height: 60 }} source={require("../../../assets/images/Logo-Semen-Indonesia.png")}></Image>
                  </View>
                  <View style={{ flex: 1, flexDirection: 'row', marginBottom: 20 }}>
                    <View style={{ flex: 1 }}>
                      <View style={{ borderWidth: 1, height: 40, width: 90, borderColor: colors.graydark, borderRadius: 10, alignSelf: 'center', alignItems: 'center', justifyContent: "center", }}>
                        <Icon
                          name="ios-calendar"
                          style={{ fontSize: 13, alignSelf: 'center', alignItems: 'center', paddingLeft: 10 }}
                        />
                        <Text style={{ fontSize: 12, }}>{this.state.itemRekapitulasi.comitment_date}</Text>
                      </View>
                    </View>
                    <View style={{ flex: 1 }}>
                      <View style={{ borderWidth: 1, height: 40, width: 70, borderColor: colors.graydark, borderRadius: 10, alignSelf: 'center', alignItems: 'center', justifyContent: "center", }}>
                        <Icon
                          name="ios-clock"
                          style={{ fontSize: 13, alignSelf: 'center', alignItems: 'center', paddingLeft: 10 }}
                        />
                        <Text style={{ fontSize: 12, }}>{this.state.itemRekapitulasi.o_clock_incident}</Text>
                      </View>
                    </View>
                    <View style={{ flex: 1 }}>
                      <View style={{ borderWidth: 1, height: 40, width: 70, borderColor: colors.graydark, borderRadius: 10, alignSelf: 'center', alignItems: 'center', justifyContent: "center", }}>
                        <Icon
                          name="ios-unlock"
                          style={{ fontSize: 13, alignSelf: 'center', alignItems: 'center', paddingLeft: 10 }}
                        />
                        <Text style={{ fontSize: 12, fontWeight: 'bold', color: colors.green0 }}>{this.state.itemRekapitulasi.status}</Text>
                      </View>
                    </View>
                  </View>
                  <View style={{ flex: 1, flexDirection: 'row', marginBottom: 20 }}>
                    <View style={{ flex: 1, marginLeft: 10 }}>
                      <Text style={{ fontSize: 12, fontWeight: "bold", }}>is SMIG</Text>
                      {this.state.itemRekapitulasi.is_smig == 1 ? (
                        <Text style={{ fontSize: 12, fontFamily: "Montserrat-Regular" }}>SMIG</Text>
                      ) : (
                          <Text style={{ fontSize: 12, fontFamily: "Montserrat-Regular" }}>Non-SMIG</Text>
                        )}
                    </View>
                    <View style={{ flex: 1, marginRight: 10 }}>
                      <Text style={{ fontSize: 12, fontWeight: "bold", }}>Unit Kerja Officer</Text>
                      <Text style={{ fontSize: 12, fontFamily: "Montserrat-Regular" }}>{this.state.itemRekapitulasi.unit_code}-{this.state.itemRekapitulasi.unit_name}</Text>
                    </View>
                  </View>
                  <View style={{ flex: 1, flexDirection: 'row', marginBottom: 20 }}>
                    <View style={{ flex: 1, marginLeft: 10 }}>
                      <Text style={{ fontSize: 12, fontWeight: "bold", }}>Unsafe Type</Text>
                      <Text style={{ fontSize: 12, fontFamily: "Montserrat-Regular" }}>{this.state.itemRekapitulasi.unsafe_type}</Text>
                    </View>
                    <View style={{ flex: 1, marginRight: 10 }}>
                      <Text style={{ fontSize: 12, fontWeight: "bold", }}>Report Category</Text>
                      <Text style={{ fontSize: 12, fontFamily: "Montserrat-Regular" }}>{this.state.itemRekapitulasi.deskripsi}</Text>
                    </View>
                  </View>
                  <View style={{ flex: 1, flexDirection: 'row', marginBottom: 20 }}>
                    <View style={{ flex: 1, marginLeft: 10, marginRight: 10 }}>
                      <Text style={{ fontSize: 12, fontWeight: "bold", }}>Uraian aktivitas :</Text>
                      <Text style={{ fontSize: 12, fontFamily: "Montserrat-Regular" }}>{this.state.itemRekapitulasi.activity_description}</Text>
                    </View>
                  </View>
                  <View style={{ flex: 1, flexDirection: 'row', marginBottom: 20 }}>
                    <View style={{ flex: 1, marginLeft: 10, marginRight: 10 }}>
                      <Text style={{ fontSize: 12, fontWeight: "bold", }}>Potensi terjadinya masalah :</Text>
                      <Text style={{ fontSize: 12, fontFamily: "Montserrat-Regular" }}>{this.state.itemRekapitulasi.potential_hazard}</Text>
                    </View>
                  </View>
                  <View style={{ flex: 1, flexDirection: 'row', marginBottom: 20 }}>
                    <View style={{ flex: 1, marginLeft: 10, marginRight: 10 }}>
                      <Text style={{ fontSize: 12, fontWeight: "bold", }}>Penyebab masalah :</Text>
                      <Text style={{ fontSize: 12, fontFamily: "Montserrat-Regular" }}>{this.state.itemRekapitulasi.trouble_maker}</Text>
                    </View>
                  </View>
                  <View style={{ flex: 1, flexDirection: 'row', marginBottom: 20 }}>
                    <View style={{ flex: 1, marginLeft: 10 }}>
                      <Text style={{ fontSize: 12, fontWeight: "bold", }}>Rekomendasi safety :</Text>
                      <Text style={{ fontSize: 12, fontFamily: "Montserrat-Regular" }}>{this.state.itemRekapitulasi.recomendation}</Text>
                    </View>
                    <View style={{ flex: 1, marginRight: 10 }}>
                      <Text style={{ fontSize: 12, fontWeight: "bold", }}>Tindak lanjut :</Text>
                      <Text style={{ fontSize: 12, fontFamily: "Montserrat-Regular" }}>{this.state.itemRekapitulasi.tindak_lanjut}</Text>
                    </View>
                  </View>
                </View>
              </View>
            </CardItem>

          </View>
          <View style={{ width: 270, position: "absolute" }}>
            <Dialog
              visible={this.state.visibleDialogSubmit}
              dialogTitle={<DialogTitle title="Deleting Detail Header Report K3 .." />}
            >
              <DialogContent>
                {<ActivityIndicator size="large" color="#330066" animating />}
              </DialogContent>
            </Dialog>
          </View>
        </Content>
      </Container>
    );
  }
}
