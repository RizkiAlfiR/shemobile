import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  TouchableOpacity,
  FlatList,
  Dimensions,
  Card,
} from "react-native";
import {
  Container,
  Header,
  Title,
  CardItem,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon
} from "native-base";

import NewsContentsReport from "../../components/NewsContentReport";
import NewsContentsReportK3 from "../../components/NewsContentReportK3";
import NewsContentsRekapitulasi from "../../components/NewsContentRekapitulasi";
import NewsContentsLotto from "../../components/NewsContentLotto";
import NewsContentsAccidentReport from "../../components/NewsContentAccidentReport";
import LinearGradient from "react-native-linear-gradient";
import Ripple from "react-native-material-ripple";
import SubMenuSafety from "../../components/SubMenuSafety";
import styles from "../styles/SafetyMenu";
import GlobalConfig from "../../components/GlobalConfig";
import NewsContentsTools from "../../components/NewsContentTools";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";

var that;
class ListItemNewsDailyReport extends React.PureComponent {
  navigateToScreen(route, id_Unsafe, shift) {
    var idAndShift = id_Unsafe + "+" + shift;
    console.log(idAndShift);
    AsyncStorage.setItem("noDokumen", this.props.data.no_dokumen).then(() => {
      AsyncStorage.setItem("idAndShift", idAndShift).then(() => {
        that.props.navigation.navigate(route);
      });
    });
  }

  render() {
    return (
      <Ripple
        style={{
          flex: 2,
          justifyContent: "center",
          alignItems: "center"
        }}
        rippleSize={176}
        rippleDuration={600}
        rippleContainerBorderRadius={15}
        onPress={() =>
          this.navigateToScreen(
            "DailyReportDetailList",
            this.props.data.id,
            this.props.data.shift
          )
        }
        rippleColor={colors.accent}
      >
        <View style={{ marginTop: 0, marginRight: 10, borderRadius: 5, backgroundColor: colors.white }}>
          <NewsContentsReport
            imageUri={require("../../../assets/images/imgorder.png")}
            id={this.props.data.id}
            shift={this.props.data.shift}
            date={this.props.data.tanggal}
            inspector={this.props.data.no_dokumen}
            navigation={this.props.navigation}
          />
        </View>
      </Ripple>
    );
  }
}

class ListItemNewsAccident extends React.PureComponent {
  navigateToScreen(route, id_Accident) {
    // alert(id_Unsafe)
    AsyncStorage.setItem("list", JSON.stringify(id_Accident)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <Ripple
        rippleSize={176}
        rippleDuration={600}
        rippleContainerBorderRadius={15}
        onPress={() =>
          this.navigateToScreen("AccidentReportDetail", this.props.data)
        }
        rippleColor={colors.accent}
      >
        <View style={{ marginTop: 0, marginRight: 10, borderRadius: 5, backgroundColor: colors.white }}>
          <NewsContentsAccidentReport
            imageUri={{
              uri: GlobalConfig.LOCALHOST + "public/uploads/accident/" + this.props.data.pict_before
            }}
            name={this.props.data.name_vict}
            badge={this.props.data.badge_vict}
            navigation={this.props.navigation}
          />
        </View>
      </Ripple>
    );
  }
}

class ListItemNewsTools extends React.PureComponent {
  navigateToScreen(route, listToolsCertification) {
    AsyncStorage.setItem(
      "listAlat",
      JSON.stringify(listToolsCertification)
    ).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <Ripple
        style={{
          flex: 2,
          justifyContent: "center",
          alignItems: "center"
        }}
        rippleSize={176}
        rippleDuration={600}
        rippleContainerBorderRadius={15}
        onPress={() =>
          this.navigateToScreen("ToolsCertificationDetail", this.props.data)
        }
        rippleColor={colors.accent}
      >
        <View style={{ marginTop: 0, marginRight: 10, borderRadius: 5, backgroundColor: colors.white }}>
          <NewsContentsTools
            imageUri={{
              uri: GlobalConfig.LOCALHOST + "public/uploads/tools/" + this.props.data.file_buku
            }}
            noBuku={this.props.data.no_buku}
            equipname={this.props.data.equipment}
            endDate={this.props.data.uji_ulang}
          />
        </View>
      </Ripple>
    );
  }
}

class ListItemNewsReportK3 extends React.PureComponent {
  navigateToScreen(route, listHeaderDailyReport) {
    AsyncStorage.setItem("list", JSON.stringify(listHeaderDailyReport)).then(
      () => {
        that.props.navigation.navigate(route);
      }
    );
  }

  render() {
    return (
      <Ripple
        rippleSize={176}
        rippleDuration={600}
        rippleContainerBorderRadius={15}
        onPress={() =>
          this.navigateToScreen("DailyReportK3Detail", this.props.data)
        }
        rippleColor={colors.accent}
      >
        <View style={{ marginTop: 0, marginRight: 10, borderRadius: 5, backgroundColor: colors.white }}>
          <NewsContentsReportK3
            imageUri={require("../../../assets/images/imgorder.png")}
            id={this.props.data}
            type={this.props.data.sub_area_txt}
            inspector={this.props.data.inspector_name}
          />
        </View>
      </Ripple>
    );
  }
}

class ListItemNewsLotto extends React.PureComponent {
  navigateToScreen(route, listLotto) {
    AsyncStorage.setItem("list", JSON.stringify(listLotto)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <Ripple
        rippleSize={176}
        rippleDuration={600}
        rippleContainerBorderRadius={15}
        onPress={() => this.navigateToScreen("LottoDetail", this.props.data)}
        rippleColor={colors.accent}
      >
        <View
          style={{
            marginTop: 0,
            marginLeft: 5,
            marginRight: 5
          }}
        >
          <NewsContentsLotto
            number={this.props.data.EQUIPMENT_NUMBER}
            kegiatan={this.props.data.KEGIATAN}
            date={this.props.data.DRAWOUT_DATE}
            time={this.props.data.DRAWOUT_TIME}
            kegiatan={this.props.data.KEGIATAN}
            note={this.props.data.NOTE}
            create={this.props.data.CREATED_BY}
            navigation={this.props.navigation}
          />
        </View>
      </Ripple>
    );
  }
}

class ListItemNewsRekapitulasi extends React.PureComponent {
  navigateToScreen(route, listRekapitulasi) {
    AsyncStorage.setItem("list", JSON.stringify(listRekapitulasi)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <Ripple
        rippleSize={176}
        rippleDuration={600}
        rippleContainerBorderRadius={15}
        onPress={() =>
          this.navigateToScreen("RecapitulationsDetail", this.props.data)
        }
        rippleColor={colors.accent}
      >
        <View
          style={{
            marginTop: 0, marginRight: 10, borderRadius: 5, backgroundColor: colors.white
          }}
        >
          <NewsContentsRekapitulasi
            imageUri={{
              uri: GlobalConfig.LOCALHOST + "public/uploads/unsafe/" + this.props.data.foto_aft
            }}
            type={this.props.data.unsafe_type}
            dateCommit={this.props.data.comitment_date}
            status={this.props.data.status}
          />
        </View>
      </Ripple>
    );
  }
}

class SafetyMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: true,
      isLoadingNews: false,
      dataSource: [],
      dataNewsDailyReport: [],
      dataNewsAccident: [],
      dataNewsReportK3: [],
      dataNewsReport: [],
      dataNewsLotto: [],
      dataNewsRekapitulasi: [],
      dataNewsTools: [],
      listFileUpload: [],
      dataRole: []
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    AsyncStorage.getItem("token")
      .then(value => {
        this.setState({ token: value });
      })
      .then(res => {
        // this.loadRole();
        this.loadNewsDailyReport();
        this.loadNewsAccident();
        this.loadNewsRekapitulasi();
        this.loadNewsDailyReportK3();
        // this.loadNewsLotto();
        this.loadNewsTools();
      });
  }

  // loadRole() {
  //   AsyncStorage.getItem("validasiUser").then(value => {
  //     this.setState({
  //       dataRole: JSON.parse(value).role,
  //       name: JSON.parse(value).data.name,
  //       isloading: false
  //     });
  //   });
  // }

  loadNewsDailyReport() {
    this.setState({
      isLoadingNews: true
    })
    AsyncStorage.getItem('token').then((value) => {
      const url = GlobalConfig.LOCALHOST + 'api/unsafedetail?token=' + value;

      fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
          // alert(JSON.stringify(responseJson.data));
          if (responseJson.status == 200) {
            this.setState({
              dataNewsDailyReport: responseJson.data,
              isLoadingNews: false
            });
          }
        })
        .catch((error) => {
          console.log(error)
          this.setState({
            isLoadingNews: false
          });
        })
    })
  }

  loadNewsAccident() {
    this.setState({
      isLoadingNews: true
    })
    AsyncStorage.getItem("token").then(value => {
      const url = GlobalConfig.LOCALHOST + "api/accidentreports?token=" + value
      // alert(url)
      fetch(url)
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.status == "200") {
            // alert(JSON.stringify(responseJson.data))
            this.setState({
              dataNewsAccident: responseJson.data,
              isloadingNews: false
            });
          }
          // alert(JSON.stringify(responseJson));
        })
        // console.log(this.state.isLoadingNews)
        .catch(error => {
          console.log(error);
        });
    });
  }

  loadNewsTools() {
    // load category
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.LOCALHOST + "api/tools/search?token=" + value;
      var formData = new FormData();
      formData.append("search", '');

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        // fetch(url)
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.status == "200") {
            this.setState({
              dataNewsTools: responseJson.data
              // isloadingNews: false
            });
          }
          // alert(JSON.stringify(responseJson));
        })
        // console.log(this.state.isLoadingNews)
        .catch(error => {
          console.log(error);
        });
    });
  }

  loadNewsRekapitulasi() {
    // load category
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.LOCALHOST + "api/recapitulation/search?token=" + value;
      var formData = new FormData();
      formData.append("search", '');

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.status == 200) {
            this.setState({
              dataNewsRekapitulasi: responseJson.data
              // isloadingNews: false
            });
          }
          // alert(JSON.stringify(responseJson));
        })
        // console.log(this.state.isLoadingNews)
        .catch(error => {
          console.log(error);
        });
    });
  }

  loadNewsDailyReportK3() {
    // load category
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.LOCALHOST + "api/unsafek3header/?token=" + value;
      // var formData = new FormData();
      // formData.append("token", this.state.token);
      // formData.append("search", "");
      // formData.append("length", 5);
      // formData.append("start", 0);
      // // formData.append("id_unsafe_report", '');

      // fetch(url, {
      //   headers: {
      //     "Content-Type": "multipart/form-data"
      //   },
      //   method: "POST",
      //   body: formData
      // })
      fetch(url)
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            dataNewsReportK3: responseJson.data
            // isloadingNews: false
          });

          // alert(JSON.stringify(responseJson));
        })
        // console.log(this.state.isLoadingNews)
        .catch(error => {
          console.log(error);
        });
    })
  }

  // loadNewsLotto() {
  //   // load category
  //   const url = GlobalConfig.SERVERHOST + "api/v_mobile/firesystem/lotto/view";
  //   var formData = new FormData();
  //   formData.append("token", this.state.token);
  //   formData.append("limit", 4);

  //   fetch(url, {
  //     headers: {
  //       "Content-Type": "multipart/form-data"
  //     },
  //     method: "POST",
  //     body: formData
  //   })
  //     .then(response => response.json())
  //     .then(responseJson => {
  //       if (responseJson.status == 200) {
  //         this.setState({
  //           dataNewsLotto: responseJson.data
  //           // isloadingNews: false
  //         });
  //       }
  //       // alert(responseJson)
  //     })
  //     // console.log(this.state.isLoadingNews)
  //     .catch(error => {
  //       console.log(error);
  //     });
  // }

  _renderItemNewsDailyReport = ({ item }) => (
    <ListItemNewsDailyReport data={item} />
  );

  _renderItemNewsAccident = ({ item }) => <ListItemNewsAccident data={item} />;

  _renderItemNewsTools = ({ item }) => <ListItemNewsTools data={item} />;

  _renderItemNewsReportK3 = ({ item }) => <ListItemNewsReportK3 data={item} />;

  // _renderItemNewsLotto = ({ item }) => <ListItemNewsLotto data={item} />;

  _renderItemNewsRekapitulasi = ({ item }) => (
    <ListItemNewsRekapitulasi data={item} />
  );

  render() {
    that = this;
    return (
      <Container style={styles.wrapper}>
        <View style={{ backgroundColor: colors.primer, height: 130, paddingBottom: 20 }}>
          <View style={{ flex: 1, flexDirection: 'row', marginLeft: 20, marginRight: 20, marginTop: 30 }}>
            <View style={{ width: '10%' }}>
              <View style={{ backgroundColor: colors.second, borderRadius: 5, height: 30, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity
                  transparent
                  onPress={() => this.props.navigation.navigate("HomeMenu")}>
                  <Icon
                    name='ios-home'
                    style={{ fontSize: 20, color: colors.white }}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ width: '90%', paddingRight: 30, alignContent: 'center', alignItems: 'center' }}>
              <Text style={{ fontSize: 18, fontWeight: 'bold', color: colors.white }}>MENU SAFETY</Text>
            </View>
          </View>
        </View>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{
          marginLeft: 20, marginRight: 20, marginBottom: 20, marginTop: -50, backgroundColor: colors.white, borderRadius: 5, height: 200,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 1,
          shadowRadius: 2,
          elevation: 3,
        }}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ width: '33%', paddingTop: 25 }}>
              <SubMenuSafety
                handleOnPress={() =>
                  this.props.navigation.navigate("DailyReport")
                }
                imageUri={require("../../../assets/images/iconDaily.png")}
                name="Daily Report"
              />
            </View>
            <View style={{ width: '33%', paddingTop: 25 }}>
              <SubMenuSafety
                handleOnPress={() =>
                  this.props.navigation.navigate("AccidentReport")
                }
                imageUri={require("../../../assets/images/iconAccident.png")}
                name="Accident Report"
              />
            </View>
            <View style={{ width: '33%', paddingTop: 25 }}>
              <SubMenuSafety
                handleOnPress={() =>
                  this.props.navigation.navigate("Recapitulations")
                }
                imageUri={require("../../../assets/images/iconRekapitulasi.png")}
                name="Recapitulations"
              />
            </View>
          </View>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ width: '50%', paddingTop: 10 }}>
              <SubMenuSafety
                handleOnPress={() =>
                  this.props.navigation.navigate("ToolsCertification")
                }
                imageUri={require("../../../assets/images/iconSIO.png")}
                name="SIO Sertification"
              />
            </View>
            <View style={{ width: '50%', paddingTop: 10 }}>
              <SubMenuSafety
                handleOnPress={() =>
                  this.props.navigation.navigate("DailyReportK3")
                }
                imageUri={require("../../../assets/images/iconDailyK3.png")}
                name="Daily Report K3"
              />
            </View>
          </View>
        </View>
        <ScrollView>
          <View style={{ flex: 1, marginBottom: 45 }}>
            <View style={{ flex: 1, flexDirection: 'row', paddingTop: 10 }}>
              <View style={{ width: '50%', paddingLeft: 20 }}>
                <Text style={{ fontSize: 12, fontWeight: 'bold' }}>Daily Report</Text>
              </View>
              <View style={{ width: '50%', paddingRight: 20 }}>
                <TouchableOpacity
                  transparent
                  onPress={() => this.props.navigation.navigate("DailyReport")}
                >
                  <Text style={{ fontSize: 12, textAlign: 'right' }}>View All</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ marginLeft: 20, marginRight: 20, marginTop: 5 }}>
              <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                <FlatList
                  horizontal={true}
                  data={this.state.dataNewsDailyReport}
                  renderItem={this._renderItemNewsDailyReport}
                  keyExtractor={(item, index) => index.toString()}
                />
              </ScrollView>
            </View>

            <View style={{ flex: 1, flexDirection: 'row', paddingTop: 15 }}>
              <View style={{ width: '50%', paddingLeft: 20 }}>
                <Text style={{ fontSize: 12, fontWeight: 'bold' }}>Accident Report</Text>
              </View>
              <View style={{ width: '50%', paddingRight: 20 }}>
                <TouchableOpacity
                  transparent
                  onPress={() => this.props.navigation.navigate("AccidentReport")}
                >
                  <Text style={{ fontSize: 12, textAlign: 'right' }}>View All</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ marginLeft: 20, marginRight: 20, marginTop: 5 }}>
              <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                <FlatList
                  horizontal={true}
                  data={this.state.dataNewsAccident}
                  renderItem={this._renderItemNewsAccident}
                  keyExtractor={(item, index) => index.toString()}
                />
              </ScrollView>
            </View>

            <View style={{ flex: 1, flexDirection: 'row', paddingTop: 15 }}>
              <View style={{ width: '50%', paddingLeft: 20 }}>
                <Text style={{ fontSize: 12, fontWeight: 'bold' }}>Tools & SIO</Text>
              </View>
              <View style={{ width: '50%', paddingRight: 20 }}>
                <TouchableOpacity
                  transparent
                  onPress={() => this.props.navigation.navigate("ToolsCertification")}
                >
                  <Text style={{ fontSize: 12, textAlign: 'right' }}>View All</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ marginLeft: 20, marginRight: 20, marginTop: 5 }}>
              <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                <FlatList
                  horizontal={true}
                  data={this.state.dataNewsTools}
                  renderItem={this._renderItemNewsTools}
                  keyExtractor={(item, index) => index.toString()}
                />
              </ScrollView>
            </View>

            <View style={{ flex: 1, flexDirection: 'row', paddingTop: 15 }}>
              <View style={{ width: '50%', paddingLeft: 20 }}>
                <Text style={{ fontSize: 12, fontWeight: 'bold' }}>Daily Report K3</Text>
              </View>
              <View style={{ width: '50%', paddingRight: 20 }}>
                <TouchableOpacity
                  transparent
                  onPress={() => this.props.navigation.navigate("DailyReportK3")}
                >
                  <Text style={{ fontSize: 12, textAlign: 'right' }}>View All</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ marginLeft: 20, marginRight: 20, marginTop: 5 }}>
              <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                <FlatList
                  horizontal={true}
                  data={this.state.dataNewsReportK3}
                  renderItem={this._renderItemNewsReportK3}
                  keyExtractor={(item, index) => index.toString()}
                />
              </ScrollView>
            </View>

            <View style={{ flex: 1, flexDirection: 'row', paddingTop: 15 }}>
              <View style={{ width: '50%', paddingLeft: 20 }}>
                <Text style={{ fontSize: 12, fontWeight: 'bold' }}>Recapitulation</Text>
              </View>
              <View style={{ width: '50%', paddingRight: 20 }}>
                <TouchableOpacity
                  transparent
                  onPress={() => this.props.navigation.navigate("Recapitulations")}
                >
                  <Text style={{ fontSize: 12, textAlign: 'right' }}>View All</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ marginLeft: 20, marginRight: 20, marginTop: 5 }}>
              <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                <FlatList
                  horizontal={true}
                  data={this.state.dataNewsRekapitulasi}
                  renderItem={this._renderItemNewsRekapitulasi}
                  keyExtractor={(item, index) => index.toString()}
                />
              </ScrollView>
            </View>
          </View>
        </ScrollView>
        {/* <Content style={{ marginTop: 0, marginBottom: 20, backgroundColor: colors.gray01 }}>
          <ScrollView>

            <CardItem style={{ borderRadius: 10, marginTop: 10, marginHorizontal: 5, marginBottom: 5, borderWidth: 2, borderColor: colors.graydar }}>
              <View style={styles.newsWrapper}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, marginLeft: 5, width: 260 }}>
                    <Text style={{ fontSize: 12, fontWeight: "bold" }}>
                      Daily Report
                    </Text>
                  </View>
                  <View style={{ flex: 0 }}>
                    <Text
                      style={{
                        fontSize: 11,
                        fontWeight: "bold",
                        color: colors.green01
                      }}
                      onPress={() =>
                        this.props.navigation.navigate("DailyReport")
                      }
                    >
                      Lihat Semua
                    </Text>
                  </View>
                </View>
                <ScrollView
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                >
                  <FlatList
                    horizontal={true}
                    data={this.state.dataNewsDailyReport}
                    renderItem={this._renderItemNewsDailyReport}
                    keyExtractor={(item, index) => index.toString()}
                  />
                </ScrollView>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 10, marginHorizontal: 5, marginBottom: 5, borderWidth: 2, borderColor: colors.graydar }}>
              <View style={styles.newsWrapper}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, marginLeft: 5, width: 260 }}>
                    <Text style={{ fontSize: 12, fontWeight: "bold" }}>
                      Accident Report
                    </Text>
                  </View>
                  <View style={{ flex: 0 }}>
                    <Text
                      style={{
                        fontSize: 11,
                        fontWeight: "bold",
                        color: colors.green01
                      }}
                      onPress={() =>
                        this.props.navigation.navigate("AccidentReport")
                      }
                    >
                      Lihat Semua
                    </Text>
                  </View>
                </View>
                <ScrollView
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                >
                  <FlatList
                    horizontal={true}
                    data={this.state.dataNewsAccident}
                    renderItem={this._renderItemNewsAccident}
                    keyExtractor={(item, index) => index.toString()}
                  />
                </ScrollView>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 10, marginHorizontal: 5, marginBottom: 5, borderWidth: 2, borderColor: colors.graydar }}>
              <View style={styles.newsWrapper}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, marginLeft: 5, width: 260 }}>
                    <Text style={{ fontSize: 12, fontWeight: "bold" }}>
                      Tools & SIO
                    </Text>
                  </View>
                  <View style={{ flex: 0 }}>
                    <Text
                      style={{
                        fontSize: 11,
                        fontWeight: "bold",
                        color: colors.green01
                      }}
                      onPress={() =>
                        this.props.navigation.navigate("ToolsCertification")
                      }
                    >
                      Lihat Semua
                    </Text>
                  </View>
                </View>
                <ScrollView
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                >
                  <FlatList
                    horizontal={true}
                    data={this.state.dataNewsTools}
                    renderItem={this._renderItemNewsTools}
                    keyExtractor={(item, index) => index.toString()}
                  />
                </ScrollView>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 10, marginHorizontal: 5, marginBottom: 5, borderWidth: 2, borderColor: colors.graydar }}>
              <View style={styles.newsWrapper}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, marginLeft: 5, width: 260 }}>
                    <Text style={{ fontSize: 12, fontWeight: "bold" }}>
                      Daily Report K3
                    </Text>
                  </View>
                  <View style={{ flex: 0 }}>
                    <Text
                      style={{
                        fontSize: 11,
                        fontWeight: "bold",
                        color: colors.green01
                      }}
                      onPress={() =>
                        this.props.navigation.navigate("DailyReportK3")
                      }
                    >
                      Lihat Semua
                    </Text>
                  </View>
                </View>
                <ScrollView
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                >
                  <FlatList
                    horizontal={true}
                    data={this.state.dataNewsReportK3}
                    renderItem={this._renderItemNewsReportK3}
                    keyExtractor={(item, index) => index.toString()}
                  />
                </ScrollView>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 10, marginHorizontal: 5, marginBottom: 15, borderWidth: 2, borderColor: colors.graydar }}>
              <View style={styles.newsWrapper}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, marginLeft: 5, width: 260 }}>
                    <Text style={{ fontSize: 12, fontWeight: "bold" }}>
                      Recapitulations
                    </Text>
                  </View>
                  <View style={{ flex: 0 }}>
                    <Text
                      style={{
                        fontSize: 11,
                        fontWeight: "bold",
                        color: colors.green01
                      }}
                      onPress={() =>
                        this.props.navigation.navigate("Recapitulations")
                      }
                    >
                      Lihat Semua
                    </Text>
                  </View>
                </View>
                <ScrollView
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                >
                  <FlatList
                    horizontal={true}
                    data={this.state.dataNewsRekapitulasi}
                    renderItem={this._renderItemNewsRekapitulasi}
                    keyExtractor={(item, index) => index.toString()}
                  />
                </ScrollView>
              </View>
            </CardItem>
          </ScrollView>
        </Content> */}
        <CustomFooter navigation={this.props.navigation} menu="Safety" />
      </Container>
    );
  }
}

export default SafetyMenu;
