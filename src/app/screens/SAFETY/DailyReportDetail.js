import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Icon,
    Item,
    Input
} from "native-base";

import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewReportDetail from "../../components/ListViewReportDetail";

class DailyReportDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            dataSource: [],
            dataDetail: [],
            isLoading: true,
            headerDailyReport: []
        };
    }

    static navigationOptions = {
        header: null
    };

    _renderItem = ({ item }) => (
        <ListItem data={item}></ListItem>
    )

    navigateToScreen(route, idUnsafe) {
        AsyncStorage.setItem('id', idUnsafe).then(() => {
            this.props.navigation.navigate(route);
        })
    }

    componentDidMount() {
        // AsyncStorage.getItem('id').then((id_Unsafe) => {
        //     this.setState({ 'id': id_Unsafe });
        //     this.loadData(id_Unsafe);
        //     this.loadDataDetail(id_Unsafe);
        // })
        // this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
        //     this.loadData(id_Unsafe);
        //     this.loadDataDetail(id_Unsafe);
        // });
        AsyncStorage.getItem('list').then((id_Unsafe) => {
            this.setState({ headerDailyReport: JSON.parse(id_Unsafe) });
            this.setState({ isLoading: false });
            console.log(this.state.headerDailyReport);
        })
        this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
            //this.loadData(id_Unsafe);
        });

    }

    // loadData(id_Unsafe) {
    //     AsyncStorage.getItem('token').then((value) => {
    //         const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/unsafe/list_view_detail';
    //         var formData = new FormData();
    //         formData.append("token", value)
    //         formData.append("ID", id_Unsafe)

    //         fetch(url, {
    //             headers: {
    //                 'Content-Type': 'multipart/form-data'
    //             },
    //             method: 'POST',
    //             body: formData
    //         })
    //             .then((response) => response.json())
    //             .then((responseJson) => {
    //                 // alert(JSON.stringify(responseJson));
    //                 this.setState({
    //                     dataDetail: responseJson.data,
    //                     isloading: false,
    //                 });
    //                 // alert(dataSource);
    //             })
    //             .catch((error) => {
    //                 console.log(error)
    //             })
    //     })
    // }

    deleteDetailHeader(id) {
        AsyncStorage.getItem('token').then((value) => {
            // alert(JSON.stringify(value));
            const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/unsafe/delete_detail';
            var formData = new FormData();
            formData.append("token", value);
            formData.append("ID", id);

            fetch(url, {
                headers: {
                    "Content-Type": "multipart/form-data"
                },
                method: "POST",
                body: formData
            })
                .then(response => response.json())
                .then(response => {
                    if (response.status == 200) {
                        Alert.alert('Success', 'Delete Success', [{
                            text: 'Okay',
                        }])
                        this.componentDidMount();
                    } else {
                        Alert.alert('Error', 'Delete Failed', [{
                            text: 'Okay'
                        }])
                    }
                });
        })
    }

    render() {
        var listReportK3;
        var listDetailReportK3;
        return (
            <Container style={styles.wrapper}>
                <Header style={styles.header}>
                    <Left>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate("DailyReport")}
                        >
                            <Icon
                                name="ios-arrow-back"
                                size={20}
                                style={styles.facebookButtonIconOrder2}
                            />
                        </Button>
                    </Left>
                    <Body>
                        <Title style={styles.textbody}>Unsafe Detail Report</Title>
                    </Body>
                    {/* <Right/> */}
                </Header>
                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                <Content style={{ marginTop: 10 }}>
                    <ScrollView>
                        <View style={{ justifyContent: "center", alignItems: "center" }}>
                            <Text style={{ fontSize: 12, color: colors.green01 }}>Unsafe Detail Report</Text>
                        </View>
                        <CardItem style={{ borderRadius: 0, marginTop: 5, }}>
                            <View style={{ flex: 1, flexDirection: "row" }}>
                                <View style={{ marginLeft: 10, width: 260 }}>
                                    <Text style={{ fontSize: 10 }}>No Dokumen</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerDailyReport.NO_DOKUMEN}</Text>
                                    <Text style={{ fontSize: 10, paddingTop: 10 }}>Ditemukan Oleh</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerDailyReport.CREATE_BY}</Text>
                                </View>
                                <View style={{ width: 70, marginRight: 10 }}>
                                    <Text style={{ fontSize: 10, textAlign: 'right' }}>Report Date</Text>
                                    <Text style={{ fontSize: 10, textAlign: 'right' }}>{this.state.headerDailyReport.CREATE_AT}</Text>
                                    <Text style={{ fontSize: 10, paddingTop: 10, textAlign: 'right' }}>Status</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold", textAlign: 'right', color: colors.green01 }}>{this.state.headerDailyReport.STATUS}</Text>
                                </View>
                            </View>
                        </CardItem>
                        <CardItem style={{ borderRadius: 0 }}>
                            <View style={{ flex: 1, flexDirection: "row" }}>
                                <View style={{ marginLeft: 10, width: 260 }}>
                                    <Text style={{ fontSize: 10, paddingTop: 10 }}>Shift</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerDailyReport.SHIFT}</Text>
                                    <Text style={{ fontSize: 10, paddingTop: 10 }}>Plant</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerDailyReport.PLANT}</Text>
                                </View>
                            </View>
                        </CardItem>
                        <CardItem style={{ borderRadius: 0, marginTop: 5, }}>
                            <View style={{ flex: 1, flexDirection: "row" }}>
                                <View style={{ marginLeft: 10, marginTop: 0, width: "100%" }}>
                                    <Button transparent onPress={() => this.navigateToScreen('DailyReportDetailList', this.state.headerDailyReport.ID)}>
                                        <Text style={{ fontSize: 10, fontWeight: "bold", color: colors.green01 }}>List Report Temuan</Text>
                                    </Button>
                                </View>
                            </View>
                        </CardItem>
                    </ScrollView>
                </Content>
                <CustomFooter navigation={this.props.navigation} menu='Safety' />
            </Container>

        );
    }
}

export default DailyReportDetail;
