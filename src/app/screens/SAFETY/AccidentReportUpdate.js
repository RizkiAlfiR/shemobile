import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Textarea,
    Icon,
    Picker,
    Form,
    Input,
    Item
} from "native-base";

import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton
} from "react-native-popup-dialog";
import moment from 'moment';
import Ripple from "react-native-material-ripple";
import ImagePicker from "react-native-image-picker";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DatePicker from 'react-native-datepicker';

class ListItem extends React.PureComponent {
    navigateToScreen(route, id_rusak) {
        // alert(route);
        AsyncStorage.setItem("id", id_rusak).then(() => {
            that.props.navigation.navigate(route);
        });
    }

    render() {
        return (
            <View style={{ backgroundColor: "#FEFEFE" }}>
                <Ripple
                    style={{
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                    rippleSize={176}
                    rippleDuration={600}
                    rippleContainerBorderRadius={15}
                    onPress={() =>
                        this.props.setSelectedPegLabel(this.props.data)
                    }
                    rippleColor={colors.accent}
                >
                    <CardItem
                        style={{
                            borderRadius: 0,
                            marginTop: 4,
                            backgroundColor: colors.gray
                        }}
                    >
                        <View
                            style={{
                                flex: 1,
                                flexDirection: "row",
                                marginTop: 4,
                                backgroundColor: colors.gray
                            }}
                        >
                            <View >
                                <Text style={{ fontSize: 12 }}>{this.props.data.mk_nopeg} - {this.props.data.mk_nama}</Text>
                            </View>
                        </View>
                    </CardItem>
                </Ripple>
            </View>
        );
    }
}

class ListItemSpv extends React.PureComponent {
    navigateToScreen(route, id_rusak) {
        // alert(route);
        AsyncStorage.setItem("id", id_rusak).then(() => {
            that.props.navigation.navigate(route);
        });
    }

    render() {
        return (
            <View style={{ backgroundColor: "#FEFEFE" }}>
                <Ripple
                    style={{
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                    rippleSize={176}
                    rippleDuration={600}
                    rippleContainerBorderRadius={15}
                    onPress={() =>
                        this.props.setSelectedSpvLabel(this.props.data)
                    }
                    rippleColor={colors.accent}
                >
                    <CardItem
                        style={{
                            borderRadius: 0,
                            marginTop: 4,
                            backgroundColor: colors.gray
                        }}
                    >
                        <View
                            style={{
                                flex: 1,
                                flexDirection: "row",
                                marginTop: 4,
                                backgroundColor: colors.gray
                            }}
                        >
                            <View >
                                <Text style={{ fontSize: 12 }}>{this.props.data.mk_nopeg} - {this.props.data.mk_nama}</Text>
                            </View>
                        </View>
                    </CardItem>
                </Ripple>
            </View>
        );
    }
}

class ListItemSaksi extends React.PureComponent {
    navigateToScreen(route, id_rusak) {
        // alert(route);
        AsyncStorage.setItem("id", id_rusak).then(() => {
            that.props.navigation.navigate(route);
        });
    }

    render() {
        return (
            <View style={{ backgroundColor: "#FEFEFE" }}>
                <Ripple
                    style={{
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                    rippleSize={176}
                    rippleDuration={600}
                    rippleContainerBorderRadius={15}
                    onPress={() =>
                        this.props.setSelectedSaksiLabel(this.props.data)
                    }
                    rippleColor={colors.accent}
                >
                    <CardItem
                        style={{
                            borderRadius: 0,
                            marginTop: 4,
                            backgroundColor: colors.gray
                        }}
                    >
                        <View
                            style={{
                                flex: 1,
                                flexDirection: "row",
                                marginTop: 4,
                                backgroundColor: colors.gray
                            }}
                        >
                            <View >
                                <Text style={{ fontSize: 12 }}>{this.props.data.mk_nopeg} - {this.props.data.mk_nama}</Text>
                            </View>
                        </View>
                    </CardItem>
                </Ripple>
            </View>
        );
    }
}

class ListItemPic extends React.PureComponent {
    navigateToScreen(route, id_rusak) {
        // alert(route);
        AsyncStorage.setItem("id", id_rusak).then(() => {
            that.props.navigation.navigate(route);
        });
    }

    render() {
        return (
            <View style={{ backgroundColor: "#FEFEFE" }}>
                <Ripple
                    style={{
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                    rippleSize={176}
                    rippleDuration={600}
                    rippleContainerBorderRadius={15}
                    onPress={() =>
                        this.props.setSelectedPicLabel(this.props.data)
                    }
                    rippleColor={colors.accent}
                >
                    <CardItem
                        style={{
                            borderRadius: 0,
                            marginTop: 4,
                            backgroundColor: colors.gray
                        }}
                    >
                        <View
                            style={{
                                flex: 1,
                                flexDirection: "row",
                                marginTop: 4,
                                backgroundColor: colors.gray
                            }}
                        >
                            <View >
                                <Text style={{ fontSize: 12 }}>{this.props.data.mk_nopeg} - {this.props.data.mk_nama}</Text>
                            </View>
                        </View>
                    </CardItem>
                </Ripple>
            </View>
        );
    }
}

export default class AccidentReportUpdate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            isLoading: true,
            headerAccidentReport: [],
            // AccidentDate: '',
            // selectedShift: '1',
            // AccidentTime: '',
            // selectedStatVict: '0',
            // selectedStatVictText: 'Non-Karyawan',
            // // badgeVict: '2180801',
            // // nameVict: '',
            // ageVict: '',
            // addressict: '',
            // noJamsos: '',
            // ukVict: '',
            // ukVictText: '',
            // nameVict: '',
            // posText: '',
            // dateInVict: '',

            // pickedImage: '',
            uri: '',
            // fileName: '',    

            // workTask: 'Mengerjakan Projek SHE Mobile',
            // spvName: 'INDRA NOFIANDI',
            // locText: 'Ruang Kerja ICT Gedung Semen Indonesia',
            // acdCaused: 'Overload',
            // // saksiName: 'Adhiq Rahmaduha',
            // // saksiPosText: 'Pogrammer',
            // injText: 'Programmer terlalu lama di depan komputer',
            // injNote: 'Muncul bisul',
            // firstAid: 'Buatan Nafas',
            // nextAid: 'Dibawa ke UGD',
            // safetyReq: 'Kerja Looss',
            // acdRptText: 'Bekerja oveload didepan laptop',
            // unsafeAct: 'Memaksakan diri',
            // unsafeCond: 'Laptop tidak memenuhi requirement',
            // note: 'Porsi kerja dioptimalkan',
            // invName: 'Adhiq Rahmaduha',

            visibleLoadingPegawai: false,
            visibleSearchListPeg: false,
            selectedPegLabel: '',
            searchWordPegawai: '',

            visibleLoadingSaksi: false,
            visibleSearchListSaksi: false,
            selectedSaksiLabel: '',
            searchWordSaksi: '',

            visibleDialogSubmit: false,
            enableScrollViewScroll: true
        };
    }

    static navigationOptions = {
        header: null
    };

    componentDidMount() {
        AsyncStorage.getItem('list').then((headerAccidentReport) => {
            // console.log(JSON.parse(headerAccidentReport).DATE_ACD)
            // var acdDate = moment(JSON.parse(headerAccidentReport).DATE_ACD, 'DD-MMM-YYYY');
            // var acdDateString = acdDate.format('YYYY-MM-DD')
            // console.log(acdDateString)

            // console.log(JSON.parse(headerAccidentReport).DATE_IN)
            // var vicDateIn = moment(JSON.parse(headerAccidentReport).DATE_IN, 'DD-MMM-YYYY');
            // var vicDateInString = vicDateIn.format('YYYY-MM-DD')
            // console.log(vicDateIn)

            this.setState({
                listHeader: JSON.parse(headerAccidentReport),
                id: JSON.parse(headerAccidentReport).id,
                AccidentDate: JSON.parse(headerAccidentReport).date_acd,
                selectedShift: JSON.parse(headerAccidentReport).shift,
                AccidentTime: JSON.parse(headerAccidentReport).time_acd,
                selectedStatVict: JSON.parse(headerAccidentReport).stat_vict,
                selectedStatVictText: JSON.parse(headerAccidentReport).state_vict_text,
                badgeVict: JSON.parse(headerAccidentReport).badge_vict,
                nameVict: JSON.parse(headerAccidentReport).name_vict,
                ukVict: JSON.parse(headerAccidentReport).uk_vict,
                ukVictText: JSON.parse(headerAccidentReport).uk_vict_text,
                dateInVict: JSON.parse(headerAccidentReport).date_in,
                pos: JSON.parse(headerAccidentReport).position,
                posText: JSON.parse(headerAccidentReport).position_text,
                ageVict: JSON.parse(headerAccidentReport).age,
                noJamsos: JSON.parse(headerAccidentReport).no_jamsos,
                selectedShift: JSON.parse(headerAccidentReport).shift,

                pickedImage: GlobalConfig.LOCALHOST + 'public/uploads/accident/' + JSON.parse(headerAccidentReport).pict_before,

                addressict: JSON.parse(headerAccidentReport).region,
                workTask: JSON.parse(headerAccidentReport).work_task,
                spvBadge: JSON.parse(headerAccidentReport).spv_badge,
                spvName: JSON.parse(headerAccidentReport).spv_name,
                spvPos: JSON.parse(headerAccidentReport).spv_position,
                spvPosText: JSON.parse(headerAccidentReport).spv_position_text,
                loc: JSON.parse(headerAccidentReport).location,
                locText: JSON.parse(headerAccidentReport).location_text,
                acdCaused: JSON.parse(headerAccidentReport).acd_cause,
                saksiBadge: JSON.parse(headerAccidentReport).s_badge,
                saksiName: JSON.parse(headerAccidentReport).s_name,
                saksiPos: JSON.parse(headerAccidentReport).s_position,
                saksiPosText: JSON.parse(headerAccidentReport).s_pos_text,
                injText: JSON.parse(headerAccidentReport).inj_text,
                injNote: JSON.parse(headerAccidentReport).inj_note,
                firstAid: JSON.parse(headerAccidentReport).first_aid,
                nextAid: JSON.parse(headerAccidentReport).next_aid,
                safetyReq: JSON.parse(headerAccidentReport).safety_req,
                acdRptText: JSON.parse(headerAccidentReport).acd_rpt_text,
                unsafeAct: JSON.parse(headerAccidentReport).unsafe_act,
                unsafeCond: JSON.parse(headerAccidentReport).unsafe_cond,
                note: JSON.parse(headerAccidentReport).note,
                picBadge: JSON.parse(headerAccidentReport).inv_badge,
                picName: JSON.parse(headerAccidentReport).inv_name,

            });
        })
        this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {

        });
    }

    reset = () => {
        this.setState({
            pickedImage: null
        });
    }

    pickImageHandler = () => {
        ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
            if (res.didCancel) {
                console.log("User cancelled!");
            } else if (res.error) {
                console.log("Error", res.error);
            } else {
                this.setState({
                    pickedImage: res.uri,
                    uri: res.uri,
                    fileName: res.fileName,
                    fileType: res.type
                });
                console.log(res.type)
            }
        });
    }

    resetHandler = () => {
        this.reset();
    }

    // _renderItem = ({ item }) => (
    //     <ListItem data={item}></ListItem>
    // )

    // componentDidMount() {
    //     this.loadData();
    // }

    // loadData() {

    // }

    UpdateAccident() {
        console.log(this.state.uri)
        if (this.state.uri == '') {
            this.setState({
                visibleDialogSubmit: true
            })
            AsyncStorage.getItem("token").then(value => {
                const url = GlobalConfig.LOCALHOST + 'api/accidentreports/update';
                var formData = new FormData();
                formData.append("token", value);
                formData.append("id", this.state.id);
                formData.append("date_acd", this.state.AccidentDate);
                formData.append("time_acd", this.state.AccidentTime);
                formData.append("stat_vict", this.state.selectedStatVict);
                formData.append("state_vict_text", this.state.selectedStatVictText);
                formData.append("badge_vict", this.state.badgeVict);
                formData.append("name_vict", this.state.nameVict);
                formData.append("uk_vict", this.state.ukVict);
                formData.append("uk_vict_text", this.state.ukVictText);
                formData.append("company_code", 2000);
                formData.append("company_text", "PT. Semen Indonesia, Tbk.");
                formData.append("ven_code", null);
                formData.append("ven_text", null);
                formData.append("date_in", this.state.dateInVict);
                formData.append("position", this.state.posVict);
                formData.append("position_text", this.state.posText);
                formData.append("age", this.state.ageVict);
                formData.append("no_jamsos", this.state.noJamsos);
                formData.append("no_bpjs", null);
                formData.append("shift", this.state.selectedShift);
                formData.append("shift_text", "Shift" + this.state.selectedShift);
                formData.append("region", this.state.addressict);
                formData.append("work_task", this.state.workTask);
                formData.append("spv_badge", this.state.spvBadge);
                formData.append("spv_name", this.state.spvName);
                formData.append("spv_position", this.state.spvPos);
                formData.append("spv_position_text", this.state.spvPosText);
                formData.append("location", this.state.loc);
                formData.append("location_text", this.state.locText);
                formData.append("acd_cause", this.state.acdCaused);
                formData.append("s_badge", this.state.saksiBadge);
                formData.append("s_name", this.state.saksiName);
                formData.append("s_position", this.state.saksiPos);
                formData.append("s_pos_text", this.state.saksiPosText);
                formData.append("inj_note", this.state.injNote);
                formData.append("inj_text", this.state.injText);
                formData.append("first_aid", this.state.firstAid);
                formData.append("next_aid", this.state.nextAid);
                formData.append("safety_req", this.state.safetyReq);
                formData.append("acd_rpt_text", this.state.acdRptText);
                formData.append("unsafe_act", this.state.unsafeAct);
                formData.append("unsafe_cond", this.state.unsafeCond);
                formData.append("note", this.state.note);
                formData.append("inv_badge", this.state.picBadge);
                formData.append("inv_name", this.state.picName);
                formData.append("pict_before", null);
                formData.append("pict_after", null);
                formData.append("follow_up", null);

                // alert(JSON.stringify(url));

                fetch(url, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    },
                    method: 'POST',
                    body: formData
                })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        // alert(responseJson);
                        if (responseJson.status == 200) {
                            this.setState({
                                visibleDialogSubmit: false
                            })
                            Alert.alert('Success', 'Update Accident Success', [{
                                text: 'Okay'
                            }])
                            this.props.navigation.navigate('AccidentReport')
                        } else {
                            this.setState({
                                visibleDialogSubmit: false
                            })
                            Alert.alert('Error', 'Update Accident Failed', [{
                                text: 'Okay'
                            }])
                        }
                    })
                    .catch((error) => {
                        this.setState({
                            visibleDialogSubmit: false
                        })
                        Alert.alert('Error', 'Update Accident Failed', [{
                            text: 'Okay'
                        }])
                        console.log(error)
                    })
            })
        } else {
            this.setState({
                visibleDialogSubmit: true
            })
            // alert(JSON.stringify(this.state.id));
            AsyncStorage.getItem("token").then(value => {
                const url = GlobalConfig.LOCALHOST + 'api/accidentreports/update';
                var formData = new FormData();
                formData.append("token", value);
                formData.append("id", this.state.id);
                formData.append("date_acd", this.state.AccidentDate);
                formData.append("time_acd", this.state.AccidentTime);
                formData.append("stat_vict", this.state.selectedStatVict);
                formData.append("state_vict_text", this.state.selectedStatVictText);
                formData.append("badge_vict", this.state.badgeVict);
                formData.append("name_vict", this.state.nameVict);
                formData.append("uk_vict", this.state.ukVict);
                formData.append("uk_vict_text", this.state.ukVictText);
                formData.append("company_code", 2000);
                formData.append("company_text", "PT. Semen Indonesia, Tbk.");
                formData.append("ven_code", null);
                formData.append("ven_text", null);
                formData.append("date_in", this.state.dateInVict);
                formData.append("position", this.state.posVict);
                formData.append("position_text", this.state.posText);
                formData.append("age", this.state.ageVict);
                formData.append("no_jamsos", this.state.noJamsos);
                formData.append("no_bpjs", null);
                formData.append("shift", this.state.selectedShift);
                formData.append("shift_text", "Shift" + this.state.selectedShift);
                formData.append("region", this.state.addressict);
                formData.append("work_task", this.state.workTask);
                formData.append("spv_badge", this.state.spvBadge);
                formData.append("spv_name", this.state.spvName);
                formData.append("spv_position", this.state.spvPos);
                formData.append("spv_position_text", this.state.spvPosText);
                formData.append("location", this.state.loc);
                formData.append("location_text", this.state.locText);
                formData.append("acd_cause", this.state.acdCaused);
                formData.append("s_badge", this.state.saksiBadge);
                formData.append("s_name", this.state.saksiName);
                formData.append("s_position", this.state.saksiPos);
                formData.append("s_pos_text", this.state.saksiPosText);
                formData.append("inj_note", this.state.injNote);
                formData.append("inj_text", this.state.injText);
                formData.append("first_aid", this.state.firstAid);
                formData.append("next_aid", this.state.nextAid);
                formData.append("safety_req", this.state.safetyReq);
                formData.append("acd_rpt_text", this.state.acdRptText);
                formData.append("unsafe_act", this.state.unsafeAct);
                formData.append("unsafe_cond", this.state.unsafeCond);
                formData.append("note", this.state.note);
                formData.append("inv_badge", this.state.picBadge);
                formData.append("inv_name", this.state.picName);
                formData.append("pict_before", {
                    uri: this.state.pickedImage,
                    name: this.state.pickedImage,
                    type: this.state.fileType,
                });
                formData.append("pict_after", null);
                formData.append("follow_up", null);

                console.log(formData)
                // alert(JSON.stringify(url));

                fetch(url, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    },
                    method: 'POST',
                    body: formData
                })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        // alert(responseJson);
                        if (responseJson.status == 200) {
                            this.setState({
                                visibleDialogSubmit: false
                            })
                            Alert.alert('Success', 'Update Accident Success', [{
                                text: 'Okay'
                            }])
                            this.props.navigation.navigate('AccidentReport')
                        } else {
                            this.setState({
                                visibleDialogSubmit: false
                            })
                            Alert.alert('Error', 'Update Accident Failed', [{
                                text: 'Okay'
                            }])
                        }
                    })
                    .catch((error) => {
                        this.setState({
                            visibleDialogSubmit: false
                        })
                        Alert.alert('Error', 'Update Accident Failed', [{
                            text: 'Okay'
                        }])
                        console.log(error)
                    })
            })
        }
    }

    onChangePegawai(text) {
        this.setState({
            searchWordPegawai: text
        })
        this.setState({
            visibleLoadingPegawai: true
        })
        AsyncStorage.getItem('token').then((value) => {
            const url = GlobalConfig.LOCALHOST + 'api/master/pegawai/search?searchEmp=' + this.state.searchWordPegawai;

            fetch(url)
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        visibleLoadingPegawai: false
                    })
                    this.setState({
                        listKaryawan: responseJson.data,
                        listKaryawanMaster: responseJson.data,
                        isloading: false
                    });
                })
                .catch((error) => {
                    this.setState({
                        visibleLoadingPegawai: false
                    })
                    console.log(error)
                })
        })
        // this.setState({
        //   listKaryawan:this.state.listKaryawanMaster.filter(x => x.mk_nama.includes(text)),
        // })
    }

    onChangeSpv(text) {
        this.setState({
            searchWordSpv: text
        })
        this.setState({
            visibleLoadingSpv: true
        })
        AsyncStorage.getItem('token').then((value) => {
            const url = GlobalConfig.LOCALHOST + 'api/master/pegawai/search?searchEmp=' + this.state.searchWordSpv;

            fetch(url)
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        visibleLoadingPegawai: false
                    })
                    this.setState({
                        listSpv: responseJson.data,
                        listSpvMaster: responseJson.data,
                        isloading: false
                    });
                })
                .catch((error) => {
                    this.setState({
                        visibleLoadingSpv: false
                    })
                    console.log(error)
                })
        })
        // this.setState({
        //   listKaryawan:this.state.listKaryawanMaster.filter(x => x.mk_nama.includes(text)),
        // })
    }

    onChangeSaksi(text) {
        this.setState({
            searchWordSaksi: text
        })
        this.setState({
            visibleLoadingSaksi: true
        })
        AsyncStorage.getItem('token').then((value) => {
            const url = GlobalConfig.LOCALHOST + 'api/master/pegawai/search?searchEmp=' + this.state.searchWordSaksi;

            fetch(url)
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        visibleLoadingSaksi: false
                    })
                    this.setState({
                        listSaksi: responseJson.data,
                        listSaksiMaster: responseJson.data,
                        isloading: false
                    });
                })
                .catch((error) => {
                    this.setState({
                        visibleLoadingSaksi: false
                    })
                    console.log(error)
                })
        })
        // this.setState({
        //   listKaryawan:this.state.listKaryawanMaster.filter(x => x.mk_nama.includes(text)),
        // })
    }

    onChangePic(text) {
        this.setState({
            searchWordPic: text
        })
        this.setState({
            visibleLoadingPic: true
        })
        AsyncStorage.getItem('token').then((value) => {
            const url = GlobalConfig.LOCALHOST + 'api/master/pegawai/search?searchEmp=' + this.state.searchWordPic;

            fetch(url)
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        visibleLoadingPic: false
                    })
                    this.setState({
                        listPic: responseJson.data,
                        listPicMaster: responseJson.data,
                        isloading: false
                    });
                })
                .catch((error) => {
                    this.setState({
                        visibleLoadingPegawai: false
                    })
                    console.log(error)
                })
        })
        // this.setState({
        //   listKaryawan:this.state.listKaryawanMaster.filter(x => x.mk_nama.includes(text)),
        // })
    }

    onClickSearchPeg() {
        console.log('masuk')
        if (this.state.visibleSearchListPeg) {
            this.setState({
                visibleSearchListPeg: false
            })
        } else {
            this.setState({
                visibleSearchListPeg: true
            })
        }
    }

    onClickSearchSpv() {
        console.log('masuk')
        if (this.state.visibleSearchListSpv) {
            this.setState({
                visibleSearchListSpv: false
            })
        } else {
            this.setState({
                visibleSearchListSpv: true
            })
        }
    }

    onClickSearchSaksi() {
        console.log('masuk')
        if (this.state.visibleSearchListSaksi) {
            this.setState({
                visibleSearchListSaksi: false
            })
        } else {
            this.setState({
                visibleSearchListSaksi: true
            })
        }
    }

    onClickSearchPic() {
        console.log('masuk')
        if (this.state.visibleSearchListPic) {
            this.setState({
                visibleSearchListPic: false
            })
        } else {
            this.setState({
                visibleSearchListPic: true
            })
        }
    }

    setSelectedPegLabel(pegawai) {
        var lahir = new Date(pegawai.mk_tgl_lahir);
        var umurDate = new Date(Date.now() - lahir);
        var umur = Math.abs(umurDate.getUTCFullYear() - 1970)
        this.setState({
            badgeVict: pegawai.mk_nopeg,
            nameVict: pegawai.mk_nama,
            dateInVict: pegawai.mk_tgl_masuk,
            posVict: pegawai.mk_employee_emp_subgroup,
            posText: pegawai.mk_employee_emp_subgroup_text,
            addressict: pegawai.mk_alamat_rumah,
            ukVict: pegawai.muk_short,
            ukVictText: pegawai.muk_nama,
            ageVict: umur.toString(),
            visibleSearchListPeg: false,
            selectedStatVict: '1',
            selectedStatVictText: 'Karyawan'
        })
    }

    setSelectedSpvLabel(spv) {
        this.setState({
            spvBadge: spv.mk_nopeg,
            spvName: spv.mk_nama,
            spvPos: spv.mk_employee_emp_subgroup,
            spvPosText: spv.mk_employee_emp_subgroup_text,
            visibleSearchListSpv: false,
        })
    }

    setSelectedSaksiLabel(saksi) {
        this.setState({
            saksiBadge: saksi.mk_nopeg,
            saksiName: saksi.mk_nama,
            saksiPosText: saksi.mk_employee_emp_subgroup_text,
            visibleSearchListSaksi: false,
        })
    }

    setSelectedPicLabel(pic) {
        this.setState({
            picBadge: pic.mk_nopeg,
            picName: pic.mk_nama,
            picPosText: pic.mk_employee_emp_subgroup_text,
            visibleSearchListPic: false,
        })
    }

    _renderItem = ({ item }) => <ListItem data={item} setSelectedPegLabel={text => this.setSelectedPegLabel(text)} />;

    _renderItemSpv = ({ item }) => <ListItemSpv data={item} setSelectedSpvLabel={text => this.setSelectedSpvLabel(text)} />;

    _renderItemSaksi = ({ item }) => <ListItemSaksi data={item} setSelectedSaksiLabel={text => this.setSelectedSaksiLabel(text)} />;

    _renderItemPic = ({ item }) => <ListItemPic data={item} setSelectedPicLabel={text => this.setSelectedPicLabel(text)} />;

    render() {
        return (
            <Container style={styles.wrapper}>
                <Header style={styles.header}>
                    <Left style={{ flex: 1 }}>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.goBack()}
                        >
                            <Icon
                                name="ios-arrow-back"
                                size={20}
                                style={styles.facebookButtonIconOrder2}
                            />
                        </Button>
                    </Left>
                    <Body style={{ flex: 3, alignItems: 'center' }}>
                        <Title style={styles.textbody}>Update Accident Report</Title>
                    </Body>

                    <Right style={{ flex: 1 }} />
                </Header>
                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                <View style={{ flex: 1 }}
                    onStartShouldSetResponderCapture={() => {
                        this.setState({ enableScrollViewScroll: true });
                    }}>
                    <Content style={{ marginTop: 0 }}
                        scrollEnabled={this.state.enableScrollViewScroll}
                        ref={myScroll => (this._myScroll = myScroll)}>
                        <View style={{ justifyContent: "center", alignItems: "center" }}>
                            <Text style={{ fontSize: 12, marginTop: 20, color: colors.green01 }}>Accident Update {this.state.badgeVict} - {this.state.nameVict}</Text>
                        </View>
                        <View style={{ backgroundColor: '#FEFEFE' }}>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View style={{ paddingBottom: 5 }}>
                                        <Text style={styles.viewMerkFoto}>Upload Foto (jpg, png) max : 500 kb</Text>
                                    </View>
                                    <View>
                                        <Ripple
                                            style={{
                                                flex: 2,
                                                justifyContent: "center",
                                                alignItems: "center"
                                            }}
                                            rippleSize={176}
                                            rippleDuration={600}
                                            rippleContainerBorderRadius={15}
                                            onPress={this.pickImageHandler}
                                            rippleColor={colors.accent}
                                        >
                                            <View style={styles.placeholder}>
                                                <Image source={{ uri: this.state.pickedImage + '?' + new Date() }} style={styles.previewImage} />
                                            </View>
                                        </Ripple>
                                    </View>
                                </View>
                            </CardItem>

                            <CardItem style={{ borderRadius: 0, marginTop: 4, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View>
                                        <Text style={styles.titleInput}>Accident Date (*)</Text>
                                        <DatePicker
                                            style={{ width: 320, fontSize: 10, borderRadius: 20 }}
                                            date={this.state.AccidentDate}
                                            mode="date"
                                            placeholder="Choose Date ..."
                                            format="YYYY-MM-DD"
                                            minDate="2018-01-01"
                                            maxDate="2050-12-31"
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            customStyles={{
                                                dateIcon: {
                                                    position: 'absolute',
                                                    left: 0,
                                                    top: 5,
                                                    marginLeft: 0
                                                },
                                                dateInput: {
                                                    marginLeft: 50, height: 35, borderRadius: 20, fontSize: 10
                                                }
                                            }}
                                            onDateChange={(date) => { this.setState({ AccidentDate: date }) }} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View style={{ flex: 1 }}>
                                        <Text style={styles.viewMerk}>Shift</Text>
                                        <Form underlineColorAndroid='transparent'>
                                            <Picker
                                                mode="dropdown"
                                                iosIcon={<Icon name={Platform.OS ? "ios-arrow-down" : 'ios-arrow-down-outline'} />}
                                                style={{ width: '100%' }}
                                                placeholder="Quantity"
                                                placeholderStyle={{ color: "#bfc6ea" }}
                                                placeholderIconColor="#007aff"
                                                selectedValue={this.state.selectedShift}
                                                onValueChange={(itemValue) => this.setState({ selectedShift: itemValue })}>
                                                <Picker.Item label="Shift 1" value="1" />
                                                <Picker.Item label="Shift 2" value="2" />
                                                <Picker.Item label="Shift 3" value="3" />
                                            </Picker>
                                        </Form>
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View>
                                        <Text style={styles.titleInput}>Accident Time (*)</Text>
                                        <DatePicker
                                            style={{ width: 320, fontSize: 10, borderRadius: 20 }}
                                            date={this.state.AccidentTime}
                                            mode="time"
                                            placeholder="Choose Date ..."
                                            format="HH:mm:ss"
                                            minDate="00:00:00"
                                            maxDate="12:60:60"
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            customStyles={{
                                                dateIcon: {
                                                    position: 'absolute',
                                                    left: 0,
                                                    top: 5,
                                                    marginLeft: 0
                                                },
                                                dateInput: {
                                                    marginLeft: 50, height: 35, borderRadius: 20, fontSize: 10
                                                }
                                            }}
                                            onDateChange={(date) => { this.setState({ AccidentTime: date }) }} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View style={{ flex: 1 }}>
                                        <Text style={styles.viewMerk}>Status Karyawan</Text>
                                        <Form underlineColorAndroid='transparent'>
                                            <Picker
                                                mode="dropdown"
                                                iosIcon={<Icon name={Platform.OS ? "ios-arrow-down" : 'ios-arrow-down-outline'} />}
                                                style={{ width: '100%' }}
                                                placeholder="Quantity"
                                                placeholderStyle={{ color: "#bfc6ea" }}
                                                placeholderIconColor="#007aff"
                                                selectedValue={this.state.selectedStatVict}
                                                onValueChange={(itemValue) => this.setState({
                                                    selectedStatVict: itemValue
                                                })}>
                                                <Picker.Item label="1 - Karyawan" value="1" />
                                                <Picker.Item label="0 - Non-Karyawan" value="0" />
                                            </Picker>
                                        </Form>
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>No. Badge Korban</Text>
                                    </View>
                                    {this.state.selectedStatVict == 0 ? (
                                        <View>
                                            <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11, height: 35 }} rowSpan={3} bordered value={this.state.badgeVict} placeholder='Type Something ...' onChangeText={(text) => this.setState({ badgeVict: text })} />
                                        </View>
                                    ) : (
                                            <View style={{ flex: 1, flexDirection: 'column' }}>
                                                <Button
                                                    block
                                                    style={{ justifyContent: "flex-start", flex: 1, borderWidth: Platform.OS === 'ios' ? 1 : 0, paddingLeft: 10, marginRight: 5, marginLeft: 5, backgroundColor: colors.gray, height: 35, borderColor: Platform.OS === 'ios' ? colors.lightGray : null }}
                                                    onPress={() => this.onClickSearchPeg()}>
                                                    <Text style={{ fontSize: 12 }}>{this.state.badgeVict}</Text>
                                                </Button>
                                            </View>
                                        )}
                                    {this.state.visibleSearchListPeg &&
                                        <View style={{ height: 300, flexDirection: 'column', borderWidth: 1, padding: 10, backgroundColor: colors.gray, margin: 5, borderColor: "#E6E6E6", }}>
                                            <View>

                                                <Textarea value={this.state.searchWordPegawai} style={{ marginLeft: 5, marginRight: 5, fontSize: 11 }} bordered rowSpan={1.5} onChangeText={(text) => this.onChangePegawai(text)} placeholder='Ketik nama karyawan' />

                                            </View>
                                            <View style={{ flex: 1 }}
                                                onStartShouldSetResponderCapture={() => {
                                                    this.setState({ enableScrollViewScroll: false });
                                                    if (this._myScroll.contentOffset === 0
                                                        && this.state.enableScrollViewScroll === false) {
                                                        this.setState({ enableScrollViewScroll: true });
                                                    }
                                                }}>
                                                <FlatList
                                                    data={this.state.listKaryawan}
                                                    renderItem={this._renderItem}
                                                    keyExtractor={(item, index) => index.toString()}
                                                />
                                            </View>
                                        </View>
                                    }
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Nama Korban</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.nameVict} placeholder='Type Something ...' onChangeText={(text) => this.setState({ nameVict: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Umur Korban</Text>
                                    </View>
                                    <View>
                                        <Textarea editable={this.state.selectedStatVict == "1" ? false : true} style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.ageVict} placeholder='Type Something ...' onChangeText={(text) => this.setState({ ageVict: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Alamat Korban</Text>
                                    </View>
                                    <View>
                                        <Textarea editable={this.state.selectedStatVict == "1" ? false : true} style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.addressict} placeholder='Type Something ...' onChangeText={(text) => this.setState({ addressict: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={styles.viewMerk}>No. Jamsos </Text>
                                        <Text style={{ fontWeight: 'bold' }}>(Wajib diisi untuk Non-Karyawan)</Text>
                                    </View>
                                    <View>
                                        <Textarea editable={this.state.selectedStatVict == "1" ? false : true} style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.noJamsos} placeholder='Type Something ...' onChangeText={(text) => this.setState({ noJamsos: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Kode Unit Kerja Korban</Text>
                                    </View>
                                    <View>
                                        <Textarea editable={this.state.selectedStatVict == "1" ? false : true} style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.ukVict} placeholder='Type Something ...' onChangeText={(text) => this.setState({ ukVict: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Nama Unit Kerja Korban</Text>
                                    </View>
                                    <View>
                                        <Textarea editable={this.state.selectedStatVict == "1" ? false : true} style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.ukVictText} placeholder='Type Something ...' onChangeText={(text) => this.setState({ ukVictText: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Posisi Korban di Unit Kerja</Text>
                                    </View>
                                    <View>
                                        <Textarea editable={false} style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.posText} placeholder='Type Something ...' onChangeText={(text) => this.setState({ posText: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View>
                                        <Text style={styles.titleInput}>Date in Korban (*)</Text>
                                        <DatePicker
                                            style={{ width: 320, fontSize: 10, borderRadius: 20 }}
                                            date={this.state.dateInVict}
                                            mode="date"
                                            placeholder="Choose Date ..."
                                            format="YYYY-MM-DD"
                                            minDate="2018-01-01"
                                            maxDate="2050-12-31"
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            customStyles={{
                                                dateIcon: {
                                                    position: 'absolute',
                                                    left: 0,
                                                    top: 5,
                                                    marginLeft: 0
                                                },
                                                dateInput: {
                                                    marginLeft: 50, height: 35, borderRadius: 20, fontSize: 10
                                                }
                                            }}
                                            onDateChange={(date) => { this.setState({ dateInVict: date }) }} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 20, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Task Pekerjaan Korban</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.workTask} placeholder='Type Something ...' onChangeText={(text) => this.setState({ workTask: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Nama Supervisor Korban</Text>
                                    </View>
                                    {/* <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.spvName} placeholder='Type Something ...' onChangeText={(text) => this.setState({ spvName: text })} />
                                    </View> */}
                                    <View style={{ flex: 1, flexDirection: 'column' }}>
                                        <Button
                                            block
                                            style={{ justifyContent: "flex-start", flex: 1, borderWidth: Platform.OS === 'ios' ? 1 : 0, borderColor: Platform.OS === 'ios' ? colors.lightGray : null, paddingLeft: 10, marginRight: 5, marginLeft: 5, backgroundColor: colors.gray, height: 35 }}
                                            onPress={() => this.onClickSearchSpv()}>
                                            <Text style={{ fontSize: 12 }}>{this.state.spvBadge} - {this.state.spvName}</Text>
                                        </Button>
                                    </View>

                                    {this.state.visibleSearchListSpv &&
                                        <View style={{ height: 300, flexDirection: 'column', borderWidth: 1, padding: 10, backgroundColor: colors.gray, margin: 5, borderColor: "#E6E6E6", }}>
                                            <View>

                                                <Textarea value={this.state.searchWordSpv} style={{ marginLeft: 5, marginRight: 5, fontSize: 11 }} bordered rowSpan={1.5} onChangeText={(text) => this.onChangeSpv(text)} placeholder='Ketik nama karyawan' />

                                            </View>
                                            <View style={{ flex: 1 }}
                                                onStartShouldSetResponderCapture={() => {
                                                    this.setState({ enableScrollViewScroll: false });
                                                    if (this._myScroll.contentOffset === 0
                                                        && this.state.enableScrollViewScroll === false) {
                                                        this.setState({ enableScrollViewScroll: true });
                                                    }
                                                }}>
                                                <FlatList
                                                    data={this.state.listSpv}
                                                    renderItem={this._renderItemSpv}
                                                    keyExtractor={(item, index) => index.toString()}
                                                />
                                            </View>
                                        </View>
                                    }
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Lokasi Kejadian</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.locText} placeholder='Type Something ...' onChangeText={(text) => this.setState({ locText: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Penyebab Kejadian</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.acdCaused} placeholder='Type Something ...' onChangeText={(text) => this.setState({ acdCaused: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>No. Badge Saksi</Text>
                                    </View>
                                    <View style={{ flex: 1, flexDirection: 'column' }}>
                                        <Button
                                            block
                                            style={{ justifyContent: "flex-start", flex: 1, borderWidth: Platform.OS === 'ios' ? 1 : 0, borderColor: Platform.OS === 'ios' ? colors.lightGray : null, paddingLeft: 10, marginRight: 5, marginLeft: 5, backgroundColor: colors.gray, height: 35, }}
                                            onPress={() => this.onClickSearchSaksi()}>
                                            <Text style={{ fontSize: 12 }}>{this.state.saksiBadge}</Text>
                                        </Button>
                                    </View>

                                    {this.state.visibleSearchListSaksi &&
                                        <View style={{ height: 300, flexDirection: 'column', borderWidth: 1, padding: 10, backgroundColor: colors.gray, margin: 5, borderColor: "#E6E6E6", }}>
                                            <View>

                                                <Textarea value={this.state.searchWordSaksi} style={{ marginLeft: 5, marginRight: 5, fontSize: 11 }} bordered rowSpan={1.5} onChangeText={(text) => this.onChangeSaksi(text)} placeholder='Ketik nama karyawan' />

                                            </View>
                                            <View style={{ flex: 1 }}
                                                onStartShouldSetResponderCapture={() => {
                                                    this.setState({ enableScrollViewScroll: false });
                                                    if (this._myScroll.contentOffset === 0
                                                        && this.state.enableScrollViewScroll === false) {
                                                        this.setState({ enableScrollViewScroll: true });
                                                    }
                                                }}>
                                                <FlatList
                                                    data={this.state.listSaksi}
                                                    renderItem={this._renderItemSaksi}
                                                    keyExtractor={(item, index) => index.toString()}
                                                />
                                            </View>
                                        </View>
                                    }
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Nama Saksi</Text>
                                    </View>
                                    <View>
                                        <Textarea editable={this.state.selectedStatVict == "1" ? false : true} style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.saksiName} placeholder='Type Something ...' onChangeText={(text) => this.setState({ saksiName: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Jabatan Saksi Kejadian</Text>
                                    </View>
                                    <View>
                                        <Textarea editable={this.state.selectedStatVict == "1" ? false : true} style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.saksiPosText} placeholder='Type Something ...' onChangeText={(text) => this.setState({ saksiPosText: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Cedera Yang Dialami Korban</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.injText} placeholder='Type Something ...' onChangeText={(text) => this.setState({ injText: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Keterangan Cedera Korban</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.injNote} placeholder='Type Something ...' onChangeText={(text) => this.setState({ injNote: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Pertolongan Pertama Korban</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.firstAid} placeholder='Type Something ...' onChangeText={(text) => this.setState({ firstAid: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Pertolongan Lanjutan Korban</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.nextAid} placeholder='Type Something ...' onChangeText={(text) => this.setState({ nextAid: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Keadaan Syarat Keselamatan Kerja</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.safetyReq} placeholder='Type Something ...' onChangeText={(text) => this.setState({ safetyReq: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Penjelasan Mengenai Kejadian</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.acdRptText} placeholder='Type Something ...' onChangeText={(text) => this.setState({ acdRptText: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Unsafe Action Pada Kejadian</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.unsafeAct} placeholder='Type Something ...' onChangeText={(text) => this.setState({ unsafeAct: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Unsafe Condition Pada Kejadian</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.unsafeCond} placeholder='Type Something ...' onChangeText={(text) => this.setState({ unsafeCond: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Note Terhadap Kejadian (Saran)</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.note} placeholder='Type Something ...' onChangeText={(text) => this.setState({ note: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Petugas Kejadian</Text>
                                    </View>
                                    {/* <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.invName} placeholder='Type Something ...' onChangeText={(text) => this.setState({ invName: text })} />
                                    </View> */}
                                    <View style={{ flex: 1, flexDirection: 'column' }}>
                                        <Button
                                            block
                                            style={{ justifyContent: "flex-start", flex: 1, borderWidth: Platform.OS === 'ios' ? 1 : 0, borderColor: Platform.OS === 'ios' ? colors.lightGray : null, paddingLeft: 10, marginRight: 5, marginLeft: 5, backgroundColor: colors.gray, height: 35 }}
                                            onPress={() => this.onClickSearchPic()}>
                                            <Text style={{ fontSize: 12 }}>{this.state.picBadge} - {this.state.picName}</Text>
                                        </Button>
                                    </View>

                                    {this.state.visibleSearchListPic &&
                                        <View style={{ height: 300, flexDirection: 'column', borderWidth: 1, padding: 10, backgroundColor: colors.gray, margin: 5, borderColor: "#E6E6E6", }}>
                                            <View>

                                                <Textarea value={this.state.searchWordPic} style={{ marginLeft: 5, marginRight: 5, fontSize: 11 }} bordered rowSpan={1.5} onChangeText={(text) => this.onChangePic(text)} placeholder='Ketik nama karyawan' />

                                            </View>
                                            <View style={{ flex: 1 }}
                                                onStartShouldSetResponderCapture={() => {
                                                    this.setState({ enableScrollViewScroll: false });
                                                    if (this._myScroll.contentOffset === 0
                                                        && this.state.enableScrollViewScroll === false) {
                                                        this.setState({ enableScrollViewScroll: true });
                                                    }
                                                }}>
                                                <FlatList
                                                    data={this.state.listPic}
                                                    renderItem={this._renderItemPic}
                                                    keyExtractor={(item, index) => index.toString()}
                                                />
                                            </View>
                                        </View>
                                    }
                                </View>
                            </CardItem>
                        </View>
                        <View style={styles.Contentsave}>
                            <Button
                                block
                                style={{
                                    height: 45,
                                    marginLeft: 20,
                                    marginRight: 20,
                                    marginBottom: 20,
                                    borderWidth: 1,
                                    backgroundColor: "#00b300",
                                    borderColor: "#00b300",
                                    borderRadius: 4
                                }}
                                onPress={() => this.UpdateAccident()}
                            >
                                <Text style={styles.buttonText}>Submit</Text>
                            </Button>
                        </View>
                    </Content>

                    <View style={{ width: 270, position: "absolute" }}>
                        <Dialog
                            visible={this.state.visibleDialogSubmit}
                            dialogTitle={<DialogTitle title="Updating Accident Report .." />}
                        >
                            <DialogContent>
                                {<ActivityIndicator size="large" color="#330066" animating />}
                            </DialogContent>
                        </Dialog>
                    </View>
                </View>
                <CustomFooter navigation={this.props.navigation} menu='Safety' />
            </Container>

        );
    }
}
