import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  TextInput,
  AsyncStorage,
  FlatList,
  Alert,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  ListItem,
  Body,
  Fab,
  Textarea,
  Switch,
  Icon,
  Picker,
  Form,
  Input,
  Label,
  Item
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import Moment from "moment";
import SwitchToggle from 'react-native-switch-toggle';

export default class UnitCheckPMKCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      visibleDialogSubmit:false,
      isLoading: true,
      tabNumber: "tab1",

      idVehicle:'',
      dataVehicle:'',
      save:false,
      shift: 1,
      pemanasan:0,
      spedometer:0,
      workingPressure:0,
      hourMeterMesin:0,
      hourMeterPompa:0,
      levelBBM:'R',
      oliMesin:'L',
      oliRem:'L',
      oliPower:'L',
      airRadiator:'R',
      airCadRadiator:'R',
      airWiper:'R',
      banDepanKiri:0,
      banDepanKanan:0,
      banBelakangKiriDalam:0,
      banBelakangKiriLuar:0,
      banBelakangKananDalam:0,
      banBelakangKananLuar:0,
      lampuCabinCheck:false,
      lampuKotaCheck:false,
      lampuJauhCheck:false,
      lampuSeinKiriCheck:false,
      lampuSeinKananCheck:false,
      lampuRemCheck:false,
      lampuAtretCheck:false,
      lampuSorotCheck:false,
      panelDashboardCheck:false,
      kacaSpionKiriCheck:false,
      kacaSpionKananCheck:false,
      sirineCheck:false,
      acsesorisCheck:false,
      dongkrakCheck:false,
      stangKabinCheck:false,
      ganjalBanCheck:false,
      kunciRodaCheck:false,
      hammerCheck:false,
      kotakPPPKCheck:false,
      lampuCabin:'',
      lampuKota:'',
      lampuJauh:'',
      lampuSeinKiri:'',
      lampuSeinKanan:'',
      lampuRem:'',
      lampuAtret:'',
      lampuSorot:'',
      panelDashboard:'',
      kacaSpionKiri:'',
      kacaSpionKanan:'',
      sirine:'',
      acsesoris:'',
      dongkrak:'',
      stangKabin:'',
      ganjalBan:'',
      kunciRoda:'',
      hammer:'',
      kotakPPPK:'',
      note:'',
      hose25:0,
      hose15:0,
      hoseSection:0,
      kunciHose:0,
      nozzle15JS:0,
      nozzle15Akron:0,
      nozzle15Jet:0,
      nozzleFoam:0,
      yValve:0,
      red25:0,
      kunciHydrant:0,
      apar:0,
      kapak:0,
      linggis:0,
      tanggaGanda:0,
      senter:0,
      gantol:0,
      timba:0,
      karung:0,
      jacketPMK:0,
      helmPMK:0,
      pic_badge:'',
      pic_name:'',
    };
  }

  onPressLampuCabin = () => {
    this.setState({ lampuCabinCheck: !this.state.lampuCabinCheck });
  }
  onPressLampuKota = () => {
    this.setState({ lampuKotaCheck: !this.state.lampuKotaCheck });
  }
  onPressLampuJauh = () => {
    this.setState({ lampuJauhCheck: !this.state.lampuJauhCheck });
  }
  onPressLampuSeinKiri = () => {
    this.setState({ lampuSeinKiriCheck: !this.state.lampuSeinKiriCheck });
  }
  onPressLampuSeinKanan = () => {
    this.setState({ lampuSeinKananCheck: !this.state.lampuSeinKananCheck });
  }
  onPressLampuRem = () => {
    this.setState({ lampuRemCheck: !this.state.lampuRemCheck });
  }
  onPressLampuAtret = () => {
    this.setState({ lampuAtretCheck: !this.state.lampuAtretCheck });
  }
  onPressLampuSorot = () => {
    this.setState({ lampuSorotCheck: !this.state.lampuSorotCheck });
  }
  onPressPanelDashboard = () => {
    this.setState({ panelDashboardCheck: !this.state.panelDashboardCheck });
  }
  onPressKacaSpionKiri = () => {
    this.setState({ kacaSpionKiriCheck: !this.state.kacaSpionKiriCheck });
  }
  onPressKacaSpionKanan = () => {
    this.setState({ kacaSpionKananCheck: !this.state.kacaSpionKananCheck });
  }
  onPressSirine = () => {
    this.setState({ sirineCheck: !this.state.sirineCheck });
  }
  onPressAcsesoris = () => {
    this.setState({ acsesorisCheck: !this.state.acsesorisCheck });
  }

  onPressDongkrak = () => {
    this.setState({ dongkrakCheck: !this.state.dongkrakCheck });
  }
  onPressStangKabin = () => {
    this.setState({ stangKabinCheck: !this.state.stangKabinCheck });
  }
  onPressGanjalBan = () => {
    this.setState({ ganjalBanCheck: !this.state.ganjalBanCheck });
  }
  onPressKunciRoda = () => {
    this.setState({ kunciRodaCheck: !this.state.kunciRodaCheck });
  }
  onPressHammer = () => {
    this.setState({ hammerCheck: !this.state.hammerCheck });
  }
  onPressKotakPPPK = () => {
    this.setState({ kotakPPPKCheck: !this.state.kotakPPPKCheck });
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    AsyncStorage.getItem('idVehicle').then((idVehicle) => {
      this.setState({
        idVehicle: idVehicle,
      });
      this.loadVehicle();
    })

    AsyncStorage.getItem("token").then(token => {
      const url = GlobalConfig.SERVERHOST + 'validasi';
      var formData = new FormData();
      formData.append("token", token)
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            pic_name: responseJson.user.name,
            pic_badge: responseJson.user.no_badge,
            isloading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
    });


    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {

    });
  }

  loadVehicle() {
    this.setState({ visibleDialog: false, isLoading: true });
      const url = GlobalConfig.SERVERHOST + "getListVehicle";
      var formData = new FormData();
      formData.append("id_vehicle", this.state.idVehicle);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            dataVehicle: responseJson.data,
            isLoading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
  }



  validasiFieldTab1() {
    if (this.state.pemanasan == 0) {
      alert("Input Pemanasan");
    } else if (this.state.spedometer == 0) {
      alert("Input Spedometer");
    } else if (this.state.workingPressure == 0) {
      alert("Input Working Pressure");
    } else if (this.state.hourMeterMesin == 0) {
      alert("Input Hour Meter Machine");
    } else if (this.state.hourMeterPompa == 0) {
      alert("Input Hour Meter Pump");
    } else {
      this.setState({
        tabNumber: "tab2"
      });
    }
  }

  validasiFieldTab2() {
    if (this.state.banDepanKiri == 0) {
      alert("Input Tyre Pressure");
    } else if (this.state.banDepanKanan == 0) {
      alert("Input Tyre Pressure");
    } else if (this.state.banBelakangKiriDalam == 0) {
      alert("Input Tyre Pressure");
    } else if (this.state.banBelakangKiriLuar == 0) {
      alert("Input Tyre Pressure");
    } else if (this.state.banBelakangKananDalam == 0) {
      alert("Input Tyre Pressure");
    } else if (this.state.banBelakangKananLuar == 0) {
      alert("Input Tyre Pressure");
    } else {
      this.setState({
        tabNumber: "tab3"
      });
    }
  }

  validasiFieldTab3() {
    this.setState({
      tabNumber: "tab4"
    });
  }

  validasiCreate() {
    if(this.state.lampuCabinCheck == false){
      this.setState({lampuCabin: 'MATI'})
    } else {
      this.setState({lampuCabin: 'BAIK'})
    }
    if(this.state.lampuKotaCheck == false){
      this.setState({lampuKota: 'MATI'})
    } else {
      this.setState({lampuKota: 'BAIK'})
    }
    if(this.state.lampuJauhCheck == false){
      this.setState({lampuJauh: 'MATI'})
    } else {
      this.setState({lampuJauh: 'BAIK'})
    }
    if(this.state.lampuSeinKiriCheck == false){
      this.setState({lampuSeinKiri: 'MATI'})
    } else {
      this.setState({lampuSeinKiri: 'BAIK'})
    }
    if(this.state.lampuSeinKananCheck == false){
      this.setState({lampuSeinKanan: 'MATI'})
    } else {
      this.setState({lampuSeinKanan: 'BAIK'})
    }
    if(this.state.lampuRemCheck == false){
      this.setState({lampuRem: 'MATI'})
    } else {
      this.setState({lampuRem: 'BAIK'})
    }
    if(this.state.lampuAtretCheck == false){
      this.setState({lampuAtret: 'MATI'})
    } else {
      this.setState({lampuAtret: 'BAIK'})
    }
    if(this.state.lampuSorotCheck == false){
      this.setState({lampuSorot: 'MATI'})
    } else {
      this.setState({lampuSorot: 'BAIK'})
    }
    if(this.state.panelDashboardCheck == false){
      this.setState({panelDashboard: 'MATI'})
    } else {
      this.setState({panelDashboard: 'BAIK'})
    }
    if(this.state.kacaSpionKiriCheck == false){
      this.setState({kacaSpionKiri: 'PECAH'})
    } else {
      this.setState({kacaSpionKiri: 'BAIK'})
    }
    if(this.state.kacaSpionKananCheck == false){
      this.setState({kacaSpionKanan: 'PECAH'})
    } else {
      this.setState({kacaSpionKanan: 'BAIK'})
    }
    if(this.state.sirineCheck == false){
      this.setState({sirine: 'MATI'})
    } else {
      this.setState({sirine: 'BAIK'})
    }
    if(this.state.acsesorisCheck == false){
      this.setState({acsesoris: 'MATI'})
    } else {
      this.setState({acsesoris: 'BAIK'})
    }
    if(this.state.dongkrakCheck == false){
      this.setState({dongkrak: 'TIDAK'})
    } else {
      this.setState({dongkrak: 'ADA'})
    }
    if(this.state.stangKabinCheck == false){
      this.setState({stangKabin: 'TIDAK'})
    } else {
      this.setState({stangKabin: 'ADA'})
    }
    if(this.state.ganjalBanCheck == false){
      this.setState({ganjalBan: 'TIDAK'})
    } else {
      this.setState({ganjalBan: 'ADA'})
    }
    if(this.state.kunciRodaCheck == false){
      this.setState({kunciRoda: 'TIDAK'})
    } else {
      this.setState({kunciRoda: 'ADA'})
    }
    if(this.state.hammerCheck == false){
      this.setState({hammer: 'TIDAK'})
    } else {
      this.setState({hammer: 'ADA'})
    }
    if(this.state.kotakPPPKCheck == false){
      this.setState({kotakPPPK: 'TIDAK'})
    } else {
      this.setState({kotakPPPK: 'ADA'})
    }
    this.setState({
      save:true,
    });
  }

  createInspection(){
    this.setState({
      visibleDialogSubmit:true,
      save:false,
    });
        var url = GlobalConfig.SERVERHOST + "addInspectionPMK";
        var formData = new FormData();
        formData.append("vehicle_id", this.state.idVehicle);
        formData.append("shift", this.state.shift);
        formData.append("pemanasan", this.state.pemanasan);
        formData.append("spedometer", this.state.spedometer);
        formData.append("level_bbm", this.state.levelBBM);
        formData.append("working_pressure", this.state.workingPressure);
        formData.append("hour_meter_mesin", this.state.hourMeterMesin);
        formData.append("hour_meter_pompa", this.state.hourMeterPompa);
        formData.append("oli_mesin", this.state.oliMesin);
        formData.append("oli_rem", this.state.oliRem);
        formData.append("oli_power", this.state.oliPower);
        formData.append("air_radiator", this.state.airRadiator);
        formData.append("air_cad_radiator", this.state.airCadRadiator);
        formData.append("air_wiper", this.state.airWiper);
        formData.append("lampu_cabin", this.state.lampuCabin);
        formData.append("lampu_kota", this.state.lampuKota);
        formData.append("lampu_jauh", this.state.lampuJauh);
        formData.append("lampu_sign_kiri", this.state.lampuSeinKiri);
        formData.append("lampu_sign_kanan", this.state.lampuSeinKanan);
        formData.append("lampu_rem", this.state.lampuRem);
        formData.append("lampu_atret", this.state.lampuAtret);
        formData.append("lampu_sorot", this.state.lampuSorot);
        formData.append("panel_dashboard", this.state.panelDashboard);
        formData.append("ban_depan_kiri", this.state.banDepanKiri);
        formData.append("ban_depan_kanan", this.state.banDepanKiri);
        formData.append("ban_belakang_kiri_dalam", this.state.banBelakangKiriDalam);
        formData.append("ban_belakang_kiri_luar", this.state.banBelakangKiriLuar);
        formData.append("ban_belakang_kanan_dalam", this.state.banBelakangKananDalam);
        formData.append("ban_belakang_kanan_luar", this.state.banBelakangKananLuar);
        formData.append("kaca_spion_kiri", this.state.kacaSpionKiri);
        formData.append("kaca_spion_kanan", this.state.kacaSpionKanan);
        formData.append("dongkrak", this.state.dongkrak);
        formData.append("stang_kabin", this.state.stangKabin);
        formData.append("ganjal_ban", this.state.ganjalBan);
        formData.append("kunci_roda", this.state.kunciRoda);
        formData.append("hammer", this.state.hammer);
        formData.append("kotak_pppk", this.state.kotakPPPK);
        formData.append("hose_25", this.state.hose25);
        formData.append("hose_15", this.state.hose15);
        formData.append("nozzle_15_js", this.state.nozzle15JS);
        formData.append("nozzle_15_akron", this.state.nozzle15Akron);
        formData.append("nozzle_15_jet", this.state.nozzle15Jet);
        formData.append("y_valve", this.state.yValve);
        formData.append("red_25", this.state.red25);
        formData.append("nozzle_foam", this.state.nozzleFoam);
        formData.append("kunci_hydrant", this.state.kunciHydrant);
        formData.append("hose_section", this.state.hoseSection);
        formData.append("kunci_hose", this.state.kunciHose);
        formData.append("apar", this.state.apar);
        formData.append("kapak", this.state.kapak);
        formData.append("linggis", this.state.linggis);
        formData.append("tangga_ganda", this.state.tanggaGanda);
        formData.append("senter", this.state.senter);
        formData.append("gantol", this.state.gantol);
        formData.append("timba", this.state.timba);
        formData.append("karung", this.state.karung);
        formData.append("jacket_pmk", this.state.jacketPMK);
        formData.append("helm_pmk", this.state.helmPMK);
        formData.append("sirine", this.state.sirine);
        formData.append("acsesoris", this.state.acsesoris);
        formData.append("pic_badge", this.state.pic_badge);
        formData.append("pic_name", this.state.pic_name);
        formData.append("note", this.state.note);

        fetch(url, {
            headers: {
                "Content-Type": "multipart/form-data"
            },
            method: "POST",
            body: formData
        })
            .then(response => response.json())
            .then(response => {
                if (response.status == 200) {
                    this.setState({
                        visibleDialogSubmit:false
                    })
                    Alert.alert('Success', 'Create Inspection PMK Success', [{
                        text: 'Okay'
                    }])
                    this.props.navigation.navigate('UnitCheckDetailPMKEngine')
                } else {
                    this.setState({
                        visibleDialogSubmit:false
                    })
                    Alert.alert('Error', 'Create Inspection PMK Failed', [{
                        text: 'Okay'
                    }])
                }
            })
            .catch((error)=>{
                console.log(error)
                this.setState({
                    visibleDialogSubmit:false
                })
                Alert.alert('Error', 'Create Inspection PMK Failed', [{
                    text: 'Okay'
                }])
            })
  }

  render() {
    that = this;
    return (
      <Container style={styles.wrapper2}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            {this.state.tabNumber=='tab1' ? (
              <Button
                transparent
                onPress={() => this.props.navigation.navigate("UnitCheckDetailPMKEngine")}
              >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          ):this.state.tabNumber=='tab2' ? (
              <Button
                transparent
                onPress={() => this.setState({
                  tabNumber:'tab1'
                })}
              >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          ):this.state.tabNumber=='tab3' ?(
              <Button
                transparent
                onPress={() => this.setState({
                  tabNumber:'tab2'
                })}
              >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
            ):(
              <Button
                transparent
                onPress={() => this.setState({
                  tabNumber:'tab3'
                })}
              >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          )}
          </Left>
          <Body style={{flex:3, alignItems:'center'}}>
              <Title style={styles.textbody}>{this.state.dataVehicle.vehicle_code}</Title>
          </Body>
          <Right style={{flex:1}}>
            {this.state.tabNumber=='tab1' ? (
              <Button
                transparent
                onPress={() => this.validasiFieldTab1()}
              >
                <Text style={{color:colors.white, fontWeight:'bold'}}>Next</Text>
              </Button>
            ):this.state.tabNumber=='tab2' ?(
              <Button
                transparent
                onPress={() => this.validasiFieldTab2()}
              >
                <Text style={{color:colors.white, fontWeight:'bold'}}>Next</Text>
              </Button>
            ):this.state.tabNumber=='tab3' ? (
              <Button
                transparent
                onPress={() => this.validasiFieldTab3()}
              >
                <Text style={{color:colors.white, fontWeight:'bold'}}>Next</Text>
              </Button>
            ):(
              <Button
                transparent
                onPress={() => this.validasiCreate()}
              >
                <Icon
                  name="ios-checkmark"
                  style={{fontSize:40, color:colors.white}}
                />
              </Button>
            )}
          </Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ flex: 1 }}>
          <Content style={{ marginTop: 0, backgroundColor: colors.white }}>
            <View style={{ backgroundColor: colors.white }}>
              {this.state.tabNumber == "tab1" ? (
                <View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.white
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "25%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Inspection
                        </Text>
                      </View>
                      <View style={{ width: "25%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.graydar,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text
                          style={{
                            fontSize: 10,
                            fontWeight: "bold",
                            color: colors.graydar
                          }}
                        >
                          Condition PMK
                        </Text>
                      </View>
                      <View style={{ width: "25%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.graydar,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text
                          style={{
                            fontSize: 10,
                            fontWeight: "bold",
                            color: colors.graydar,
                          }}
                        >
                          Sparepart
                        </Text>
                      </View>
                      <View style={{ width: "25%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.graydar,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text
                          style={{
                            fontSize: 10,
                            fontWeight: "bold",
                            color: colors.graydar
                          }}
                        >
                          Condition Tools
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:0, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={styles.titleInput}>Vehicle Code</Text>
                      </View>
                      <TextInput
                        style={{
                          height: 40,
                          marginLeft: 5,
                          marginRight: 5,
                          marginBottom: 5,
                          fontSize: 11,
                          marginTop:5,
                          borderWidth: 1,
                          borderColor: colors.graydar,
                          borderRadius:5
                        }}
                        value={this.state.dataVehicle.vehicle_code}
                        bordered
                        editable={false}
                        placeholder="Vehicle Code"
                      />
                    </View>
                  </View>
                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:10, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={styles.titleInput}>Shift *</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginRight: 5,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: "100%", height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.shift}
                            onValueChange={(itemValue) => this.setState({shift:itemValue})}>
                              <Picker.Item label="Shift I (Satu)" value="1" />
                              <Picker.Item label="Shift II (Dua)" value="2" />
                              <Picker.Item label="Shift III (Tiga)" value="3" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </View>
                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:10, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width:'33%' }}>
                          <Text style={styles.titleInput}>Pemanasan (Minute)</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            value={this.state.pemanasan}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ pemanasan: text })
                            }
                          />
                      </View>
                      <View style={{ width:'33%' }}>
                          <Text style={styles.titleInput}>Spedometer (km)</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            value={this.state.spedometer}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ spedometer: text })
                            }
                          />
                      </View>
                      <View style={{ width:'33%' }}>
                          <Text style={styles.titleInput}>Level BBM</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 35
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ width: 100, height: 35 }}
                              placeholder="Select Shift ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.levelBBM}
                              onValueChange={(itemValue) => this.setState({levelBBM:itemValue})}>
                                <Picker.Item label="R" value="R" />
                                <Picker.Item label="1/4" value="1/4" />
                                <Picker.Item label="1/2" value="1/2" />
                                <Picker.Item label="3/4" value="3/4" />
                                <Picker.Item label="F" value="F" />
                            </Picker>
                          </Form>
                      </View>
                    </View>
                  </View>

                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:10, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width:'33%' }}>
                          <Text style={styles.titleInput}>Working Pressure        (Bar)</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            value={this.state.workingPressure}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ workingPressure: text })
                            }
                          />
                      </View>
                      <View style={{ width:'33%' }}>
                          <Text style={styles.titleInput}>Hour Meter Machine (Hour)</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            value={this.state.hourMeterMesin}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ hourMeterMesin: text })
                            }
                          />
                      </View>
                      <View style={{ width:'33%' }}>
                          <Text style={styles.titleInput}>Hour Meter Pump (Hour)</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            value={this.state.hourMeterPompa}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ hourMeterPompa: text })
                            }
                          />
                      </View>
                    </View>
                  </View>
                </View>
              ) : this.state.tabNumber == "tab2" ? (
                <View>
                <CardItem
                  style={{
                    borderRadius: 0,
                    marginTop: 0,
                    backgroundColor: colors.white
                  }}
                >
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ width: "25%", alignItems: "center" }}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{
                          color: colors.graydar,
                          fontSize: 20,
                          paddingLeft: 8
                        }}
                      />
                      <Text style={{ fontSize: 10, fontWeight: "bold", color: colors.graydar }}>
                        Inspection
                      </Text>
                    </View>
                    <View style={{ width: "25%", alignItems: "center" }}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{
                          color: colors.green01,
                          fontSize: 20,
                          paddingLeft: 8
                        }}
                      />
                      <Text
                        style={{
                          fontSize: 10,
                          fontWeight: "bold",
                        }}
                      >
                        Condition PMK
                      </Text>
                    </View>
                    <View style={{ width: "25%", alignItems: "center" }}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{
                          color: colors.graydar,
                          fontSize: 20,
                          paddingLeft: 8
                        }}
                      />
                      <Text
                        style={{
                          fontSize: 10,
                          fontWeight: "bold",
                          color: colors.graydar,
                        }}
                      >
                        Sparepart
                      </Text>
                    </View>
                    <View style={{ width: "25%", alignItems: "center" }}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{
                          color: colors.graydar,
                          fontSize: 20,
                          paddingLeft: 8
                        }}
                      />
                      <Text
                        style={{
                          fontSize: 10,
                          fontWeight: "bold",
                          color: colors.graydar
                        }}
                      >
                        Condition Tools
                      </Text>
                    </View>
                  </View>
                </CardItem>
                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:0, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={{fontSize: 12, fontWeight: "bold"}}>Condition Oil</Text>
                      </View>
                    </View>
                  </View>
                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:0, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Oil Machine</Text>
                        <Form
                          style={{ borderWidth: 1, borderRadius: 5, marginTop: 5, marginRight: 5, marginLeft: 5, borderColor: "#E6E6E6", height: 35 }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.oliMesin}
                            onValueChange={itemValue =>
                              this.setState({ oliMesin: itemValue })
                            }
                          >
                            <Picker.Item label="L" value="L" />
                            <Picker.Item label="M" value="M" />
                            <Picker.Item label="H" value="H" />
                          </Picker>
                        </Form>
                      </View>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Oil Brake</Text>
                        <Form
                          style={{ borderWidth: 1, borderRadius: 5, marginTop: 5, marginRight: 5, marginLeft: 5, borderColor: "#E6E6E6", height: 35 }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.oliRem}
                            onValueChange={itemValue =>
                              this.setState({ oliRem: itemValue })
                            }
                          >
                            <Picker.Item label="L" value="L" />
                            <Picker.Item label="M" value="M" />
                            <Picker.Item label="H" value="H" />
                          </Picker>
                        </Form>
                      </View>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Oil Power Steering</Text>
                        <Form
                          style={{ borderWidth: 1, borderRadius: 5, marginTop: 5, marginRight: 5, marginLeft: 5, borderColor: "#E6E6E6", height: 35 }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.oliPower}
                            onValueChange={itemValue =>
                              this.setState({ oliPower: itemValue })
                            }
                          >
                            <Picker.Item label="L" value="L" />
                            <Picker.Item label="M" value="M" />
                            <Picker.Item label="H" value="H" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </View>

                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:15, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={{fontSize: 12, fontWeight: "bold"}}>Condition Water</Text>
                      </View>
                    </View>
                  </View>
                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:0, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Radiator Water</Text>
                        <Form
                          style={{ borderWidth: 1, borderRadius: 5, marginTop: 5, marginRight: 5, marginLeft: 5, borderColor: "#E6E6E6", height: 35 }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.airRadiator}
                            onValueChange={itemValue =>
                              this.setState({ airRadiator: itemValue })
                            }
                          >
                            <Picker.Item label="R" value="R" />
                            <Picker.Item label="1/4" value="1/4" />
                            <Picker.Item label="1/2" value="1/2" />
                            <Picker.Item label="3/4" value="3/4" />
                            <Picker.Item label="F" value="F" />
                          </Picker>
                        </Form>
                      </View>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Radiator Water Cad</Text>
                        <Form
                          style={{ borderWidth: 1, borderRadius: 5, marginTop: 5, marginRight: 5, marginLeft: 5, borderColor: "#E6E6E6", height: 35 }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.airCadRadiator}
                            onValueChange={itemValue =>
                              this.setState({ airCadRadiator: itemValue })
                            }
                          >
                            <Picker.Item label="R" value="R" />
                            <Picker.Item label="1/4" value="1/4" />
                            <Picker.Item label="1/2" value="1/2" />
                            <Picker.Item label="3/4" value="3/4" />
                            <Picker.Item label="F" value="F" />
                          </Picker>
                        </Form>
                      </View>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Wiper Water</Text>
                        <Form
                          style={{ borderWidth: 1, borderRadius: 5, marginTop: 5, marginRight: 5, marginLeft: 5, borderColor: "#E6E6E6", height: 35 }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.airWiper}
                            onValueChange={itemValue =>
                              this.setState({ airWiper: itemValue })
                            }
                          >
                            <Picker.Item label="R" value="R" />
                            <Picker.Item label="1/4" value="1/4" />
                            <Picker.Item label="1/2" value="1/2" />
                            <Picker.Item label="3/4" value="3/4" />
                            <Picker.Item label="F" value="F" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </View>

                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:15, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={{fontSize: 12, fontWeight: "bold"}}>Condition Tyre Pressure (Bar)</Text>
                      </View>
                    </View>
                  </View>

                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:10, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Left Front</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.banDepanKiri}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ banDepanKiri: text })
                            }
                          />
                      </View>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Right Front</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.banDepanKanan}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ banDepanKanan: text })
                            }
                          />
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Back Left Outside</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.banBelakangKiriLuar}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ banBelakangKiriLuar: text })
                            }
                          />
                      </View>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Back Right Outside</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.banBelakangKananLuar}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ banBelakangKananLuar: text })
                            }
                          />
                      </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Back Left Inside</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.banBelakangKiriDalam}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ banBelakangKiriDalam: text })
                            }
                          />
                      </View>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Back Right Inside</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.banBelakangKananDalam}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ banBelakangKananDalam: text })
                            }
                          />
                      </View>
                    </View>
                  </View>
                </View>
              ) : this.state.tabNumber == "tab3" ? (
                <View>
                <CardItem
                  style={{
                    borderRadius: 0,
                    marginTop: 0,
                    backgroundColor: colors.white
                  }}
                >
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ width: "25%", alignItems: "center" }}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{
                          color: colors.graydar,
                          fontSize: 20,
                          paddingLeft: 8
                        }}
                      />
                      <Text style={{ fontSize: 10, fontWeight: "bold", color:colors.graydar }}>
                        Inspection
                      </Text>
                    </View>
                    <View style={{ width: "25%", alignItems: "center" }}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{
                          color: colors.graydar,
                          fontSize: 20,
                          paddingLeft: 8
                        }}
                      />
                      <Text
                        style={{
                          fontSize: 10,
                          fontWeight: "bold",
                          color: colors.graydar
                        }}
                      >
                        Condition PMK
                      </Text>
                    </View>
                    <View style={{ width: "25%", alignItems: "center" }}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{
                          color: colors.green01,
                          fontSize: 20,
                          paddingLeft: 8
                        }}
                      />
                      <Text
                        style={{
                          fontSize: 10,
                          fontWeight: "bold",
                        }}
                      >
                        Sparepart
                      </Text>
                    </View>
                    <View style={{ width: "25%", alignItems: "center" }}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{
                          color: colors.graydar,
                          fontSize: 20,
                          paddingLeft: 8
                        }}
                      />
                      <Text
                        style={{
                          fontSize: 10,
                          fontWeight: "bold",
                          color: colors.graydar,
                        }}
                      >
                        Condition Tools
                      </Text>
                    </View>
                  </View>
                </CardItem>
                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:0, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={{fontSize: 12, fontWeight: "bold"}}>Condition Lamp</Text>
                      </View>
                    </View>
                  </View>
                  <View style={{ borderRadius: 0, marginLeft:10, marginRight:15, marginTop:0, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "column" }}>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-airplane" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Cabin
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.lampuCabinCheck}
                            onPress={this.onPressLampuCabin}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-airplane" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Kota
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.lampuKotaCheck}
                            onPress={this.onPressLampuKota}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-airplane" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Jauh
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.lampuJauhCheck}
                            onPress={this.onPressLampuJauh}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-airplane" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Sein Kiri
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.lampuSeinKiriCheck}
                            onPress={this.onPressLampuSeinKiri}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-airplane" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Sein Kanan
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.lampuSeinKananCheck}
                            onPress={this.onPressLampuSeinKanan}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-airplane" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Rem
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.lampuRemCheck}
                            onPress={this.onPressLampuRem}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-airplane" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Atret
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.lampuAtretCheck}
                            onPress={this.onPressLampuAtret}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-airplane" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Sorot
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.lampuSorotCheck}
                            onPress={this.onPressLampuSorot}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-airplane" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Panel Dashboard
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.panelDashboardCheck}
                            onPress={this.onPressPanelDashboard}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                    </View>
                  </View>

                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:15, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={{fontSize: 12, fontWeight: "bold"}}>Condition Rear-View Mirror</Text>
                      </View>
                    </View>
                  </View>
                  <View style={{ borderRadius: 0, marginLeft:10, marginRight:15, marginTop:0, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "column" }}>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-contrast" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Kaca Spion Kiri
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.kacaSpionKiriCheck}
                            onPress={this.onPressKacaSpionKiri}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-contrast" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Kaca Spion Kanan
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.kacaSpionKananCheck}
                            onPress={this.onPressKacaSpionKanan}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                    </View>
                  </View>
                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:15, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={{fontSize: 12, fontWeight: "bold"}}>Tools PMK</Text>
                      </View>
                    </View>
                  </View>
                  <View style={{ borderRadius: 0, marginLeft:10, marginRight:15, marginBottom:20, arginTop:0, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "column" }}>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-contrast" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Sirine
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.sirineCheck}
                            onPress={this.onPressSirine}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-contrast" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Acsesoris
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.acsesorisCheck}
                            onPress={this.onPressAcsesoris}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                    </View>
                  </View>
                </View>
              ):(
                <View>
                <CardItem
                  style={{
                    borderRadius: 0,
                    marginTop: 0,
                    backgroundColor: colors.white
                  }}
                >
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ width: "25%", alignItems: "center" }}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{
                          color: colors.graydar,
                          fontSize: 20,
                          paddingLeft: 8
                        }}
                      />
                      <Text style={{ fontSize: 10, fontWeight: "bold", color: colors.graydar, }}>
                        Inspection
                      </Text>
                    </View>
                    <View style={{ width: "25%", alignItems: "center" }}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{
                          color: colors.graydar,
                          fontSize: 20,
                          paddingLeft: 8
                        }}
                      />
                      <Text
                        style={{
                          fontSize: 10,
                          fontWeight: "bold",
                          color: colors.graydar
                        }}
                      >
                        Condition PMK
                      </Text>
                    </View>
                    <View style={{ width: "25%", alignItems: "center" }}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{
                          color: colors.graydar,
                          fontSize: 20,
                          paddingLeft: 8
                        }}
                      />
                      <Text
                        style={{
                          fontSize: 10,
                          fontWeight: "bold",
                          color: colors.graydar,
                        }}
                      >
                        Sparepart
                      </Text>
                    </View>
                    <View style={{ width: "25%", alignItems: "center" }}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{
                          color: colors.green01,
                          fontSize: 20,
                          paddingLeft: 8
                        }}
                      />
                      <Text
                        style={{
                          fontSize: 10,
                          fontWeight: "bold",
                          color: colors.green01
                        }}
                      >
                        Condition Tools
                      </Text>
                    </View>
                  </View>
                </CardItem>
                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:0, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={{fontSize: 12, fontWeight: "bold"}}>Tools</Text>
                      </View>
                    </View>
                  </View>
                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:0, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Kunci Hydrant</Text>
                        <Textarea
                          style={{
                            marginLeft: 5,
                            marginRight: 55,
                            marginBottom: 5,
                            borderRadius: 5,
                            fontSize: 11,
                            width: 100
                          }}
                          value={this.state.kunciHydrant}
                          keyboardType="numeric"
                          rowSpan={1.5}
                          bordered
                          placeholder="Ex : 0"
                          onChangeText={text =>
                            this.setState({ kunciHydrant: text })
                          }
                        />
                      </View>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Apar</Text>
                        <Textarea
                          style={{
                            marginLeft: 5,
                            marginRight: 55,
                            marginBottom: 5,
                            borderRadius: 5,
                            fontSize: 11,
                            width: 100
                          }}
                          value={this.state.apar}
                          keyboardType="numeric"
                          rowSpan={1.5}
                          bordered
                          placeholder="Ex : 0"
                          onChangeText={text =>
                            this.setState({ apar: text })
                          }
                        />
                      </View>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Kapak</Text>
                        <Textarea
                          style={{
                            marginLeft: 5,
                            marginRight: 55,
                            marginBottom: 5,
                            borderRadius: 5,
                            fontSize: 11,
                            width: 100
                          }}
                          value={this.state.kapak}
                          keyboardType="numeric"
                          rowSpan={1.5}
                          bordered
                          placeholder="Ex : 0"
                          onChangeText={text =>
                            this.setState({ kapak: text })
                          }
                        />
                      </View>
                    </View>
                  </View>

                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:0, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Linggis</Text>
                        <Textarea
                          style={{
                            marginLeft: 5,
                            marginRight: 55,
                            marginBottom: 5,
                            borderRadius: 5,
                            fontSize: 11,
                            width: 100
                          }}
                          value={this.state.linggis}
                          keyboardType="numeric"
                          rowSpan={1.5}
                          bordered
                          placeholder="Ex : 0"
                          onChangeText={text =>
                            this.setState({ linggis: text })
                          }
                        />
                      </View>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Tangga Ganda</Text>
                        <Textarea
                          style={{
                            marginLeft: 5,
                            marginRight: 55,
                            marginBottom: 5,
                            borderRadius: 5,
                            fontSize: 11,
                            width: 100
                          }}
                          value={this.state.tanggaGanda}
                          keyboardType="numeric"
                          rowSpan={1.5}
                          bordered
                          placeholder="Ex : 0"
                          onChangeText={text =>
                            this.setState({ tanggaGanda: text })
                          }
                        />
                      </View>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Senter</Text>
                        <Textarea
                          style={{
                            marginLeft: 5,
                            marginRight: 55,
                            marginBottom: 5,
                            borderRadius: 5,
                            fontSize: 11,
                            width: 100
                          }}
                          value={this.state.senter}
                          keyboardType="numeric"
                          rowSpan={1.5}
                          bordered
                          placeholder="Ex : 0"
                          onChangeText={text =>
                            this.setState({ senter: text })
                          }
                        />
                      </View>
                    </View>
                  </View>

                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:0, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Gantol / Pengait</Text>
                        <Textarea
                          style={{
                            marginLeft: 5,
                            marginRight: 55,
                            marginBottom: 5,
                            borderRadius: 5,
                            fontSize: 11,
                            width: 100
                          }}
                          value={this.state.gantol}
                          keyboardType="numeric"
                          rowSpan={1.5}
                          bordered
                          placeholder="Ex : 0"
                          onChangeText={text =>
                            this.setState({ gantol: text })
                          }
                        />
                      </View>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Timba</Text>
                        <Textarea
                          style={{
                            marginLeft: 5,
                            marginRight: 55,
                            marginBottom: 5,
                            borderRadius: 5,
                            fontSize: 11,
                            width: 100
                          }}
                          value={this.state.timba}
                          keyboardType="numeric"
                          rowSpan={1.5}
                          bordered
                          placeholder="Ex : 0"
                          onChangeText={text =>
                            this.setState({ timba: text })
                          }
                        />
                      </View>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Karung</Text>
                        <Textarea
                          style={{
                            marginLeft: 5,
                            marginRight: 55,
                            marginBottom: 5,
                            borderRadius: 5,
                            fontSize: 11,
                            width: 100
                          }}
                          value={this.state.karung}
                          keyboardType="numeric"
                          rowSpan={1.5}
                          bordered
                          placeholder="Ex : 0"
                          onChangeText={text =>
                            this.setState({ karung: text })
                          }
                        />
                      </View>
                    </View>
                  </View>

                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:0, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Jacket PMK</Text>
                        <Textarea
                          style={{
                            marginLeft: 5,
                            marginRight: 55,
                            marginBottom: 5,
                            borderRadius: 5,
                            fontSize: 11,
                            width: 100
                          }}
                          value={this.state.jacketPMK}
                          keyboardType="numeric"
                          rowSpan={1.5}
                          bordered
                          placeholder="Ex : 0"
                          onChangeText={text =>
                            this.setState({ jacketPMK: text })
                          }
                        />
                      </View>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Helm PMK</Text>
                        <Textarea
                          style={{
                            marginLeft: 5,
                            marginRight: 55,
                            marginBottom: 5,
                            borderRadius: 5,
                            fontSize: 11,
                            width: 100
                          }}
                          value={this.state.helmPMK}
                          keyboardType="numeric"
                          rowSpan={1.5}
                          bordered
                          placeholder="Ex : 0"
                          onChangeText={text =>
                            this.setState({ helmPMK: text })
                          }
                        />
                      </View>
                      <View style={{width:'33%'}}>

                      </View>
                    </View>
                  </View>

                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:15, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={{fontSize: 12, fontWeight: "bold"}}>Condition Tyre Pressure (Bar)</Text>
                      </View>
                    </View>
                  </View>

                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:10, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Hose 2.5</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.hose25}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ hose25: text })
                            }
                          />
                      </View>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Hose 1.5</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.hose15}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ hose15: text })
                            }
                          />
                      </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Hose Section 4</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.hoseSection}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ hoseSection: text })
                            }
                          />
                      </View>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Kunci Hose Section 4</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.kunciHose}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ kunciHose: text })
                            }
                          />
                      </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Nozzle 1.5 J/S</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.nozzle15JS}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ nozzle15JS: text })
                            }
                          />
                      </View>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Nozzle 1.5 Akron</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.nozzle15Akron}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ nozzle15Akron: text })
                            }
                          />
                      </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Nozzle 1.5 Jet</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.nozzle15Jet}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ nozzle15Jet: text })
                            }
                          />
                      </View>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Nozzle Foam</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.nozzleFoam}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ nozzleFoam: text })
                            }
                          />
                      </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Y Valve</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.yValve}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ yValve: text })
                            }
                          />
                      </View>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Red 2.5 to 1.5</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.red25}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ red25: text })
                            }
                          />
                      </View>
                    </View>
                  </View>

                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:15, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={{fontSize: 12, fontWeight: "bold"}}>Tools Seet</Text>
                      </View>
                    </View>
                  </View>
                  <View style={{ borderRadius: 0, marginLeft:10, marginRight:15, marginTop:0, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "column" }}>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-construct" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Dongkrak
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.dongkrakCheck}
                            onPress={this.onPressDongkrak}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-construct" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Stang Kabin
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.stangKabinCheck}
                            onPress={this.onPressStangKabin}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-construct" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Ganjal Ban
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.ganjalBanCheck}
                            onPress={this.onPressGanjalBan}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-construct" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Kunci Roda
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.kunciRodaCheck}
                            onPress={this.onPressKunciRoda}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-construct" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Hammer
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.hammerCheck}
                            onPress={this.onPressHammer}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-construct" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Kotak PPPK
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.kotakPPPKCheck}
                            onPress={this.onPressKotakPPPK}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                    </View>
                  </View>
                  <View style={{ borderRadius: 0, marginLeft:10, marginRight:15, marginTop:5, marginBottom:20, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "column" }}>
                      <View style={{ marginLeft: 10, flex: 2 }}>
                        <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                          Note
                        </Text>
                      </View>
                      <View>
                        <Textarea
                          style={{
                            marginLeft: 5,
                            marginRight: 5,
                            marginBottom: 5,
                            borderRadius: 5,
                            fontSize: 11
                          }}
                          rowSpan={2}
                          bordered
                          value={this.state.note}
                          placeholder="Type something .. "
                          onChangeText={text =>
                            this.setState({ note: text })
                          }
                        />
                      </View>
                    </View>
                  </View>
                </View>
              )}
            </View>
          </Content>
        </View>

        <View style={{ width: '100%', position: "absolute"}}>
          <Dialog
            visible={this.state.save}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog, width:300}}
            onTouchOutside={() => {
              this.setState({ login: false });
            }}
          >
          <DialogContent>
          {
            <View>
              <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
                <Text style={{fontSize:15, alignItems:'center', color:colors.black}}>Do you want to save this data ?</Text>
              </View>
              <View style={{flexDirection:'row', flex:1, paddingTop:10, width:'100%'}}>
                <View style={{width:'50%', paddingRight:5}}>
                    <Button
                      block
                      style={{
                        width:'100%',
                        marginTop:10,
                        height: 35,
                        marginBottom: 5,
                        borderWidth: 0,
                        backgroundColor: colors.primer,
                        borderRadius: 15
                      }}
                      onPress={() => this.createInspection()}
                    >
                      <Text style={{color:colors.white}}>Yes</Text>
                    </Button>
                </View>
                <View style={{width:'50%', paddingLeft:5}}>
                    <Button
                      block
                      style={{
                        width:'100%',
                        marginTop:10,
                        height: 35,
                        marginBottom: 5,
                        borderWidth: 0,
                        backgroundColor: colors.primer,
                        borderRadius: 15
                      }}
                      onPress={() => this.setState({
                        save:false,
                      })}
                    >
                      <Text style={{color:colors.white}}>No</Text>
                    </Button>
                </View>
              </View>
            </View>
          }
          </DialogContent>
          </Dialog>
        </View>
        <View style={{ width: 270, position: "absolute" }}>
            <Dialog
                visible={this.state.visibleDialogSubmit}
            >
                <DialogContent>
                {
                  <View>
                    <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
                      <Text style={{fontSize:15, alignItems:'center', color:colors.black}}>Creating Inpection PMK ...</Text>
                    </View>
                    <ActivityIndicator size="large" color="#330066" animating />
                  </View>
                }
                </DialogContent>
            </Dialog>
        </View>
      </Container>
    );
  }
}
