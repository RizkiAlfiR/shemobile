import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Switch,
  ListItem
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import SwitchToggle from "react-native-switch-toggle";
import Ripple from "react-native-material-ripple";
import ListViewReportMonth from "../../components/ListViewReportMonth";
import ListViewReportWeek from "../../components/ListViewReportWeek";

class ListItemDetail extends React.PureComponent {
  navigateToScreen(route, dataDetail) {
    AsyncStorage.setItem("dataDetail", JSON.stringify(dataDetail)).then(() => {
      that.props.navigation.navigate(route);
    });
  }
  render() {
    return (
      <View>
        {this.props.data != null && (
          <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
            <Ripple
              style={{
                flex: 2,
                justifyContent: "center",
                alignItems: "center"
              }}
              rippleSize={176}
              rippleDuration={600}
              rippleContainerBorderRadius={15}
              onPress={() => this.navigateToScreen("InspectionReportAlarmUpdate",this.props.data)}
              rippleColor={colors.accent}
            >
              <View style={{ marginTop: 0, marginLeft: 5, marginRight: 5}}>
                <ListViewReportWeek
                  typeKondisi={this.props.data.TYPE_KONDISI}
                  kondisi={this.props.data.KONDISI_VALUE}
                  periode={this.props.data.TYPE_PERIODE}
                  parameter={this.props.data.PARAM_NAME}
                  status={this.props.data.STATUS_COND}
                  note={this.props.data.NOTE}
                  year={this.props.data.YEAR}
                  month={this.props.data.MONTH}
                  dateCek={this.props.data.TANGGAL_CEK_AKHIR}
                />
              </View>
            </Ripple>
          </Card>
        )}
        </View>
    );
  }
}

class ListItemParameterWeek extends React.PureComponent {
  navigateToScreen(route, dataDetail) {
    AsyncStorage.setItem("dataDetail", JSON.stringify(dataDetail)).then(() => {
      that.props.navigation.navigate(route);
    });
  }
  _renderItemDetail = ({ item }) => <ListItemDetail data={item} />;
  render() {
    return (
      <FlatList
        horizontal={false}
        data={this.props.data.DETAIL}
        renderItem={this._renderItemDetail}
        keyExtractor={(item, index) => index.toString()}
      />
    );
  }
}

class ListItemParameterMonth extends React.PureComponent {
  navigateToScreen(route, dataDetail) {
    AsyncStorage.setItem("dataDetail", JSON.stringify(dataDetail)).then(() => {
      that.props.navigation.navigate(route);
    });
  }
  render() {
    return (
      <View>
        {this.props.data.DETAIL.BULAN != null && (
          <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
            <Ripple
              style={{
                flex: 2,
                justifyContent: "center",
                alignItems: "center"
              }}
              rippleSize={176}
              rippleDuration={600}
              rippleContainerBorderRadius={15}
              onPress={() => this.navigateToScreen("InspectionReportAlarmUpdate",this.props.data.DETAIL.BULAN)}
              rippleColor={colors.accent}
            >
              <View style={{ marginTop: 0, marginLeft: 5, marginRight: 5 }}>
                <ListViewReportMonth
                  typeKondisi={this.props.data.DETAIL.BULAN.TYPE_KONDISI}
                  kondisi={this.props.data.DETAIL.BULAN.KONDISI_VALUE}
                  periode={this.props.data.DETAIL.BULAN.TYPE_PERIODE}
                  parameter={this.props.data.DETAIL.BULAN.PARAM_NAME}
                  status={this.props.data.DETAIL.BULAN.STATUS_COND}
                  note={this.props.data.DETAIL.BULAN.NOTE}
                  year={this.props.data.DETAIL.BULAN.YEAR}
                  month={this.props.data.DETAIL.BULAN.MONTH}
                  dateCek={this.props.data.DETAIL.BULAN.TANGGAL_CEK_AKHIR}
                />
              </View>
            </Ripple>
          </Card>
        )}
        </View>
    );
  }
}

class InspectionReportAlarmHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      dataDetail: [],
      isLoading: true,
      listReportAlarm: [],
      listParameterWeek: [],
      listParameterMonth: [],
      idLokasi: "",
      typePeriode: "",
      paramID: "",
      visibleDialogSubmit: false,
      periode: ""
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItemParameterWeek = ({ item }) => (
    <ListItemParameterWeek data={item} />
  );

  _renderItemParameterMonth = ({ item }) => (
    <ListItemParameterMonth data={item} />
  );

  componentDidMount() {
    AsyncStorage.getItem("idLokasi").then(idLokasi => {
      AsyncStorage.getItem("periode").then(periode => {
        this.setState({ idLokasi: idLokasi, periode: periode });
        this.setState({ isLoading: false });
        console.log(this.state.idLokasi);
        console.log(this.state.periode);
      });
    });
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadData();
      }
    );
  }

  loadData() {
    this.loadParameterWeek();
    this.loadParameterMonth();
  }

  loadParameterWeek() {
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST +
        "api/v_mobile/firesystem/alarm_n/view_parameter_type/WEEK";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("periode", this.state.periode);
      formData.append("param_id", this.state.idLokasi);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            listParameterWeek: responseJson.data,
            isLoading: false
          });
        });
    });
  }

  loadParameterMonth() {
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST +
        "api/v_mobile/firesystem/alarm_n/view_parameter_type/MONTH";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("periode", this.state.periode);
      formData.append("param_id", this.state.idLokasi);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            listParameterMonth: responseJson.data,
            isLoading: false
          });
        });
    });
  }

  konfirmasideleteAlarm(kondisi, id) {
    Alert.alert(
      "Apakah Anda Yakin Ingin Menghapus Inspection Report Alarm",
      kondisi,
      [
        { text: "Yes", onPress: () => this.deleteAlarm(id) },
        { text: "Cancel" }
      ],
      { cancelable: false }
    );
  }

  navigateToScreen(route, listReport) {
    AsyncStorage.setItem("listReport", JSON.stringify(listReport)).then(() => {
      this.props.navigation.navigate(route);
    });
    //alert(JSON.stringify(listReport))
  }

  deleteAlarm(id) {
    this.setState({
      visibleDialogSubmit: true
    });
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST +
        "api/v_mobile/firesystem/alarm_n/m_log_cek_alarm/delete";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("ID", id);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.Status == 200) {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Success", "Delete Success", [
              {
                text: "Oke"
              }
            ]);
            this.props.navigation.navigate("InspectionReportAlarm");
          } else {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Error", "Delete Failed", [
              {
                text: "Oke"
              }
            ]);
          }
        });
    });
  }

  _renderItemParameterWeek = ({ item }) => (
    <ListItemParameterWeek data={item} />
  );

  _renderItemParameterMonth = ({ item }) => (
    <ListItemParameterMonth data={item} />
  );
  render() {
    var that = this;
    return (
      <Container style={styles.wrapper}>
          <Header style={styles.header}>
              <Left style={{ flex: 1 }}>
                <Button
                  transparent
                  onPress={() =>
                    that.props.navigation.navigate("InspectionReportAlarmDetail")
                  }
                >
                  <Icon
                    name="ios-arrow-back"
                    size={20}
                    style={styles.facebookButtonIconOrder2}
                  />
                </Button>
              </Left>
              <Body style={{ flex: 3, alignItems: "center" }}>
                <Title style={styles.textbody}>History
                </Title>
              </Body>
              <Right style={{ flex: 1 }}>

              </Right>
            </Header>
            <StatusBar
              backgroundColor={colors.green03}
              barStyle="light-content"
            />
            <ScrollView>
              <Content style={{ marginTop: 5 }}>
                  <View style={{ marginTop: 10,flex:1,flexDirection:'column' }}>
                      <FlatList
                          horizontal={false}
                          data={this.state.listParameterWeek}
                          renderItem={this._renderItemParameterWeek}
                          keyExtractor={(item, index) => index.toString()}
                      />
                  </View>
                  <View style={{ marginTop: 10,flex:1,flexDirection:'column' }}>
                      <FlatList
                          horizontal={false}
                          data={this.state.listParameterMonth}
                          renderItem={this._renderItemParameterMonth}
                          keyExtractor={(item, index) => index.toString()}
                        />
                  </View>
                <View style={{ width: 270, position: "absolute" }}>
                  <Dialog
                    visible={this.state.visibleDialogSubmit}
                    dialogTitle={
                      <DialogTitle title="Delete Inspection Report Alarm .." />
                    }
                  >
                    <DialogContent>
                      {
                        <ActivityIndicator
                          size="large"
                          color="#330066"
                          animating
                        />
                      }
                    </DialogContent>
                  </Dialog>
                </View>
              </Content>
            </ScrollView>
      </Container>
    );
  }
}

export default InspectionReportAlarmHistory;
