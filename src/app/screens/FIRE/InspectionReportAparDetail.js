import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator,
  RefreshControl
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import Ripple from "react-native-material-ripple";
import Moment from "moment";

class InspectionReportAparDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataLokasi: [],
      isLoading: true,
      listReportApar: [],
      idApar: "",
      idReport: "",
      hose: false,
      seal: false,
      sapot: false,
      kondisiHose: "",
      kondisiSeal: "",
      kondisiSapot: "",
      visibleDialogSubmit: false,
      periode: "",
      exp_inspection:"",
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => (
    <ListItem data={item} navigation={this.props.navigation} />
  );

  componentDidMount() {
    AsyncStorage.getItem("idLokasi").then(idLokasi => {
        this.setState({
          idApar: idLokasi,
          isLoading: false,
        });
        this.loadLokasi();
    });
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadData();

      }
    );
  }

  loadLokasi() {
    this.setState({ visibleDialog: false, isLoading: true });
      const url = GlobalConfig.SERVERHOST + "getLokasiApar";
      var formData = new FormData();
      formData.append("id_apar", this.state.idApar);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            dataLokasi: responseJson.data,
            isLoading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
  }

  loadData() {
    AsyncStorage.getItem("token").then(value => {
      const url = GlobalConfig.SERVERHOST + "detailInspectionApar";
      var formData = new FormData();
      formData.append("id_apar", this.state.idApar);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            listReportApar: responseJson.data,
            isLoading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  navigateToScreen(route, listReportApar) {
    AsyncStorage.setItem("listReportApar", JSON.stringify(listReportApar)).then(() => {
        this.props.navigation.navigate(route);
      }
    );
  }

  navigateToScreenCreate(route, idApar) {
    AsyncStorage.setItem("idApar", idApar).then(() => {
        this.props.navigation.navigate(route);
        // alert(JSON.stringify(idApar))
      }
    );
  }

  render() {
    var that = this;
    var dateNow = Moment().format('YYYY-MM-DD');
    return (
      <Container style={styles.wrapper2}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() =>
                that.props.navigation.navigate("InspectionReportApar")
              }
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3, alignItems: "center" }}>
            <Title style={styles.textbody}>
              {this.state.dataLokasi.area}
            </Title>
          </Body>
          <Right style={{ flex: 1 }}>
            <View style={{ flex: 0, flexDirection: "row" }}>
              {dateNow <= this.state.dataLokasi.exp_inspection && (
              <Button
                transparent
                onPress={() =>
                  this.navigateToScreen("InspectionReportAparUpdate", this.state.listReportApar)
                }
              >
                <Icon
                  name="ios-create"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            )}
            </View>
          </Right>
        </Header>
        <StatusBar
          backgroundColor={colors.green03}
          barStyle="light-content"
        />
        <View style={{marginTop:10, marginLeft:10, marginRight:10, backgroundColor:colors.white, height:90, marginBottom:5, borderRadius:10, paddingLeft:5, paddingRight:10, paddingTop:5,
            shadowColor:'#000',
            shadowOffset:{
              width:0,
              height:1,
            },
            shadowOpacity:1,
            shadowRadius:2,
            elevation:3,
          }}>
          <View style={{flex:1, flexDirection:'row'}}>
            <View style={{width:'20%'}}>
              <View style={{width:'100%', paddingTop:3, paddingBottom:5}}>
                <Image
                  style={{ marginTop:0, width: '100%', height: '100%', resizeMode: "contain", borderRadius:5}}
                  source={require("../../../assets/images/fire.png")}
                />
              </View>
            </View>
            <View style={{width:'80%', justifyContent:'center'}}>
              <Text style={{ fontSize: 12, fontWeight: "bold" }}>
                {this.state.dataLokasi.area}
              </Text>
              <Text style={{ fontSize: 10 }}>Area</Text>
              <Text style={{ fontSize: 12, paddingTop: 5, fontWeight: "bold" }}>
                {this.state.dataLokasi.unit_kerja}
              </Text>
              <Text style={{ fontSize: 10 }}>
                Unit Kerja
              </Text>
            </View>
          </View>
        </View>
        <ScrollView>
          <Content>
            <View style={{paddingLeft:10, paddingTop:10, paddingBottom:10}}>
                <Text style={{ fontSize: 10 }}>Status</Text>
                {dateNow <= this.state.dataLokasi.exp_inspection ? (
                  <Text
                    style={{
                      fontSize: 10,
                      fontWeight: "bold",
                      color: colors.green01
                    }}
                  >
                    CHECKED
                  </Text>
                ) : (
                  <Text
                    style={{
                      fontSize: 10,
                      fontWeight: "bold",
                      color: "#FF0101"
                    }}
                  >
                    UNCECKED
                  </Text>
                )}
            </View>

            {dateNow <= this.state.dataLokasi.exp_inspection && this.state.listReportApar!=null ? (
              <View>
                <View style={{marginTop:10, marginLeft:10, marginRight:10, backgroundColor:colors.white, height:50, marginBottom:5, borderRadius:10, paddingLeft:5, paddingRight:10,
                  shadowColor:'#000',
                  shadowOffset:{
                    width:0,
                    height:1,
                  },
                  shadowOpacity:1,
                  shadowRadius:2,
                  elevation:3,
                }}>
                  <View style={{flex:1, flexDirection:'row'}}>
                    <View style={{width:'13%', justifyContent:'center'}}>
                      <Icon
                        name="ios-contact"
                        style={{fontSize:40, color:colors.graydark}}
                      />
                    </View>
                    <View style={{width:'87%', justifyContent:'center'}}>
                      <Text style={{ fontSize: 11, fontWeight: "bold" }}>
                        {this.state.listReportApar.pic_badge}
                      </Text>
                      <Text style={{ fontSize: 13,}}>
                        {this.state.listReportApar.pic_name}
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={{flex:1, paddingLeft:10, paddingRight:10, flexDirection:'row', marginTop:10}}>
                  <View style={{width:'40%'}}>
                    <View style={{backgroundColor:colors.white, height:100, marginBottom:5, borderRadius:10, marginRight:5, paddingTop:5,
                        shadowColor:'#000',
                        shadowOffset:{
                          width:0,
                          height:1,
                        },
                        shadowOpacity:1,
                        shadowRadius:2,
                        elevation:3,
                        alignItems:'center'
                      }}>
                        <Icon
                          name="ios-today"
                          style={{fontSize:40, color:colors.green0}}
                        />
                        <Text style={{fontSize:9}}>Klem</Text>
                        {this.state.listReportApar.klem=='K' ? (
                          <Text style={{fontSize:25, fontWeight:'bold'}}>Klem</Text>
                        ):this.state.listReportApar.klem=='T' ? (
                          <Text style={{fontSize:25, fontWeight:'bold'}}>Tanduk</Text>
                        ):this.state.listReportApar.klem=='U' ? (
                          <Text style={{fontSize:25, fontWeight:'bold'}}>U Klem</Text>
                        ):this.state.listReportApar.klem=='B' ? (
                          <Text style={{fontSize:25, fontWeight:'bold'}}>Box</Text>
                        ):(
                          <Text style={{fontSize:25, fontWeight:'bold'}}>Duduk</Text>
                        )}
                    </View>
                  </View>
                  <View style={{width:'30%'}}>
                    <View style={{backgroundColor:colors.white, height:100, marginLeft:5, marginBottom:5, borderRadius:10, marginRight:5, paddingTop:5,
                        shadowColor:'#000',
                        shadowOffset:{
                          width:0,
                          height:1,
                        },
                        shadowOpacity:1,
                        shadowRadius:2,
                        elevation:3,
                        alignItems:'center'
                      }}>
                        <Icon
                          name="ios-speedometer"
                          style={{fontSize:40, color:colors.green0}}
                        />
                        <Text style={{fontSize:9}}>Press</Text>
                        <Text style={{fontSize:25, fontWeight:'bold'}}>{this.state.listReportApar.press}</Text>
                    </View>
                  </View>
                  <View style={{width:'30%'}}>
                    <View style={{backgroundColor:colors.white, height:100, marginBottom:5, borderRadius:10, marginLeft:5, paddingTop:5,
                        shadowColor:'#000',
                        shadowOffset:{
                          width:0,
                          height:1,
                        },
                        shadowOpacity:1,
                        shadowRadius:2,
                        elevation:3,
                        alignItems:'center'
                      }}>
                        <Icon
                          name="ios-cube"
                          style={{fontSize:40, color:colors.green0}}
                        />
                        <Text style={{fontSize:9}}>Weight</Text>
                        <Text style={{fontSize:25, fontWeight:'bold'}}>{this.state.listReportApar.weight}</Text>
                    </View>
                  </View>
                </View>
                <CardItem style={{ borderRadius: 0 }}>
                  <View style={{ flexDirection: "row" }}>
                    <View style={{ flex: 2 }}>
                      <Text style={{ fontSize: 10 }}>Inspection Date</Text>
                      <Text style={{ fontSize: 13, fontWeight: "bold" }}>
                        {Moment(this.state.listReportApar.inspection_date).format('DD MMMM YYYY')}
                      </Text>
                    </View>
                    <View style={{ flex: 1 }}>
                      <Text style={{ fontSize: 10 }}>Hose</Text>
                        <Text style={{ fontSize: 13, fontWeight: "bold" }}>
                          {this.state.listReportApar.hose}
                        </Text>
                    </View>
                    <View style={{ flex: 1 }}>
                      <Text style={{ fontSize: 10 }}>Seal</Text>
                        <Text style={{ fontSize: 13, fontWeight: "bold" }}>
                          {this.state.listReportApar.seal}
                        </Text>
                    </View>
                    <View style={{ flex: 1 }}>
                      <Text style={{ fontSize: 10 }}>Sapot</Text>
                        <Text style={{ fontSize: 13, fontWeight: "bold" }}>
                          {this.state.listReportApar.sapot}
                        </Text>
                    </View>
                    <View style={{ flex: 1 }}>
                      <Text style={{ fontSize: 10 }}>Kondisi</Text>
                      <Text style={{ fontSize: 13, fontWeight: "bold" }}>
                        {this.state.listReportApar.status_cond}
                      </Text>
                    </View>
                  </View>
                </CardItem>
                <View style={{marginTop:10, marginLeft:10, marginRight:10, backgroundColor:colors.white, height:100, marginBottom:5, borderRadius:10, paddingLeft:10, paddingRight:10, paddingTop:5,
                    shadowColor:'#000',
                    shadowOffset:{
                      width:0,
                      height:1,
                    },
                    shadowOpacity:1,
                    shadowRadius:2,
                    elevation:3,
                  }}>
                    <Text style={{fontSize:15, fontWeight:'bold'}}>Note :</Text>
                    <Text style={{fontSize:12}}>{this.state.listReportApar.note}</Text>
                </View>
              </View>
            ):(
              <Button
                block
                style={{
                  height: 45,
                  marginLeft: 20,
                  marginRight: 20,
                  marginBottom: 20,
                  borderWidth: 1,
                  backgroundColor: "#00b300",
                  borderColor: "#00b300",
                  borderRadius: 4
                }}
                onPress={() =>
                  this.navigateToScreenCreate("InspectionReportAparCreate", this.state.idApar)
                }
              >
                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: "bold",
                    color: colors.white
                  }}
                >
                  Create Inspection Apar
                </Text>
              </Button>
            )}
            <View style={{ width: 270, position: "absolute" }}>
              <Dialog
                visible={this.state.visibleDialogSubmit}
                dialogTitle={
                  <DialogTitle title="Delete Inspection Report Apar .." />
                }
              >
                <DialogContent>
                  {
                    <ActivityIndicator
                      size="large"
                      color="#330066"
                      animating
                    />
                  }
                </DialogContent>
              </Dialog>
            </View>
          </Content>
        </ScrollView>
      </Container>
    );
  }
}

export default InspectionReportAparDetail;
