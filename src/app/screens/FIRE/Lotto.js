import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator,
  RefreshControl,
  Dimensions
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Thumbnail,
  Form,
  Picker
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import ListViewLotto from "../../components/FireSystem/ListViewLotto";
import Ripple from "react-native-material-ripple";
import DatePicker from "react-native-datepicker";

class ListItem extends React.PureComponent {
  navigateToScreen(route, idLotto) {
    AsyncStorage.setItem("idLotto", JSON.stringify(idLotto)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View style={{marginLeft: 10, marginRight: 10, borderRadius: 5, backgroundColor:colors.white, marginBottom:10,
        shadowColor:'#000',
        shadowOffset:{
          width:0,
          height:2,
        },
        shadowOpacity:0.25,
        shadowRadius:3.84,
        elevation:2,
      }}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() => this.navigateToScreen("LottoDetail", this.props.data.id)}
          rippleColor={colors.accent}
        >
          <View>
              <ListViewLotto
                imageUri={require("../../../assets/images/report.png")}
                equipmentNumber={this.props.data.equipment_number}
                noER={this.props.data.no_er}
                area={this.props.data.location}
                kegiatan={this.props.data.activity_description}
                date={this.props.data.date}
                time={this.props.data.time}
              />
          </View>
        </Ripple>
      </View>
    );
  }
}

class Lotto extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isLoading: true,
      searchText:'',
      isEmpty:false,
      visibleDialog:false,
      filterDateStart: "",
      filterDateEnd: "",
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => (
    <ListItem data={item} navigation={this.props.navigation} />
  );

  onRefresh() {
    console.log("refreshing");
    this.setState({ isLoading: true }, function() {
      this.setState({
        searchText:''
      })
      this.loadData();
    });
  }

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.setState({
          searchText:''
        })
        this.loadData();
      }
    );
  }

  navigateToScreen(route, listLotto) {
    AsyncStorage.setItem("list", JSON.stringify(listLotto)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  searchData(){
    this.setState({
      isLoading: true
    });
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/firesystem/lotto/view";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("search", this.state.searchText);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          console.log(responseJson)
          if (responseJson.status===200){
            this.setState({
              dataSource: responseJson.data,
              isLoading: false
            });
            this.setState({
              isEmpty:false
            })
          }else{
            this.setState({
              dataSource:[],
              isEmpty:true
            })
            this.setState({
              isLoading:false
            })
          }

        })
        .catch(error => {
          this.setState({
            dataSource:[],
            isEmpty:true
          })
          this.setState({
            isLoading:false
          })
          console.log(error);
        });
    });
  }

  loadData() {
    this.setState({
      isLoading: true,
      visibleDialog:false,
    });
      const url = GlobalConfig.SERVERHOST + "getLotto";
      var formData = new FormData();
      formData.append("token", 'token');
      if (this.state.filterDateStart != "") {
        formData.append("start_date", this.state.filterDateStart);
      }
      if (this.state.filterDateEnd != "") {
        formData.append("end_date", this.state.filterDateEnd);
      }
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
            this.setState({
              dataSource: responseJson.data,
              isLoading: false
            });
        })
        .catch(error => {
          this.setState({
            isEmpty:true
          })
          this.setState({
            isLoading:false
          })
          console.log(error);
        });
  }

  onClickSearch() {
    this.setState({
      visibleDialog: true
    });
  }

  render() {
    var list;
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.dataSource=='') {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list =
        <FlatList
        data={this.state.dataSource}
        renderItem={this._renderItem}
        keyExtractor={(item, index) => index.toString()}
        refreshControl={
          <RefreshControl
            refreshing={this.state.isLoading}
            onRefresh={this.onRefresh.bind(this)}
          />}
      />
      }
    }
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("FireMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>Lotto</Title>
          </Body>
          <Right style={{flex:1}}>
            <Button transparent onPress={() => this.onClickSearch()}>
              <Icon
                name="ios-funnel"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ marginTop: 10 ,flex:1,flexDirection:'column'}}>{list}</View>
        <Fab
          active={this.state.active}
          direction="up"
          containerStyle={{}}
          style={{ backgroundColor: colors.green01,marginBottom: ((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?80:50 }}
          position="bottomRight"
          onPress={() => this.props.navigation.navigate("LottoCreate")}
        >
          <Icon name="ios-add" style={{ fontSize: 50, fontWeight: "bold" ,paddingTop: Platform.OS === 'ios' ? 25:null,}} />
        </Fab>
        <View style={{ width: 270, position: "absolute",}}>
          <Dialog
            visible={this.state.visibleDialog}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog }}
            onTouchOutside={() => {
              this.setState({ visibleDialog: false });
            }}
          >
            <DialogContent>
              {
                <View>
                  <View style={{alignItems:'center', justifyContent:'center', paddingTop:20, width:'100%'}}>
                    <Text style={{fontSize:17, alignItems:'center', color:colors.black}}>Select sorting here</Text>
                  </View>
                  <View style={{flex:1, flexDirection:'row'}}>
                    <View style={{width:130}}>
                      <Text style={{ paddingTop: 10, fontSize: 10, marginBottom:5 }}>
                        Start Date
                      </Text>
                      <DatePicker
                        style={{ width: 120, fontSize: 10, borderRadius: 20 }}
                        date={this.state.filterDateStart}
                        mode="date"
                        placeholder="Choose"
                        format="YYYY-MM-DD"
                        minDate="2018-01-01"
                        maxDate="5000-12-31"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        iconSource='{this.timeIcon}'
                        customStyles={{
                          dateInput: {
                            marginLeft: 5,
                            marginRight: 5,
                            height: 35,
                            borderRadius: 5,
                            fontSize: 10,
                            borderWidth: 1,
                            borderColor: "#E6E6E6"
                          },
                          dateIcon: {
                            position: "absolute",
                            left: 0,
                            top: 5
                          }
                        }}
                        onDateChange={date => {
                          this.setState({ filterDateStart: date });
                        }}
                      />
                    </View>
                    <View style={{width:130}}>
                      <Text style={{ paddingTop: 10, fontSize: 10, marginBottom:5 }}>
                        End Date
                      </Text>
                      <DatePicker
                        style={{ width: 120, fontSize: 10, borderRadius: 20 }}
                        date={this.state.filterDateEnd}
                        mode="date"
                        placeholder="Choose"
                        format="YYYY-MM-DD"
                        minDate="2018-01-01"
                        maxDate="5000-12-31"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        iconSource='{this.timeIcon}'
                        customStyles={{
                          dateInput: {
                            marginLeft: 5,
                            marginRight: 5,
                            height: 35,
                            borderRadius: 5,
                            fontSize: 10,
                            borderWidth: 1,
                            borderColor: "#E6E6E6"
                          },
                          dateIcon: {
                            position: "absolute",
                            left: 0,
                            top: 5
                          }
                        }}
                        onDateChange={date => {
                          this.setState({ filterDateEnd: date });
                        }}
                      />
                    </View>
                  </View>


                  <View style={{paddingTop:10, width:'100%', alignItems:'center'}}>
                    <View style={{width:'40%', paddingRight:5}}>
                        <Button
                          block
                          style={{
                            width:'100%',
                            marginTop:10,
                            height: 35,
                            marginBottom: 5,
                            borderWidth: 0,
                            backgroundColor: colors.primer,
                            borderRadius: 15
                          }}
                          onPress={() => this.loadData()}
                        >
                          <Text style={{color:colors.white}}>Sort</Text>
                        </Button>
                    </View>
                  </View>
                </View>
              }
            </DialogContent>
          </Dialog>
        </View>
      </Container>
    );
  }
}

export default Lotto;
