import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Thumbnail,
  Textarea,
  Picker,
  Form
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import ListViewFireReport from "../../components/FireSystem/ListViewFireReport";
import Ripple from "react-native-material-ripple";
import DatePicker from "react-native-datepicker";

var that;
class ListItem extends React.PureComponent {
  navigateToScreen(route, id) {
    AsyncStorage.setItem("id", JSON.stringify(id)).then(() => {
      that.props.navigation.navigate(route);
    });
  }
  render() {
    return (
      <View style={{marginLeft: 10, marginRight: 10, borderRadius: 5, backgroundColor:colors.white, marginBottom:10,
        shadowColor:'#000',
        shadowOffset:{
          width:0,
          height:2,
        },
        shadowOpacity:0.25,
        shadowRadius:3.84,
        elevation:2,
      }}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.navigateToScreen("FireReportDetail", this.props.data.id)
          }
          rippleColor={colors.accent}
        >
          <View style={{ marginTop: 0, marginLeft: 5, marginRight: 5 }}>
            <ListViewFireReport
              imageUri={require("../../../assets/images/iconFire.png")}
              fireNumber={this.props.data.fire_number}
              place={this.props.data.incident_place}
              pic={this.props.data.pic_name}
              status={this.props.data.status}
              fireDate={this.props.data.fire_date}
              fireTime={this.props.data.fire_time}
            />
          </View>
        </Ripple>
      </View>
    );
  }
}

class FireReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isLoading: true,
      visibleDialog: false,
      fireReport:[],
      filterDateStart: "",
      filterDateEnd: "",
      delete:'',
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} />;

  onRefresh() {
    console.log("refreshing");
    this.setState({ isLoading: true }, function() {
      this.loadData();
    });
  }

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadData();
        this.setState({ isLoading: true });
      }
    );
  }

  loadData() {
    this.setState({ visibleDialog: false, isLoading: true });
      var url = GlobalConfig.SERVERHOST + "getFireReport";
      var formData = new FormData();
      formData.append();
      if (this.state.filterDateStart != "") {
        formData.append("start_date", this.state.filterDateStart);
      }
      if (this.state.filterDateEnd != "") {
        formData.append("end_date", this.state.filterDateEnd);
      }
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      }).then((response) => response.json())
        .then((responseJson) => {
            this.setState({
              fireReport: responseJson.data,
              isLoading: false
            });
            this.setState({
              visibleDialogSubmit:false
            })
        })
        .catch((error) => {
          Alert.alert('Cannot Log in', 'Check Your Internet Connection', [{
            text: 'Ok'
          }])
          this.setState({
            visibleDialogSubmit:false
          })
          console.log(error)
      })
  }

  onClickSearch() {
    this.setState({
      visibleDialog: true
    });
  }

  render() {
    that = this;
    var list;
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.fireReport == '') {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = (
          <FlatList
            data={this.state.fireReport}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index.toString()}
            refreshControl={
              <RefreshControl
                refreshing={this.state.isLoading}
                onRefresh={this.onRefresh.bind(this)}
              />
            }
          />
        );
      }
    }
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("FireMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3, alignItems: "center" }}>
            <Title style={styles.textbody}>Fire Report</Title>
          </Body>
          <Right style={{ flex: 1 }}>
            <Button transparent onPress={() => this.onClickSearch()}>
              <Icon
                name="ios-funnel"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ marginTop: 10, flex: 1, flexDirection: "column" }}>
          {list}
        </View>
        <Fab
          active={this.state.active}
          direction="up"
          containerStyle={{}}
          style={{ backgroundColor: colors.green01, marginBottom: 30 }}
          position="bottomRight"
          onPress={() => this.props.navigation.navigate("FireReportCreate")}
        >
          <Icon
            name="ios-add"
            style={{
              fontSize: 50,
              fontWeight: "bold",
              paddingTop: Platform.OS === "ios" ? 25 : null
            }}
          />
        </Fab>

        <View style={{ width: 270, position: "absolute",}}>
          <Dialog
            visible={this.state.visibleDialog}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog }}
            onTouchOutside={() => {
              this.setState({ visibleDialog: false });
            }}
          >
            <DialogContent>
              {
                <View>
                  <View style={{alignItems:'center', justifyContent:'center', paddingTop:20, width:'100%'}}>
                    <Text style={{fontSize:17, alignItems:'center', color:colors.black}}>Select sorting here</Text>
                  </View>
                  <View style={{flex:1, flexDirection:'row'}}>
                    <View style={{width:130}}>
                      <Text style={{ paddingTop: 10, fontSize: 10, marginBottom:5 }}>
                        Start Date
                      </Text>
                      <DatePicker
                        style={{ width: 120, fontSize: 10, borderRadius: 20 }}
                        date={this.state.filterDateStart}
                        mode="date"
                        placeholder="Choose"
                        format="YYYY-MM-DD"
                        minDate="2018-01-01"
                        maxDate="5000-12-31"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        iconSource='{this.timeIcon}'
                        customStyles={{
                          dateInput: {
                            marginLeft: 5,
                            marginRight: 5,
                            height: 35,
                            borderRadius: 5,
                            fontSize: 10,
                            borderWidth: 1,
                            borderColor: "#E6E6E6"
                          },
                          dateIcon: {
                            position: "absolute",
                            left: 0,
                            top: 5
                          }
                        }}
                        onDateChange={date => {
                          this.setState({ filterDateStart: date });
                        }}
                      />
                    </View>
                    <View style={{width:130}}>
                      <Text style={{ paddingTop: 10, fontSize: 10, marginBottom:5 }}>
                        End Date
                      </Text>
                      <DatePicker
                        style={{ width: 120, fontSize: 10, borderRadius: 20 }}
                        date={this.state.filterDateEnd}
                        mode="date"
                        placeholder="Choose"
                        format="YYYY-MM-DD"
                        minDate="2018-01-01"
                        maxDate="5000-12-31"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        iconSource='{this.timeIcon}'
                        customStyles={{
                          dateInput: {
                            marginLeft: 5,
                            marginRight: 5,
                            height: 35,
                            borderRadius: 5,
                            fontSize: 10,
                            borderWidth: 1,
                            borderColor: "#E6E6E6"
                          },
                          dateIcon: {
                            position: "absolute",
                            left: 0,
                            top: 5
                          }
                        }}
                        onDateChange={date => {
                          this.setState({ filterDateEnd: date });
                        }}
                      />
                    </View>
                  </View>


                  <View style={{paddingTop:10, width:'100%', alignItems:'center'}}>
                    <View style={{width:'40%', paddingRight:5}}>
                        <Button
                          block
                          style={{
                            width:'100%',
                            marginTop:10,
                            height: 35,
                            marginBottom: 5,
                            borderWidth: 0,
                            backgroundColor: colors.primer,
                            borderRadius: 15
                          }}
                          onPress={() => this.loadData()}
                        >
                          <Text style={{color:colors.white}}>Sort</Text>
                        </Button>
                    </View>
                  </View>
                </View>
              }
            </DialogContent>
          </Dialog>
        </View>
      </Container>
    );
  }
}

export default FireReport;
