import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Icon,
    Item,
    Input
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import Moment from 'moment';

class ListTools extends React.PureComponent {
  render() {
    return (
      <View style={{marginTop:10, backgroundColor:colors.white, height:30, marginBottom:5, borderRadius:10, paddingLeft:10, paddingRight:10, paddingTop:5,
          shadowColor:'#000',
          shadowOffset:{
            width:0,
            height:2,
          },
          shadowOpacity:1,
          shadowRadius:2,
          elevation:3,
      }}>
          <View style={{ marginTop: 0, marginLeft: 5, marginRight: 5 }}>
            <Text>{this.props.data}</Text>
          </View>
      </View>
    );
  }
}

class LottoDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      isLoading: true,
      listHeader:[],
      listLotto:[],
      listTools:[],
      visibleDialogSubmit:false,
      idLotto:'',
      dataLotto:[],
      delete:false,
    };
  }

  static navigationOptions = {
      header: null
  };

  _renderItem = ({ item }) => (
      <ListItem data={item}></ListItem>
  )

  navigateToScreen(route, idLotto) {
      AsyncStorage.setItem('idLotto', idLotto).then(() => {
          this.props.navigation.navigate(route);
      })
  }

  navigateToScreenUpdate(route, dataLotto) {
      AsyncStorage.setItem('dataLotto', JSON.stringify(dataLotto)).then(() => {
          this.props.navigation.navigate(route);
      })
  }

  componentDidMount() {
      AsyncStorage.getItem('idLotto').then((idLotto) => {
          this.setState({
            idLotto: idLotto,
            isLoading: false,
          });
          this.loadLotto();
      })
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
          this.loadLotto();
      });
  }

  loadLotto(){
    const url = GlobalConfig.SERVERHOST + "getLotto";
    var formData = new FormData();
    formData.append("id_lotto", this.state.idLotto);

    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.status==200){
          this.setState({
            dataLotto: responseJson.data,
            isLoading: false
          });
          this.setState({
            listTools: JSON.parse(this.state.dataLotto.safety_tools),
          })
        }
      })
      .catch(error => {
        this.setState({
          isEmpty:true
        })
        console.log(error);
      });
  }

  konfirmasideleteLotto() {
    this.setState({
      delete: true
    });
  }

  deleteLotto() {
      this.setState({
        visibleDialogSubmit:true,
        delete:false
      })
          const url = GlobalConfig.SERVERHOST + 'deleteLotto';
          var formData = new FormData();
          formData.append("id", this.state.dataLotto.id);

          fetch(url, {
              headers: {
                  "Content-Type": "multipart/form-data"
              },
              method: "POST",
              body: formData
          })
              .then(response => response.json())
              .then(response => {
                  if (response.status == 200) {
                      this.setState({
                        visibleDialogSubmit:false
                      })
                      Alert.alert('Success', 'Delete Success', [{
                          text: 'Oke',
                      }])
                      this.props.navigation.navigate("Lotto")
                  } else {
                      this.setState({
                        visibleDialogSubmit:false
                      })
                      Alert.alert('Error', 'Delete Failed', [{
                          text: 'Oke'
                      }])
                  }
              })
              .catch((error)=>{
                console.log(error)
                this.setState({
                  visibleDialogSubmit:false
                })
                Alert.alert('Error', 'Delete Failed', [{
                    text: 'Oke'
                }])
              })

  }

  _renderTools = ({ item }) => <ListTools data={item} />;

  render() {
    return (
      <Container style={styles.wrapper2}>
          <Header style={styles.header}>
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.goBack()}
              >
                <Icon
                  name="ios-arrow-back"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            </Left>
            <Body>
              <Title style={styles.textbody}>Detail Lotto</Title>
            </Body>
            <Right style={{width:'20%'}}>
              <Button
                transparent
                onPress={() => this.navigateToScreenUpdate('LottoUpdate', this.state.dataLotto)}
              >
                <Icon
                  name="ios-create"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
              <Button
                transparent
                onPress={() => this.konfirmasideleteLotto()}
              >
                <Icon
                  name="ios-trash"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
              <Button
                transparent
                onPress={() => this.navigateToScreen('LottoDrawIN', this.state.idLotto)}
              >
                <Icon
                  name="ios-key"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            </Right>
          </Header>
          <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
          <Content style={{marginTop:5}}>
          <View style={{marginLeft:15, marginRight:15, marginTop:10}}>
            <View style={{flex:1, flexDirection:'row'}}>
              <View style={{width:'70%'}}>
                <Text style={{fontSize:12}}>Nomor Equipment</Text>
                <Text style={{fontSize:13, fontWeight:"bold"}}>{this.state.dataLotto.equipment_number}</Text>
                <Text style={{fontSize:12, paddingTop:10}}>Nomor ER</Text>
                <Text style={{fontSize:13, fontWeight:"bold"}}>{this.state.dataLotto.no_er}</Text>
                <Text style={{fontSize:12, paddingTop:10}}>Area</Text>
                <Text style={{fontSize:13, fontWeight:"bold"}}>{this.state.dataLotto.location}</Text>
              </View>
              <View style={{width:'30%'}}>
                <Text style={{fontSize:13, fontWeight:"bold", textAlign:'right'}}>{Moment(this.state.dataLotto.date).format('DD MMMM YYYY')}</Text>
                <Text style={{fontSize:20, fontWeight:"bold", textAlign:'right'}}>{this.state.dataLotto.time}</Text>
              </View>
            </View>

            <Text style={{fontSize:12, paddingTop:10}}>Kegiatan</Text>
            <Text style={{fontSize:13, fontWeight:"bold"}}>{this.state.dataLotto.activity_description}</Text>
            <View style={{marginTop:10, backgroundColor:colors.white, height:100, marginBottom:5, borderRadius:10, paddingLeft:10, paddingRight:10, paddingTop:5,
                shadowColor:'#000',
                shadowOffset:{
                  width:0,
                  height:1,
                },
                shadowOpacity:1,
                shadowRadius:2,
                elevation:3,
              }}>
                <Text style={{fontSize:13, fontWeight:'bold'}}>Note :</Text>
                <Text style={{fontSize:12}}>{this.state.dataLotto.note}</Text>
            </View>
            <Text style={{fontSize:10, paddingTop:10}}>Safety Key</Text>
            <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.dataLotto.safety_key}</Text>
            <Text style={{fontSize:10, paddingTop:10}}>Safety Tools</Text>
            <FlatList
                data={this.state.listTools}
                renderItem={this._renderTools}
                keyExtractor={(item, index) => index.toString()}
            />

            <View style={{ width: 270, position: "absolute" }}>
              <Dialog
                visible={this.state.visibleDialogSubmit}
              >
                <DialogContent>
                  {
                    <View>
                    <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
                      <Text style={{fontSize:15, alignItems:'center', color:colors.black}}>Deleting Lotto ...</Text>
                    </View>
                    <ActivityIndicator size="large" color="#330066" animating />
                    </View>
                  }
                </DialogContent>
              </Dialog>
            </View>
          </View>

          <View style={{ width: '100%', position: "absolute"}}>
            <Dialog
              visible={this.state.delete}
              dialogAnimation={
                new SlideAnimation({
                  slideFrom: "bottom"
                })
              }
              dialogStyle={{ position: "absolute", top: this.state.posDialog, width:300}}
              onTouchOutside={() => {
                this.setState({ login: false });
              }}
            >
            <DialogContent>
            {
              <View>
                <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
                  <Text style={{fontSize:15, alignItems:'center', color:colors.black}}>Do you want to remove this data ?</Text>
                </View>
                <View style={{flexDirection:'row', flex:1, paddingTop:10, width:'100%'}}>
                  <View style={{width:'50%', paddingRight:5}}>
                      <Button
                        block
                        style={{
                          width:'100%',
                          marginTop:10,
                          height: 35,
                          marginBottom: 5,
                          borderWidth: 0,
                          backgroundColor: colors.primer,
                          borderRadius: 15
                        }}
                        onPress={() => this.deleteLotto()}
                      >
                        <Text style={{color:colors.white}}>Yes</Text>
                      </Button>
                  </View>
                  <View style={{width:'50%', paddingLeft:5}}>
                      <Button
                        block
                        style={{
                          width:'100%',
                          marginTop:10,
                          height: 35,
                          marginBottom: 5,
                          borderWidth: 0,
                          backgroundColor: colors.primer,
                          borderRadius: 15
                        }}
                        onPress={() => this.setState({
                          delete:false,
                        })}
                      >
                        <Text style={{color:colors.white}}>No</Text>
                      </Button>
                  </View>
                </View>
              </View>
            }
            </DialogContent>
            </Dialog>
          </View>
          </Content>
      </Container>
    );
  }
}

export default LottoDetail;
