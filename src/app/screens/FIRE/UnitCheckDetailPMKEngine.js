import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Picker,
  Content,
  Text,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Form,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Thumbnail,
  Item,
  Input,
  Switch,
  ListItem
} from "native-base";

import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import DatePicker from "react-native-datepicker";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import Moment from "moment";
import SwitchToggle from 'react-native-switch-toggle';

class UnitCheckDetailPMKEngine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataVehicleChecklist: [],
      isLoading: true,
      visibleFilter: false,
      headerUnitCheck: [],
      noPeg: "",
      visibleDialogSubmit: false,
      idVehicle:'',
      dataVehicle:[],
      listPMKEngine:[],
      lampuCabin:false,
      lampuKota:false,
      lampuJauh:false,
      lampuSignKiri:false,
      lampuSignKanan:false,
      lampuRem:false,
      lampuAtret:false,
      lampuSorot:false,
      panelDashboard:false,
      kacaSpionKiri:false,
      kacaSpionKanan:false,
      sirine:false,
      acsesoris:false,
    };
  }

  static navigationOptions = {
    header: null
  };

  navigateToScreen(route, idVehicle) {
    AsyncStorage.setItem("idVehicle", idVehicle).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  navigateToScreenUpdate(route, listReportPMK) {
    AsyncStorage.setItem("listReportPMK", JSON.stringify(listReportPMK)).then(() => {
        this.props.navigation.navigate(route);
        // alert(JSON.stringify(listReportPMK))
      }
    );
  }

  componentDidMount() {
    AsyncStorage.getItem("idVehicle").then(idVehicle => {
        this.setState({
          idVehicle: idVehicle,
          isLoading: false,
        });
        this.loadData();
        this.loadVehicle();
    });

    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadData();
        this.loadVehicle();
      }
    );
  }

  loadVehicle() {
    this.setState({ visibleDialog: false, isLoading: true });
      const url = GlobalConfig.SERVERHOST + "getListVehicle";
      var formData = new FormData();
      formData.append("id_vehicle", this.state.idVehicle);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            dataVehicle: responseJson.data,
            isLoading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
  }

  loadData() {
      const url = GlobalConfig.SERVERHOST + "detailInspectionPMK";
      var formData = new FormData();
      formData.append("vehicle_id", this.state.idVehicle);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            listPMKEngine: responseJson.data,
            isLoading: false
          });
          this.loadVerifikasi();
        })
        .catch(error => {
          console.log(error);
        });
  }

  loadVerifikasi(){
    if(this.state.listPMKEngine.lampu_cabin == 'BAIK'){
      this.setState({
        lampuCabin:true,
      })
    }
    if(this.state.listPMKEngine.lampu_kota == 'BAIK'){
      this.setState({
        lampuKota:true,
      })
    }
    if(this.state.listPMKEngine.lampu_jauh == 'BAIK'){
      this.setState({
        lampuJauh:true,
      })
    }
    if(this.state.listPMKEngine.lampu_sign_kiri == 'BAIK'){
      this.setState({
        lampuSignKiri:true,
      })
    }
    if(this.state.listPMKEngine.lampu_sign_kanan == 'BAIK'){
      this.setState({
        lampuSignKanan:true,
      })
    }
    if(this.state.listPMKEngine.lampu_rem == 'BAIK'){
      this.setState({
        lampuRem:true,
      })
    }
    if(this.state.listPMKEngine.lampu_atret == 'BAIK'){
      this.setState({
        lampuAtret:true,
      })
    }
    if(this.state.listPMKEngine.lampu_sorot == 'BAIK'){
      this.setState({
        lampuSorot:true,
      })
    }
    if(this.state.listPMKEngine.panel_dashboard == 'BAIK'){
      this.setState({
        panelDashboard:true,
      })
    }
    if(this.state.listPMKEngine.kaca_spion_kiri == 'BAIK'){
      this.setState({
        kacaSpionKiri:true,
      })
    }
    if(this.state.listPMKEngine.kaca_spion_kanan == 'BAIK'){
      this.setState({
        kacaSpionKanan:true,
      })
    }
    if(this.state.listPMKEngine.sirine == 'BAIK'){
      this.setState({
        sirine:true,
      })
    }
    if(this.state.listPMKEngine.acsesoris == 'BAIK'){
      this.setState({
        acsesoris:true,
      })
    }
  }

  render() {
    var dateNow = Moment().format('YYYY-MM-DD');
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
        list = (
          <ScrollView>
          <View style={{paddingLeft:10, paddingTop:10, paddingBottom:10}}>
              <Text style={{ fontSize: 10 }}>Status</Text>
              {dateNow <= this.state.dataVehicle.exp_inspection ? (
                <Text
                  style={{
                    fontSize: 10,
                    fontWeight: "bold",
                    color: colors.green01
                  }}
                >
                  CHECKED
                </Text>
              ) : (
                <Text
                  style={{
                    fontSize: 10,
                    fontWeight: "bold",
                    color: "#FF0101"
                  }}
                >
                  UNCECKED
                </Text>
              )}
          </View>

          {dateNow <= this.state.dataVehicle.exp_inspection && this.state.listPMKEngine!=null ? (
            <View>
              <View style={{marginTop:10, marginLeft:10, marginRight:10, backgroundColor:colors.white, height:50, marginBottom:5, borderRadius:10, paddingLeft:5, paddingRight:10,
                shadowColor:'#000',
                shadowOffset:{
                  width:0,
                  height:1,
                },
                shadowOpacity:1,
                shadowRadius:2,
                elevation:3,
              }}>
                <View style={{flex:1, flexDirection:'row'}}>
                  <View style={{width:'13%', justifyContent:'center'}}>
                    <Icon
                      name="ios-contact"
                      style={{fontSize:40, color:colors.graydark}}
                    />
                  </View>
                  <View style={{width:'87%', justifyContent:'center'}}>
                    <Text style={{ fontSize: 11, fontWeight: "bold" }}>
                      {this.state.listPMKEngine.pic_badge}
                    </Text>
                    <Text style={{ fontSize: 13,}}>
                      {this.state.listPMKEngine.pic_name}
                    </Text>
                  </View>
                </View>
              </View>
              <View style={{flex:1, paddingLeft:10, paddingRight:10, flexDirection:'row', marginTop:10}}>
                <View style={{width:'40%'}}>
                  <View style={{backgroundColor:colors.white, height:100, marginBottom:5, borderRadius:10, marginRight:5, paddingTop:5,
                      shadowColor:'#000',
                      shadowOffset:{
                        width:0,
                        height:1,
                      },
                      shadowOpacity:1,
                      shadowRadius:2,
                      elevation:3,
                      alignItems:'center'
                    }}>
                    <Image
                      style={{
                        height: 50,
                        width:80,
                        borderColor: colors.gray,
                        borderRadius: 10
                      }}
                      source={require("../../../assets/images/truckfire.jpg")}
                    />
                      <Text style={{fontSize:9}}>Negative List</Text>
                      <Text style={{fontSize:25, fontWeight:'bold'}}>{this.state.listPMKEngine.sum_negative}</Text>
                  </View>
                </View>
                <View style={{width:'30%'}}>
                  <View style={{backgroundColor:colors.white, justifyContent:'center', height:100, marginLeft:5, marginBottom:5, borderRadius:10, marginRight:5, paddingTop:5,
                      shadowColor:'#000',
                      shadowOffset:{
                        width:0,
                        height:1,
                      },
                      shadowOpacity:1,
                      shadowRadius:2,
                      elevation:3,
                      alignItems:'center'
                    }}>
                      <Text style={{fontSize:9}}>Date</Text>
                      <Text style={{fontSize:25, fontWeight:'bold'}}>{Moment(this.state.listPMKEngine.report_date).format('DD')}</Text>
                      <Text style={{fontSize:15,}}>{Moment(this.state.listPMKEngine.report_date).format('MMMM')}</Text>
                  </View>
                </View>
                <View style={{width:'30%'}}>
                  <View style={{backgroundColor:colors.white, height:100, marginBottom:5, borderRadius:10, marginLeft:5, paddingTop:5,
                      shadowColor:'#000',
                      shadowOffset:{
                        width:0,
                        height:1,
                      },
                      shadowOpacity:1,
                      shadowRadius:2,
                      elevation:3,
                      alignItems:'center'
                    }}>
                      <Icon
                        name="ios-contacts"
                        style={{fontSize:40, color:colors.green0}}
                      />
                      <Text style={{fontSize:9}}>Shift</Text>
                      <Text style={{fontSize:25, fontWeight:'bold'}}>{this.state.listPMKEngine.shift}</Text>
                  </View>
                </View>
              </View>

              <View style={{flex:1, flexDirection:'row', marginLeft:5}}>
                <View style={{width:'33%'}}>
                  <View style={{ marginLeft: 10, flex: 2 }}>
                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                      Pemanasan (Menit)
                    </Text>
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {this.state.listPMKEngine.pemanasan}
                    </Text>
                  </View>
                </View>
                <View style={{width:'33%'}}>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Spedometer (Km)
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listPMKEngine.spedometer}
                  </Text>
                </View>
                <View style={{width:'33%'}}>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Level BBM
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listPMKEngine.level_bbm}
                  </Text>
                </View>
              </View>

              <View style={{flex:1, flexDirection:'row', marginLeft:5}}>
                <View style={{width:'33%'}}>
                  <View style={{ marginLeft: 10, flex: 2 }}>
                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                      Working Pressure (Bar)
                    </Text>
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {this.state.listPMKEngine.working_pressure}
                    </Text>
                  </View>
                </View>
                <View style={{width:'33%'}}>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Hour Meter Mesin (Jam)
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listPMKEngine.hour_meter_mesin}
                  </Text>
                </View>
                <View style={{width:'33%'}}>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Hour Meter Pompa (Jam)
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listPMKEngine.hour_meter_pompa}
                  </Text>
                </View>
              </View>

              <View style={{flex:1, flexDirection:'row'}}>
                <Text style={{ fontSize: 10, marginLeft:15, paddingTop: 20, fontWeight: "bold"}}>Condition Oil :</Text>
              </View>

                <View style={{ flex: 1, flexDirection: "row", marginTop:10}}>
                  <View style={{ marginLeft: 15, flex: 2, width: "37%" }}>
                    <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                      Oli Mesin
                    </Text>
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKEngine.oli_mesin}</Text>
                  </View>
                  <View
                    style={{ flex: 2, marginRight: 20, marginLeft: 20, width: "34%" }}
                  >
                    <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                      Oli Rem
                    </Text>
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKEngine.oli_rem}</Text>
                  </View>
                  <View style={{ marginRight: 20, width: "30%" }}>
                    <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                      Oli Power Steering
                    </Text>
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKEngine.oli_power}</Text>
                  </View>
                </View>

              <View style={{flex:1, flexDirection:'row'}}>
                <Text style={{ fontSize: 10, marginLeft:15, paddingTop: 20, fontWeight: "bold"}}>Condition Oil :</Text>
              </View>

                <View style={{ flex: 1, flexDirection: "row", marginTop:10 }}>
                  <View style={{ marginLeft: 15, flex: 2, width: "33%" }}>
                    <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                      Air Radiator
                    </Text>
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKEngine.air_radiator}</Text>
                  </View>
                  <View
                    style={{ flex: 2, marginRight: 20, marginLeft: 20, width: "37%" }}
                  >
                    <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                      Cadangan Air Radiator
                    </Text>
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKEngine.air_cad_radiator}</Text>
                  </View>
                  <View style={{ marginRight: 20, width: "30%" }}>
                    <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                      Air Wiper
                    </Text>
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKEngine.air_wiper}</Text>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ marginLeft: 15, flex: 2, width: "33%" }}>
                    <Text
                      style={{ fontSize: 10, paddingTop: 20, fontWeight: "bold" }}
                    >
                      Condition Tekanan Ban (Bar) :
                    </Text>
                  </View>
                </View>
              <CardItem style={{ borderRadius: 0 }}>
                <View style={{ flex: 1, flexDirection: "column" }}>
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ marginLeft: 10, flex: 2, width: "30%" }}>
                      <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                        Ban Depan Kiri
                      </Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKEngine.ban_depan_kiri}</Text>
                    </View>
                    <View
                      style={{
                        flex: 2,
                        marginRight: 20,
                        marginLeft: 40,
                        width: "70%"
                      }}
                    >
                      <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                        Ban Depan Kanan
                      </Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKEngine.ban_depan_kanan}</Text>
                    </View>
                  </View>
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ marginLeft: 10, flex: 2, width: "30%" }}>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Ban Belakang Kiri Dalam
                      </Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKEngine.ban_belakang_kiri_dalam}</Text>
                    </View>
                    <View
                      style={{
                        flex: 2,
                        marginRight: 20,
                        marginLeft: 40,
                        width: "70%"
                      }}
                    >
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Ban Belakang Kiri Luar
                      </Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKEngine.ban_belakang_kiri_luar}</Text>
                    </View>
                  </View>
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ marginLeft: 10, flex: 2, width: "30%" }}>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Ban Belakang Kanan Dalam
                      </Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKEngine.ban_belakang_kanan_dalam}</Text>
                    </View>
                    <View
                      style={{
                        flex: 2,
                        marginRight: 20,
                        marginLeft: 40,
                        width: "70%"
                      }}
                    >
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Ban Belakang Kanan Luar
                      </Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKEngine.ban_belakang_kanan_luar}</Text>
                    </View>
                  </View>
                </View>
              </CardItem>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ marginLeft: 15, flex: 2, width: "33%" }}>
                    <Text
                      style={{ fontSize: 10, paddingTop: 10, fontWeight: "bold" }}
                    >
                      Condition Lampu-Lampu :
                    </Text>
                  </View>
                </View>
              <CardItem
                style={{
                  borderRadius: 10,
                  backgroundColor: colors.gray,
                  marginLeft: 30,
                  marginRight: 30,
                  marginTop:10,
                  paddingLeft:-10
                }}
              >
                <View style={{ flex: 1, flexDirection: "column" }}>
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-airplane" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Lampu Cabin
                      </Text>
                    </Body>
                    <Right style={{marginRight:-15}}>
                      <SwitchToggle
                        containerStyle={{
                          width: 70,
                          height: 30,
                          borderRadius: 25,
                          padding: 5,
                        }}
                        backgroundColorOff={colors.whitedar}
                        backgroundColorOn='#00b300'
                        circleStyle={{
                          width: 22,
                          height: 22,
                          borderRadius: 19,
                          backgroundColor: 'white',
                        }}
                        switchOn={this.state.lampuCabin}

                        circleColorOff='white'
                        circleColorOn='white'
                        duration={500}
                      />
                    </Right>
                  </ListItem>
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-airplane" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Lampu Kota
                      </Text>
                    </Body>
                    <Right style={{marginRight:-15}}>
                      <SwitchToggle
                        containerStyle={{
                          width: 70,
                          height: 30,
                          borderRadius: 25,
                          padding: 5,
                        }}
                        backgroundColorOff={colors.whitedar}
                        backgroundColorOn='#00b300'
                        circleStyle={{
                          width: 22,
                          height: 22,
                          borderRadius: 19,
                          backgroundColor: 'white',
                        }}
                        switchOn={this.state.lampuKota}
                        circleColorOff='white'
                        circleColorOn='white'
                        duration={500}
                      />
                    </Right>
                  </ListItem>
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-airplane" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Lampu Jauh
                      </Text>
                    </Body>
                    <Right style={{marginRight:-15}}>
                      <SwitchToggle
                        containerStyle={{
                          width: 70,
                          height: 30,
                          borderRadius: 25,
                          padding: 5,
                        }}
                        backgroundColorOff={colors.whitedar}
                        backgroundColorOn='#00b300'
                        circleStyle={{
                          width: 22,
                          height: 22,
                          borderRadius: 19,
                          backgroundColor: 'white',
                        }}
                        switchOn={this.state.lampuJauh}

                        circleColorOff='white'
                        circleColorOn='white'
                        duration={500}
                      />
                    </Right>
                  </ListItem>
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-airplane" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Lampu Sein Kiri
                      </Text>
                    </Body>
                    <Right style={{marginRight:-15}}>
                      <SwitchToggle
                        containerStyle={{
                          width: 70,
                          height: 30,
                          borderRadius: 25,
                          padding: 5,
                        }}
                        backgroundColorOff={colors.whitedar}
                        backgroundColorOn='#00b300'
                        circleStyle={{
                          width: 22,
                          height: 22,
                          borderRadius: 19,
                          backgroundColor: 'white',
                        }}
                        switchOn={this.state.lampuSignKiri}

                        circleColorOff='white'
                        circleColorOn='white'
                        duration={500}
                      />
                    </Right>
                  </ListItem>
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-airplane" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Lampu Sein Kanan
                      </Text>
                    </Body>
                    <Right style={{marginRight:-15}}>
                      <SwitchToggle
                        containerStyle={{
                          width: 70,
                          height: 30,
                          borderRadius: 25,
                          padding: 5,
                        }}
                        backgroundColorOff={colors.whitedar}
                        backgroundColorOn='#00b300'
                        circleStyle={{
                          width: 22,
                          height: 22,
                          borderRadius: 19,
                          backgroundColor: 'white',
                        }}
                        switchOn={this.state.lampuSignKanan}

                        circleColorOff='white'
                        circleColorOn='white'
                        duration={500}
                      />
                    </Right>
                  </ListItem>
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-airplane" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Lampu Rem
                      </Text>
                    </Body>
                    <Right style={{marginRight:-15}}>
                      <SwitchToggle
                        containerStyle={{
                          width: 70,
                          height: 30,
                          borderRadius: 25,
                          padding: 5,
                        }}
                        backgroundColorOff={colors.whitedar}
                        backgroundColorOn='#00b300'
                        circleStyle={{
                          width: 22,
                          height: 22,
                          borderRadius: 19,
                          backgroundColor: 'white',
                        }}
                        switchOn={this.state.lampuRem}

                        circleColorOff='white'
                        circleColorOn='white'
                        duration={500}
                      />
                    </Right>
                  </ListItem>
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-airplane" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Lampu Atret
                      </Text>
                    </Body>
                    <Right style={{marginRight:-15}}>
                      <SwitchToggle
                        containerStyle={{
                          width: 70,
                          height: 30,
                          borderRadius: 25,
                          padding: 5,
                        }}
                        backgroundColorOff={colors.whitedar}
                        backgroundColorOn='#00b300'
                        circleStyle={{
                          width: 22,
                          height: 22,
                          borderRadius: 19,
                          backgroundColor: 'white',
                        }}
                        switchOn={this.state.lampuAtret}

                        circleColorOff='white'
                        circleColorOn='white'
                        duration={500}
                      />
                    </Right>
                  </ListItem>
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-airplane" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Lampu Sorot
                      </Text>
                    </Body>
                    <Right style={{marginRight:-15}}>
                      <SwitchToggle
                        containerStyle={{
                          width: 70,
                          height: 30,
                          borderRadius: 25,
                          padding: 5,
                        }}
                        backgroundColorOff={colors.whitedar}
                        backgroundColorOn='#00b300'
                        circleStyle={{
                          width: 22,
                          height: 22,
                          borderRadius: 19,
                          backgroundColor: 'white',
                        }}
                        switchOn={this.state.lampuSorot}

                        circleColorOff='white'
                        circleColorOn='white'
                        duration={500}
                      />
                    </Right>
                  </ListItem>
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-airplane" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Panel Dashboard
                      </Text>
                    </Body>
                    <Right style={{marginRight:-15}}>
                      <SwitchToggle
                        containerStyle={{
                          width: 70,
                          height: 30,
                          borderRadius: 25,
                          padding: 5,
                        }}
                        backgroundColorOff={colors.whitedar}
                        backgroundColorOn='#00b300'
                        circleStyle={{
                          width: 22,
                          height: 22,
                          borderRadius: 19,
                          backgroundColor: 'white',
                        }}
                        switchOn={this.state.panelDashboard}

                        circleColorOff='white'
                        circleColorOn='white'
                        duration={500}
                      />
                    </Right>
                  </ListItem>
                </View>
              </CardItem>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ marginLeft: 15, flex: 2, width: "33%" }}>
                    <Text
                      style={{ fontSize: 10, paddingTop: 20, fontWeight: "bold" }}
                    >
                      Condition Kaca Spion :
                    </Text>
                  </View>
                </View>
              <CardItem
                style={{
                  borderRadius: 10,
                  backgroundColor: colors.gray,
                  marginLeft: 30,
                  marginRight: 30,
                  marginTop:10,
                  paddingLeft:-10,
                }}
              >
                <View style={{ flex: 1, flexDirection: "column" }}>
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-contrast" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Kaca Spion Kiri
                      </Text>
                    </Body>
                    <Right style={{marginRight:-15}}>
                      <SwitchToggle
                        containerStyle={{
                          width: 70,
                          height: 30,
                          borderRadius: 25,
                          padding: 5,
                        }}
                        backgroundColorOff={colors.whitedar}
                        backgroundColorOn='#00b300'
                        circleStyle={{
                          width: 22,
                          height: 22,
                          borderRadius: 19,
                          backgroundColor: 'white',
                        }}
                        switchOn={this.state.kacaSpionKiri}

                        circleColorOff='white'
                        circleColorOn='white'
                        duration={500}
                      />
                    </Right>
                  </ListItem>
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-contrast" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Kaca Spion Kanan
                      </Text>
                    </Body>
                    <Right style={{marginRight:-15}}>
                      <SwitchToggle
                        containerStyle={{
                          width: 70,
                          height: 30,
                          borderRadius: 25,
                          padding: 5,
                        }}
                        backgroundColorOff={colors.whitedar}
                        backgroundColorOn='#00b300'
                        circleStyle={{
                          width: 22,
                          height: 22,
                          borderRadius: 19,
                          backgroundColor: 'white',
                        }}
                        switchOn={this.state.kacaSpionKanan}

                        circleColorOff='white'
                        circleColorOn='white'
                        duration={500}
                      />
                    </Right>
                  </ListItem>
                </View>
              </CardItem>

              <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={{ marginLeft: 15, flex: 2, width: "33%" }}>
                  <Text
                    style={{ fontSize: 10, paddingTop: 20, fontWeight: "bold" }}
                  >
                    Tools :
                  </Text>
                </View>
              </View>
            <CardItem
              style={{
                borderRadius: 10,
                backgroundColor: colors.gray,
                marginLeft: 30,
                marginRight: 30,
                marginTop:10,
                paddingLeft:-10,
              }}
            >
              <View style={{ flex: 1, flexDirection: "column" }}>
                <ListItem icon>
                  <Left>
                    <Button style={{ backgroundColor: "#FF9501" }}>
                      <Icon active name="ios-outlet" />
                    </Button>
                  </Left>
                  <Body>
                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                      Sirine
                    </Text>
                  </Body>
                  <Right style={{marginRight:-15}}>
                    <SwitchToggle
                      containerStyle={{
                        width: 70,
                        height: 30,
                        borderRadius: 25,
                        padding: 5,
                      }}
                      backgroundColorOff={colors.whitedar}
                      backgroundColorOn='#00b300'
                      circleStyle={{
                        width: 22,
                        height: 22,
                        borderRadius: 19,
                        backgroundColor: 'white',
                      }}
                      switchOn={this.state.sirine}

                      circleColorOff='white'
                      circleColorOn='white'
                      duration={500}
                    />
                  </Right>
                </ListItem>
                <ListItem icon>
                  <Left>
                    <Button style={{ backgroundColor: "#FF9501" }}>
                      <Icon active name="ios-outlet" />
                    </Button>
                  </Left>
                  <Body>
                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                      Acsesoris
                    </Text>
                  </Body>
                  <Right style={{marginRight:-15}}>
                    <SwitchToggle
                      containerStyle={{
                        width: 70,
                        height: 30,
                        borderRadius: 25,
                        padding: 5,
                      }}
                      backgroundColorOff={colors.whitedar}
                      backgroundColorOn='#00b300'
                      circleStyle={{
                        width: 22,
                        height: 22,
                        borderRadius: 19,
                        backgroundColor: 'white',
                      }}
                      switchOn={this.state.acsesoris}
                      circleColorOff='white'
                      circleColorOn='white'
                      duration={500}
                    />
                  </Right>
                </ListItem>
              </View>
            </CardItem>
              <View style={{marginTop:10, marginLeft:10, marginRight:10, backgroundColor:colors.white, height:100, marginBottom:20, borderRadius:10, paddingLeft:10, paddingRight:10, paddingTop:5,
                  shadowColor:'#000',
                  shadowOffset:{
                    width:0,
                    height:1,
                  },
                  shadowOpacity:1,
                  shadowRadius:2,
                  elevation:3,
                }}>
                  <Text style={{fontSize:15, fontWeight:'bold'}}>Note :</Text>
                  <Text style={{fontSize:12}}>{this.state.listPMKEngine.note}</Text>
              </View>
            </View>
          ):(
            <Button
              block
              style={{
                height: 45,
                marginLeft: 20,
                marginRight: 20,
                marginBottom: 20,
                borderWidth: 1,
                backgroundColor: "#00b300",
                borderColor: "#00b300",
                borderRadius: 4
              }}
              onPress={() =>
                this.navigateToScreen("UnitCheckPMKCreate", this.state.idVehicle)
              }
            >
              <Text
                style={{
                  fontSize: 14,
                  fontWeight: "bold",
                  color: colors.white
                }}
              >
                Create Inspection PMK
              </Text>
            </Button>
          )}
          </ScrollView>
    )}
    return (
      <Container style={styles.wrapper2}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("UnitCheckPMK")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3, alignItems: "center" }}>
            <Title style={styles.textbody}>
              {this.state.dataVehicle.vehicle_code}
            </Title>
          </Body>
          <Right style={{ flex: 1 }}>
            <View style={{ flex: 0, flexDirection: "row" }}>
              {dateNow <= this.state.dataVehicle.exp_inspection && this.state.listPMKEngine!==null && (
                <Button
                  transparent
                  onPress={() =>
                    this.navigateToScreenUpdate("UnitCheckPMKUpdate", this.state.listPMKEngine)
                  }>
                  <Icon
                    name="ios-create"
                    size={20}
                    style={styles.facebookButtonIconOrder2}
                  />
                </Button>
              )}
            </View>
          </Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <Footer>
          <FooterTab style={styles.tabfooter}>
            <Button active style={styles.tabfooter}>
              <View style={{ height: "40%" }} />
              <View style={{ height: "50%" }}>
                <Text style={styles.textbody}>Engine</Text>
              </View>
              <View style={{ height: "20%" }} />
              <View
                style={{
                  borderWidth: 2,
                  marginTop: 2,
                  height: 0.5,
                  width: "100%",
                  borderColor: colors.white
                }}
              />
            </Button>
            <Button
              onPress={() =>
                this.props.navigation.navigate("UnitCheckDetailPMKTools")
              }
            >
              <Text style={{ color: "white", fontWeight: "bold" }}>
                Tools
              </Text>
            </Button>
          </FooterTab>
        </Footer>
        <View style={{ marginTop: 5, flex: 1, flexDirection: "column" }}>
          {list}
        </View>
      </Container>
    );
  }
}

export default UnitCheckDetailPMKEngine;
