import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Thumbnail,
  Textarea,
  Picker,
  Form
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import ChartView from "react-native-highcharts";

class DashboardUnitCheckPMK extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      isLoading: false,
      visibleDialog: false,

      vehicleCode:['PMK-001','PMK-002','PMK-003','PMK-004','PMK-005','PMK-006','PMK-007'],
      week1:[20,23,12,3,31,35,10],
      week2:[4,10,21,23,6,12,23],
      week3:[4,8,9,10,8,6,45],
      week4:[10,13,45,10,5,45,30],
    };
  }

  static navigationOptions = {
    header: null
  };


  render() {

    var Highcharts = "Highcharts";
    var conf = {
      chart: {
        type: "bar",
        animation: Highcharts.svg,
        marginRight: 20
      },
      title: {
        text: "Report PMK Performances"
      },
      credits: {
        enabled: false
      },
      xAxis: {
        categories: this.state.vehicleCode,
        title: {
          text: "Vehicle Code"
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: "Total Negatif List",
          align: "high"
        },
        labels: {
          overflow: "justify"
        }
      },
      tooltip: {
        valueSuffix: " Negatif List"
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        enabled: false
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        marginBottom:20,
        // x: -20,
        y: 30,
        floating: true,
        borderWidth: 1,
        backgroundColor: colors.white,
        shadow: true
    },
      exporting: {
        enabled: false
      },
      series: [
        {
          name: "Week 1",
          data: this.state.week1,
          color: "#63ED7A"
        },
        {
          name: "Week 2",
          data: this.state.week2,
          color: "#3ABAF4"
        },
        {
          name: "Week 3",
          data: this.state.week3,
          color: "#FFA426"
        },
        {
          name: "Week 4",
          data: this.state.week4,
          color: "#FC544B"
        }
      ]
    };

    const options = {
      global: {
        useUTC: false
      },
      lang: {
        decimalPoint: ",",
        thousandsSep: "."
      }
    };

    var list;
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.vehicleCode == null) {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = (
          <ScrollView>
            <ChartView
              style={{ height: 800 }}
              config={conf}
              options={options}
              originWhitelist={[""]}
            />
          </ScrollView>
        );
      }
    }
    return (
      <Container style={styles.wrapper2}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("FireMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex:3, alignItems:'center' }}>
            <Title style={styles.textbody}>Dashboard Unit Check</Title>
          </Body>
          <Right style={{flex:1}}/>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <Footer>
          <FooterTab style={styles.tabfooter}>
            <Button
              onPress={() =>
                this.props.navigation.navigate("DashboardUnitCheckPickup")
              }
            >
              <Text style={{ color: "white", fontWeight: "bold" }}>
                Pick Up
              </Text>
            </Button>
            <Button active style={styles.tabfooter}>
              <View style={{ height: "40%" }} />
              <View style={{ height: "50%" }}>
                <Text style={styles.textbody}>PMK</Text>
              </View>
              <View style={{ height: "20%" }} />
              <View
                style={{
                  borderWidth: 2,
                  marginTop: 2,
                  height: 0.5,
                  width: "100%",
                  borderColor: colors.white
                }}
              />
            </Button>
          </FooterTab>
        </Footer>
        <Content style={{ marginTop: 10 }}>{list}</Content>
      </Container>
    );
  }
}

export default DashboardUnitCheckPMK;
