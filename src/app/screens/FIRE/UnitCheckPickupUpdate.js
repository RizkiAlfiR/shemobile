import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  TextInput,
  AsyncStorage,
  FlatList,
  Alert,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  ListItem,
  Body,
  Fab,
  Textarea,
  Switch,
  Icon,
  Picker,
  Form,
  Input,
  Label,
  Item
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import Ripple from "react-native-material-ripple";
import CheckBox from "react-native-check-box";
import Moment from "moment";
import SwitchToggle from 'react-native-switch-toggle';

export default class UnitCheckPickupUpdate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      visibleDialogSubmit:false,
      isLoading: true,
      tabNumber: "tab1",
      id:'',
      idVehicle:'',
      dataVehicle:'',
      save:false,
      shift: 1,
      pemanasan:0,
      spedometer:0,
      levelBBM:'R',
      oliMesin:'L',
      oliRem:'L',
      oliPower:'L',
      airRadiator:'R',
      airCadRadiator:'R',
      airWiper:'R',
      banDepanKiri:0,
      banDepanKanan:0,
      banBelakangKiriDalam:0,
      banBelakangKiriLuar:0,
      banBelakangKananDalam:0,
      banBelakangKananLuar:0,
      lampuCabinCheck:false,
      lampuKotaCheck:false,
      lampuJauhCheck:false,
      lampuSeinKiriCheck:false,
      lampuSeinKananCheck:false,
      lampuRemCheck:false,
      lampuAtretCheck:false,
      lampuSorotCheck:false,
      panelDashboardCheck:false,
      kacaSpionKiriCheck:false,
      kacaSpionKananCheck:false,
      lampuCabin:'',
      lampuKota:'',
      lampuJauh:'',
      lampuSeinKiri:'',
      lampuSeinKanan:'',
      lampuRem:'',
      lampuAtret:'',
      lampuSorot:'',
      panelDashboard:'',
      kacaSpionKiri:'',
      kacaSpionKanan:'',
      note:'',
      pic_badge:'',
      pic_name:'',

      vehicleCode:'',
      reportDate:'',
      reportTime:'',
    };
  }

  onPressLampuCabin = () => {
    this.setState({ lampuCabinCheck: !this.state.lampuCabinCheck });
  }
  onPressLampuKota = () => {
    this.setState({ lampuKotaCheck: !this.state.lampuKotaCheck });
  }
  onPressLampuJauh = () => {
    this.setState({ lampuJauhCheck: !this.state.lampuJauhCheck });
  }
  onPressLampuSeinKiri = () => {
    this.setState({ lampuSeinKiriCheck: !this.state.lampuSeinKiriCheck });
  }
  onPressLampuSeinKanan = () => {
    this.setState({ lampuSeinKananCheck: !this.state.lampuSeinKananCheck });
  }
  onPressLampuRem = () => {
    this.setState({ lampuRemCheck: !this.state.lampuRemCheck });
  }
  onPressLampuAtret = () => {
    this.setState({ lampuAtretCheck: !this.state.lampuAtretCheck });
  }
  onPressLampuSorot = () => {
    this.setState({ lampuSorotCheck: !this.state.lampuSorotCheck });
  }
  onPressPanelDashboard = () => {
    this.setState({ panelDashboardCheck: !this.state.panelDashboardCheck });
  }
  onPressKacaSpionKiri = () => {
    this.setState({ kacaSpionKiriCheck: !this.state.kacaSpionKiriCheck });
  }
  onPressKacaSpionKanan = () => {
    this.setState({ kacaSpionKananCheck: !this.state.kacaSpionKananCheck });
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    AsyncStorage.getItem('listReportPickup').then((listReportPickup) => {
      this.setState({
        id: JSON.parse(listReportPickup).id,
        idVehicle: JSON.parse(listReportPickup).vehicle_id,
        vehicleCode: JSON.parse(listReportPickup).vehicle_code,
        shift: JSON.parse(listReportPickup).shift,
        reportDate: JSON.parse(listReportPickup).report_date,
        reportTime: JSON.parse(listReportPickup).report_time,
        pemanasan: JSON.parse(listReportPickup).pemanasan,
        spedometer: JSON.parse(listReportPickup).spedometer,
        levelBBM: JSON.parse(listReportPickup).level_bbm,
        oliMesin: JSON.parse(listReportPickup).oli_mesin,
        oliRem: JSON.parse(listReportPickup).oli_rem,
        oliPower: JSON.parse(listReportPickup).oli_power,
        banDepanKiri: JSON.parse(listReportPickup).ban_depan_kiri,
        banDepanKanan: JSON.parse(listReportPickup).ban_depan_kanan,
        banBelakangKiriDalam: JSON.parse(listReportPickup).ban_belakang_kiri_dalam,
        banBelakangKiriLuar: JSON.parse(listReportPickup).ban_belakang_kiri_luar,
        banBelakangKananDalam: JSON.parse(listReportPickup).ban_belakang_kanan_dalam,
        banBelakangKananLuar: JSON.parse(listReportPickup).ban_belakang_kanan_luar,
        airRadiator: JSON.parse(listReportPickup).air_radiator,
        airCadRadiator: JSON.parse(listReportPickup).air_cad_radiator,
        airWiper: JSON.parse(listReportPickup).air_wiper,
        lampuCabin: JSON.parse(listReportPickup).lampu_cabin,
        lampuKota: JSON.parse(listReportPickup).lampu_kota,
        lampuJauh: JSON.parse(listReportPickup).lampu_jauh,
        lampuSeinKiri: JSON.parse(listReportPickup).lampu_sign_kiri,
        lampuSeinKanan: JSON.parse(listReportPickup).lampu_sign_kanan,
        lampuRem: JSON.parse(listReportPickup).lampu_rem,
        lampuAtret: JSON.parse(listReportPickup).lampu_atret,
        lampuSorot: JSON.parse(listReportPickup).lampu_sorot,
        panelDashboard: JSON.parse(listReportPickup).panel_dashboard,
        kacaSpionKiri: JSON.parse(listReportPickup).kaca_spion_kiri,
        kacaSpionKanan: JSON.parse(listReportPickup).kaca_spion_kanan,
        note: JSON.parse(listReportPickup).note,
        pic_badge: JSON.parse(listReportPickup).pic_badge,
        pic_name: JSON.parse(listReportPickup).pic_name,

      });
      if(this.state.lampuCabin=='BAIK'){
        this.setState({lampuCabinCheck:true})
      }
      if(this.state.lampuKota=='BAIK'){
        this.setState({lampuKotaCheck:true})
      }
      if(this.state.lampuJauh=='BAIK'){
        this.setState({lampuJauhCheck:true})
      }
      if(this.state.lampuSeinKiri=='BAIK'){
        this.setState({lampuSeinKiriCheck:true})
      }
      if(this.state.lampuSeinKanan=='BAIK'){
        this.setState({lampuSeinKananCheck:true})
      }
      if(this.state.lampuRem=='BAIK'){
        this.setState({lampuRemCheck:true})
      }
      if(this.state.lampuAtret=='BAIK'){
        this.setState({lampuAtretCheck:true})
      }
      if(this.state.lampuSorot=='BAIK'){
        this.setState({lampuSorotCheck:true})
      }
      if(this.state.panelDashboard=='BAIK'){
        this.setState({panelDashboardCheck:true})
      }
      if(this.state.kacaSpionKiri=='BAIK'){
        this.setState({kacaSpionKiriCheck:true})
      }
      if(this.state.kacaSpionKanan=='BAIK'){
        this.setState({kacaSpionKananCheck:true})
      }
    })

    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {

    });
  }

  validasiFieldTab1() {
    if (this.state.pemanasan == 0) {
      alert("Input Pemanasan");
    } else if (this.state.spedometer == 0) {
      alert("Input Spedometer");
    } else {
      this.setState({
        tabNumber: "tab2"
      });
    }
  }

  validasiFieldTab2() {
    if (this.state.banDepanKiri == 0) {
      alert("Input Tyre Pressure");
    } else if (this.state.banDepanKanan == 0) {
      alert("Input Tyre Pressure");
    } else if (this.state.banBelakangKiriDalam == 0) {
      alert("Input Tyre Pressure");
    } else if (this.state.banBelakangKiriLuar == 0) {
      alert("Input Tyre Pressure");
    } else if (this.state.banBelakangKananDalam == 0) {
      alert("Input Tyre Pressure");
    } else if (this.state.banBelakangKananLuar == 0) {
      alert("Input Tyre Pressure");
    } else {
      this.setState({
        tabNumber: "tab3"
      });
    }
  }

  validasiCreate() {
    if(this.state.lampuCabinCheck == false){
      this.setState({lampuCabin: 'MATI'})
    } else {
      this.setState({lampuCabin: 'BAIK'})
    }
    if(this.state.lampuKotaCheck == false){
      this.setState({lampuKota: 'MATI'})
    } else {
      this.setState({lampuKota: 'BAIK'})
    }
    if(this.state.lampuJauhCheck == false){
      this.setState({lampuJauh: 'MATI'})
    } else {
      this.setState({lampuJauh: 'BAIK'})
    }
    if(this.state.lampuSeinKiriCheck == false){
      this.setState({lampuSeinKiri: 'MATI'})
    } else {
      this.setState({lampuSeinKiri: 'BAIK'})
    }
    if(this.state.lampuSeinKananCheck == false){
      this.setState({lampuSeinKanan: 'MATI'})
    } else {
      this.setState({lampuSeinKanan: 'BAIK'})
    }
    if(this.state.lampuRemCheck == false){
      this.setState({lampuRem: 'MATI'})
    } else {
      this.setState({lampuRem: 'BAIK'})
    }
    if(this.state.lampuAtretCheck == false){
      this.setState({lampuAtret: 'MATI'})
    } else {
      this.setState({lampuAtret: 'BAIK'})
    }
    if(this.state.lampuSorotCheck == false){
      this.setState({lampuSorot: 'MATI'})
    } else {
      this.setState({lampuSorot: 'BAIK'})
    }
    if(this.state.panelDashboardCheck == false){
      this.setState({panelDashboard: 'MATI'})
    } else {
      this.setState({panelDashboard: 'BAIK'})
    }
    if(this.state.kacaSpionKiriCheck == false){
      this.setState({kacaSpionKiri: 'PECAH'})
    } else {
      this.setState({kacaSpionKiri: 'BAIK'})
    }
    if(this.state.kacaSpionKananCheck == false){
      this.setState({kacaSpionKanan: 'PECAH'})
    } else {
      this.setState({kacaSpionKanan: 'BAIK'})
    }
    this.setState({
      save:true,
    });
  }

  updateInspection(){
    this.setState({
      visibleDialogSubmit:true,
      save:false,
    });
        var url = GlobalConfig.SERVERHOST + "updateInspectionPickup";
        var formData = new FormData();
        formData.append("id", this.state.id);
        formData.append("vehicle_id", this.state.idVehicle);
        formData.append("vehicle_code", this.state.vehicleCode);
        formData.append("report_date", this.state.reportDate);
        formData.append("report_time", this.state.reportTime);
        formData.append("shift", this.state.shift);
        formData.append("pemanasan", this.state.pemanasan);
        formData.append("spedometer", this.state.spedometer);
        formData.append("level_bbm", this.state.levelBBM);
        formData.append("oli_mesin", this.state.oliMesin);
        formData.append("oli_rem", this.state.oliRem);
        formData.append("oli_power", this.state.oliPower);
        formData.append("air_radiator", this.state.airRadiator);
        formData.append("air_cad_radiator", this.state.airCadRadiator);
        formData.append("air_wiper", this.state.airWiper);
        formData.append("lampu_cabin", this.state.lampuCabin);
        formData.append("lampu_kota", this.state.lampuKota);
        formData.append("lampu_jauh", this.state.lampuJauh);
        formData.append("lampu_sign_kiri", this.state.lampuSeinKiri);
        formData.append("lampu_sign_kanan", this.state.lampuSeinKanan);
        formData.append("lampu_rem", this.state.lampuRem);
        formData.append("lampu_atret", this.state.lampuAtret);
        formData.append("lampu_sorot", this.state.lampuSorot);
        formData.append("panel_dashboard", this.state.panelDashboard);
        formData.append("ban_depan_kiri", this.state.banDepanKiri);
        formData.append("ban_depan_kanan", this.state.banDepanKiri);
        formData.append("ban_belakang_kiri_dalam", this.state.banBelakangKiriDalam);
        formData.append("ban_belakang_kiri_luar", this.state.banBelakangKiriLuar);
        formData.append("ban_belakang_kanan_dalam", this.state.banBelakangKananDalam);
        formData.append("ban_belakang_kanan_luar", this.state.banBelakangKananLuar);
        formData.append("kaca_spion_kiri", this.state.kacaSpionKiri);
        formData.append("kaca_spion_kanan", this.state.kacaSpionKanan);
        formData.append("pic_badge", this.state.pic_badge);
        formData.append("pic_name", this.state.pic_name);
        formData.append("note", this.state.note);

        fetch(url, {
            headers: {
                "Content-Type": "multipart/form-data"
            },
            method: "POST",
            body: formData
        })
            .then(response => response.json())
            .then(response => {
                if (response.status == 200) {
                    this.setState({
                        visibleDialogSubmit:false
                    })
                    Alert.alert('Success', 'Update Inspection Pickup Success', [{
                        text: 'Okay'
                    }])
                    this.props.navigation.navigate('UnitCheckDetailPickup')
                } else {
                    this.setState({
                        visibleDialogSubmit:false
                    })
                    Alert.alert('Error', 'Update Inspection Pickup Failed', [{
                        text: 'Okay'
                    }])
                }
            })
            .catch((error)=>{
                console.log(error)
                this.setState({
                    visibleDialogSubmit:false
                })
                Alert.alert('Error', 'Update Inspection Pickup Failed', [{
                    text: 'Okay'
                }])
            })
  }

  render() {
    that = this;
    return (
      <Container style={styles.wrapper2}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            {this.state.tabNumber=='tab1' ? (
              <Button
                transparent
                onPress={() => this.props.navigation.navigate("UnitCheckDetailPickup")}
              >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          ):this.state.tabNumber=='tab2' ? (
              <Button
                transparent
                onPress={() => this.setState({
                  tabNumber:'tab1'
                })}
              >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
            ):(
              <Button
                transparent
                onPress={() => this.setState({
                  tabNumber:'tab2'
                })}
              >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          )}
          </Left>
          <Body style={{flex:3, alignItems:'center'}}>
              <Title style={styles.textbody}>{this.state.vehicleCode}</Title>
          </Body>
          <Right style={{flex:1}}>
            {this.state.tabNumber=='tab1' ? (
              <Button
                transparent
                onPress={() => this.validasiFieldTab1()}
              >
                <Text style={{color:colors.white, fontWeight:'bold'}}>Next</Text>
              </Button>
            ):this.state.tabNumber=='tab2' ?(
              <Button
                transparent
                onPress={() => this.validasiFieldTab2()}
              >
                <Text style={{color:colors.white, fontWeight:'bold'}}>Next</Text>
              </Button>
            ):(
              <Button
                transparent
                onPress={() => this.validasiCreate()}
              >
                <Icon
                  name="ios-checkmark"
                  style={{fontSize:40, color:colors.white}}
                />
              </Button>
            )}
          </Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ flex: 1 }}>
          <Content style={{ marginTop: 0, backgroundColor: colors.white }}>
            <View style={{ backgroundColor: colors.white }}>
              {this.state.tabNumber == "tab1" ? (
                <View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.white
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "33%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Inspection
                        </Text>
                      </View>
                      <View style={{ width: "33%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.graydar,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text
                          style={{
                            fontSize: 10,
                            fontWeight: "bold",
                            color: colors.graydar
                          }}
                        >
                          Condition Pickup
                        </Text>
                      </View>
                      <View style={{ width: "33%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.graydar,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text
                          style={{
                            fontSize: 10,
                            fontWeight: "bold",
                            color: colors.graydar
                          }}
                        >
                          Condition Sparepart
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:0, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={styles.titleInput}>Vehicle Code</Text>
                      </View>
                      <TextInput
                        style={{
                          height: 40,
                          marginLeft: 5,
                          marginRight: 5,
                          marginBottom: 5,
                          fontSize: 11,
                          marginTop:5,
                          borderWidth: 1,
                          borderColor: colors.graydar,
                          borderRadius:5
                        }}
                        value={this.state.vehicleCode}
                        bordered
                        editable={false}
                        placeholder="Vehicle Code"
                      />
                    </View>
                  </View>
                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:10, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={styles.titleInput}>Shift *</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginRight: 5,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: "100%", height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.shift}
                            onValueChange={(itemValue) => this.setState({shift:itemValue})}>
                              <Picker.Item label="Shift I (Satu)" value="1" />
                              <Picker.Item label="Shift II (Dua)" value="2" />
                              <Picker.Item label="Shift III (Tiga)" value="3" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </View>
                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:10, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width:'33%' }}>
                          <Text style={styles.titleInput}>Pemanasan (Minute)</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            value={this.state.pemanasan}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ pemanasan: text })
                            }
                          />
                      </View>
                      <View style={{ width:'33%' }}>
                          <Text style={styles.titleInput}>Spedometer (km)</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            value={this.state.spedometer}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ spedometer: text })
                            }
                          />
                      </View>
                      <View style={{ width:'33%' }}>
                          <Text style={styles.titleInput}>Level BBM</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 35
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ width: 100, height: 35 }}
                              placeholder="Select Shift ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.levelBBM}
                              onValueChange={(itemValue) => this.setState({levelBBM:itemValue})}>
                                <Picker.Item label="R" value="R" />
                                <Picker.Item label="1/4" value="1/4" />
                                <Picker.Item label="1/2" value="1/2" />
                                <Picker.Item label="3/4" value="3/4" />
                                <Picker.Item label="F" value="F" />
                            </Picker>
                          </Form>
                      </View>
                    </View>
                  </View>
                </View>
              ) : this.state.tabNumber == "tab2" ? (
                <View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.white
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "33%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Inspection
                        </Text>
                      </View>
                      <View style={{ width: "33%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Condition Pickup
                        </Text>
                      </View>
                      <View style={{ width: "33%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.graydar,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text
                          style={{
                            fontSize: 10,
                            fontWeight: "bold",
                            color: colors.graydar
                          }}
                        >
                          Condition Sparepart
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:0, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={{fontSize: 12, fontWeight: "bold"}}>Condition Oil</Text>
                      </View>
                    </View>
                  </View>
                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:0, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Oil Machine</Text>
                        <Form
                          style={{ borderWidth: 1, borderRadius: 5, marginTop: 5, marginRight: 5, marginLeft: 5, borderColor: "#E6E6E6", height: 35 }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.oliMesin}
                            onValueChange={itemValue =>
                              this.setState({ oliMesin: itemValue })
                            }
                          >
                            <Picker.Item label="L" value="L" />
                            <Picker.Item label="M" value="M" />
                            <Picker.Item label="H" value="H" />
                          </Picker>
                        </Form>
                      </View>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Oil Brake</Text>
                        <Form
                          style={{ borderWidth: 1, borderRadius: 5, marginTop: 5, marginRight: 5, marginLeft: 5, borderColor: "#E6E6E6", height: 35 }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.oliRem}
                            onValueChange={itemValue =>
                              this.setState({ oliRem: itemValue })
                            }
                          >
                            <Picker.Item label="L" value="L" />
                            <Picker.Item label="M" value="M" />
                            <Picker.Item label="H" value="H" />
                          </Picker>
                        </Form>
                      </View>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Oil Power Steering</Text>
                        <Form
                          style={{ borderWidth: 1, borderRadius: 5, marginTop: 5, marginRight: 5, marginLeft: 5, borderColor: "#E6E6E6", height: 35 }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.oliPower}
                            onValueChange={itemValue =>
                              this.setState({ oliPower: itemValue })
                            }
                          >
                            <Picker.Item label="L" value="L" />
                            <Picker.Item label="M" value="M" />
                            <Picker.Item label="H" value="H" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </View>

                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:15, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={{fontSize: 12, fontWeight: "bold"}}>Condition Water</Text>
                      </View>
                    </View>
                  </View>
                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:0, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Radiator Water</Text>
                        <Form
                          style={{ borderWidth: 1, borderRadius: 5, marginTop: 5, marginRight: 5, marginLeft: 5, borderColor: "#E6E6E6", height: 35 }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.airRadiator}
                            onValueChange={itemValue =>
                              this.setState({ airRadiator: itemValue })
                            }
                          >
                            <Picker.Item label="R" value="R" />
                            <Picker.Item label="1/4" value="1/4" />
                            <Picker.Item label="1/2" value="1/2" />
                            <Picker.Item label="3/4" value="3/4" />
                            <Picker.Item label="F" value="F" />
                          </Picker>
                        </Form>
                      </View>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Radiator Water Cad</Text>
                        <Form
                          style={{ borderWidth: 1, borderRadius: 5, marginTop: 5, marginRight: 5, marginLeft: 5, borderColor: "#E6E6E6", height: 35 }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.airCadRadiator}
                            onValueChange={itemValue =>
                              this.setState({ airCadRadiator: itemValue })
                            }
                          >
                            <Picker.Item label="R" value="R" />
                            <Picker.Item label="1/4" value="1/4" />
                            <Picker.Item label="1/2" value="1/2" />
                            <Picker.Item label="3/4" value="3/4" />
                            <Picker.Item label="F" value="F" />
                          </Picker>
                        </Form>
                      </View>
                      <View style={{width:'33%'}}>
                        <Text style={styles.titleInput}>Wiper Water</Text>
                        <Form
                          style={{ borderWidth: 1, borderRadius: 5, marginTop: 5, marginRight: 5, marginLeft: 5, borderColor: "#E6E6E6", height: 35 }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.airWiper}
                            onValueChange={itemValue =>
                              this.setState({ airWiper: itemValue })
                            }
                          >
                            <Picker.Item label="R" value="R" />
                            <Picker.Item label="1/4" value="1/4" />
                            <Picker.Item label="1/2" value="1/2" />
                            <Picker.Item label="3/4" value="3/4" />
                            <Picker.Item label="F" value="F" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </View>

                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:15, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={{fontSize: 12, fontWeight: "bold"}}>Condition Tyre Pressure (Bar)</Text>
                      </View>
                    </View>
                  </View>

                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:10, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Left Front</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.banDepanKiri}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ banDepanKiri: text })
                            }
                          />
                      </View>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Right Front</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.banDepanKanan}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ banDepanKanan: text })
                            }
                          />
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Back Left Outside</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.banBelakangKiriLuar}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ banBelakangKiriLuar: text })
                            }
                          />
                      </View>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Back Right Outside</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.banBelakangKananLuar}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ banBelakangKananLuar: text })
                            }
                          />
                      </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Back Left Inside</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.banBelakangKiriDalam}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ banBelakangKiriDalam: text })
                            }
                          />
                      </View>
                      <View style={{ width:'50%' }}>
                          <Text style={styles.titleInput}>Back Right Inside</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 100
                            }}
                            value={this.state.banBelakangKananDalam}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ banBelakangKananDalam: text })
                            }
                          />
                      </View>
                    </View>
                  </View>
                </View>
              ) : (
                <View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.white
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "33%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Inspection
                        </Text>
                      </View>
                      <View style={{ width: "33%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Condition Pickup
                        </Text>
                      </View>
                      <View style={{ width: "33%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Condition Sparepart
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:0, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={{fontSize: 12, fontWeight: "bold"}}>Condition Lamp</Text>
                      </View>
                    </View>
                  </View>
                  <View style={{ borderRadius: 0, marginLeft:10, marginRight:15, marginTop:0, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "column" }}>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-airplane" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Cabin
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.lampuCabinCheck}
                            onPress={this.onPressLampuCabin}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-airplane" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Kota
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.lampuKotaCheck}
                            onPress={this.onPressLampuKota}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-airplane" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Jauh
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.lampuJauhCheck}
                            onPress={this.onPressLampuJauh}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-airplane" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Sein Kiri
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.lampuSeinKiriCheck}
                            onPress={this.onPressLampuSeinKiri}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-airplane" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Sein Kanan
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.lampuSeinKananCheck}
                            onPress={this.onPressLampuSeinKanan}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-airplane" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Rem
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.lampuRemCheck}
                            onPress={this.onPressLampuRem}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-airplane" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Atret
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.lampuAtretCheck}
                            onPress={this.onPressLampuAtret}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-airplane" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Sorot
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.lampuSorotCheck}
                            onPress={this.onPressLampuSorot}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-airplane" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Panel Dashboard
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.panelDashboardCheck}
                            onPress={this.onPressPanelDashboard}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                    </View>
                  </View>

                  <View style={{ borderRadius: 0, marginLeft:15, marginRight:15, marginTop:15, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={{fontSize: 12, fontWeight: "bold"}}>Condition Rear-View Mirror</Text>
                      </View>
                    </View>
                  </View>
                  <View style={{ borderRadius: 0, marginLeft:10, marginRight:15, marginTop:0, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "column" }}>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-contrast" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Kaca Spion Kiri
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.kacaSpionKiriCheck}
                            onPress={this.onPressKacaSpionKiri}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-contrast" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Kaca Spion Kanan
                          </Text>
                        </Body>
                        <Right>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.whitedar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}
                            switchOn={this.state.kacaSpionKananCheck}
                            onPress={this.onPressKacaSpionKanan}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                        </Right>
                      </ListItem>
                    </View>
                  </View>

                  <View style={{ borderRadius: 0, marginLeft:10, marginRight:15, marginTop:5, marginBottom:20, backgroundColor:colors.white  }}>
                    <View style={{ flex: 1, flexDirection: "column" }}>
                      <View style={{ marginLeft: 10, flex: 2 }}>
                        <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                          Note
                        </Text>
                      </View>
                      <View>
                        <Textarea
                          style={{
                            marginLeft: 5,
                            marginRight: 5,
                            marginBottom: 5,
                            borderRadius: 5,
                            fontSize: 11
                          }}
                          rowSpan={2}
                          bordered
                          value={this.state.note}
                          placeholder="Type something .. "
                          onChangeText={text =>
                            this.setState({ note: text })
                          }
                        />
                      </View>
                    </View>
                  </View>

                </View>
              )}
            </View>
          </Content>
        </View>

        <View style={{ width: '100%', position: "absolute"}}>
          <Dialog
            visible={this.state.save}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog, width:300}}
            onTouchOutside={() => {
              this.setState({ login: false });
            }}
          >
          <DialogContent>
          {
            <View>
              <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
                <Text style={{fontSize:15, alignItems:'center', color:colors.black}}>Do you want to update this data ?</Text>
              </View>
              <View style={{flexDirection:'row', flex:1, paddingTop:10, width:'100%'}}>
                <View style={{width:'50%', paddingRight:5}}>
                    <Button
                      block
                      style={{
                        width:'100%',
                        marginTop:10,
                        height: 35,
                        marginBottom: 5,
                        borderWidth: 0,
                        backgroundColor: colors.primer,
                        borderRadius: 15
                      }}
                      onPress={() => this.updateInspection()}
                    >
                      <Text style={{color:colors.white}}>Yes</Text>
                    </Button>
                </View>
                <View style={{width:'50%', paddingLeft:5}}>
                    <Button
                      block
                      style={{
                        width:'100%',
                        marginTop:10,
                        height: 35,
                        marginBottom: 5,
                        borderWidth: 0,
                        backgroundColor: colors.primer,
                        borderRadius: 15
                      }}
                      onPress={() => this.setState({
                        save:false,
                      })}
                    >
                      <Text style={{color:colors.white}}>No</Text>
                    </Button>
                </View>
              </View>
            </View>
          }
          </DialogContent>
          </Dialog>
        </View>
        <View style={{ width: 270, position: "absolute" }}>
            <Dialog
                visible={this.state.visibleDialogSubmit}
            >
                <DialogContent>
                {
                  <View>
                    <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
                      <Text style={{fontSize:15, alignItems:'center', color:colors.black}}>Updating Inpection Pickup ...</Text>
                    </View>
                    <ActivityIndicator size="large" color="#330066" animating />
                  </View>
                }
                </DialogContent>
            </Dialog>
        </View>
      </Container>
    );
  }
}
