import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import Moment from "moment";

class ListItemPemadam extends React.PureComponent {
  render() {
    return (
      <View style={{ flex: 1, flexDirection: "row" }}>
        <View style={{ width: "5%" }}>
          <Text style={{ fontSize: 10, fontWeight: "bold" }}>-</Text>
        </View>
        <View style={{ width: "90%" }}>
          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
            {this.props.data}
          </Text>
        </View>
      </View>
    );
  }
}

class ListItemTembusan extends React.PureComponent {
  render() {
    return (
      <View style={{ flex: 1, flexDirection: "row" }}>
        <View style={{ width: "5%" }}>
          <Text style={{ fontSize: 10, fontWeight: "bold" }}>-</Text>
        </View>
        <View style={{ width: "90%" }}>
          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
            {this.props.data}
          </Text>
        </View>
      </View>
    );
  }
}

class ListItemPeralatan extends React.PureComponent {
  render() {
    return (
      <View style={{ flex: 1, flexDirection: "row" }}>
        <View style={{ width: "5%" }}>
          <Text style={{ fontSize: 10, fontWeight: "bold" }}>-</Text>
        </View>
        <View style={{ width: "90%" }}>
          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
            {this.props.data}
          </Text>
        </View>
      </View>
    );
  }
}

class FireReportDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isLoading: true,
      listFireReport: [],
      listPemadam: [],
      listPeralatan: [],
      listTembusan: [],
      listFile: [],
      idFireReport: "",
      visibleDialogSubmit: false,
      delete:false,
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItemPeralatan = ({ item }) => <ListItemPeralatan data={item} />;

  _renderItemPemadam = ({ item }) => <ListItemPemadam data={item} />;

  _renderItemTembusan = ({ item }) => <ListItemTembusan data={item} />;

  navigateToScreen(route, listFireReport) {
    AsyncStorage.setItem("list", JSON.stringify(listFireReport)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        AsyncStorage.getItem("id").then(id => {
          const url = GlobalConfig.SERVERHOST + "detailFireReport";
          var formData = new FormData();
          formData.append("id", id);
          fetch(url, {
            headers: {
              "Content-Type": "multipart/form-data"
            },
            method: "POST",
            body: formData
          })
            .then(response => response.json())
            .then(responseJson => {
              // alert(JSON.stringify(responseJson))
              this.setState({
                listFireReport: responseJson.data,
                isLoading: false,
              });
            })
            .catch(error => {
              console.log(error);
            });
        });
      }
    );
  }

  konfirmasideleteFireReport() {
    this.setState({
      delete: true
    });
  }

  deleteFireReport() {
    this.setState({
      visibleDialogSubmit: true,
      delete:false,
    });
      const url =
        GlobalConfig.SERVERHOST + "deleteFireReport";
      var formData = new FormData();
      formData.append("id", this.state.listFireReport.id);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Success", "Delete Success", [
              {
                text: "Oke"
              }
            ]);
            this.props.navigation.navigate("FireReport");
          } else {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Error", "Delete Failed", [
              {
                text: "Oke"
              }
            ]);
          }
        });
  }

  render() {
    var list;
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.listFireReport == null) {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = (
          <View style={{}}>
            <View style={{ flex: 1, flexDirection: "row", marginLeft:10, marginRight:10, }}>
              <View style={{ width: "90%" }}>
                <Text style={{ fontSize: 10 }}>Date Incident</Text>
                <Text style={{ fontSize: 12, fontWeight: "bold" }}>
                  {Moment(this.state.listFireReport.fire_date).format('DD MMMM YYYY')}
                </Text>
                <Text style={{ fontSize: 10 }}>
                  {this.state.listFireReport.job_walking_timestart} - {this.state.listFireReport.job_walking_timeend}
                </Text>
              </View>
              <View style={{ width: "10%" }}>
                <Text style={{ fontSize: 10, textAlign: "right" }}>Shift</Text>
                <Text
                  style={{ fontSize: 10, fontWeight: "bold", textAlign: "right" }}
                >
                  Shift {this.state.listFireReport.shift}
                </Text>
              </View>
            </View>
            <View style={{marginTop:10, marginLeft:10, marginRight:10, backgroundColor:colors.white, height:50, marginBottom:5, borderRadius:10, paddingLeft:5, paddingRight:10,
              shadowColor:'#000',
              shadowOffset:{
                width:0,
                height:1,
              },
              shadowOpacity:1,
              shadowRadius:2,
              elevation:3,
            }}>
              <View style={{flex:1, flexDirection:'row'}}>
                <View style={{width:'13%', justifyContent:'center'}}>
                  <Icon
                    name="ios-contact"
                    style={{fontSize:40, color:colors.graydark}}
                  />
                </View>
                <View style={{width:'87%', justifyContent:'center'}}>
                  <Text style={{ fontSize: 11, fontWeight: "bold" }}>
                    {this.state.listFireReport.pic_badge}
                  </Text>
                  <Text style={{ fontSize: 13,}}>
                    {this.state.listFireReport.pic_name}
                  </Text>
                </View>
              </View>
            </View>

            <View style={{flex:1, paddingLeft:10, paddingRight:10, flexDirection:'row', marginTop:10}}>
                  <View style={{width:'70%'}}>
                    <View style={{backgroundColor:colors.white, height:100, marginBottom:5, borderRadius:10, marginRight:5, paddingTop:5,
                        shadowColor:'#000',
                        shadowOffset:{
                          width:0,
                          height:1,
                        },
                        shadowOpacity:1,
                        shadowRadius:2,
                        elevation:3,
                        alignItems:'center'
                      }}>
                        <Icon
                          name="ios-compass"
                          style={{fontSize:50, color:colors.green0}}
                        />
                        <View style={{flex:1, flexDirection:'row', marginTop:5}}>
                          <View style={{width:'50%', alignItems:'center'}}>
                            <Text style={{fontSize:9}}>Incident Place</Text>
                            <Text style={{fontSize:10, fontWeight:'bold'}}>{this.state.listFireReport.incident_place}</Text>
                          </View>
                          <View style={{width:'50%', alignItems:'center'}}>
                            <Text style={{fontSize:9}}>Place Note</Text>
                            <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                              {this.state.listFireReport.note_place}
                            </Text>
                          </View>
                        </View>
                    </View>
                  </View>
                  <View style={{width:'30%'}}>
                    <View style={{backgroundColor:colors.white, height:100, marginBottom:5, borderRadius:10, marginLeft:5, paddingTop:5,
                        shadowColor:'#000',
                        shadowOffset:{
                          width:0,
                          height:1,
                        },
                        shadowOpacity:1,
                        shadowRadius:2,
                        elevation:3,
                        alignItems:'center'
                      }}>
                        <Icon
                          name="ios-contacts"
                          style={{fontSize:40, color:colors.green0}}
                        />
                        <Text style={{fontSize:9}}>Victims</Text>
                        <Text style={{fontSize:25, fontWeight:'bold'}}>{this.state.listFireReport.number_victims}</Text>
                    </View>
                  </View>
                </View>
                <View style={{marginLeft:15, marginRight:15}}>
                  <Text style={{ fontSize: 10, paddingTop:10 }}>
                    Pekerjaan Sewaktu Kebakaran
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listFireReport.work_on_fire}
                  </Text>
                  <Text style={{ fontSize: 10, paddingTop: 10 }}>Penjelasan</Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listFireReport.description_of_fire}
                  </Text>
                  <Text style={{ fontSize: 10, paddingTop: 10 }}>
                    Objek Yang Terbakar
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listFireReport.fire_happened_on}
                  </Text>
                  <Text style={{ fontSize: 10, paddingTop: 10 }}>
                    Jenis Bahan Yang Terbakar
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listFireReport.fire_material_type}
                  </Text>
                  <Text style={{ fontSize: 10, paddingTop: 10 }}>
                    Kerusakan Yang Timbul
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listFireReport.damage_from_fire}
                  </Text>
                </View>
                <View style={{flex:1, flexDirection:'row'}}>
                  <View style={{width:'50%'}}>
                    <View style={{marginTop:10, marginLeft:10, marginRight:10, backgroundColor:colors.white, height:100, marginBottom:5, borderRadius:10, paddingLeft:10, paddingRight:10, paddingTop:5,
                        shadowColor:'#000',
                        shadowOffset:{
                          width:0,
                          height:1,
                        },
                        shadowOpacity:1,
                        shadowRadius:2,
                        elevation:3,
                      }}>
                        <Text style={{fontSize:15, fontWeight:'bold'}}>Unsafe Condition :</Text>
                        <Text style={{fontSize:12}}>{this.state.listFireReport.unsafe_condition}</Text>
                    </View>
                  </View>
                  <View style={{width:'50%'}}>
                  <View style={{marginTop:10, marginLeft:10, marginRight:10, backgroundColor:colors.white, height:100, marginBottom:5, borderRadius:10, paddingLeft:10, paddingRight:10, paddingTop:5,
                      shadowColor:'#000',
                      shadowOffset:{
                        width:0,
                        height:1,
                      },
                      shadowOpacity:1,
                      shadowRadius:2,
                      elevation:3,
                    }}>
                      <Text style={{fontSize:15, fontWeight:'bold'}}>Unsafe Action :</Text>
                      <Text style={{fontSize:12}}>{this.state.listFireReport.unsafe_action}</Text>
                  </View>
                  </View>
                </View>

                <View style={{marginTop:10, marginLeft:10, marginRight:10, backgroundColor:colors.white, height:100, marginBottom:5, borderRadius:10, paddingLeft:10, paddingRight:10, paddingTop:5,
                    shadowColor:'#000',
                    shadowOffset:{
                      width:0,
                      height:1,
                    },
                    shadowOpacity:1,
                    shadowRadius:2,
                    elevation:3,
                  }}>
                    <Text style={{fontSize:15, fontWeight:'bold'}}>Saran :</Text>
                    <Text style={{fontSize:12}}>{this.state.listFireReport.saran}</Text>
                </View>

            <CardItem>
            <View>
            <View>


              <Text style={{ fontSize: 10, paddingTop: 10 }}>
                Alat Pemadam Yang Digunakan
              </Text>
              <FlatList
                data={JSON.parse(this.state.listFireReport.fire_extinguishers)}
                renderItem={this._renderItemPeralatan}
                keyExtractor={(item, index) => index.toString()}
              />
              <Text style={{ fontSize: 10, paddingTop: 10 }}>
                Anggota PMK Yang Melakukan Pemadaman
              </Text>
              <FlatList
                data={JSON.parse(this.state.listFireReport.list_employees)}
                renderItem={this._renderItemPemadam}
                keyExtractor={(item, index) => index.toString()}
              />

              <Text style={{ fontSize: 10, paddingTop: 10 }}>Tembusan Surat</Text>
              <FlatList
                data={JSON.parse(this.state.listFireReport.list_tembusan)}
                renderItem={this._renderItemTembusan}
                keyExtractor={(item, index) => index.toString()}
              />

            </View>
            </View>
            </CardItem>
          </View>
        );
      }
    }
    return (
      <Container style={styles.wrapper2}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.props.navigation.goBack()}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3, alignItems: "center" }}>
            <Title style={styles.textbody}>
            </Title>
          </Body>
          <Right style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() =>
                this.navigateToScreen(
                  "FireReportImage",
                  this.state.listFireReport.id
                )
              }
            >
              <Icon
                name="ios-images"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
            <Button
              transparent
              onPress={() =>
                this.navigateToScreen(
                  "FireReportUpdate",
                  this.state.listFireReport
                )
              }
            >
              <Icon
                name="ios-create"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
            <Button
              transparent
              onPress={() => this.konfirmasideleteFireReport()}
            >
              <Icon
                name="ios-trash"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ marginTop: 10, flex: 1, flexDirection: "column" }}>
        <ScrollView>
          {list}
        </ScrollView>
        </View>
        <View style={{ width: '100%', position: "absolute"}}>
          <Dialog
            visible={this.state.delete}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog, width:300}}
            onTouchOutside={() => {
              this.setState({ login: false });
            }}
          >
          <DialogContent>
          {
            <View>
              <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
                <Text style={{fontSize:15, alignItems:'center', color:colors.black}}>Do you want to remove this data ?</Text>
              </View>
              <View style={{flexDirection:'row', flex:1, paddingTop:10, width:'100%'}}>
                <View style={{width:'50%', paddingRight:5}}>
                    <Button
                      block
                      style={{
                        width:'100%',
                        marginTop:10,
                        height: 35,
                        marginBottom: 5,
                        borderWidth: 0,
                        backgroundColor: colors.primer,
                        borderRadius: 15
                      }}
                      onPress={() => this.deleteFireReport()}
                    >
                      <Text style={{color:colors.white}}>Yes</Text>
                    </Button>
                </View>
                <View style={{width:'50%', paddingLeft:5}}>
                    <Button
                      block
                      style={{
                        width:'100%',
                        marginTop:10,
                        height: 35,
                        marginBottom: 5,
                        borderWidth: 0,
                        backgroundColor: colors.primer,
                        borderRadius: 15
                      }}
                      onPress={() => this.setState({
                        delete:false,
                      })}
                    >
                      <Text style={{color:colors.white}}>No</Text>
                    </Button>
                </View>
              </View>
            </View>
          }
          </DialogContent>
          </Dialog>
        </View>
          <View style={{ width: 270, position: "absolute" }}>
            <Dialog
              visible={this.state.visibleDialogSubmit}
            >
              <DialogContent>
                {
                  <View>
                  <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
                    <Text style={{fontSize:15, alignItems:'center', color:colors.black}}>Deleting Fire Report ...</Text>
                  </View>
                  <ActivityIndicator size="large" color="#330066" animating />
                  </View>
                }
              </DialogContent>
            </Dialog>
          </View>
      </Container>
    );
  }
}

export default FireReportDetail;
