import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Thumbnail,
  Item,
  Input,
  Picker,
  Form,
  Switch,
  ListItem
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import DatePicker from "react-native-datepicker";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import Moment from "moment";
import SwitchToggle from 'react-native-switch-toggle';

class UnitCheckDetailPMKTools extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      isLoading: true,
      visibleFilter: false,
      adatidak: false,
      noPeg: "",
      visibleDialogSubmit: false,
      idVehicle:'',
      dataVehicle:[],
      listPMKTools:[],
      dongkrak:false,
      stangKabin:false,
      ganjalBan:false,
      kunciRoda:false,
      hammer:false,
      kotakPPPK:false,
    };
  }

  static navigationOptions = {
    header: null
  };

  navigateToScreen(route, idVehicle) {
    AsyncStorage.setItem("idVehicle", idVehicle).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  navigateToScreenUpdate(route, listReportPMK) {
    AsyncStorage.setItem("listReportPMK", JSON.stringify(listReportPMK)).then(() => {
        this.props.navigation.navigate(route);
        // alert(JSON.stringify(listReportPMK))
      }
    );
  }


  componentDidMount() {
    AsyncStorage.getItem("idVehicle").then(idVehicle => {
        this.setState({
          idVehicle: idVehicle,
          isLoading: false,
        });
        this.loadData();
        this.loadVehicle();
    });

    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
      }
    );
  }

  loadVehicle() {
    this.setState({ visibleDialog: false, isLoading: true });
      const url = GlobalConfig.SERVERHOST + "getListVehicle";
      var formData = new FormData();
      formData.append("id_vehicle", this.state.idVehicle);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            dataVehicle: responseJson.data,
            isLoading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
  }

  loadData() {
      const url = GlobalConfig.SERVERHOST + "detailInspectionPMK";
      var formData = new FormData();
      formData.append("vehicle_id", this.state.idVehicle);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            listPMKTools: responseJson.data,
            isLoading: false
          });
          this.loadVerifikasi();
        })
        .catch(error => {
          console.log(error);
        });
  }

  loadVerifikasi(){
    if(this.state.listPMKTools.dongkrak == 'ADA'){
      this.setState({
        dongkrak:true,
      })
    }
    if(this.state.listPMKTools.stang_kabin == 'ADA'){
      this.setState({
        stangKabin:true,
      })
    }
    if(this.state.listPMKTools.ganjal_ban == 'ADA'){
      this.setState({
        ganjalBan:true,
      })
    }
    if(this.state.listPMKTools.kunci_roda == 'ADA'){
      this.setState({
        kunciRoda:true,
      })
    }
    if(this.state.listPMKTools.hammer == 'ADA'){
      this.setState({
        hammer:true,
      })
    }
    if(this.state.listPMKTools.kotak_pppk == 'ADA'){
      this.setState({
        kotakPPPK:true,
      })
    }
  }


  render() {
    var dateNow = Moment().format('YYYY-MM-DD');
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
        list = (
          <ScrollView>
          <View style={{paddingLeft:10, paddingTop:10, paddingBottom:10}}>
              <Text style={{ fontSize: 10 }}>Status</Text>
              {dateNow <= this.state.dataVehicle.exp_inspection ? (
                <Text
                  style={{
                    fontSize: 10,
                    fontWeight: "bold",
                    color: colors.green01
                  }}
                >
                  CHECKED
                </Text>
              ) : (
                <Text
                  style={{
                    fontSize: 10,
                    fontWeight: "bold",
                    color: "#FF0101"
                  }}
                >
                  UNCECKED
                </Text>
              )}
          </View>

          {dateNow <= this.state.dataVehicle.exp_inspection && this.state.listPMKTools!=null ? (
            <View>
              <View style={{marginTop:10, marginLeft:10, marginRight:10, backgroundColor:colors.white, height:50, marginBottom:5, borderRadius:10, paddingLeft:5, paddingRight:10,
                shadowColor:'#000',
                shadowOffset:{
                  width:0,
                  height:1,
                },
                shadowOpacity:1,
                shadowRadius:2,
                elevation:3,
              }}>
                <View style={{flex:1, flexDirection:'row'}}>
                  <View style={{width:'13%', justifyContent:'center'}}>
                    <Icon
                      name="ios-contact"
                      style={{fontSize:40, color:colors.graydark}}
                    />
                  </View>
                  <View style={{width:'87%', justifyContent:'center'}}>
                    <Text style={{ fontSize: 11, fontWeight: "bold" }}>
                      {this.state.listPMKTools.pic_badge}
                    </Text>
                    <Text style={{ fontSize: 13,}}>
                      {this.state.listPMKTools.pic_name}
                    </Text>
                  </View>
                </View>
              </View>
              <View style={{flex:1, paddingLeft:10, paddingRight:10, flexDirection:'row', marginTop:10}}>
                <View style={{width:'40%'}}>
                  <View style={{backgroundColor:colors.white, height:100, marginBottom:5, borderRadius:10, marginRight:5, paddingTop:5,
                      shadowColor:'#000',
                      shadowOffset:{
                        width:0,
                        height:1,
                      },
                      shadowOpacity:1,
                      shadowRadius:2,
                      elevation:3,
                      alignItems:'center'
                    }}>
                    <Image
                      style={{
                        height: 50,
                        width:80,
                        borderColor: colors.gray,
                        borderRadius: 10
                      }}
                      source={require("../../../assets/images/truckfire.jpg")}
                    />
                      <Text style={{fontSize:9}}>Negative List</Text>
                      <Text style={{fontSize:25, fontWeight:'bold'}}>{this.state.listPMKTools.sum_negative}</Text>
                  </View>
                </View>
                <View style={{width:'30%'}}>
                  <View style={{backgroundColor:colors.white, justifyContent:'center', height:100, marginLeft:5, marginBottom:5, borderRadius:10, marginRight:5, paddingTop:5,
                      shadowColor:'#000',
                      shadowOffset:{
                        width:0,
                        height:1,
                      },
                      shadowOpacity:1,
                      shadowRadius:2,
                      elevation:3,
                      alignItems:'center'
                    }}>
                      <Text style={{fontSize:9}}>Date</Text>
                      <Text style={{fontSize:25, fontWeight:'bold'}}>{Moment(this.state.listPMKTools.report_date).format('DD')}</Text>
                      <Text style={{fontSize:15,}}>{Moment(this.state.listPMKTools.report_date).format('MMMM')}</Text>
                  </View>
                </View>
                <View style={{width:'30%'}}>
                  <View style={{backgroundColor:colors.white, height:100, marginBottom:5, borderRadius:10, marginLeft:5, paddingTop:5,
                      shadowColor:'#000',
                      shadowOffset:{
                        width:0,
                        height:1,
                      },
                      shadowOpacity:1,
                      shadowRadius:2,
                      elevation:3,
                      alignItems:'center'
                    }}>
                      <Icon
                        name="ios-contacts"
                        style={{fontSize:40, color:colors.green0}}
                      />
                      <Text style={{fontSize:9}}>Shift</Text>
                      <Text style={{fontSize:25, fontWeight:'bold'}}>{this.state.listPMKTools.shift}</Text>
                  </View>
                </View>
              </View>

              <View style={{flex:1, flexDirection:'row', marginLeft:5, marginTop:10}}>
                <View style={{width:'33%'}}>
                  <View style={{ marginLeft: 10, flex: 2 }}>
                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                      Kunci Hydrant
                    </Text>
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {this.state.listPMKTools.kunci_hydrant}
                    </Text>
                  </View>
                </View>
                <View style={{width:'33%'}}>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Apar
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listPMKTools.apar}
                  </Text>
                </View>
                <View style={{width:'33%'}}>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Kapak
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listPMKTools.kapak}
                  </Text>
                </View>
              </View>

              <View style={{flex:1, flexDirection:'row', marginLeft:5}}>
                <View style={{width:'33%'}}>
                  <View style={{ marginLeft: 10, flex: 2 }}>
                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                      Linggis
                    </Text>
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {this.state.listPMKTools.linggis}
                    </Text>
                  </View>
                </View>
                <View style={{width:'33%'}}>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Tangga Ganda
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listPMKTools.tangga_ganda}
                  </Text>
                </View>
                <View style={{width:'33%'}}>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Senter
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listPMKTools.senter}
                  </Text>
                </View>
              </View>

              <View style={{flex:1, flexDirection:'row', marginLeft:5}}>
                <View style={{width:'33%'}}>
                  <View style={{ marginLeft: 10, flex: 2 }}>
                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                      Gantol / Pengait
                    </Text>
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {this.state.listPMKTools.gantol}
                    </Text>
                  </View>
                </View>
                <View style={{width:'33%'}}>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Timba
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listPMKTools.timba}
                  </Text>
                </View>
                <View style={{width:'33%'}}>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Karung
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listPMKTools.karung}
                  </Text>
                </View>
              </View>

              <View style={{flex:1, flexDirection:'row', marginLeft:5}}>
                <View style={{width:'33%'}}>
                  <View style={{ marginLeft: 10, flex: 2 }}>
                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                      Jacket PMK
                    </Text>
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {this.state.listPMKTools.jacket_pmk}
                    </Text>
                  </View>
                </View>
                <View style={{width:'33%'}}>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Heml PMK
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listPMKTools.helm_pmk}
                  </Text>
                </View>
                <View style={{width:'33%'}}>
                </View>
              </View>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ marginLeft: 15, flex: 2, width: "33%" }}>
                    <Text style={{ fontSize: 10, paddingTop: 20, fontWeight: "bold" }}>
                      Condition Nozzle
                    </Text>
                  </View>
                </View>
              <CardItem style={{ borderRadius: 0 }}>
                <View style={{ flex: 1, flexDirection: "column" }}>
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ marginLeft: 10, flex: 2, width: "30%" }}>
                      <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                        Hose 2.5
                      </Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKTools.hose_25}</Text>
                    </View>
                    <View
                      style={{
                        flex: 2,
                        marginRight: 20,
                        marginLeft: 40,
                        width: "70%"
                      }}
                    >
                      <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                        Hose 1.5
                      </Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKTools.hose_15}</Text>
                    </View>
                  </View>
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ marginLeft: 10, flex: 2, width: "30%" }}>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Hose Section 4
                      </Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKTools.hose_section}</Text>
                    </View>
                    <View
                      style={{
                        flex: 2,
                        marginRight: 20,
                        marginLeft: 40,
                        width: "70%"
                      }}
                    >
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Kunci Hose Section 4
                      </Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKTools.kunci_hose}</Text>
                    </View>
                  </View>
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ marginLeft: 10, flex: 2, width: "30%" }}>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Nozzle 1.5 J/S
                      </Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKTools.nozzle_15_js}</Text>
                    </View>
                    <View
                      style={{
                        flex: 2,
                        marginRight: 20,
                        marginLeft: 40,
                        width: "70%"
                      }}
                    >
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Nozzle 1.5 Akron
                      </Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKTools.nozzle_15_akron}</Text>
                    </View>
                  </View>
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ marginLeft: 10, flex: 2, width: "30%" }}>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Nozzle 1.5 Jet
                      </Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKTools.nozzle_15_jet}</Text>
                    </View>
                    <View
                      style={{
                        flex: 2,
                        marginRight: 20,
                        marginLeft: 40,
                        width: "70%"
                      }}
                    >
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Nozzle Foam
                      </Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKTools.nozzle_foam}</Text>
                    </View>
                  </View>
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ marginLeft: 10, flex: 2, width: "30%" }}>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Y Valve
                      </Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKTools.y_valve}</Text>
                    </View>
                    <View
                      style={{
                        flex: 2,
                        marginRight: 20,
                        marginLeft: 40,
                        width: "70%"
                      }}
                    >
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Red 2.5 to 1.5
                      </Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.listPMKTools.red_25}</Text>
                    </View>
                  </View>
                </View>
              </CardItem>
              <CardItem
                style={{
                  borderRadius: 10,
                  backgroundColor: colors.gray,
                  marginLeft: 30,
                  marginRight: 30,
                  marginTop:10,
                  paddingLeft:-10
                }}
              >
                <View style={{ flex: 1, flexDirection: "column" }}>
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-outlet" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Dongkrak
                      </Text>
                    </Body>
                    <Right style={{marginRight:-15}}>
                      <SwitchToggle
                        containerStyle={{
                          width: 70,
                          height: 30,
                          borderRadius: 25,
                          padding: 5,
                        }}
                        backgroundColorOff={colors.whitedar}
                        backgroundColorOn='#00b300'
                        circleStyle={{
                          width: 22,
                          height: 22,
                          borderRadius: 19,
                          backgroundColor: 'white',
                        }}
                        switchOn={this.state.dongkrak}
                        circleColorOff='white'
                        circleColorOn='white'
                        duration={500}
                      />
                    </Right>
                  </ListItem>
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-outlet" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Stang Kabin
                      </Text>
                    </Body>
                    <Right style={{marginRight:-15}}>
                      <SwitchToggle
                        containerStyle={{
                          width: 70,
                          height: 30,
                          borderRadius: 25,
                          padding: 5,
                        }}
                        backgroundColorOff={colors.whitedar}
                        backgroundColorOn='#00b300'
                        circleStyle={{
                          width: 22,
                          height: 22,
                          borderRadius: 19,
                          backgroundColor: 'white',
                        }}
                        switchOn={this.state.stangKabin}
                        circleColorOff='white'
                        circleColorOn='white'
                        duration={500}
                      />
                    </Right>
                  </ListItem>
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-outlet" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Ganjal Ban
                      </Text>
                    </Body>
                    <Right style={{marginRight:-15}}>
                      <SwitchToggle
                        containerStyle={{
                          width: 70,
                          height: 30,
                          borderRadius: 25,
                          padding: 5,
                        }}
                        backgroundColorOff={colors.whitedar}
                        backgroundColorOn='#00b300'
                        circleStyle={{
                          width: 22,
                          height: 22,
                          borderRadius: 19,
                          backgroundColor: 'white',
                        }}
                        switchOn={this.state.ganjalBan}
                        circleColorOff='white'
                        circleColorOn='white'
                        duration={500}
                      />
                    </Right>
                  </ListItem>
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-outlet" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Kunci Roda
                      </Text>
                    </Body>
                    <Right style={{marginRight:-15}}>
                      <SwitchToggle
                        containerStyle={{
                          width: 70,
                          height: 30,
                          borderRadius: 25,
                          padding: 5,
                        }}
                        backgroundColorOff={colors.whitedar}
                        backgroundColorOn='#00b300'
                        circleStyle={{
                          width: 22,
                          height: 22,
                          borderRadius: 19,
                          backgroundColor: 'white',
                        }}
                        switchOn={this.state.kunciRoda}
                        circleColorOff='white'
                        circleColorOn='white'
                        duration={500}
                      />
                    </Right>
                  </ListItem>
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-outlet" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Hammer
                      </Text>
                    </Body>
                    <Right style={{marginRight:-15}}>
                      <SwitchToggle
                        containerStyle={{
                          width: 70,
                          height: 30,
                          borderRadius: 25,
                          padding: 5,
                        }}
                        backgroundColorOff={colors.whitedar}
                        backgroundColorOn='#00b300'
                        circleStyle={{
                          width: 22,
                          height: 22,
                          borderRadius: 19,
                          backgroundColor: 'white',
                        }}
                        switchOn={this.state.hammer}
                        circleColorOff='white'
                        circleColorOn='white'
                        duration={500}
                      />
                    </Right>
                  </ListItem>
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-outlet" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        Kotak PPPK
                      </Text>
                    </Body>
                    <Right style={{marginRight:-15}}>
                      <SwitchToggle
                        containerStyle={{
                          width: 70,
                          height: 30,
                          borderRadius: 25,
                          padding: 5,
                        }}
                        backgroundColorOff={colors.whitedar}
                        backgroundColorOn='#00b300'
                        circleStyle={{
                          width: 22,
                          height: 22,
                          borderRadius: 19,
                          backgroundColor: 'white',
                        }}
                        switchOn={this.state.kotakPPPK}
                        circleColorOff='white'
                        circleColorOn='white'
                        duration={500}
                      />
                    </Right>
                  </ListItem>
                </View>
              </CardItem>
              <View style={{marginTop:10, marginLeft:10, marginRight:10, backgroundColor:colors.white, height:100, marginBottom:20, borderRadius:10, paddingLeft:10, paddingRight:10, paddingTop:5,
                  shadowColor:'#000',
                  shadowOffset:{
                    width:0,
                    height:1,
                  },
                  shadowOpacity:1,
                  shadowRadius:2,
                  elevation:3,
                }}>
                  <Text style={{fontSize:15, fontWeight:'bold'}}>Note :</Text>
                  <Text style={{fontSize:12}}>{this.state.listPMKTools.note}</Text>
              </View>
            </View>
          ):(
            <Button
              block
              style={{
                height: 45,
                marginLeft: 20,
                marginRight: 20,
                marginBottom: 20,
                borderWidth: 1,
                backgroundColor: "#00b300",
                borderColor: "#00b300",
                borderRadius: 4
              }}
              onPress={() =>
                this.navigateToScreen("UnitCheckPMKCreate", this.state.idVehicle)
              }
            >
              <Text
                style={{
                  fontSize: 14,
                  fontWeight: "bold",
                  color: colors.white
                }}
              >
                Create Inspection PMK
              </Text>
            </Button>
          )}
          </ScrollView>
    )}
    return (
      <Container style={styles.wrapper2}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("UnitCheckPMK")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3, alignItems: "center" }}>
            <Title style={styles.textbody}>
              {this.state.dataVehicle.vehicle_code}
            </Title>
          </Body>
          <Right style={{ flex: 1 }}>
            <View style={{ flex: 0, flexDirection: "row" }}>
              {dateNow <= this.state.dataVehicle.exp_inspection && this.state.listPMKTools!==null && (
                <Button
                  transparent
                  onPress={() =>
                    this.navigateToScreenUpdate("UnitCheckPMKUpdate", this.state.listPMKTools)
                  }>
                  <Icon
                    name="ios-create"
                    size={20}
                    style={styles.facebookButtonIconOrder2}
                  />
                </Button>
              )}
            </View>
          </Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <Footer>
          <FooterTab style={styles.tabfooter}>
            <Button
              onPress={() =>
                this.props.navigation.navigate("UnitCheckDetailPMKEngine")
              }
            >
              <Text style={{ color: "white", fontWeight: "bold" }}>
                Engine
              </Text>
            </Button>
            <Button active style={styles.tabfooter}>
              <View style={{ height: "40%" }} />
              <View style={{ height: "50%" }}>
                <Text style={styles.textbody}> Tools </Text>
              </View>
              <View style={{ height: "20%" }} />
              <View
                style={{
                  borderWidth: 2,
                  marginTop: 2,
                  height: 0.5,
                  width: "100%",
                  borderColor: colors.white
                }}
              />
            </Button>
          </FooterTab>
        </Footer>
        <View style={{ marginTop: 5, flex: 1, flexDirection: "column" }}>
          {list}
        </View>
      </Container>
    );
  }
}

export default UnitCheckDetailPMKTools;
