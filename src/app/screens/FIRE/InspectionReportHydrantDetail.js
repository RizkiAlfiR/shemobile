import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import Moment from "moment";

class InspectionReportHydrantDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      dataDetail: [],
      isLoading: true,
      dataLokasi:[],
      listReportHydrant: [],
      listParameter: [],
      visibleDialogSubmit: false,
      idLokasi: "",
      idHydrant:"",
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} />;

  componentDidMount() {
    AsyncStorage.getItem("idLokasi").then(idLokasi => {
        this.setState({
          idHydrant: idLokasi,
          isLoading: false,
        });
        this.loadLokasi();
    });
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadData();
      }
    );
  }

  loadLokasi() {
    this.setState({ visibleDialog: false, isLoading: true });
      const url = GlobalConfig.SERVERHOST + "getLokasiHydrant";
      var formData = new FormData();
      formData.append("id_hydrant", this.state.idHydrant);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            dataLokasi: responseJson.data,
            isLoading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
  }

  loadData() {
    AsyncStorage.getItem("token").then(value => {
      const url = GlobalConfig.SERVERHOST + "detailInspectionHydrant";
      var formData = new FormData();
      formData.append("id_hydrant", this.state.idHydrant);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            listReportHydrant: responseJson.data,
            isLoading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  navigateToScreen(route, listReportHydrant) {
    AsyncStorage.setItem("listReportHydrant", JSON.stringify(listReportHydrant)).then(() => {
        this.props.navigation.navigate(route);
    });
  }

  navigateToScreenCreate(route, idHydrant) {
    AsyncStorage.setItem("idHydrant", idHydrant).then(() => {
        this.props.navigation.navigate(route);
        // alert(JSON.stringify(idHydrant))
      }
    );
  }

  render() {
    var that = this;
    var dateNow = Moment().format('YYYY-MM-DD');
    return (
      <Container style={styles.wrapper2}>
      <Header style={styles.header}>
        <Left style={{ flex: 1 }}>
          <Button
            transparent
            onPress={() =>
              that.props.navigation.navigate("InspectionReportHydrant")
            }
          >
            <Icon
              name="ios-arrow-back"
              size={20}
              style={styles.facebookButtonIconOrder2}
            />
          </Button>
        </Left>
        <Body style={{ flex: 3, alignItems: "center" }}>
          <Title style={styles.textbody}>
            {this.state.dataLokasi.area}
          </Title>
        </Body>
        <Right style={{ flex: 1 }}>
          <View style={{ flex: 0, flexDirection: "row" }}>
            {dateNow <= this.state.dataLokasi.exp_inspection && (
            <Button
              transparent
              transparent
              onPress={() =>
                this.navigateToScreen("InspectionReportHydrantUpdate", this.state.listReportHydrant)
              }
            >
              <Icon
                name="ios-create"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          )}
          </View>
        </Right>
      </Header>
      <StatusBar
        backgroundColor={colors.green03}
        barStyle="light-content"
      />
      <View style={{marginTop:10, marginLeft:10, marginRight:10, backgroundColor:colors.white, height:90, marginBottom:5, borderRadius:10, paddingLeft:5, paddingRight:10, paddingTop:5,
          shadowColor:'#000',
          shadowOffset:{
            width:0,
            height:1,
          },
          shadowOpacity:1,
          shadowRadius:2,
          elevation:3,
        }}>
        <View style={{flex:1, flexDirection:'row'}}>
          <View style={{width:'20%'}}>
            <View style={{width:'100%', paddingTop:3, paddingBottom:5}}>
              <Image
                style={{ marginTop:0, width: '100%', height: '100%', resizeMode: "contain", borderRadius:5}}
                source={require("../../../assets/images/hydrant.png")}
              />
            </View>
          </View>
          <View style={{width:'80%', justifyContent:'center'}}>
            <Text style={{ fontSize: 12, fontWeight: "bold" }}>
              {this.state.dataLokasi.area}
            </Text>
            <Text style={{ fontSize: 10 }}>Area</Text>
            <Text style={{ fontSize: 12, paddingTop: 5, fontWeight: "bold" }}>
              {this.state.dataLokasi.unit_kerja}
            </Text>
            <Text style={{ fontSize: 10 }}>
              Unit Kerja
            </Text>
          </View>
        </View>
      </View>
      <ScrollView>
        <Content>
          <View style={{paddingLeft:10, paddingTop:10, paddingBottom:10}}>
              <Text style={{ fontSize: 10 }}>Status</Text>
              {dateNow <= this.state.dataLokasi.exp_inspection ? (
                <Text
                  style={{
                    fontSize: 10,
                    fontWeight: "bold",
                    color: colors.green01
                  }}
                >
                  CHECKED
                </Text>
              ) : (
                <Text
                  style={{
                    fontSize: 10,
                    fontWeight: "bold",
                    color: "#FF0101"
                  }}
                >
                  UNCECKED
                </Text>
              )}
            </View>
          {dateNow <= this.state.dataLokasi.exp_inspection && this.state.listReportHydrant!=null ? (
            <View style={{}}>
              <View style={{marginTop:10, marginLeft:10, marginRight:10, backgroundColor:colors.white, height:50, marginBottom:5, borderRadius:10, paddingLeft:5, paddingRight:10,
                shadowColor:'#000',
                shadowOffset:{
                  width:0,
                  height:1,
                },
                shadowOpacity:1,
                shadowRadius:2,
                elevation:3,
              }}>
                <View style={{flex:1, flexDirection:'row'}}>
                  <View style={{width:'13%', justifyContent:'center'}}>
                    <Icon
                      name="ios-contact"
                      style={{fontSize:40, color:colors.graydark}}
                    />
                  </View>
                  <View style={{width:'87%', justifyContent:'center'}}>
                    <Text style={{ fontSize: 11, fontWeight: "bold" }}>
                      {this.state.listReportHydrant.pic_badge}
                    </Text>
                    <Text style={{ fontSize: 13,}}>
                      {this.state.listReportHydrant.pic_name}
                    </Text>
                  </View>
                </View>
              </View>

              <CardItem style={{ borderRadius: 0 }}>
                <View style={{ flex: 1, flexDirection: "column" }}>
                  <View>
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      Inspection Box
                    </Text>
                  </View>

                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      paddingTop: 5
                    }}
                  >
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>Casing</Text>
                      {this.state.listReportHydrant.box_casing == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_casing == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_casing == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_casing == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_casing == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>Hose</Text>
                      {this.state.listReportHydrant.box_hose == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_hose == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_hose == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_hose == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_hose == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>Nozle</Text>
                      {this.state.listReportHydrant.box_nozle == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_nozle == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_nozle == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_nozle == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_nozle == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>Kunci</Text>
                      {this.state.listReportHydrant.box_kunci == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_kunci == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_kunci == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_kunci == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_kunci == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                  </View>

                  <View style={{ paddingTop: 15 }}>
                    <Text style={{ fontSize: 10 }}>Box Note</Text>
                    {this.state.listReportHydrant.box_note != null ? (
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                        {this.state.listReportHydrant.box_note}
                      </Text>
                    ) : (
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                        {" "}
                        -
                      </Text>
                    )}
                  </View>
                </View>
              </CardItem>

              <CardItem style={{ borderRadius: 0 }}>
                <View style={{ flex: 1, flexDirection: "column" }}>
                  <View>
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      Inspection Copling
                    </Text>
                  </View>

                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      paddingTop: 5
                    }}
                  >
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>Left Copling</Text>
                      {this.state.listReportHydrant.copling_kiri == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.copling_kiri == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.copling_kiri == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.copling_kiri == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.copling_kiri == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>Right Copling</Text>
                      {this.state.listReportHydrant.copling_kanan == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.copling_kanan == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.copling_kanan == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.copling_kanan == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.copling_kanan == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>T Left</Text>
                      {this.state.listReportHydrant.t_copling_kiri == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.t_copling_kiri == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.t_copling_kiri == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.t_copling_kiri == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.t_copling_kiri == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>T Right</Text>
                      {this.state.listReportHydrant.t_copling_kanan == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.t_copling_kanan == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.t_copling_kanan == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.t_copling_kanan == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.t_copling_kanan == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                  </View>

                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      paddingTop: 20
                    }}
                  >
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>Valve Left</Text>
                      {this.state.listReportHydrant.valve_kiri == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_kiri == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_kiri == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_kiri == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_kiri == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>Valve Right</Text>
                      {this.state.listReportHydrant.valve_kanan == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_kanan == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_kanan == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_kanan == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_kanan == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>Valve Top</Text>
                      {this.state.listReportHydrant.valve_atas == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_atas == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_atas == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_atas == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_atas == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                  </View>

                  <View style={{ paddingTop: 15 }}>
                    <Text style={{ fontSize: 10 }}>Valve Note</Text>
                    {this.state.listReportHydrant.box_note != 'null' ? (
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                        {this.state.listReportHydrant.valve_note}
                      </Text>
                    ) : (
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                        {" "}
                        -
                      </Text>
                    )}
                  </View>
                </View>
              </CardItem>

              <CardItem style={{ borderRadius: 0 }}>
                <View>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    Inspection Pilar
                  </Text>
                  <Text style={{ fontSize: 10, paddingTop: 5 }}>
                    Body
                  </Text>
                  {this.state.listReportHydrant.pilar_body == "V" && (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      Baik
                    </Text>
                  )}
                  {this.state.listReportHydrant.pilar_body == "K" && (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      Kotor
                    </Text>
                  )}
                  {this.state.listReportHydrant.pilar_body == "-" && (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      Tidak Ada
                    </Text>
                  )}
                  {this.state.listReportHydrant.pilar_body == "X" && (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      Rusak
                    </Text>
                  )}
                  {this.state.listReportHydrant.pilar_body == "I" && (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      Seret/Macet/Merembes
                    </Text>
                  )}
                  <Text style={{ fontSize: 10, paddingTop: 15 }}>
                    Pilar Note
                  </Text>
                  {this.state.listReportHydrant.pilar_note != 'null' ? (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {this.state.listReportHydrant.pilar_note}
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {" "}
                      -
                    </Text>
                  )}
                </View>
              </CardItem>

              <View style={{flex:1, flexDirection:'row'}}>
                <View style={{width:'50%'}}>
                  <View style={{width:'100%', paddingTop:5, paddingBottom:5, paddingLeft:15}}>
                    <Image
                      style={{ marginTop:0, width: '100%', height: '100%', resizeMode: "contain", borderRadius:5}}
                      source={require("../../../assets/images/iconCO2.jpg")}
                    />
                  </View>
                </View>
                <View style={{width:'50%', paddingLeft:25, paddingTop:20}}>
                  <Text style={styles.detailContent2}>
                    Tuban
                  </Text>
                  <Text style={styles.detailTitle}>Plant</Text>

                  <Text style={styles.detailContent2}>
                    PT Semen Indonesia
                  </Text>
                  <Text style={styles.detailTitle}>Company</Text>

                  <Text style={styles.detailContent}>
                    {Moment(this.state.listReportHydrant.inspection_date).format('DD MMMM YYYY')}
                  </Text>
                  <Text style={styles.detailTitle}>Inspection Date</Text>
                </View>
              </View>

              <CardItem style={{ borderRadius: 0 }}>
                <View>
                  <Text style={{ fontSize: 10 }}>Press</Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listReportHydrant.press}
                  </Text>
                  <Text style={{ fontSize: 10, paddingTop: 15 }}>
                    Temuan
                  </Text>
                  {this.state.listReportHydrant.temuan != 'null' ? (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {this.state.listReportHydrant.temuan}
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {" "}
                      -
                    </Text>
                  )}
                </View>
              </CardItem>
              <CardItem>
                <View style={{ backgroundColor: '#eee', width: '100%', height: 200, borderRadius: 10, marginBottom: 80 }}>
                  <Image
                    style={{
                      width: "100%",
                      height: "100%",
                      borderRadius: 10,
                      marginBottom: 20
                    }}
                    source={{
                      uri:
                        GlobalConfig.IMAGEHOST + 'InspectionHydrant/' + this.state.listReportHydrant.foto_temuan
                    }}
                  />
                </View>
              </CardItem>
            </View>
          ):(
            <Button
              block
              style={{
                height: 45,
                marginLeft: 20,
                marginRight: 20,
                marginBottom: 20,
                borderWidth: 1,
                backgroundColor: "#00b300",
                borderColor: "#00b300",
                borderRadius: 4
              }}
              onPress={() =>
                this.navigateToScreenCreate("InspectionReportHydrantCreate", this.state.idHydrant)
              }
            >
              <Text
                style={{
                  fontSize: 14,
                  fontWeight: "bold",
                  color: colors.white
                }}
              >
                Create Inspection Hydrant
              </Text>
            </Button>
          )}
            <View style={{ width: 270, position: "absolute" }}>
              <Dialog
                visible={this.state.visibleDialogSubmit}
                dialogTitle={
                  <DialogTitle title="Delete Inspection Report Alarm .." />
                }
              >
                <DialogContent>
                  {
                    <ActivityIndicator
                      size="large"
                      color="#330066"
                      animating
                    />
                  }
                </DialogContent>
              </Dialog>
            </View>
          </Content>
        </ScrollView>
      </Container>
    );
  }
}

export default InspectionReportHydrantDetail;
