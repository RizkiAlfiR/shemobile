import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    TextInput,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Textarea,
    Icon,
    Picker,
    Form,
    Input,
    Item,
    Switch
} from "native-base";

import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton
  } from "react-native-popup-dialog";
import ImagePicker from "react-native-image-picker";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DatePicker from 'react-native-datepicker';
import Ripple from "react-native-material-ripple";
import Moment from 'moment'
import SwitchToggle from 'react-native-switch-toggle';

export default class InspectionReportAlarmCreate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            dataSource: [],
            isLoading: true,
            visibleDialogSubmit:false,
            typePeriode:'',
            listReportAlarm:[],
            paramID:[],
            paramName:[],
            statusCond:[],
            note:[],
            date:'',
            month:'',
            year:'',
        };
    }

    static navigationOptions = {
        header: null
    };


    _renderItem = ({ item }) => (
        <ListItem data={item}></ListItem>
    )

    componentDidMount() {
      AsyncStorage.getItem('listReport').then((listReport) => {
        this.setState({
          listReportAlarm: JSON.parse(listReport),
        });
      })

        this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
        this.loadData();
      });
      var dateNow = Moment().format('YYYY-MM-DD');
      var monthNow = Moment().format('MM');
      var yearNow = Moment().format('YYYY');
      this.setState({
          date:dateNow,
          month:monthNow,
          year:yearNow,
      })
    }

    loadData() {
    }

    loadParameter(){
      AsyncStorage.getItem('token').then((value) => {
          const url = GlobalConfig.SERVERHOST + 'api/v_mobile/firesystem/alarm_n/view_parameter_type/' + this.state.typePeriode ;
          var formData = new FormData();
          formData.append("token", value)

          fetch(url, {
              headers: {
                  'Content-Type': 'multipart/form-data'
              },
              method: 'POST',
              body: formData
          })
              .then((response) => response.json())
              .then((responseJson) => {
                  this.setState({
                      dataSource: responseJson.data,
                      isLoading: false
                  });
                  var x;
                  this.setState({
                    paramID:[],
                    paramName:[],
                    statusCond:[],
                    note:[],
                  })
                  for(x=0; x<this.state.dataSource.length; x++){
                      var status = 'X'
                      var note = ''
                      this.state.paramID.push(this.state.dataSource[x].ID)
                      this.state.paramName.push(this.state.dataSource[x].PARAMETER_NAME)
                      this.state.statusCond.push(false)
                      this.state.note.push(note)
                  }
              })
              .catch((error) => {
                  console.log(error)
              })
      })
      }

      createReportAlarm(){
        if(this.state.typePeriode == ''){
          alert("Masukkan Tipe Pengecekan")
        } else {
          this.setState({
              visibleDialogSubmit:true
          })
          AsyncStorage.getItem("token").then(value => {
              var url = GlobalConfig.SERVERHOST + "api/v_mobile/firesystem/alarm_n/cek_record_alarm/create";
              var formData = new FormData();
              formData.append("token", value);
              formData.append("LOCATION_ID", this.state.listReportAlarm.ID_LOKASI_ALARM);
              formData.append("LOCATION_NAME", this.state.listReportAlarm.KONDISI_VALUE);
              formData.append("YEAR", this.state.year);
              formData.append("MONTH", this.state.month);
              formData.append("INSPECTION_DATE", this.state.date);
              formData.append("TYPE_PERIODE", this.state.typePeriode);
              for(x=0; x<this.state.dataSource.length; x++){
                formData.append("PARAM_ID["+ x +"]", this.state.paramID[x]);
                formData.append("PARAM_NAME["+ x +"]", this.state.paramName[x]);
                if(this.state.statusCond[x]==true) {
                  formData.append("STATUS_COND["+ x +"]", 'V');
                } else {
                  formData.append("STATUS_COND["+ x +"]", 'X');
                }
                formData.append("NOTE["+ x +"]", this.state.note[x]);
              }
              fetch(url, {
                  headers: {
                      "Content-Type": "multipart/form-data"
                  },
                  method: "POST",
                  body: formData
              })
                  .then(response => response.json())
                  .then(response => {
                      if (response.Status == 200) {
                          this.setState({
                              visibleDialogSubmit:false
                          })
                          Alert.alert('Success', 'Create Inspection Report Alarm Success', [{
                              text: 'Okay'
                          }])
                          this.props.navigation.navigate('InspectionReportAlarm')
                      } else {
                          this.setState({
                              visibleDialogSubmit:false
                          })
                          Alert.alert('Error', 'Create Inspection Report Alarm Failed', [{
                              text: 'Okay'
                          }])
                      }
                  })
                  .catch((error)=>{
                      console.log(error)
                      this.setState({
                          visibleDialogSubmit:false
                      })
                      Alert.alert('Error', 'Create Inspection Report Alarm Failed', [{
                          text: 'Okay'
                      }])
                  })
            })
        }
    }

    onPressStatus(index) {
        let arr = this.state.statusCond;
        let cond = !this.state.statusCond[index];
        arr[index] = cond;
        this.setState({
          statusCond: arr
        });
    }

    ChangeText(text, index) {
      let arr = this.state.note;
      arr[index] = text;
      this.setState({
        note: arr
      });
    }

    render() {
      var that=this
      listParameter=(
          <View style={{ flex: 1 }}>
            {this.state.dataSource.map((listParameter, index) => (
              <View key={index} style={{ backgroundColor: colors.gray }}>
                <View style={{marginTop: 5, marginBottom: 5,}}>
                  <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: '78%' }}>
                          <Text style={{ fontSize: 10, paddingTop: 5}}>
                            {listParameter.PARAMETER_NAME}
                          </Text>
                      </View>
                      <View style={{ width: '22%' }}>
                          <SwitchToggle
                            containerStyle={{
                              width: 70,
                              height: 30,
                              borderRadius: 25,
                              padding: 5,
                            }}
                            backgroundColorOff={colors.graydar}
                            backgroundColorOn='#00b300'
                            circleStyle={{
                              width: 22,
                              height: 22,
                              borderRadius: 19,
                              backgroundColor: 'white',
                            }}

                            switchOn={this.state.statusCond[index]}
                            onPress={() => this.onPressStatus(index)}
                            circleColorOff='white'
                            circleColorOn='white'
                            duration={500}
                          />
                      </View>
                  </View>
                  <View style={{ flex: 1 }}>
                      <View>
                          <Textarea style={{
                              marginBottom: 5,
                              borderRadius: 5,
                              paddingTop:10,
                              fontSize: 11 }}
                              rowSpan={2}
                              bordered value={this.state.note[index]}
                              placeholder='Keterangan ...'
                              onChangeText={(text) => this.ChangeText(text, index)} />
                      </View>
                  </View>
                </View>
               </View>
            ))}
          </View>
      )
        return (
            <Container style={styles.wrapper}>
                <Header style={styles.header}>
                    <Left style={{flex:1}}>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate("InspectionReportAlarmDetail")}
                        >
                            <Icon
                                name="ios-arrow-back"
                                size={20}
                                style={styles.facebookButtonIconOrder2}
                            />
                        </Button>
                    </Left>
                    <Body style={{ flex:3, alignItems:'center' }}>
                        <Title style={styles.textbody}>Inspection Alarm</Title>
                    </Body>
                    <Right style={{flex:1}}/>
                </Header>
                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />

                <View style={{ flex: 1 }}>
                    <Content style={{ marginTop: 0}}>
                        <View style={{ backgroundColor: colors.gray }}>
                        <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                            <View>
                            <Text style={{fontSize:10}}>Lokasi</Text>
                            <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.listReportAlarm.TYPE_KONDISI}</Text>
                            <Text style={{fontSize:10, paddingTop: 5}}>Kondisi</Text>
                            <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.listReportAlarm.KONDISI_VALUE}</Text>
                        </View>
                        </CardItem>

                        <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                            <View style={{ flex: 1,flexDirection:'row'}}>
                                <View style={{width:'80%'}}>
                                    <Text style={styles.titleInput}>Tipe Pengecekan *</Text>
                                    <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                                        <Picker
                                            mode="dropdown"
                                            iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                                            style={{ width: '100%', height: 35}}
                                            placeholder="Kelas"
                                            placeholderStyle={{ color: "#bfc6ea" }}
                                            placeholderIconColor="#007aff"
                                            selectedValue={this.state.typePeriode}
                                            onValueChange={(itemValue) => this.setState({ typePeriode: itemValue })}>
                                            <Picker.Item label="Parameter Pengecekan" value=""/>
                                            <Picker.Item label="Mingguan" value="WEEK"/>
                                            <Picker.Item label="Bulanan" value="MONTH" />
                                        </Picker>
                                    </Form>
                                </View>
                                <View style={{width:'20%', paddingTop:15}}>
                                  <Button
                                    block
                                    style={{
                                      width:'100%',
                                      height: 35,
                                      marginBottom: 0,
                                      borderWidth: 1,
                                      backgroundColor: "#00b300",
                                      borderColor: "#00b300",
                                      borderRadius: 4
                                    }}
                                    onPress={() => this.loadParameter()}
                                  >
                                  <Icon
                                    name="ios-search"
                                    size={20}
                                    style={styles.facebookButtonIconOrder2}
                                  />
                                  </Button>
                                </View>
                            </View>
                        </CardItem>

                        <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                          {listParameter}
                        </CardItem>

                        <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                          <View style={{ flex: 1}}>
                          <View style={styles.Contentsave}>
                            <Button
                              block
                              style={{
                                width:'100%',
                                height: 45,
                                marginBottom: 20,
                                borderWidth: 1,
                                backgroundColor: "#00b300",
                                borderColor: "#00b300",
                                borderRadius: 4
                              }}
                              onPress={() => this.createReportAlarm()}
                            >
                              <Text style={{color:colors.white}}>SUBMIT</Text>
                            </Button>
                          </View>
                          </View>
                        </CardItem>
                    </View>
                    <View style={{ width: 270, position: "absolute" }}>
                        <Dialog
                            visible={this.state.visibleDialogSubmit}
                            dialogTitle={<DialogTitle title="Create Inspection Report Alarm .." />}
                        >
                            <DialogContent>
                            {<ActivityIndicator size="large" color="#330066" animating />}
                            </DialogContent>
                        </Dialog>
                    </View>
                    </Content>
                </View>
                <CustomFooter navigation={this.props.navigation} menu='Safety' />
            </Container>

        );
    }
}
