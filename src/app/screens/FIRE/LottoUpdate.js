import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Textarea,
  Icon,
  Picker,
  Form,
  Input
} from "native-base";

import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import moment from 'moment';
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import DatePicker from "react-native-datepicker";
import DateTimePicker from "react-native-datepicker";
import Ripple from "react-native-material-ripple";

export default class LottoUpdate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      dataHeader: [],
      isLoading: true,
      dataLotto: [],
      equipmentNumber: "",
      noER: "",
      lokasi: "",
      aktifitas: "",
      date: "",
      time: "",
      note: "",
      safetyKey: "",
      tools: "",
      listTools: [],
      visibleDialogSubmit: false,
      idLotto:'',
      save:false,
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} />;

  componentDidMount() {
    AsyncStorage.getItem("dataLotto").then(dataLotto => {

      this.setState({
        dataLotto: JSON.parse(dataLotto),
        idLotto: JSON.parse(dataLotto).id,
        equipmentNumber: JSON.parse(dataLotto).equipment_number,
        noER: JSON.parse(dataLotto).no_er,
        lokasi: JSON.parse(dataLotto).location,
        aktifitas: JSON.parse(dataLotto).activity_description,
        date: JSON.parse(dataLotto).date,
        time: JSON.parse(dataLotto).time,
        note: JSON.parse(dataLotto).note,
        safetyKey: JSON.parse(dataLotto).safety_key
      });
      this.setState({
        listTools: JSON.parse(this.state.dataLotto.safety_tools)
      });
      if(this.state.note=='null'){
        this.setState({
          note:''
        })
      }
    });
  }

  loadData() {}

  updateLotto() {
    if (this.state.equipmentNumber == null) {
      alert("Input Equipment Number");
    } else if (this.state.lokasi == null) {
      alert("Input Location");
    } else if (this.state.noER == null) {
      alert("Input Nomor ER");
    } else if (this.state.aktifitas == null) {
      alert("Input Activity");
    } else if (this.state.date == null) {
      alert("Input Date");
    } else if (this.state.time == null) {
      alert("Input Time");
    } else if (this.state.safetyKey == null) {
      alert("Input Safety Key");
    } else if (this.state.listTools == null) {
      alert("Input Safety Tools");
    } else {
      this.setState({
        save:true
      });
    }
  }

  updateLottoFinal(){
    this.setState({
      visibleDialogSubmit: true,
      save:false,
    });
      var url = GlobalConfig.SERVERHOST + "updateLotto";
      var formData = new FormData();
      formData.append("id", this.state.idLotto);
      formData.append("equipment_number", this.state.equipmentNumber);
      formData.append("no_er", this.state.noER);
      formData.append("location", this.state.lokasi);
      formData.append("activity_description", this.state.aktifitas);
      formData.append("date", this.state.date);
      formData.append("time", this.state.time);
      if (this.state.note != "") {
        formData.append("note", this.state.note);
      } else {
        formData.append("note", "");
      }
      formData.append("safety_key", this.state.safetyKey);
      formData.append("safety_tools", JSON.stringify(this.state.listTools));
      formData.append("status", 'OPEN');

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Success", "Update Lotto Success", [
              {
                text: "Oke"
              }
            ]);
            this.props.navigation.navigate("LottoDetail");
          } else {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Error", "Update Lotto Failed", [
              {
                text: "Oke"
              }
            ]);
          }
        })
        .catch((error)=>{
          this.setState({
            visibleDialogSubmit: false
          });
          Alert.alert("Error", "Update Lotto Failed", [
            {
              text: "Oke"
            }
          ]);
          console.log(error)
        })
  }

  tambahTools() {
    this.setState({
      tools: ""
    });
    let tools = this.state.tools;
    if (tools == "") {
    } else {
      this.setState({
        listTools: [...this.state.listTools, tools]
      });
    }
  }

  deleteTools(index) {
    let arr = this.state.listTools;
    arr.splice(index, 1);
    this.setState({
      listTools: arr
    });
  }

  render() {
    return (
      <Container style={styles.wrapper2}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("Lotto")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>Edit Lotto</Title>
          </Body>
          <Right style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.updateLotto()}
            >
              <Icon
                name="ios-checkmark"
                style={{fontSize:40, color:colors.white}}
              />
            </Button>
          </Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ flex: 1 }}>
        <Content style={{ marginTop: 0 }}>
          <View style={{ backgroundColor: "#FEFEFE" }}>
            <View style={{flex:1, paddingLeft:15, paddingRight:15, flexDirection:'row', marginTop:10}}>
              <View style={{ flex: 1 }}>
                <View>
                  <Text style={styles.titleInput}>Equipment Number *</Text>
                </View>
                <View>
                  <Textarea
                    style={{
                      borderRadius: 5,
                      marginLeft: 5,
                      marginRight: 5,
                      fontSize: 10
                    }}
                    rowSpan={2}
                    bordered
                    value={this.state.equipmentNumber}
                    placeholder="Input Equipment ..."
                    onChangeText={text =>
                      this.setState({ equipmentNumber: text })
                    }
                  />
                </View>
              </View>
            </View>
            <View style={{flex:1, paddingLeft:15, paddingRight:15, flexDirection:'row', marginTop:10}}>
              <View style={{ flex: 1 }}>
                <View>
                  <Text style={styles.titleInput}>Location *</Text>
                </View>
                <View>
                  <Textarea
                    style={{
                      borderRadius: 5,
                      marginLeft: 5,
                      marginRight: 5,
                      fontSize: 10
                    }}
                    rowSpan={2}
                    bordered
                    value={this.state.lokasi}
                    placeholder="Input Location ..."
                    onChangeText={text => this.setState({ lokasi: text })}
                  />
                </View>
              </View>
            </View>
            <View style={{flex:1, paddingLeft:15, paddingRight:15, flexDirection:'row', marginTop:10}}>
              <View style={{ flex: 1 }}>
                <View>
                  <Text style={styles.titleInput}>No Er *</Text>
                </View>
                <View>
                  <Textarea
                    style={{
                      borderRadius: 5,
                      marginLeft: 5,
                      marginRight: 5,
                      fontSize: 10
                    }}
                    rowSpan={2}
                    bordered
                    value={this.state.noER}
                    placeholder="Input Nomor ER ..."
                    onChangeText={text => this.setState({ noER: text })}
                  />
                </View>
              </View>
            </View>
            <View style={{flex:1, paddingLeft:15, paddingRight:15, flexDirection:'row', marginTop:10}}>
              <View style={{ flex: 1 }}>
                <View>
                  <Text style={styles.titleInput}>
                    Activities Description *
                  </Text>
                </View>
                <View>
                  <Textarea
                    style={{
                      borderRadius: 5,
                      marginLeft: 5,
                      marginRight: 5,
                      fontSize: 10
                    }}
                    rowSpan={2}
                    bordered
                    value={this.state.aktifitas}
                    placeholder="Input Aktifitas ..."
                    onChangeText={text => this.setState({ aktifitas: text })}
                  />
                </View>
              </View>
            </View>
            <View style={{flex:1, paddingLeft:15, paddingRight:15, flexDirection:'row', marginTop:10}}>
              <View style={{width:'50%'}}>
                <Text style={styles.titleInput}>Date *</Text>
                <DatePicker
                  style={{ width: "100%", fontSize: 10, borderRadius: 20 }}
                  date={this.state.date}
                  mode="date"
                  placeholder="Choose Date ..."
                  format="YYYY-MM-DD"
                  minDate="2018-01-01"
                  maxDate="5000-12-31"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  iconSource='{this.timeIcon}'
                  customStyles={{
                    dateInput: {
                      marginLeft: 5,
                      marginRight: 5,
                      height: 35,
                      borderRadius: 5,
                      fontSize: 10,
                      borderWidth: 1,
                      borderColor: "#E6E6E6"
                    },
                    dateIcon: {
                      position: "absolute",
                      left: 0,
                      top: 5
                    }
                  }}
                  onDateChange={date => {
                    this.setState({ date: date });
                  }}
                />
              </View>
              <View style={{width:'50%'}}>
                <Text style={styles.titleInput}>Time *</Text>
                <DateTimePicker
                  style={{ width: "100%", fontSize: 10, borderRadius: 20 }}
                  date={this.state.time}
                  mode="time"
                  placeholder="Choose Time ..."
                  format="hh:mm"
                  minTime="00:00"
                  maxTime="23:59"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  iconSource='{this.timeIcon}'
                  customStyles={{
                    dateInput: {
                      marginLeft: 5,
                      marginRight: 5,
                      height: 35,
                      borderRadius: 5,
                      fontSize: 10,
                      borderWidth: 1,
                      borderColor: "#E6E6E6"
                    },
                    dateIcon: {
                      position: "absolute",
                      left: 0,
                      top: 5
                    }
                  }}
                  onDateChange={time => {
                    this.setState({ time: time });
                  }}
                />
              </View>
            </View>

            <View style={{flex:1, paddingLeft:15, paddingRight:15, flexDirection:'row', marginTop:10}}>
              <View style={{ flex: 1 }}>
                <View>
                  <Text style={styles.titleInput}>Note</Text>
                </View>
                <View>
                  <Textarea
                    style={{
                      borderRadius: 5,
                      marginLeft: 5,
                      marginRight: 5,
                      fontSize: 10
                    }}
                    rowSpan={2}
                    bordered
                    value={this.state.note}
                    placeholder="Input Note ..."
                    onChangeText={text => this.setState({ note: text })}
                  />
                </View>
              </View>
            </View>
            <View style={{flex:1, paddingLeft:15, paddingRight:15, flexDirection:'row', marginTop:10}}>
              <View style={{ flex: 1 }}>
                <View>
                  <Text style={styles.titleInput}>Safety Key *</Text>
                </View>
                <View>
                  <Textarea
                    style={{
                      borderRadius: 5,
                      marginLeft: 5,
                      marginRight: 5,
                      fontSize: 10
                    }}
                    rowSpan={2}
                    bordered
                    value={this.state.safetyKey}
                    placeholder="Input Safety Key ..."
                    onChangeText={text => this.setState({ safetyKey: text })}
                  />
                </View>
              </View>
            </View>
            <View style={{flex:1, paddingLeft:15, paddingRight:15, flexDirection:'row', marginTop:10}}>
              <View style={{ flex: 1 }}>
                <View>
                  <Text style={styles.titleInput}>Safety Tools *</Text>
                </View>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ width: 280 }}>
                    <Textarea
                      style={{ borderRadius: 5, marginLeft: 5, fontSize: 10 }}
                      rowSpan={1.5}
                      bordered
                      value={this.state.tools}
                      placeholder="Safety Tools ..."
                      onChangeText={text => this.setState({ tools: text })}
                    />
                  </View>
                  <Button
                    onPress={() => this.tambahTools()}
                    style={styles.btnTambah}
                  >
                    <Icon name="ios-add" style={styles.iconPluss} />
                  </Button>
                </View>
              </View>
            </View>
            <View style={{flex:1, paddingLeft:15, paddingRight:15, flexDirection:'row', marginTop:10, marginBottom:20}}>
              <View style={{ flex: 1 }}>
                {this.state.listTools.map((listTools, index) => (
                  <View key={index} style={{ backgroundColor: colors.gray }}>
                    <View
                      style={{
                        marginTop: 5,
                        marginBottom: 5,
                        marginLeft: 10,
                        marginRight: 10,
                        height: 30,
                        backgroundColor: colors.white,
                        borderRadius: 5
                      }}
                    >
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ width: 280 }}>
                          <Text
                            style={{
                              paddingLeft: 10,
                              fontSize: 11,
                              paddingTop: 5
                            }}
                          >
                            {listTools}
                          </Text>
                        </View>
                        <View>
                          <Ripple
                            style={{
                              flex: 2,
                              justifyContent: "center",
                              alignItems: "center"
                            }}
                            rippleSize={176}
                            rippleDuration={600}
                            rippleContainerBorderRadius={15}
                            onPress={() => this.deleteTools(index)}
                            rippleColor={colors.accent}
                          >
                            <Icon name="ios-close" style={styles.iconClose} />
                          </Ripple>
                        </View>
                      </View>
                    </View>
                  </View>
                ))}
              </View>
            </View>
            <View style={{ width: '100%', position: "absolute"}}>
              <Dialog
                visible={this.state.save}
                dialogAnimation={
                  new SlideAnimation({
                    slideFrom: "bottom"
                  })
                }
                dialogStyle={{ position: "absolute", top: this.state.posDialog, width:300}}
                onTouchOutside={() => {
                  this.setState({ login: false });
                }}
              >
              <DialogContent>
              {
                <View>
                  <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
                    <Text style={{fontSize:15, alignItems:'center', color:colors.black}}>Do you want to save this data ?</Text>
                  </View>
                  <View style={{flexDirection:'row', flex:1, paddingTop:10, width:'100%'}}>
                    <View style={{width:'50%', paddingRight:5}}>
                        <Button
                          block
                          style={{
                            width:'100%',
                            marginTop:10,
                            height: 35,
                            marginBottom: 5,
                            borderWidth: 0,
                            backgroundColor: colors.primer,
                            borderRadius: 15
                          }}
                          onPress={() => this.updateLottoFinal()}
                        >
                          <Text style={{color:colors.white}}>Yes</Text>
                        </Button>
                    </View>
                    <View style={{width:'50%', paddingLeft:5}}>
                        <Button
                          block
                          style={{
                            width:'100%',
                            marginTop:10,
                            height: 35,
                            marginBottom: 5,
                            borderWidth: 0,
                            backgroundColor: colors.primer,
                            borderRadius: 15
                          }}
                          onPress={() => this.setState({
                            save:false,
                          })}
                        >
                          <Text style={{color:colors.white}}>No</Text>
                        </Button>
                    </View>
                  </View>
                </View>
              }
              </DialogContent>
              </Dialog>
            </View>
            <View style={{ width: 270, position: "absolute" }}>
                <Dialog
                    visible={this.state.visibleDialogSubmit}
                >
                    <DialogContent>
                    {
                      <View>
                        <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
                          <Text style={{fontSize:15, alignItems:'center', color:colors.black}}>Updating Lotto ...</Text>
                        </View>
                        <ActivityIndicator size="large" color="#330066" animating />
                      </View>
                    }
                    </DialogContent>
                </Dialog>
            </View>
          </View>
        </Content>
        </View>
      </Container>
    );
  }
}
