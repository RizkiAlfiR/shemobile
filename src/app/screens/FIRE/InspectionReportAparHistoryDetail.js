import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator,
  RefreshControl
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Ripple from "react-native-material-ripple";
import SwitchToggle from "react-native-switch-toggle";

class InspectionReportAparHistoryDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isLoading: true,
      listReportAparHistory: [],
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        AsyncStorage.getItem("history").then(history => {
          this.setState({ listReportAparHistory: JSON.parse(history) });
          this.setState({ isLoading: false });
        });
      }
    );
  }

  render() {
    return (
      <Container style={styles.wrapper}>
          <Header style={styles.header}>
            <Left style={{ flex: 1 }}>
              <Button
                transparent
                onPress={() =>
                  that.props.navigation.navigate("InspectionReportAparHistory")
                }
              >
                <Icon
                  name="ios-arrow-back"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            </Left>
            <Body style={{ flex: 3, alignItems: "center" }}>
              <Title style={styles.textbody}>
                {this.state.listReportAparHistory.UK_CODE} {this.state.listReportAparHistory.UK_TEXT}
              </Title>
            </Body>
            <Right style={{ flex: 1 }}>

            </Right>
          </Header>
          <StatusBar
            backgroundColor={colors.green03}
            barStyle="light-content"
          />
          <ScrollView>
            <Content>
              <CardItem style={{ borderRadius: 0, marginTop: 5 }}>
                <View>
                  <Text style={{ fontSize: 10 }}>Area</Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listReportAparHistory.AREA_NAME}
                  </Text>
                  <Text style={{ fontSize: 10, paddingTop: 5 }}>
                    Unit Kerja
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listReportAparHistory.UK_CODE} - {this.state.listReportAparHistory.UK_TEXT}
                  </Text>
                  <Text style={{ fontSize: 10, paddingTop: 5 }}>Lokasi</Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listReportAparHistory.LOKASI}
                  </Text>
                </View>
              </CardItem>


                <View>
                  <CardItem style={{ borderRadius: 0 }}>
                    <View>
                      <Text style={{ fontSize: 10, paddingTop: 5 }}>
                        Petugas Periksa
                      </Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                        {this.state.listReportAparHistory.CREATE_BY}
                      </Text>
                      <Text style={{ fontSize: 10, paddingTop:5 }}>Tgl</Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                        {this.state.listReportAparHistory.INSPECTION_DATE}
                      </Text>
                    </View>
                  </CardItem>

                  <CardItem style={{ borderRadius: 0 }}>
                    <View style={{ flexDirection: "row" }}>
                      <View style={{ flex: 1 }}>
                        <Text style={{ fontSize: 10 }}>Klem</Text>
                          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                            {this.state.listReportAparHistory.KLEM}
                          </Text>
                      </View>
                      <View style={{ flex: 1 }}>
                        <Text style={{ fontSize: 10 }}>Press</Text>
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          {this.state.listReportAparHistory.PRESS}
                        </Text>
                      </View>
                      <View style={{ flex: 1 }}>
                        <Text style={{ fontSize: 10 }}>Weight</Text>
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          {this.state.listReportAparHistory.WEIGHT}
                        </Text>
                      </View>
                      <View style={{ flex: 1 }}>
                        <Text style={{ fontSize: 10 }}>Hose</Text>
                          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                            {this.state.listReportAparHistory.HOSE}
                          </Text>
                      </View>
                      <View style={{ flex: 1 }}>
                        <Text style={{ fontSize: 10 }}>Seal</Text>
                          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                            {this.state.listReportAparHistory.SEAL}
                          </Text>
                      </View>
                      <View style={{ flex: 1 }}>
                        <Text style={{ fontSize: 10 }}>Sapot</Text>
                          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                            {this.state.listReportAparHistory.SAPOT}
                          </Text>
                      </View>
                      <View style={{ flex: 1 }}>
                        <Text style={{ fontSize: 10 }}>Kondisi</Text>
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          {this.state.listReportAparHistory.STATUS_COND}
                        </Text>
                      </View>
                      <View style={{ flex: 1 }}>
                        <Text style={{ fontSize: 10 }}>Type</Text>
                        {this.state.listReportAparHistory.JENIS_GANTI == null ? (
                          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                            {this.state.listReportAparHistory.JENIS_PASANG}
                          </Text>
                        ) : (
                          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                            {this.state.listReportAparHistory.JENIS_GANTI}
                          </Text>
                        )}
                      </View>
                    </View>
                  </CardItem>
                  <CardItem style={{ borderRadius: 0 }}>
                    <View>
                      <Text style={{ fontSize: 10, paddingTop: 5 }}>
                        Informasi
                      </Text>
                      {this.state.listReportAparHistory.INFORMATION != null ? (
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          {this.state.listReportAparHistory.INFORMATION}
                        </Text>
                      ) : (
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          {" "}
                          -
                        </Text>
                      )}
                    </View>
                  </CardItem>
                </View>

            </Content>
          </ScrollView>
      </Container>
    );
  }
}

export default InspectionReportAparHistoryDetail;
