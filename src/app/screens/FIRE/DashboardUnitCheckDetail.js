import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input
} from "native-base";

import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";

var that;
class ListItem extends React.PureComponent {
  navigateToScreen(route, listUnitcheck) {
    AsyncStorage.setItem("list", JSON.stringify(listUnitcheck)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <Content>
        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
          - {this.props.data.NEGATIF_TEXT}{" "}
        </Text>
      </Content>
    );
  }
}

class DashboardUnitCheckDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      dataDetail: [],
      isLoading: true,
      headerUnitCheck: [],
      dataSourceNegatif: [],
      noPeg: "",
      visibleDialogSubmit: false
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} />;

  navigateToScreen(route, listUnitcheck) {
    // alert(JSON.stringify(listUnitcheck));
    AsyncStorage.setItem("id", JSON.stringify(listUnitcheck)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadData();
        AsyncStorage.getItem("list").then(listUnitcheck => {
          this.setState({ headerUnitCheck: JSON.parse(listUnitcheck) });
          this.setState({ isLoading: false });
        });
      }
    );
  }

  loadData() {
    this.setState({ visibleDialog: false, isLoading: true });
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST +
        "api/v_mobile/firesystem/unit_check/vehicle_checked_data";
      const shift = this.state.headerUnitCheck.SHIFT_CHECKED;
      var formData = new FormData();
      formData.append("token", value);
      formData.append("REPORT_DATE", this.state.headerUnitCheck.DATE_CHECKED);
      formData.append("REPORT_SHIFT", this.state.headerUnitCheck.SHIFT_CHECKED);
      formData.append("ID_VEHICLE", this.state.headerUnitCheck.ID_VEHICLE);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson.Data_Negatif_Day))
          this.setState({
            dataSourceNegatif: responseJson.Data_Negatif_Day[shift],
            isLoading: false
          });
        })
        .catch(error => {
          console.log(error);
          this.setState({
            isLoading: false
          });
        });
    });
  }

  konfirmasideleteHeader() {
    Alert.alert(
      "Apakah Anda Yakin Ingin Menghapus",
      this.state.headerAccidentReport.ID,
      [
        { text: "Yes", onPress: () => this.deleteDetailAccident() },
        { text: "Cancel" }
      ],
      { cancelable: false }
    );
  }

  deleteDetailAccident() {
    this.setState({
      visibleDialogSubmit: true
    });
    AsyncStorage.getItem("token").then(value => {
      // alert(JSON.stringify(value));
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/safety/accident/delete";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("ID", this.state.headerAccidentReport.ID);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Success", "Delete Success", [
              {
                text: "Okay"
              }
            ]);
            this.props.navigation.navigate("AccidentReport");
          } else {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Error", "Delete Failed", [
              {
                text: "Okay"
              }
            ]);
          }
        })
        .catch(error => {
          this.setState({
            visibleDialogSubmit: false
          });
          Alert.alert("Error", "Delete Failed", [
            {
              text: "Okay"
            }
          ]);
          console.log(error);
        });
    });
  }

  render() {
    var list;
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.headerUnitCheck == null) {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = (
          <ScrollView>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <Text style={{ fontSize: 12, color: colors.green01 }}>
                Unit Detail {this.state.headerUnitCheck.VEHICLE_CODE}
              </Text>
            </View>
            <CardItem style={{ borderRadius: 0, marginTop: 5 }}>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={{ marginLeft: 10, flex: 2, marginRight: 10 }}>
                  {this.state.headerUnitCheck.FILE_PATH == null ? (
                    <Image
                      style={{
                        marginTop: 10,
                        height: 200,
                        width: "100%",
                        borderColor: colors.gray,
                        borderWidth: 3,
                        borderRadius: 10
                      }}
                      source={require("../../../assets/images/truckfire.jpg")}
                    />
                  ) : (
                    <Image
                      style={{
                        marginTop: 10,
                        height: 200,
                        width: "100%",
                        borderColor: colors.gray,
                        borderWidth: 3,
                        borderRadius: 10
                      }}
                      source={{
                        uri:
                          GlobalConfig.SERVERHOST +
                          "api" +
                          this.state.headerUnitCheck.FILE_PATH
                      }}
                    />
                  )}
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0 }}>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={{ marginLeft: 10, flex: 2 }}>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Merk
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.headerUnitCheck.MERK}
                  </Text>
                </View>
                <View style={{ marginRight: 20 }}>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Tipe
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.headerUnitCheck.TIPE}
                  </Text>
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0 }}>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={{ marginLeft: 10, flex: 2 }}>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Tahun
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.headerUnitCheck.TAHUN}
                  </Text>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Kondisi
                  </Text>
                  {this.state.headerUnitCheck.CHECKED_BY == null && (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      BELUM DIPERIKSA
                    </Text>
                  )}
                  {this.state.headerUnitCheck.CHECKED_BY != null &&
                    this.state.headerUnitCheck.COUNT_NEGATIF_LIST == 0 && (
                      <Text
                        note
                        style={{
                          fontSize: 10,
                          fontWeight: "bold",
                          color: colors.green01
                        }}
                      >
                        NEGATIF LIST NOT EXIST
                      </Text>
                    )}
                  {this.state.headerUnitCheck.CHECKED_BY != null &&
                    this.state.headerUnitCheck.COUNT_NEGATIF_LIST > 0 && (
                      <Text
                        note
                        style={{
                          fontSize: 10,
                          fontWeight: "bold",
                          color: colors.red
                        }}
                      >
                        NEGATIF LIST EXIST
                      </Text>
                    )}
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Checked By
                  </Text>
                  {this.state.headerUnitCheck.CHECKED_BY == null ? (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {" "}
                      -{" "}
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {this.state.headerUnitCheck.CHECKED_BY}
                    </Text>
                  )}
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Checked Date
                  </Text>
                  {this.state.headerUnitCheck.DATE_CHECKED == null ? (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {" "}
                      -{" "}
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {this.state.headerUnitCheck.DATE_CHECKED}
                    </Text>
                  )}

                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Negatif List
                  </Text>
                  {this.state.headerUnitCheck.COUNT_NEGATIF_LIST == 0 ? (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {" "}
                      -{" "}
                    </Text>
                  ) : (
                    <FlatList
                      data={this.state.dataSourceNegatif}
                      renderItem={this._renderItem}
                      keyExtractor={(item, index) => index.toString()}
                    />
                  )}
                </View>
              </View>
            </CardItem>
          </ScrollView>
        );
      }
    }
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body>
            <Title style={styles.textbody}>Dashboard Unit Check Detail</Title>
          </Body>
          <Right>
            <Button
              transparent
              onPress={() =>
                this.navigateToScreen(
                  "UnitCheckUpdateVehicle",
                  this.state.headerUnitCheck
                )
              }
            >
              <Icon
                name="ios-create"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <Content style={{ marginTop: 10, marginBottom: 20 }}>
          {list}
          <View style={{ width: 270, position: "absolute" }}>
            <Dialog
              visible={this.state.visibleDialogSubmit}
              dialogTitle={<DialogTitle title="Deleting Accident Report .." />}
            >
              <DialogContent>
                {<ActivityIndicator size="large" color="#330066" animating />}
              </DialogContent>
            </Dialog>
          </View>
        </Content>
        <CustomFooter navigation={this.props.navigation} menu="Safety" />
      </Container>
    );
  }
}

export default DashboardUnitCheckDetail;
