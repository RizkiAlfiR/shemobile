import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator,
  RefreshControl
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Switch,
  ListItem,
  Thumbnail,
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import SwitchToggle from "react-native-switch-toggle";
import Ripple from "react-native-material-ripple";
import ListHistoryHydrant from "../../components/ListHistoryHydrant";
import DatePicker from "react-native-datepicker";

class ListItemDetail extends React.PureComponent {
  navigateToScreen(route, history) {
    AsyncStorage.setItem("history", JSON.stringify(history)).then(() => {
        that.props.navigation.navigate(route);
    });
  }
  render() {
    return (
      <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.navigateToScreen(
              "InspectionReportHydrantHistoryDetail",
              this.props.data,
            )
          }
          rippleColor={colors.accent}
        >
          <View style={{ marginTop: 0, marginLeft: 5, marginRight: 5 }}>
            <ListHistoryHydrant
              imageUri={require("../../../assets/images/hydrant.png")}
              area={this.props.data.AREA_NAME}
              lokasi={this.props.data.LOKASI}
              inspectiondate={this.props.data.INSPECTION_DATE}
              status={this.props.data.TEMUAN}
            />
          </View>
        </Ripple>
      </Card>
    );
  }
}

class InspectionReportHydrantHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      isLoading: true,
      listReportHydrant: [],
      idLokasi: "",
      visibleDialog: false,
      filterDateStart:"",
      filterDateEnd:"",
    };
  }

  static navigationOptions = {
    header: null
  };


  componentDidMount() {
    AsyncStorage.getItem("idLokasi").then(idLokasi => {
        this.setState({ idLokasi: idLokasi});
        this.setState({ isLoading: false });
        console.log(this.state.idLokasi);
        console.log(this.state.periode);
    });
    this.loadData();
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadData();
      }
    );
  }

  loadData() {
    this.setState({ visibleDialog: false, isLoading: true });
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST +
        "api/v_mobile/firesystem/hydrant_n/get_log_cek_hydrant";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("ID_LOC_HYDRANT", this.state.idLokasi);
      if (this.state.filterDateStart != "") {
        formData.append("date_start_log", this.state.filterDateStart);
      }
      if (this.state.filterDateEnd != "") {
        formData.append("date_end_log", this.state.filterDateEnd);
      }
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            listReportHydrant: responseJson.Data,
            isLoading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  _renderItem = ({ item }) => (
    <ListItemDetail data={item}/>
  );

  onRefresh() {
    console.log("refreshing");
    this.setState({ isLoading: true }, function() {
      this.loadData();
    });
  }

  onClickSearch() {
    this.setState({
      visibleDialog: true
    });
  }

  render() {
    that = this;
    var list;
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.listReportHydrant == "") {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = (
          <FlatList
            data={this.state.listReportHydrant}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index.toString()}
            onEndThreshold={0.5}
            refreshControl={
              <RefreshControl
                refreshing={this.state.isLoading}
                onRefresh={this.onRefresh.bind(this)}
              />
            }
          />
        );
      }
    }
    return (
      <Container style={styles.wrapper}>
          <Header style={styles.header}>
              <Left style={{ flex: 1 }}>
                <Button
                  transparent
                  onPress={() =>
                    that.props.navigation.navigate("InspectionReportHydrantDetail")
                  }
                >
                  <Icon
                    name="ios-arrow-back"
                    size={20}
                    style={styles.facebookButtonIconOrder2}
                  />
                </Button>
              </Left>
              <Body style={{ flex: 3, alignItems: "center" }}>
                <Title style={styles.textbody}>History
                </Title>
              </Body>
              <Right style={{ flex: 1 }}>
                <Button transparent onPress={() => this.onClickSearch()}>
                  <Icon
                    name="ios-funnel"
                    size={20}
                    style={styles.facebookButtonIconOrder2}
                  />
                </Button>
              </Right>
            </Header>
            <StatusBar
              backgroundColor={colors.green03}
              barStyle="light-content"
            />
            <ScrollView>
              <Content style={{ marginTop: 5 }}>
              <View style={{ marginTop: 10,flex:1,flexDirection:'column' }}>
                {list}
              </View>
              <View style={{ width: 270, position: "absolute" }}>
                <Dialog
                  visible={this.state.visibleDialog}
                  dialogAnimation={
                    new SlideAnimation({
                      slideFrom: "bottom"
                    })
                  }
                  dialogStyle={{ position: "absolute", top: this.state.posDialog }}
                  onTouchOutside={() => {
                    this.setState({ visibleDialog: false });
                  }}
                  dialogTitle={<DialogTitle title="Filter" />}
                  actions={[
                    <DialogButton
                      style={{
                        fontSize: 11,
                        backgroundColor: colors.white,
                        borderColor: colors.blue01
                      }}
                      text="SUBMIT"
                      onPress={() => this.loadData()}
                      // onPress={() => this.setState({ visibleFilter: false })}
                    />
                  ]}
                >
                  <DialogContent>
                    {
                      <View>
                        <Text
                          style={{ paddingTop: 20, fontWeight: "bold", fontSize: 10 }}
                        >
                          Start Date *
                        </Text>
                        <DatePicker
                          style={{ width: 300, fontSize: 10, borderRadius: 20 }}
                          date={this.state.filterDateStart}
                          mode="date"
                          placeholder="Choose Start Date ..."
                          format="YYYY-MM-DD"
                          minDate="2018-01-01"
                          maxDate="5000-12-31"
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          customStyles={{
                            dateInput: {
                              marginLeft: 5,
                              marginRight: 5,
                              height: 35,
                              borderRadius: 5,
                              fontSize: 10,
                              borderWidth: 1,
                              borderColor: "#E6E6E6"
                            },
                            dateIcon: {
                              position: "absolute",
                              left: 0,
                              top: 5
                            }
                          }}
                          onDateChange={date => {
                            this.setState({ filterDateStart: date });
                          }}
                        />
                        <Text
                          style={{ paddingTop: 20, fontWeight: "bold", fontSize: 10 }}
                        >
                          End Date *
                        </Text>
                        <DatePicker
                          style={{ width: 300, fontSize: 10, borderRadius: 20 }}
                          date={this.state.filterDateEnd}
                          mode="date"
                          placeholder="Choose End Date ..."
                          format="YYYY-MM-DD"
                          minDate="2018-01-01"
                          maxDate="5000-12-31"
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          customStyles={{
                            dateInput: {
                              marginLeft: 5,
                              marginRight: 5,
                              height: 35,
                              borderRadius: 5,
                              fontSize: 10,
                              borderWidth: 1,
                              borderColor: "#E6E6E6"
                            },
                            dateIcon: {
                              position: "absolute",
                              left: 0,
                              top: 5
                            }
                          }}
                          onDateChange={date => {
                            this.setState({ filterDateEnd: date });
                          }}
                        />
                      </View>
                    }
                  </DialogContent>
                </Dialog>
              </View>
              </Content>
            </ScrollView>
      </Container>
    );
  }
}

export default InspectionReportHydrantHistory;
