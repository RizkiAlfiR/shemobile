import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
} from "native-base";

import LinearGradient from "react-native-linear-gradient";
import SubMenuFire from "../../components/FireSystem/SubMenuFire";
import styles from "../styles/FireMenu";
import GlobalConfig from "../../components/GlobalConfig";

import NewsDaily from "../../components/FireSystem/NewsDaily";
import NewsFire from "../../components/FireSystem/NewsFire";
import NewsLotto from "../../components/FireSystem/NewsLotto";

import Ripple from "react-native-material-ripple";
import colors from "../../../styles/colors";
import Moment from 'moment';
import CustomFooter from "../../components/CustomFooter";

var that

class ListItemNewsDaily extends React.PureComponent {
  navigateToScreen(route, id) {
    AsyncStorage.setItem("id", JSON.stringify(id)).then(() => {
      that.props.navigation.navigate(route);
    });
  }
  render(){
      return(
          <Ripple
            style={{
              flex: 2,
              justifyContent: "center",
              alignItems: "center"
            }}
            rippleSize={176}
            rippleDuration={600}
            rippleContainerBorderRadius={15}
            onPress={() =>this.navigateToScreen("DailyReportAktifitasDetail", this.props.data.id)}
            rippleColor={colors.accent}
          >
            <View style={{ marginTop: 0, marginRight: 10, borderRadius:5, backgroundColor:colors.white}}>
              <NewsDaily
                imageUri={this.props.data.image}
                aksi={this.props.data.type_action}
                date={this.props.data.date_activity}
              />
            </View>
          </Ripple>
      )
    }
}

class ListItemNewsFire extends React.PureComponent {
  navigateToScreen(route, id) {
    AsyncStorage.setItem("id", JSON.stringify(id)).then(() => {
      that.props.navigation.navigate(route);
    });
  }
  render(){
      return(
          <Ripple
            style={{
              flex: 2,
              justifyContent: "center",
              alignItems: "center"
            }}
            rippleSize={176}
            rippleDuration={600}
            rippleContainerBorderRadius={15}
            onPress={() =>this.navigateToScreen("FireReportDetail", this.props.data.id)}
            rippleColor={colors.accent}
          >
            <View style={{ marginTop: 0, marginRight: 10, borderRadius:5, backgroundColor:colors.white}}>
              <NewsFire
                imageUri={require("../../../assets/images/iconFire.png")}
                number={this.props.data.fire_number}
                place={this.props.data.incident_place}
                date={this.props.data.fire_date}
              />
            </View>
          </Ripple>
      )
    }
}

class ListItemNewsLotto extends React.PureComponent {
  navigateToScreen(route, idLotto) {
    AsyncStorage.setItem("idLotto", JSON.stringify(idLotto)).then(() => {
      that.props.navigation.navigate(route);
    });
  }
  render(){
      return(
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>this.navigateToScreen("LottoDetail", this.props.data.id)}
          rippleColor={colors.accent}
        >
        <View style={{ marginTop: 0, marginRight: 10, borderRadius:5, backgroundColor:colors.white}}>
          <NewsLotto
            imageUri={require("../../../assets/images/report.png")}
            number={this.props.data.equipment_number}
            place={this.props.data.location}
            date={this.props.data.date}
          />
        </View>
        </Ripple>
      )
    }
}



class FireMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: true,
      isLoadingNews: false,
      dataSource: [],
      dataNewsOfficer:[],
      dataNewsDaily:[],
      dataNewsFire:[],
      dataNewsLotto:[],
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    this.loadNewsOfficer();
    this.loadNewsDaily();
    this.loadNewsFire();
    this.loadNewsLotto();
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadNewsOfficer();
        this.loadNewsDaily();
        this.loadNewsFire();
        this.loadNewsLotto();
      }
    );
  }

  loadNewsOfficer() {
    this.setState({ isLoading: true })
    const url = GlobalConfig.SERVERHOST + "getNewsOfficer";
    var formData = new FormData();
    formData.append("token", 'token');

    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          dataNewsOfficer: responseJson.data,
          isLoading: false
        });
      })
      .catch((error) => {
        console.log(error)
        this.setState({
          isLoading: false
        });
      })
  }
  loadNewsDaily() {
    this.setState({ isLoading: true })
    const url = GlobalConfig.SERVERHOST + "getNewsDaily";
    var formData = new FormData();
    formData.append("token", 'token');

    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then((response) => response.json())
      .then((responseJson) => {
        // alert(JSON.stringify(responseJson))
        this.setState({
          dataNewsDaily: responseJson.data,
          isLoading: false
        });
      })
      .catch((error) => {
        console.log(error)
        this.setState({
          isLoading: false
        });
      })
  }
  loadNewsFire() {
    this.setState({ isLoading: true })
    const url = GlobalConfig.SERVERHOST + "getNewsFire";
    var formData = new FormData();
    formData.append("token", 'token');

    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          dataNewsFire: responseJson.data,
          isLoading: false
        });
      })
      .catch((error) => {
        console.log(error)
        this.setState({
          isLoading: false
        });
      })
  }
  loadNewsLotto() {
    this.setState({ isLoading: true })
    const url = GlobalConfig.SERVERHOST + "getNewsLotto";
    var formData = new FormData();
    formData.append("token", 'token');

    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then((response) => response.json())
      .then((responseJson) => {
        // alert(JSON.stringify(responseJson))
        this.setState({
          dataNewsLotto: responseJson.data,
          isLoading: false
        });
      })
      .catch((error) => {
        console.log(error)
        this.setState({
          isLoading: false
        });
      })
  }


  _renderItemNewsDaily = ({ item }) => (
      <ListItemNewsDaily data={item}/>
  )

  _renderItemNewsFire = ({ item }) => (
      <ListItemNewsFire data={item}/>
  )

  _renderItemNewsLotto = ({ item }) => (
    <ListItemNewsLotto data={item}/>
  )

  render() {
    that = this;
    return (
      <Container style={styles.wrapper}>
        <View style={{backgroundColor:colors.primer, height:130, paddingBottom:20}}>
          <View style={{flex:1, flexDirection:'row', marginLeft:20, marginRight:20, marginTop:30}}>
            <View style={{width:'10%'}}>
              <View style={{backgroundColor:colors.second, borderRadius:5, height:30, justifyContent:'center', alignItems:'center'}}>
              <TouchableOpacity
                transparent
                  onPress={() => this.props.navigation.navigate("HomeMenu")}>
                  <Icon
                      name='ios-home'
                      style={{fontSize:20, color:colors.white}}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{width:'90%', paddingLeft:20}}>
              <Text style={{fontSize:18, fontWeight:'bold', color:colors.white}}>MENU FIRE SYSTEM</Text>
            </View>
          </View>
        </View>
        <View style={{marginLeft:20, marginRight:20, marginBottom:20, marginTop:-50, backgroundColor:colors.white, borderRadius:5, height:200,
          shadowColor:'#000',
          shadowOffset:{
            width:0,
            height:1,
          },
          shadowOpacity:1,
          shadowRadius:2,
          elevation:3,
        }}>
          <View style={{flex:1, flexDirection:'row'}}>
          <View style={{width:'33%', paddingTop:25}}>
              <SubMenuFire
                handleOnPress={() => this.props.navigation.navigate("DailyReportAktifitas")}
                imageUri={require("../../../assets/images/iconDailyReportSection.png")}
                name="Daily Report"
              />
          </View>
          <View style={{width:'33%', paddingTop:25}}>
              <SubMenuFire
                handleOnPress={() => this.props.navigation.navigate("InspectionReportApar")}
                imageUri={require("../../../assets/images/iconInspectionReport.png")}
                name="Inspection Report"
              />
          </View>
          <View style={{width:'33%', paddingTop:25}}>
              <SubMenuFire
                handleOnPress={() => this.props.navigation.navigate("FireReport")}
                imageUri={require("../../../assets/images/iconFireReport.png")}
                name="Fire Report"
              />
          </View>
          </View>
          <View style={{flex:1, flexDirection:'row'}}>
          <View style={{width:'33%', paddingTop:10}}>
              <SubMenuFire
                handleOnPress={() => this.props.navigation.navigate("UnitCheckPickup")}
                imageUri={require("../../../assets/images/iconUnitCheck.png")}
                name="Unit Check"
              />
          </View>
          <View style={{width:'33%', paddingTop:10}}>
              <SubMenuFire
                handleOnPress={() => this.props.navigation.navigate("DashboardUnitCheckPickup")}
                imageUri={require("../../../assets/images/iconUnitCheckDashboard.png")}
                name="Dashboard"
              />
          </View>
          <View style={{width:'33%', paddingTop:10}}>
              <SubMenuFire
                handleOnPress={() => this.props.navigation.navigate("Lotto")}
                imageUri={require("../../../assets/images/iconLotto.png")}
                name="Lotto"
              />
          </View>
          </View>
        </View>
        <ScrollView>
          <View style={{flex:1, flexDirection:'row', paddingTop:0}}>
            <View style={{width:'50%', paddingLeft:20}}>
              <Text style={{fontSize:12, fontWeight:'bold'}}>News Daily Report</Text>
            </View>
            <View style={{width:'50%', paddingRight:20}}>
              <TouchableOpacity
                transparent
                onPress={() => this.props.navigation.navigate("DailyReportAktifitas")}
              >
                <Text style={{fontSize:12, textAlign:'right'}}>View All</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{marginLeft:20, marginRight:20, marginTop:5}}>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                <FlatList
                    horizontal={true}
                    data={this.state.dataNewsDaily}
                    renderItem={this._renderItemNewsDaily}
                    keyExtractor={(item, index) => index.toString()}
                />
            </ScrollView>
          </View>

          <View style={{flex:1, flexDirection:'row', paddingTop:15}}>
            <View style={{width:'50%', paddingLeft:20}}>
              <Text style={{fontSize:12, fontWeight:'bold'}}>News Fire Report</Text>
            </View>
            <View style={{width:'50%', paddingRight:20}}>
              <TouchableOpacity
                transparent
                onPress={() => this.props.navigation.navigate("FireReport")}
              >
                <Text style={{fontSize:12, textAlign:'right'}}>View All</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{marginLeft:20, marginRight:20, marginTop:5}}>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                <FlatList
                    horizontal={true}
                    data={this.state.dataNewsFire}
                    renderItem={this._renderItemNewsFire}
                    keyExtractor={(item, index) => index.toString()}
                />
            </ScrollView>
          </View>

          <View style={{flex:1, flexDirection:'row', paddingTop:15}}>
            <View style={{width:'50%', paddingLeft:20}}>
              <Text style={{fontSize:12, fontWeight:'bold'}}>News Lotto</Text>
            </View>
            <View style={{width:'50%', paddingRight:20}}>
              <TouchableOpacity
                transparent
                onPress={() => this.props.navigation.navigate("Lotto")}
              >
                <Text style={{fontSize:12, textAlign:'right'}}>View All</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{marginLeft:20, marginRight:20, marginTop:5}}>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                <FlatList
                    horizontal={true}
                    data={this.state.dataNewsLotto}
                    renderItem={this._renderItemNewsLotto}
                    keyExtractor={(item, index) => index.toString()}
                />
            </ScrollView>
          </View>
        </ScrollView>
      </Container>
    );
  }
}

export default FireMenu;
