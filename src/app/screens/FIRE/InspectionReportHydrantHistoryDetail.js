import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator,
  RefreshControl
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Ripple from "react-native-material-ripple";
import SwitchToggle from "react-native-switch-toggle";

class InspectionReportHydrantHistoryDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isLoading: true,
      listReportHydrantHistory: [],
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        AsyncStorage.getItem("history").then(history => {
          this.setState({ listReportHydrantHistory: JSON.parse(history) });
          this.setState({ isLoading: false });
        });
      }
    );
  }

  render() {
    var that = this;
    return (
      <Container style={styles.wrapper}>
            <Header style={styles.header}>
              <Left style={{ flex: 1 }}>
                <Button
                  transparent
                  onPress={() =>
                    that.props.navigation.navigate("InspectionReportHydrantHistory")
                  }
                >
                  <Icon
                    name="ios-arrow-back"
                    size={20}
                    style={styles.facebookButtonIconOrder2}
                  />
                </Button>
              </Left>
              <Body style={{ flex: 3, alignItems: "center" }}>
                <Title style={styles.textbody}>
                  {this.state.listReportHydrantHistory.AREA_NAME} {this.state.listReportHydrantHistory.LOKASI}
                </Title>
              </Body>
              <Right style={{ flex: 1 }}>

              </Right>
            </Header>
            <StatusBar
              backgroundColor={colors.green03}
              barStyle="light-content"
            />

            <ScrollView>
              <Content>
                <CardItem style={{ borderRadius: 0, marginTop: 5 }}>
                  <View>
                    <Text style={{ fontSize: 10 }}>Area</Text>
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {this.state.listReportHydrantHistory.AREA_NAME}
                    </Text>
                    <Text style={{ fontSize: 10, paddingTop: 5 }}>Lokasi</Text>
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {this.state.listReportHydrantHistory.LOKASI}
                    </Text>
                  </View>
                </CardItem>

                  <View style={{}}>
                    <CardItem style={{ borderRadius: 0 }}>
                      <View>
                        <Text style={{ fontSize: 10 }}>Tanggal Periksa</Text>
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          {this.state.listReportHydrantHistory.INSPECTION_DATE}
                        </Text>
                        <Text style={{ fontSize: 10, paddingTop: 5 }}>
                          Petugas Periksa
                        </Text>
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          {this.state.listReportHydrantHistory.CREATE_BY}
                        </Text>
                      </View>
                    </CardItem>

                    <CardItem style={{ borderRadius: 0 }}>
                      <View style={{ flex: 1, flexDirection: "column" }}>
                        <View>
                          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                            Pengecekan BOX
                          </Text>
                        </View>

                        <View
                          style={{
                            flex: 1,
                            flexDirection: "row",
                            paddingTop: 5
                          }}
                        >
                          <View style={{ width: "25%" }}>
                            <Text style={{ fontSize: 10 }}>Casing</Text>
                            {this.state.listReportHydrantHistory.BOX_CASING == "V" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Baik
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.BOX_CASING == "K" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Kotor
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.BOX_CASING == "-" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Tidak Ada
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.BOX_CASING == "X" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Rusak
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.BOX_CASING == "I" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Seret/Macet/Merembes
                              </Text>
                            )}
                          </View>
                          <View style={{ width: "25%" }}>
                            <Text style={{ fontSize: 10 }}>Hose</Text>
                            {this.state.listReportHydrantHistory.BOX_HOSE == "V" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Baik
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.BOX_HOSE == "K" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Kotor
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.BOX_HOSE == "-" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Tidak Ada
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.BOX_HOSE == "X" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Rusak
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.BOX_HOSE == "I" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Seret/Macet/Merembes
                              </Text>
                            )}
                          </View>
                          <View style={{ width: "25%" }}>
                            <Text style={{ fontSize: 10 }}>Nozle</Text>
                            {this.state.listReportHydrantHistory.BOX_NOZLE == "V" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Baik
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.BOX_NOZLE == "K" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Kotor
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.BOX_NOZLE == "-" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Tidak Ada
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.BOX_NOZLE == "X" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Rusak
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.BOX_NOZLE == "I" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Seret/Macet/Merembes
                              </Text>
                            )}
                          </View>
                          <View style={{ width: "25%" }}>
                            <Text style={{ fontSize: 10 }}>Kunci</Text>
                            {this.state.listReportHydrantHistory.BOX_KUNCI == "V" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Baik
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.BOX_KUNCI == "K" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Kotor
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.BOX_KUNCI == "-" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Tidak Ada
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.BOX_KUNCI == "X" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Rusak
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.BOX_KUNCI == "I" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Seret/Macet/Merembes
                              </Text>
                            )}
                          </View>
                        </View>

                        <View style={{ paddingTop: 5 }}>
                          <Text style={{ fontSize: 10 }}>Keterangan</Text>
                          {this.state.listReportHydrantHistory.BOX_NOTE != null ? (
                            <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                              {this.state.listReportHydrantHistory.BOX_NOTE}
                            </Text>
                          ) : (
                            <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                              {" "}
                              -
                            </Text>
                          )}
                        </View>
                      </View>
                    </CardItem>

                    <CardItem style={{ borderRadius: 0 }}>
                      <View style={{ flex: 1, flexDirection: "column" }}>
                        <View>
                          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                            Pengecekan Copling
                          </Text>
                        </View>

                        <View
                          style={{
                            flex: 1,
                            flexDirection: "row",
                            paddingTop: 5
                          }}
                        >
                          <View style={{ width: "25%" }}>
                            <Text style={{ fontSize: 10 }}>Copling Kiri</Text>
                            {this.state.listReportHydrantHistory.COPLING_KIRI == "V" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Baik
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.COPLING_KIRI == "K" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Kotor
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.COPLING_KIRI == "-" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Tidak Ada
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.COPLING_KIRI == "X" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Rusak
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.COPLING_KIRI == "I" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Seret/Macet/Merembes
                              </Text>
                            )}
                          </View>
                          <View style={{ width: "25%" }}>
                            <Text style={{ fontSize: 10 }}>Copling Kanan</Text>
                            {this.state.listReportHydrantHistory.COPLING_KANAN == "V" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Baik
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.COPLING_KANAN == "K" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Kotor
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.COPLING_KANAN == "-" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Tidak Ada
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.COPLING_KANAN == "X" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Rusak
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.COPLING_KANAN == "I" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Seret/Macet/Merembes
                              </Text>
                            )}
                          </View>
                          <View style={{ width: "25%" }}>
                            <Text style={{ fontSize: 10 }}>Tutup Kiri</Text>
                            {this.state.listReportHydrantHistory.T_COPLING_KIRI == "V" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Baik
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.T_COPLING_KIRI == "K" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Kotor
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.T_COPLING_KIRI == "-" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Tidak Ada
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.T_COPLING_KIRI == "X" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Rusak
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.T_COPLING_KIRI == "I" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Seret/Macet/Merembes
                              </Text>
                            )}
                          </View>
                          <View style={{ width: "25%" }}>
                            <Text style={{ fontSize: 10 }}>Tutup Kanan</Text>
                            {this.state.listReportHydrantHistory.T_COPLING_KANAN == "V" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Baik
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.T_COPLING_KANAN == "K" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Kotor
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.T_COPLING_KANAN == "-" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Tidak Ada
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.T_COPLING_KANAN == "X" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Rusak
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.T_COPLING_KANAN == "I" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Seret/Macet/Merembes
                              </Text>
                            )}
                          </View>
                        </View>

                        <View
                          style={{
                            flex: 1,
                            flexDirection: "row",
                            paddingTop: 5
                          }}
                        >
                          <View style={{ width: "25%" }}>
                            <Text style={{ fontSize: 10 }}>Valve Kiri</Text>
                            {this.state.listReportHydrantHistory.VALVE_KIRI == "V" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Baik
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.VALVE_KIRI == "K" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Kotor
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.VALVE_KIRI == "-" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Tidak Ada
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.VALVE_KIRI == "X" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Rusak
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.VALVE_KIRI == "I" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Seret/Macet/Merembes
                              </Text>
                            )}
                          </View>
                          <View style={{ width: "25%" }}>
                            <Text style={{ fontSize: 10 }}>Valve Kanan</Text>
                            {this.state.listReportHydrantHistory.VALVE_KANAN == "V" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Baik
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.VALVE_KANAN == "K" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Kotor
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.VALVE_KANAN == "-" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Tidak Ada
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.VALVE_KANAN == "X" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Rusak
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.VALVE_KANAN == "I" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Seret/Macet/Merembes
                              </Text>
                            )}
                          </View>
                          <View style={{ width: "25%" }}>
                            <Text style={{ fontSize: 10 }}>Valve Atas</Text>
                            {this.state.listReportHydrantHistory.VALVE_ATAS == "V" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Baik
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.VALVE_ATAS == "K" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Kotor
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.VALVE_ATAS == "-" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Tidak Ada
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.VALVE_ATAS == "X" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Rusak
                              </Text>
                            )}
                            {this.state.listReportHydrantHistory.VALVE_ATAS == "I" && (
                              <Text
                                style={{ fontSize: 10, fontWeight: "bold" }}
                              >
                                Seret/Macet/Merembes
                              </Text>
                            )}
                          </View>
                        </View>

                        <View style={{ paddingTop: 5 }}>
                          <Text style={{ fontSize: 10 }}>Keterangan</Text>
                          {this.state.listReportHydrantHistory.BOX_NOTE != null ? (
                            <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                              {this.state.listReportHydrantHistory.BOX_NOTE}
                            </Text>
                          ) : (
                            <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                              {" "}
                              -
                            </Text>
                          )}
                        </View>
                      </View>
                    </CardItem>

                    <CardItem style={{ borderRadius: 0 }}>
                      <View>
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Pengecekan Pilar
                        </Text>
                        <Text style={{ fontSize: 10, paddingTop: 5 }}>
                          Body
                        </Text>
                        {this.state.listReportHydrantHistory.PILAR_BODY == "V" && (
                          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                            Baik
                          </Text>
                        )}
                        {this.state.listReportHydrantHistory.PILAR_BODY == "K" && (
                          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                            Kotor
                          </Text>
                        )}
                        {this.state.listReportHydrantHistory.PILAR_BODY == "-" && (
                          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                            Tidak Ada
                          </Text>
                        )}
                        {this.state.listReportHydrantHistory.PILAR_BODY == "X" && (
                          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                            Rusak
                          </Text>
                        )}
                        {this.state.listReportHydrantHistory.PILAR_BODY == "I" && (
                          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                            Seret/Macet/Merembes
                          </Text>
                        )}
                        <Text style={{ fontSize: 10, paddingTop: 5 }}>
                          Keterangan
                        </Text>
                        {this.state.listReportHydrantHistory.PILAR_NOTE != null ? (
                          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                            {this.state.listReportHydrantHistory.PILAR_NOTE}
                          </Text>
                        ) : (
                          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                            {" "}
                            -
                          </Text>
                        )}
                      </View>
                    </CardItem>

                    <CardItem style={{ borderRadius: 0 }}>
                      <View>
                        <Text style={{ fontSize: 10 }}>Press</Text>
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          {this.state.listReportHydrantHistory.PRESS}
                        </Text>
                        <Text style={{ fontSize: 10, paddingTop: 5 }}>
                          Temuan
                        </Text>
                        {this.state.listReportHydrantHistory.TEMUAN != null ? (
                          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                            {this.state.listReportHydrantHistory.TEMUAN}
                          </Text>
                        ) : (
                          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                            {" "}
                            -
                          </Text>
                        )}
                      </View>
                    </CardItem>
                    <CardItem>
                      <View style={{ backgroundColor: '#eee', width: '100%', height: 200, borderRadius: 10, marginBottom: 80 }}>
                        <Image
                          style={{
                            width: "100%",
                            height: "100%",
                            borderRadius: 10,
                            marginBottom: 20
                          }}
                          source={{
                            uri:
                              GlobalConfig.SERVERHOST +
                              "api" +
                              this.state.listReportHydrantHistory.FOTO_TEMUAN
                          }}
                        />
                      </View>
                    </CardItem>
                  </View>
              </Content>
            </ScrollView>
      </Container>
    );
  }
}

export default InspectionReportHydrantHistoryDetail;
