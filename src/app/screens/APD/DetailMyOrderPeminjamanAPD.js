import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Alert
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Card,
  CardItem,
  Form,
  Textarea,
  Icon
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import LinearGradient from "react-native-linear-gradient";
import GlobalConfig from "../../components/GlobalConfig";

import styles from "../styles/ApprovalAPD";
import colors from "../../../styles/colors";

const helmet = require("../../../assets/images/helmet.png");

var that;
class ListItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selected2: undefined
    };
  }

  onValueChange2(value) {
    this.setState({
      selected2: value
    });
  }

  render() {
    const { navigate } = that.props.navigation;
    return (
      <View>
        <Card style={{ marginBottom: 10, borderRadius:10 }}>
          <CardItem style={{borderRadius:10}}>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <View style={styles.viewLeftHeader2}>
                <Text style={styles.cardtext}>APD Name</Text>
                <Text style={styles.cardtext}>Merk</Text>
                <Text style={styles.cardtext}>Size</Text>
                <Text style={styles.cardtext}>Quantity</Text>
                <Text style={styles.cardtext}>Note</Text>
                {(this.props.data.REJECT_BY!=null||this.props.data.REJECT_BY!=undefined) &&
                  <View>
                    <Text style={styles.cardtext}>Reject</Text>
                    <Text style={styles.cardtext}>Note Reject</Text>
                  </View>
                }
                <Text style={styles.cardtext}>Released</Text>
                <Text style={styles.cardtext}>Returned</Text>

              </View>
              <View>
                <Text style={styles.cardtext}>: {this.props.data.NAMA}</Text>
                <Text style={styles.cardtext}>: {this.props.data.MERK}</Text>
                <Text style={styles.cardtext}>: {this.props.data.SIZE}</Text>
                <Text style={styles.cardtext}>: {this.props.data.JUMLAH}</Text>
                <Text style={styles.cardtext}>: {this.props.data.KETERANGAN}</Text>
                {(this.props.data.REJECT_BY!=null||this.props.data.REJECT_BY!=undefined) &&
                  <View>
                    <Text style={styles.cardtext}>: {this.props.data.REJECT_BY} [{this.props.data.REJECT_AT}]</Text>
                    <Text style={styles.cardtext}>: {this.props.data.NOTE_REJECT}</Text>
                  </View>
                }
                <Text style={styles.cardtext}>: {this.props.data.JUMLAH_RELEASE}/{this.props.data.JUMLAH}</Text>
                <Text style={styles.cardtext}>: {this.props.data.JUMLAH_RETUR}</Text>
              </View>
            </View>
          </CardItem>
        </Card>
      </View>
    );
  }
}

export default class DetailMyOrderPeminjamanAPD extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isloading: true
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item, index }) => (
    <ListItem data={item} index={index} parentFlatList={this} />
  );

  componentDidMount() {
    AsyncStorage.getItem("idOrder").then(value => {
      this.loadData(value);
    });
  }

  loadData(id) {
    AsyncStorage.getItem("token").then(value => {
      // alert(JSON.stringify(value));
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/apd/order/view_detail/peminjaman";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("id_number", id);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson));
          this.setState({
            dataSource: responseJson.data,
            isloading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  render() {
    that = this;
    let status, approver1, approver2, rejecter, noteReject;
    if (!this.state.isloading){
      if (this.state.dataSource.DETAIL[0].REJECT_BY != null) {
        status = "Rejected";
        statusPinjam = "Order Closed"
        rejecter =
          this.state.dataSource.DETAIL[0].REJECT_BY +
          " [" +
          this.state.dataSource.DETAIL[0].REJECT_AT +
          "]";
        noteReject = this.state.dataSource.DETAIL[0].NOTE_REJECT;
      } else {
        if (this.state.dataSource.APPROVE_BY != null) {
          status="Diapprove K3"
          statusPinjam = "Belum Dikembalikan"
            approver1 =
              this.state.dataSource.APPROVE_BY +
              " [" +
              this.state.dataSource.APPROVE_AT +
              "]";
        } else {
          status = "Belum Diapprove";
          statusPinjam = "Belum Dikembalikan"
        }
      }
  }
    return this.state.isloading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() =>
                this.props.navigation.navigate("MyOrderPeminjamanAPD")
              }
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIcon}
              />
            </Button>
          </Left>
          <Body style={{flex:4,alignItems:'center'}}>
            <Title style={styles.textbody}>Detail Order</Title>
          </Body>
          <Right style={{flex:1}}/>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View>
        <Card style={{ marginBottom: 15, borderWidth:0, borderRadius:10}}>
          <CardItem style={{ borderWidth:0, borderRadius:10 }}>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <View style={{ flex: 1 }}>
                <Text style={styles.cardtext}>Tanggal Order</Text>
                <Text style={styles.cardtext}>Tanggal Pinjam</Text>
                <Text style={styles.cardtext}>Batas Pinjam</Text>
                <Text style={styles.cardtext}>Kode Order / SKU</Text>
                <Text style={styles.cardtext}>Status Order</Text>
                <Text style={styles.cardtext}>Status Peminjaman</Text>
                <Text style={styles.cardtext}>Approve</Text>
              </View>
              <View style={{ flex: 2 }}>
                <Text style={styles.cardtext}>: {this.state.dataSource.CREATE_AT}</Text>
                <Text style={styles.cardtext}>: {this.state.dataSource.TANGGAL_PINJAM}</Text>
                <Text style={styles.cardtext}>: {this.state.dataSource.RETUR_DATE}</Text>
                <Text style={styles.cardtext}>: {this.state.dataSource.KODE_PINJAM}</Text>
                <Text style={styles.cardtext}>: {status}</Text>
                <Text style={styles.cardtext}>: {statusPinjam}</Text>
                <Text style={styles.cardtext}>: {approver1}</Text>
              </View>
            </View>
          </CardItem>
        </Card>
      </View>
          {/* <View style={{ alignItems: "baseline", borderWidth: 5 }}>
          <View style={{flex:1, flexDirection: "row" }}>
            <View style={{flex:1}}>
              <Text style={styles.cardtext}>Tanggal Request</Text>
              <Text style={styles.cardtext}>Kode Order / SKU</Text>
            </View>
            <View style={{flex:1}}>
              <Text style={styles.cardtext}>: {this.state.dataSource.CREATE_AT}</Text>
              <Text style={styles.cardtext}>: {this.state.dataSource.KODE_ORDER}</Text>
            </View>
          </View>
        </View> */}
        <View style={styles.homeWrapper}>
          <Content style={{ marginLeft: 20, marginRight: 20 }}>
            <FlatList
              data={this.state.dataSource.DETAIL}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
            />
          </Content>
        </View>
      </Container>
    );
  }
}
