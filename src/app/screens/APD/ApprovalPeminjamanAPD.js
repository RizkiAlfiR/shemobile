import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Platform,
  RefreshControl,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Card,
  CardItem,
  Item,
  Input,
  Icon,
  Thumbnail
} from "native-base";
// import Icon from "react-native-vector-icons/FontAwesome";
import LinearGradient from "react-native-linear-gradient";
import GlobalConfig from "../../components/GlobalConfig";

import CustomFooter from "../../components/CustomFooter";
import styles from "../styles/ApprovalAPD";
import colors from "../../../styles/colors";

const helmet = require("../../../assets/images/helmet.png");

var that;
class ListItem extends React.PureComponent {
  navigateToScreen(route, id_personal,kode) {
    // alert(route);
    AsyncStorage.setItem("idAndKode", id_personal+'+'+kode).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View>
        <Card style={{ marginLeft: 10, marginRight: 10 }}>
          <CardItem
            button
            onPress={() =>
              this.navigateToScreen(
                "ActionsApprovalPeminjamanAPD",
                this.props.data.ID, this.props.data.KODE_PINJAM
              )
            }
          >
            <Left>
              <Body>
                <Text
                  style={styles.cardtextKode}
                  // onPress={() =>
                  //   this.navigateToScreen(
                  //     "ActionsApprovalPeminjamanAPD",
                  //     this.props.data.ID
                  //   )
                  // }
                >
                  {this.props.data.KODE_PINJAM}
                </Text>
                <Text style={styles.cardtext}>
                  {this.props.data.UK_TEXT} / {this.props.data.UNIT_KERJA}
                </Text>
                <Text style={styles.cardtext}>
                  Keterangan : {this.props.data.KETERANGAN}
                </Text>
              </Body>
            </Left>
          </CardItem>
          <CardItem>
            <Left>
              <Body>
                <Text style={{ fontSize: 11 }}>
                  Tanggal Pinjam : {this.props.data.TANGGAL_PINJAM}
                </Text>
              </Body>
            </Left>
            <Right>
              <Body>
                <Text style={{ fontSize: 11 }}>
                  Return Date : {this.props.data.RETUR_DATE}
                </Text>
              </Body>
            </Right>
          </CardItem>
        </Card>
      </View>
    );
  }
}

export default class ApprovalPeminjamanAPD extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isloading: true,
      searchApproval: "",
      isEmpty: false
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} />;
  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.setState({
          searchApproval: ""
        });
        this.loadData();
      }
    );
  }

  onRefresh() {
    console.log('refreshing')
    this.setState({ isloading: true }, function() {
      this.loadData();
    });
  }

  loadData() {
    this.setState({
      isloading: true
    });
    AsyncStorage.getItem("token").then(value => {
      // alert(JSON.stringify(value));
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/apd/order/approve_list";
      var formData = new FormData();
      formData.append("token", value);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson));
          if (responseJson.status == 422) {
            this.setState({
              isloading: false
            });
            this.setState({
              isEmpty: true
            });
          } else {
            this.setState({
              dataSource: responseJson.data.peminjaman,
              isloading: false,
              isEmpty:false
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  searchData() {
    this.setState({
      isloading: true
    });
    AsyncStorage.getItem("token").then(value => {
      // alert(JSON.stringify(value));
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/apd/order/approve_list";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("search", this.state.searchApproval);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.status == 422) {
            this.setState({
              isloading: false
            });
            alert("Data Tidak Ditemukan");
          } else {
            this.setState({
              dataSource: responseJson.data.peminjaman,
              isloading: false
            });
          }
        })
        .catch(error => {
          this.setState({
            isloading: false
          });
          console.log(error);
        });
    });
  }

  render() {
    that = this;
    var list;
    if (this.state.isloading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.isEmpty||this.state.dataSource==undefined) {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = (
          // <ScrollView>
            <FlatList
              data={this.state.dataSource}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isloading}
                  onRefresh={this.onRefresh.bind(this)}
                />}
            />
          // </ScrollView>
        );
      }
    }
    return (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("LaporanAPD")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>Approval APD</Title>
          </Body>
            <Right style={{flex:1}}/>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={styles.homeWrapper}>
          <Footer style={styles.tabHeight}>
            <FooterTab style={styles.tabfooter}>
              <Button
                onPress={() =>
                  this.props.navigation.navigate("ApprovalPersonalAPD")
                }
              >
                <Text style={{color:'white', fontWeight:'bold'}}>PERSONAL</Text>
              </Button>
              <Button
                onPress={() =>
                  this.props.navigation.navigate("ApprovalUnitKerjaAPD")
                }
              >
                <Text style={{color:'white', fontWeight:'bold'}}>UNIT KERJA</Text>
              </Button>
              <Button active style={styles.tabfooter}>
                <View style={{height:'40%'}}></View>
                <View style={{height:'50%'}}>
                <Text>PEMINJAMAN</Text>
                </View>
                <View style={{height:'20%'}}></View>
                <View style={{borderWidth:2, marginTop:2, height:0.5, width:'100%', borderColor:colors.white}}></View>
              </Button>
            </FooterTab>
          </Footer>
          <View>
            <View>
              <View style={styles.viewLeftHeader}>
                <Item style={styles.searchItem}>
                  <Input
                    style={{ fontSize: 11 }}
                    placeholder="Type something here"
                    value={this.state.searchApproval}
                    onChangeText={text =>
                      this.setState({ searchApproval: text })
                    }
                  />
                  {/* <Icon
                    name="search"
                    style={{ fontSize: 20, paddingLeft: 0 }}
                  /> */}
                  <Icon
                    name="ios-search"
                    style={{ fontSize: 30, paddingLeft: 0 }}
                    onPress={() => this.searchData()}
                  />
                </Item>
              </View>
            </View>
          </View>
          <View style={{ flex: 1, flexDirection: "column" }}>
            {list}
            {/* <ScrollView></ScrollView> */}
          </View>
        </View>
        {/* <CustomFooter navigation={this.props.navigation} menu="APD" /> */}
      </Container>
    );
  }
}
