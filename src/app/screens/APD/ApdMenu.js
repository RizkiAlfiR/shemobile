import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body
} from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import LinearGradient from "react-native-linear-gradient";

import SubMenuItems from "../../components/SubMenuItems";
import styles from "../styles/ApdMenu";
import colors from "../../../styles/colors";

const helmet = require("../../../assets/images/helmet.png");

export default class ApdMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    static navigationOptions = {
        //header: null
    };

    render() {
        return (
            <Container style={styles.wrapper}>
                <Header style={styles.header}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.navigate("HomeMenu")}>
                            <Icon
                                name="arrow-left"
                                size={20}
                                style={styles.facebookButtonIcon}
                            />
                        </Button>
                    </Left>
                    <Body>
                        <Title style={styles.textbody}>Alat Pelindung Diri</Title>
                    </Body>
                    {/* <Right/> */}
                </Header>
                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                <View style={styles.homeWrapper}>
                    <View style={styles.weatherContent}>
                        <View style={{ flex: 0 }}>
                            <Image
                                style={styles.apdimage}
                                source={require('../../../assets/images/worker.png')}
                            />
                            <Text style={styles.weatherText}>Alat Pelindung Diri</Text>
                        </View>
                        <View style={{ flex: 1 }} />
                    </View>
                    <View style={styles.itemMenu}>
                        <SubMenuItems
                            handleOnPress={() => this.props.navigation.navigate("ApprovalPersonalAPD")}
                            imageUri={require("../../../assets/images/approval.png")}
                            name="Approval A.P.D."
                        />
                        <SubMenuItems
                            handleOnPress={() => this.props.navigation.navigate("ReportAPD")}
                            imageUri={require("../../../assets/images/report.png")}
                            name="Report A.P.D."
                        />
                        <SubMenuItems
                            handleOnPress={() => this.props.navigation.navigate("OrderPersonalAPD")}
                            imageUri={require("../../../assets/images/order.png")}
                            name="Order A.P.D."
                        />
                    </View>
                    <View style={styles.itemMenu}>
                        <SubMenuItems
                            handleOnPress={() => this.props.navigation.navigate("IndividualReport")}
                            imageUri={require("../../../assets/images/individualreport.png")}
                            name="Individual Report"
                        />
                        <SubMenuItems
                            handleOnPress={() => this.props.navigation.navigate("StockAPD")}
                            imageUri={require("../../../assets/images/stock.png")}
                            name="Stock A.P.D."
                        />
                    </View>
                </View>
            </Container>
        );
    }
}
