import React, { Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  AsyncStorage,
  ActivityIndicator,
  Platform,
  Alert
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Item,
  Card,
  CardItem,
  Input,
  Thumbnail,
  Fab,
  Label,
  Icon,
  Form,
  Textarea
} from "native-base";
import LinearGradient from "react-native-linear-gradient";

import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton,
  DialogFooter
} from "react-native-popup-dialog";
import RoundedButtons from "../../components/RoundedButton";
import styles from "../styles/OrderAPD";
import colors from "../../../styles/colors";
import SnackBar from "./SnackBarOrderPeminjaman";
import GlobalConfig from "../../components/GlobalConfig";
import DatePicker from "react-native-datepicker";

var that;
export default class OrderPeminjamanSubmit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      listApd: [],
      jumlahApd: [],
      dataProfile: [],
      visibleDialogSubmit: false,
      visibleKonfirmasi: false
    };
  }

  static navigationOptions = {
    header: null
  };

  valueJumlah(index) {
    let arr = this.state.jumlahApd;
    if (arr[index] == undefined) {
      return "1";
      console.log("masuk sini");
    } else {
      return this.state.jumlahApd[index];
    }
  }

  ubahJumlah(jumlah, index) {
    let arr = this.state.jumlahApd;
    arr[index] = jumlah;
    this.setState({
      jumlahApd: arr
    });
  }

  tambahJumlah(index) {
    let arr = this.state.jumlahApd;

    arr[index] = arr[index] + 1;
    this.setState({
      jumlahApd: arr
    });

    console.log(this.state.jumlahApd[index]);
  }

  kurangJumlah(index) {
    let arr = this.state.jumlahApd;
    if (arr[index] == 1 || arr[index] == null || arr[index] == undefined) {
    } else {
      arr[index] = arr[index] - 1;
      this.setState({
        jumlahApd: arr
      });
    }
  }

  sendApd() {
    this.setState({
      visibleKonfirmasi: false
    });
    if (this.state.tgl_pinjam == null) {
      alert("Masukkan Tanggal Peminjaman");
    } else if (this.state.tgl_kembali == null) {
      alert("Masukkan Tanggal Pengembalian");
    } else if (this.state.keterangan == null) {
      alert("Masukkan Keterangan");
    } else {
      this.setState({
        visibleDialogSubmit: true
      });
      AsyncStorage.getItem("token").then(value => {
        var url =
          GlobalConfig.SERVERHOST +
          "api/v_mobile/apd/order/create_order/pinjam";
        var formData = new FormData();
        formData.append("token", value);
        for (let index = 0; index < this.state.listApd.length; index++) {
          formData.append(
            "KODE_APD[" + index + "]",
            this.state.listApd[index].KODE
          );
          formData.append("JUMLAH[" + index + "]", this.state.jumlahApd[index]);
        }
        formData.append("TANGGAL_PINJAM", this.state.tgl_pinjam);
        formData.append("RETUR_DATE", this.state.tgl_kembali);
        formData.append("KETERANGAN", this.state.keterangan);
        formData.append("COMPANY", this.state.dataProfile.company);
        formData.append("PLANT", this.state.dataProfile.plant);

        fetch(url, {
          headers: {
            "Content-Type": "multipart/form-data"
          },
          method: "POST",
          body: formData
        })
          .then(response => response.json())
          .then(responseJson => {
            //alert(JSON.stringify(responseJson));
            if (responseJson.status == 200) {
              this.setState({
                visibleDialogSubmit: false
              });
              Alert.alert(
                "Success",
                "Order Success, ID ORDER: " + responseJson.data.ORDER_CODE,
                [
                  {
                    text: "Okay"
                  }
                ]
              );
              this.props.navigation.navigate("OrderPeminjamanAPD");
            } else {
              this.setState({
                visibleDialogSubmit: false
              });
              Alert.alert("Error", "Order Failed", [
                {
                  text: "Okay"
                }
              ]);
            }
          })
          .catch(error => {
            Alert.alert("Error", "Check Your Internet Connection", [
              {
                text: "Okay"
              }
            ]);
            console.log(error);
          });
      });
    }
  }

  componentDidMount() {
    this.loadData();
    that = this;
    AsyncStorage.getItem("listApd")
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({
          listApd: json
        });
        let arr = new Array(this.state.listApd.length);
        for (let index = 0; index < arr.length; index++) {
          arr[index] = 1;
        }
        this.setState({
          jumlahApd: arr
        });
        console.log(this.state.listApd);
        console.log(this.state.jumlahApd);
      });
  }

  loadData() {
    AsyncStorage.getItem("token").then(value => {
      const url = GlobalConfig.SERVERHOST + "api/Auth/user?token=" + value;

      fetch(url)
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            dataProfile: responseJson.data.data
          });
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  render() {
    let arr = this.state.jumlahApd;
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() =>
                this.props.navigation.navigate("OrderPeminjamanAPD")
              }
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 4, alignItems: "center" }}>
            <Title style={styles.textbody}>Konfirmasi Order</Title>
          </Body>
          <Right style={{ flex: 1 }} />
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ flex: 1 }}>
          <Content style={{ marginTop: 0 }}>
            {this.state.listApd.map((buttonInfo, index) => (
              <View key={index} style={{ backgroundColor: "#FEFEFE" }}>
                <CardItem
                  style={{
                    borderRadius: 0,
                    marginTop: 4,
                    backgroundColor: colors.gray
                  }}
                >
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      marginTop: 4,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{width:'70%'}}>
                      <Text style={styles.viewName}>
                        {buttonInfo.NAME_ORIGIN}
                      </Text>
                      <Text style={styles.viewMerk}>{buttonInfo.MERK}</Text>
                    </View>
                    <View style={{width:'30%'}}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <Button
                          onPress={() => this.kurangJumlah(index)}
                          style={styles.btnQTYLeft}
                        >
                          <Icon
                            name="ios-arrow-back"
                            style={styles.facebookButtonIconQTY}
                          />
                        </Button>

                        <View
                          style={{
                            width: 30,
                            height: 30,
                            marginTop: 0,
                            backgroundColor: colors.white
                          }}
                        >
                          <Input
                            style={{
                              height: 30,
                              marginTop: -5,
                              fontSize: 12,
                              textAlign: "center"
                            }}
                            value={this.state.jumlahApd[index] + ""}
                            keyboardType='numeric'
                            onChangeText={text => this.ubahJumlah(text, index)}
                          />
                        </View>
                        <Button
                          onPress={() => this.tambahJumlah(index)}
                          style={styles.btnQTYRight}
                        >
                          <Icon
                            name="ios-arrow-forward"
                            style={styles.facebookButtonIconQTY}
                          />
                        </Button>
                      </View>
                    </View>
                  </View>
                </CardItem>
              </View>
            ))}
            <View style={{ backgroundColor: "#FEFEFE" }}>
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 4,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View>
                    <Text style={styles.viewMerk}>Event Date</Text>
                    <DatePicker
                      style={{ width: 320, fontSize: 11, borderRadius: 20 }}
                      date={this.state.tgl_pinjam}
                      mode="date"
                      placeholder="Event Date"
                      format="YYYY-MM-DD"
                      minDate="2018-01-01"
                      maxDate="2050-12-31"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateIcon: {
                          position: "absolute",
                          left: 0,
                          top: 5,
                          marginLeft: 0
                        },
                        dateInput: {
                          marginLeft: 50,
                          borderRadius: 20
                        }
                      }}
                      onDateChange={date => {
                        this.setState({ tgl_pinjam: date });
                      }}
                    />
                  </View>
                </View>
              </CardItem>

              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View>
                    <Text style={styles.viewMerk}>Return Date</Text>
                    <DatePicker
                      style={{ width: 320, fontSize: 11, borderRadius: 20 }}
                      date={this.state.tgl_kembali}
                      mode="date"
                      placeholder="Return Date"
                      format="YYYY-MM-DD"
                      minDate="2018-01-01"
                      maxDate="2050-12-31"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateIcon: {
                          position: "absolute",
                          left: 0,
                          top: 5,
                          marginLeft: 0
                        },
                        dateInput: {
                          marginLeft: 50,
                          borderRadius: 20
                        }
                      }}
                      onDateChange={date => {
                        this.setState({ tgl_kembali: date });
                      }}
                    />
                  </View>
                </View>
              </CardItem>
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.viewMerk}>Keterangan</Text>
                  </View>
                  <View>
                    <Textarea
                      style={{
                        marginLeft: 5,
                        marginRight: 5,
                        marginBottom: 5,
                        fontSize: 11
                      }}
                      rowSpan={3}
                      bordered
                      value={this.state.keterangan}
                      placeholder="Description"
                      onChangeText={text => this.setState({ keterangan: text })}
                    />
                  </View>
                </View>
              </CardItem>
            </View>
          </Content>

          <View style={styles.Contentsave}>
            <Button
              block
              style={{
                height: 45,
                marginLeft: 20,
                marginRight: 20,
                marginBottom: 20,
                borderWidth: 1,
                backgroundColor: "#00b300",
                borderColor: "#00b300",
                borderRadius: 4
              }}
              onPress={() => this.setState({ visibleKonfirmasi: true })}
            >
              <Text style={styles.buttonText}>Submit</Text>
            </Button>
          </View>
        </View>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleDialogSubmit}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogTitle={<DialogTitle title="Creating Order.." />}
          >
            <DialogContent>
              {<ActivityIndicator size="large" color="#330066" animating />}
            </DialogContent>
          </Dialog>
        </View>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleKonfirmasi}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            onTouchOutside={() => {
              this.setState({ visibleKonfirmasi: false });
            }}
            // dialogStyle={{ position: 'absolute', top: this.state.posDialog }}
            dialogTitle={
              <DialogTitle
                title="Apakah yang Anda pesan sudah sesuai?"
                style={{
                  backgroundColor: colors.white,
                  marginTop:20,
                  marginBottom:20
                }}
                textStyle={{fontSize:15}}
                hasTitleBar={false}
                align="center"
              />
            }
          >
            <DialogContent
              style={{
                backgroundColor: colors.white
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                  <Button
                    block
                    style={{
                      height: 45,
                      marginLeft: 20,
                      marginRight: 20,
                      marginBottom: 20,
                      borderWidth: 1,
                      backgroundColor: colors.gray02,
                      borderColor: colors.gray02,
                      borderRadius: 4
                    }}
                    onPress={() => this.setState({ visibleKonfirmasi: false })}
                  >
                    <Text style={styles.buttonText}>Batal</Text>
                  </Button>
                </View>
                <View style={{ flex: 1 }}>
                  <Button
                    block
                    style={{
                      height: 45,
                      marginLeft: 20,
                      marginRight: 20,
                      marginBottom: 20,
                      borderWidth: 1,
                      backgroundColor: "#00b300",
                      borderColor: "#00b300",
                      borderRadius: 4
                    }}
                    onPress={() => this.sendApd()}
                  >
                    <Text style={styles.buttonText}>Iya</Text>
                  </Button>
                </View>
              </View>
            </DialogContent>
          </Dialog>
        </View>
      </Container>
    );
  }
}
