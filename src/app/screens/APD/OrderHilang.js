import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  FlatList,
  AsyncStorage,
  ActivityIndicator,
  RefreshControl,
  Platform
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Item,
  Card,
  CardItem,
  Input,
  Thumbnail,
  Fab,
  Icon
} from "native-base";
// import Icon from "react-native-vector-icons/FontAwesome";
import LinearGradient from "react-native-linear-gradient";
import GlobalConfig from "../../components/GlobalConfig";

import RoundedButtons from "../../components/RoundedButton";
import NewsContents from "../../components/NewsContent";
import SubMenuItems from "../../components/SubMenuItems";
import styles from "../styles/OrderAPD";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Ripple from "react-native-material-ripple";

var that;
class ListItem extends React.PureComponent {
  navigateToScreen(route, id_rusak) {
    // alert(route);
    AsyncStorage.setItem("id", id_rusak).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View style={{ backgroundColor: "#FEFEFE" }}>
        <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
          <Ripple
            style={{
              flex: 2,
              justifyContent: "center",
              alignItems: "center"
            }}
            rippleSize={176}
            rippleDuration={600}
            rippleContainerBorderRadius={15}
            onPress={() =>
              this.navigateToScreen(
                "OrderHilangSubmit",
                this.props.data.ID_DETAIL
              )
            }
            rippleColor={colors.accent}
          >
          <CardItem style={{borderRadius:10}}>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                marginTop: 4,
              }}
            >
              <View style={styles.viewKonfirmasiLeft}>
                <Text style={styles.viewName}>{this.props.data.NAMA}</Text>
                <Text style={styles.viewMerk}>{this.props.data.MERK}</Text>
              </View>
            </View>
          </CardItem>
          </Ripple>
        </Card>
      </View>
    );
  }
}

export default class OrderHilang extends Component {
  constructor() {
    super();
    this.state = {
      active: "true",
      dataSource: [],
      isloading: true,
      isEmpty: false
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} />;

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.setState({
          isloading:true
        })
        this.loadData();
      }
    );
    // this.loadData();
  }

  onRefresh() {
    console.log('refreshing')
    this.setState({ isloading: true }, function() {
      this.loadData();
    });
  }

  loadData() {
    AsyncStorage.getItem("token").then(value => {
      // alert(JSON.stringify(value));
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/apd/validasi/apd/list";
      var formData = new FormData();
      formData.append("token", value);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.status == "400") {
            this.setState({
              isloading: false
            });
            this.setState({
              isEmpty: true
            });
          } else {
            this.setState({
              dataSource: responseJson.data,
              isloading: false
            });
            this.setState({
              isEmpty: false
            });
          }
          //alert(JSON.stringify(responseJson));
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  render() {
    that = this;
    return (
      // this.state.isloading
      //   ?
      //   <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      //     <ActivityIndicator size="large" color="#330066" animating />
      //   </View>
      //   :
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("LaporanAPD")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>Order Hilang</Title>
          </Body>

            <Right style={{flex:1}}/>

        </Header>

        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />

        <View style={{ flex: 1, flexDirection: "column" }}>
          {/* <Content style={{ marginTop: 0 }}> */}
          {this.state.isloading ? (
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <ActivityIndicator size="large" color="#330066" animating />
            </View>
          ) : this.state.isEmpty ? (
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Thumbnail
                square
                large
                source={require("../../../assets/images/empty.png")}
              />
              <Text>No Data!</Text>
            </View>
          ) : (
            <FlatList
              data={this.state.dataSource}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isloading}
                  onRefresh={this.onRefresh.bind(this)}
                />}
            />
          )}
          {/* </Content> */}
        </View>

        {/* <CustomFooter navigation={this.props.navigation} menu="APD" /> */}
      </Container>
    );
  }
}
