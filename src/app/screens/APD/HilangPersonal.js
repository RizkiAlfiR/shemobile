import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    StatusBar,
    ScrollView,
    FlatList,
    AsyncStorage,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Text,
    Button,
    Left,
    Right,
    Body,
    Item,
    Card,
    CardItem,
    Input,
    Thumbnail,
    Fab,
} from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import LinearGradient from "react-native-linear-gradient";

import RoundedButtons from "../../components/RoundedButton";
import NewsContents from "../../components/NewsContent";
import SubMenuItems from "../../components/SubMenuItems";
import styles from "../styles/OrderAPD";
import colors from "../../../styles/colors";

const helmet = require("../../../assets/images/helmet.png");

class ListItem extends React.PureComponent {
    render() {
        return (
            <View style={{ backgroundColor: '#FEFEFE' }}>
                <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
                    <CardItem style={{ borderRadius: 10 }}>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View style={styles.viewLeftOrder}>
                                <Image
                                    style={styles.apdorderimg}
                                    source={require('../../../assets/images/imgorder.png')}
                                />
                            </View>
                            <View>
                                <Text style={styles.viewRightOrder}>{this.props.data.NAMA}</Text>
                                <Text style={styles.viewRightOrder}>{this.props.data.MERK}</Text>
                            </View>
                        </View>
                    </CardItem>
                </Card>
            </View>
        )
    }
}

export default class HilangPersonal extends Component {
    constructor() {
        super();
        this.state = {
            active: 'true',
            dataSource: [],
            isLoading: true,
        };
    }

    static navigationOptions = {
        header: null
    };

    _renderItem = ({ item }) => (
        <ListItem data={item}></ListItem>
    )

    componentDidMount() {
        this.loadData();
    }

    loadData() {
        AsyncStorage.getItem('token').then((value) => {
            // alert(JSON.stringify(value));
            const url = 'http://10.15.5.150/dev/she/api/v_mobile/apd/validasi/order_type/stock';
            var formData = new FormData();
            formData.append("token", value)

            fetch(url, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                method: 'POST',
                body: formData
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    //alert(JSON.stringify(responseJson));
                    this.setState({
                        dataSource: responseJson.data,
                        isloading: false
                    });
                })
                .catch((error) => {
                    console.log(error)
                })
        })
    }

    render() {
        return (
            this.state.isloading
                ?
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size="large" color="#330066" animating />
                </View>
                :
                <Container style={styles.wrapper}>
                    <Header style={styles.header}>
                        <Left>
                            <Button transparent onPress={() => this.props.navigation.navigate("OrderPersonalAPD")}>
                                <Icon
                                    name="arrow-left"
                                    size={20}
                                    style={styles.facebookButtonIcon}
                                />
                            </Button>
                        </Left>
                        <Body>
                            <Title style={styles.textbody}>Lapor Hilang A.P.D Personal</Title>
                        </Body>
                        {/* <Right/> */}
                    </Header>
                    <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                    <View style={styles.search}>
                        <View style={{ flex: 2, height: '100%' }}>
                            <Item style={styles.searchItem}>
                                <Icon name="search" style={{ fontSize: 20, padding: 5 }} />
                                <Input placeholder="Type something here" />
                            </Item>
                        </View>
                    </View>

                    <View style={{ flex: 1 }}>
                        <View style={styles.roundedBtn}>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                <Left>
                                    <RoundedButtons
                                        text="Filter 1"
                                        textColor={colors.white}
                                        background={colors.gray03}
                                    // handleOnPress={this.onLoginPress}
                                    />
                                </Left>
                                <Body>
                                    <RoundedButtons
                                        text="Filter 2"
                                        textColor={colors.white}
                                        background={colors.gray03}
                                    //handleOnPress={this.onLoginPress}
                                    />
                                </Body>
                                <Body>
                                    <RoundedButtons
                                        text="Filter 3"
                                        textColor={colors.white}
                                        background={colors.gray03}
                                    //handleOnPress={this.onLoginPress}
                                    />
                                </Body>
                                <Body>
                                    <RoundedButtons
                                        text="Filter 4"
                                        textColor={colors.white}
                                        background={colors.gray03}
                                    //handleOnPress={this.onLoginPress}
                                    />
                                </Body>
                                <Body>
                                    <RoundedButtons
                                        text="Filter 5"
                                        textColor={colors.white}
                                        background={colors.gray03}
                                    //handleOnPress={this.onLoginPress}
                                    />
                                </Body>
                                <Body>
                                    <RoundedButtons
                                        text="Filter 6"
                                        textColor={colors.white}
                                        background={colors.gray03}
                                    //handleOnPress={this.onLoginPress}
                                    />
                                </Body>
                            </ScrollView>
                        </View>
                        <Content style={{ marginTop: 35 }}>
                            <FlatList
                                data={this.state.dataSource}
                                renderItem={this._renderItem}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </Content>
                    </View>


                </Container>
        );
    }
}
