import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  FlatList,
  AsyncStorage,
  ActivityIndicator,
  Platform,
  Animated,
  AppRegistry
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Item,
  Card,
  CardItem,
  Input,
  Thumbnail,
  Fab,
  Icon
} from "native-base";

import LinearGradient from "react-native-linear-gradient";
import GlobalConfig from '../../components/GlobalConfig';
import RoundedButtons from "../../components/RoundedButton";
import StockContents from "../../components/StockContents";
import SubMenuItems from "../../components/SubMenuItems";
import MenuItems from "../../components/APDItems";
import styles from "../styles/StockAPD";
import colors from "../../../styles/colors";
import SnackBar from "./SnackBarOrderPersonal";
import CustomFooter from "../../components/CustomFooter";

import Router from './../../../../App';

class ListItem extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
    };
  }

  render() {
    var image
    if (this.props.data.FOTO_BEF!=null){
      image={uri:this.props.data.FOTO_BEF};
    }else{
      image=require("../../../assets/images/safety.jpg")
    }

    return (
      <View style={{ backgroundColor: '#FEFEFE' }}>
        <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 0 }}>
          <CardItem style={{ borderRadius: 0, width: 160 }}>
            <View style={styles.news}>
              <StockContents
                imageUri={image}
                name={this.props.data.NAMA}
                merk={this.props.data.MERK}
                available={this.props.data.AVAILABLE}
                jumlah={this.props.data.STOK}
              />
            </View>
          </CardItem>
        </Card>
      </View>
    )
  }
}

export default class StockAPD extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      isLoading: true,
      listApdNameChosen: [],
      listApdChosen: [],
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => (
    <ListItem data={item}></ListItem>
  )

  componentDidMount() {
    this.loadData();
  }

  loadData() {
    AsyncStorage.getItem('token').then((value) => {
      // alert(JSON.stringify(value));
      const url = GlobalConfig.SERVERHOST + 'api/v_mobile/apd/validasi/order_type/stock';
      var formData = new FormData();
      formData.append("token", value)

      fetch(url, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        method: 'POST',
        body: formData
      })
        .then((response) => response.json())
        .then((responseJson) => {
          //alert(JSON.stringify(responseJson));
          this.setState({
            dataSource: responseJson.data,
            isloading: false
          });
        })
        .catch((error) => {
          console.log(error)
        })
    })
  }

  filterData(groupAPD) {
    AsyncStorage.getItem('token').then((value) => {
      // alert(JSON.stringify(value));
      const url = GlobalConfig.SERVERHOST + 'api/v_mobile/apd/validasi/order_type/stock';
      var formData = new FormData();
      formData.append("token", value)

      fetch(url, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        method: 'POST',
        body: formData
      })
        .then((response) => response.json())
        .then((responseJson) => {
          //alert(JSON.stringify(responseJson));
          this.setState({
            dataSource: responseJson.data.filter(x => x.GROUP_CODE == groupAPD),
            isloading: false
          });
        })
        .catch((error) => {
          console.log(error)
        })
    })
  }



  render() {
    return (
      this.state.isloading
        ?
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
        :
        <Container style={styles.wrapper}>
          <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
          <View style={styles.search}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <View style={styles.viewLeftHeader}>
                <Item style={styles.searchItem}>
                  <Text style={styles.font}>Stock APD</Text>
                </Item>
              </View>
            </View>
          </View>

          <View style={{ flex: 1 }}>
            <View style={styles.roundedBtn}>
              <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                <Left>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/all.png")}
                      name="All"
                      handleOnPress={() => this.loadData()}
                    />
                  </View>
                </Left>
                <Body>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/body.jpg")}
                      name="Body"
                      handleOnPress={() => this.filterData(600)}
                    />
                  </View>
                </Body>
                <Body>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/head.jpg")}
                      name="Head"
                      handleOnPress={() => this.filterData(100)}
                    />
                  </View>
                </Body>
                <Body>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/eye.jpg")}
                      name="Eyes"
                      handleOnPress={() => this.filterData(200)}
                    />
                  </View>
                </Body>
                <Body>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/ear.jpg")}
                      name="Ear"
                      handleOnPress={() => this.filterData(300)}
                    />
                  </View>
                </Body>
                <Body>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/cloves.jpg")}
                      name="Cloves"
                      handleOnPress={() => this.filterData(500)}
                    />
                  </View>
                </Body>
                <Body>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/boots.jpg")}
                      name="Boots"
                      handleOnPress={() => this.filterData(700)}
                    />
                  </View>
                </Body>
                <Body>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/mic.png")}
                      name="Nose"
                      handleOnPress={() => this.filterData(400)}
                    />
                  </View>
                </Body>
                <Body>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/p3k.png")}
                      name="P3K"
                      handleOnPress={() => this.filterData(900)}
                    />
                  </View>
                </Body>
                <Body>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/full.jpg")}
                      name="Full Body"
                      handleOnPress={() => this.filterData(800)}
                    />
                  </View>
                </Body>
                <Body>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/stock.png")}
                      name="Lain"
                      handleOnPress={() => this.filterData(910)}
                    />
                  </View>
                </Body>
              </ScrollView>
            </View>

            <Content style={{ marginTop: 35 }}>
              <FlatList
                data={this.state.dataSource}
                renderItem={this._renderItem}
                numColumns={2}
                keyExtractor={(item, index) => index.toString()}
              />
            </Content>
          </View>
          {/* <CustomFooter navigation={this.props.navigation} menu='APD' /> */}
        </Container>
    );
  }
}
AppRegistry.registerComponent('StockAPD', () => StockAPD);
