import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  FlatList,
  AsyncStorage,
  ActivityIndicator,
  Platform,
  Animated,
  AppRegistry,
  Alert,
  RefreshControl,
  Dimensions
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Item,
  Card,
  CardItem,
  Input,
  Thumbnail,
  Fab,
  Icon
} from "native-base";

import LinearGradient from "react-native-linear-gradient";
import GlobalConfig from "../../components/GlobalConfig";
import RoundedButtons from "../../components/RoundedButton";
import NewsContents from "../../components/APDContent";
import SubMenuItems from "../../components/SubMenuItems";
import MenuItems from "../../components/APDItems";
import styles from "../styles/OrderAPD";
import colors from "../../../styles/colors";
import SnackBar from "./SnackBarOrderPeminjaman";
import CustomFooter from "../../components/CustomFooter";

var that;

class ListItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      active: "true"
    };
  }

  componentDidMount() {
    this.setState({
      listApdTroli: []
    });
  }

  navigateToScreen(route, kodeAPD) {
    // alert(route);
    AsyncStorage.setItem("id", kodeAPD).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  cekAPD() {
    var status=''
    // alert("APD Tidak Bisa di Order");

    if ((this.props.data.REQUESTED==false) || (this.props.data.ORDER!="ENABLED")){
      status+="- Item sudah direquest \n"
    }
    if (this.props.data.pinjam!=1){
      status+='- Item tidak diperuntukkan peminjaman \n'
    }
    if (this.props.data.STOK==0){
      status+='- Tidak ada stok'
    }
    Alert.alert("APD Tidak Bisa di Order",status);
  }

  render() {
    return (
      <View style={{ backgroundColor: "#FEFEFE" ,width:'50%'}}>
        <Card
          style={{ marginLeft: 5, marginRight: 5, borderRadius: 0, flex:1 }}
        >
          <View style={styles.news}>
            <NewsContents
              imageUri={require("../../../assets/images/nopic.png")}
              name={this.props.data.NAME_ORIGIN}
              merk={this.props.data.MERK}
              stok={this.props.data.STOK}
            />
            <View style={{ flex: 1, flexDirection: "row" }}>
              {this.props.data.STOK > 0 &&
              this.props.data.ORDER == "ENABLED" &&
              this.props.data.REQUESTED == true &&
              this.props.data.PINJAM == 1 ? (
                <Button
                  style={{
                    backgroundColor: "#27ae60",
                    width: "50%",
                    height: 25,
                    marginTop: 2,
                    borderRadius: 0
                  }}
                  onPress={() => this.props.showSnack(this.props.data)}
                >
                  <Icon name="ios-add" style={styles.facebookButtonIconOrder} />
                </Button>
              ) : (
                <Button
                  style={{
                    backgroundColor: "#27ae60",
                    width: "50%",
                    height: 25,
                    marginTop: 2,
                    borderRadius: 0
                  }}
                  onPress={() => this.cekAPD()}
                >
                  <Icon name="ios-add" style={styles.facebookButtonIconOrder} />
                </Button>
              )}

              {this.props.data.STOK > 0 &&
              this.props.data.ORDER == "ENABLED" &&
              this.props.data.REQUESTED == true &&
              this.props.data.PINJAM == 1 ? (
                <Button
                  style={{
                    backgroundColor: "#d8d8d8",
                    width: "50%",
                    height: 25,
                    marginTop: 2,
                    borderRadius: 0
                  }}
                  onPress={() => this.props.troli(this.props.data.KODE)}
                >
                  <Icon
                    name="ios-cart"
                    style={styles.facebookButtonIconOrderOrder}
                  />
                </Button>
              ) : (
                <Button
                  style={{
                    backgroundColor: "#d8d8d8",
                    width: "50%",
                    height: 25,
                    marginTop: 2,
                    borderRadius: 0
                  }}
                  onPress={() => this.cekAPD()}
                >
                  <Icon
                    name="ios-cart"
                    style={styles.facebookButtonIconOrderOrder}
                  />
                </Button>
              )}
            </View>
          </View>
        </Card>
      </View>
    );
  }
}

export default class OrderPeminjamanAPD extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isloading: true,
      listApdNameChosen: [],
      listApdChosen: [],
      dataProfile: [],
      dataWishlist: [], //solving issue
      listKodeWishlist: [], //solving issue
      listClickedKatAPD: [],
      isEmpty: false
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item, index }) => (
    <ListItem
      data={item}
      index={index}
      showSnack={nama => this.DisplaySnackBar(nama)}
      troli={kodeAPD => this.troli(kodeAPD)}
    />
  );

  setClickedKategori(clickedIndex) {
    var tempclickAPD = [];
    for (let i = 0; i < 11; i++) {
      if (i == clickedIndex) {
        tempclickAPD.push(true);
      } else {
        tempclickAPD.push(false);
      }
    }
    this.setState({
      listClickedKatAPD: tempclickAPD
    });
  }

  troli = kodeAPD => {
    var ketemu = false;
    for (let i = 0; i < this.state.listKodeWishlist.length; i++) {
      let tempApd = this.state.listKodeWishlist[i];
      if (kodeAPD == tempApd) {
        ketemu = true;
      }
    }
    if (ketemu == true) {
      alert("APD sudah ada di dalam keranjang!");
    } else {
      var tempWishlistKode = this.state.listKodeWishlist;
      tempWishlistKode.push(kodeAPD);
      console.log(tempWishlistKode);
      this.setState({
        listKodeWishlist: tempWishlistKode
      });

      AsyncStorage.getItem("token").then(value => {
        // alert(JSON.stringify(value));
        const url =
          GlobalConfig.SERVERHOST + "api/v_mobile/apd/stock/wishlist/create";
        var formData = new FormData();
        formData.append("token", value);
        formData.append("KODE_APD", kodeAPD);
        formData.append("QTY", 1);
        formData.append("ORDER_TYPE", "PJM");
        formData.append("CODE_COMP", this.state.dataProfile.company);
        formData.append("CODE_PLANT", this.state.dataProfile.plant);

        fetch(url, {
          headers: {
            "Content-Type": "multipart/form-data"
          },
          method: "POST",
          body: formData
        })
          .then(response => response.json())
          .then(response => {
            if (response.status == 200) {
              Alert.alert("Success", "Tambahkan ke Keranjang", [
                {
                  text: "Okay"
                }
              ]);
            } else {
              Alert.alert("Error", "Troli Failed", [
                {
                  text: "Okay"
                }
              ]);
            }
          });
      });
    }
  };

  DisplaySnackBar = nama => {
    var ketemu = false;
    for (let index = 0; index < this.state.listApdChosen.length; index++) {
      let tempApd = this.state.listApdChosen[index];
      if (nama.ID == tempApd.ID) {
        ketemu = true;
      }
    }
    if (ketemu == false) {
      let arrName = this.state.listApdNameChosen;
      let arrApd = this.state.listApdChosen;
      arrName.push(nama.NAME_ORIGIN);
      arrApd.push(nama);
      this.setState({
        listApdNameChosen: arrName,
        listApdChosen: arrApd
      });
    } else {
      let idx = this.state.listApdChosen.indexOf(nama);
      let arrName = this.state.listApdNameChosen;
      let arrApd = this.state.listApdChosen;
      arrName.splice(idx, 1);
      arrApd.splice(idx, 1);
      this.setState({
        listApdNameChosen: arrName,
        listApdChosen: arrApd
      });
      console.log(this.state.listApdChosen);
    }
    if (this.state.listApdChosen.length == 0) {
      console.log("masuk undefined");
      this.refs.ReactNativeSnackBar.SnackBarCloseFunction();
    } else {
      this.refs.ReactNativeSnackBar.ShowSnackBarFunction(
        this.state.listApdNameChosen,
        this.state.listApdChosen
      );
    }
  };

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadData();
        this.setClickedKategori(0);
        this.loadProfile();
        this.loadWishlist();
        console.log("masuk didfokus");

        this.setState({
          listApdNameChosen: [],
          listApdChosen: []
        });

        this.refs.ReactNativeSnackBar.EmptySnackBarFunction();
        this.refs.ReactNativeSnackBar.SnackBarCloseFunction();
      }
    );

  }

  loadWishlist() {
    AsyncStorage.getItem("token").then(value => {
      // alert(JSON.stringify(value));
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/apd/stock/wishlist/get";
      var formData = new FormData();
      formData.append("token", value);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.data==undefined){
            this.setState({
              listKodeWishlist: []
            });
          }else{
            this.setState({
              dataWishlist: responseJson.data.PJM
            });
            var arr = new Array(this.state.dataWishlist.length);
            var tempData = this.state.dataWishlist;
            for (let index = 0; index < arr.length; index++) {
              arr[index] = tempData[index].KODE_APD;
            }
            console.log(arr);
            this.setState({
              listKodeWishlist: arr
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  loadProfile() {
    AsyncStorage.getItem("token").then(value => {
      const url = GlobalConfig.SERVERHOST + "api/Auth/user?token=" + value;

      fetch(url)
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            dataProfile: responseJson.data.data
          });
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  loadData() {
    this.setClickedKategori(0);
    this.setState({
      isloading: true
    });

    AsyncStorage.getItem("token").then(value => {
      // alert(JSON.stringify(value));
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/apd/validasi/order_type/pinjam";
      var formData = new FormData();
      formData.append("token", value);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          //alert(JSON.stringify(responseJson));
          this.setState({
            dataSource: responseJson.data.filter(x => x.PINJAM == 1),
            isloading: false
          });
          this.setState({
            isEmpty: false
          });
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  onRefresh() {
    console.log('refreshing')
    this.setState({ isloading: true }, function() {
      this.loadData();
    });
  }

  filterData(groupAPD,clicked) {
    this.setClickedKategori(clicked);
    this.setState({
      isloading: true
    });
    AsyncStorage.getItem("token").then(value => {
      // alert(JSON.stringify(value));
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/apd/validasi/order_type/pinjam";
      var formData = new FormData();
      formData.append("token", value);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          //alert(JSON.stringify(responseJson));
          this.setState({
            dataSource: responseJson.data.filter(x => (x.GROUP_CODE == groupAPD)&&(x.PINJAM == 1)),
            isloading: false
          });
          this.setState({
            isEmpty: false
          });
          if(this.state.dataSource.length==0){
            this.setState({
              isEmpty:true
            })
          }
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  searchData() {
    this.setState({
      isloading: true
    });
    AsyncStorage.getItem("token").then(value => {
      // alert(JSON.stringify(value));
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/apd/validasi/order_type/pinjam";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("search", this.state.searchAPD);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.status == 422) {
            this.setState({
              isloading: false
            });
            alert("Data Tidak Ditemukan");
          } else {
            this.setClickedKategori(undefined)
            this.setState({
              dataSource: responseJson.data.filter(x => x.PINJAM == 1),
              isloading: false
            });
            this.setState({
              isEmpty: false
            });
            if(this.state.dataSource.length==0){
              this.setState({
                isEmpty:true
              })
            }
          }
        })
        .catch(error => {
          this.setState({
            isloading: false
          });
          console.log(error);
        });
    });
  }

  render() {
    var list;
    if (this.state.isloading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center",marginTop:35  }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.isEmpty) {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center",marginTop:100  }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Item!</Text>
          </View>
        );
      } else {
        list = (
          <FlatList
            data={this.state.dataSource}
            renderItem={this._renderItem}
            numColumns={2}
            keyExtractor={(item, index) => index.toString()}
            refreshControl={
            <RefreshControl
              refreshing={this.state.isloading}
              onRefresh={this.onRefresh.bind(this)}
            />}
          />
        );
      }
    }
    return (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("HomeMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>Order APD</Title>
          </Body>

            <Right style={{flex:1}}/>

        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <Footer style={styles.tabHeight}>
            <FooterTab style={styles.tabfooter}>
              <Button onPress={() =>
                  this.props.navigation.navigate("OrderPersonalAPD")
                }>
                <Text style={[styles.titleTabFont,{color:'white', fontWeight:'bold'}]}>PERSONAL</Text>
              </Button>
              <Button
                onPress={() =>
                  this.props.navigation.navigate("OrderUnitKerjaAPD")
                }
              >
                <Text style={[styles.titleTabFont,{color:'white', fontWeight:'bold'}]}>UNIT KERJA</Text>
              </Button>
              <Button active style={styles.tabfooter}>
                <View style={{height:'40%'}}></View>
                <View style={{height:'50%'}}>
                <Text style={styles.titleTabFont}>PEMINJAMAN</Text>
                </View>
                <View style={{height:'20%'}}></View>
                <View style={{borderWidth:2, marginTop:2, height:0.5, width:'100%', borderColor:colors.white}}></View>
              </Button>
            </FooterTab>
          </Footer>
          <View style={{ flex: 0, flexDirection: "row" }}>
            <View style={styles.viewLeftHeader}>
              <Item style={styles.searchItem}>
                <Input
                  style={{ fontSize: 11 }}
                  placeholder="Type something here"
                  value={this.state.searchAPD}
                  onChangeText={text => this.setState({ searchAPD: text })}
                />
                <Icon
                  name="ios-search"
                  style={{ fontSize: 30, paddingLeft: 0 }}
                  onPress={() => this.searchData()}
                />
              </Item>
            </View>
            <View style={styles.viewRightHeader}>
              <Button
                transparent
                onPress={() =>
                  this.props.navigation.navigate("OrderPeminjamanTroli")
                }
              >
                <Icon name="ios-cart" style={styles.facebookButtonIcon} />
              </Button>
            </View>

        </View>

        <View style={{ flex: 1 }}>
          <View style={styles.roundedBtn}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              <Left>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/all.png")}
                    name="All"
                    clicked={this.state.listClickedKatAPD[0] ? true : false} //coba j
                    handleOnPress={() => this.loadData()}
                  />
                </View>
              </Left>
              <Body>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/body.jpg")}
                    name="Body"
                    clicked={this.state.listClickedKatAPD[1] ? true : false} //coba j
                    handleOnPress={() => this.filterData(600,1)}
                  />
                </View>
              </Body>
              <Body>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/head.jpg")}
                    name="Head"
                    clicked={this.state.listClickedKatAPD[2] ? true : false} //coba j
                    handleOnPress={() => this.filterData(100,2)}
                  />
                </View>
              </Body>
              <Body>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/eye.jpg")}
                    name="Eyes"
                    clicked={this.state.listClickedKatAPD[3] ? true : false} //coba j
                    handleOnPress={() => this.filterData(200,3)}
                  />
                </View>
              </Body>
              <Body>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/ear.jpg")}
                    name="Ear"
                    clicked={this.state.listClickedKatAPD[4] ? true : false} //coba j
                    handleOnPress={() => this.filterData(300,4)}
                  />
                </View>
              </Body>
              <Body>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/cloves.jpg")}
                    name="Cloves"
                    clicked={this.state.listClickedKatAPD[5] ? true : false} //coba j
                    handleOnPress={() => this.filterData(500,5)}
                  />
                </View>
              </Body>
              <Body>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/boots.jpg")}
                    name="Boots"
                    clicked={this.state.listClickedKatAPD[6] ? true : false} //coba j
                    handleOnPress={() => this.filterData(700,6)}
                  />
                </View>
              </Body>
              <Body>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/nose.png")}
                    name="Nose"
                    clicked={this.state.listClickedKatAPD[7] ? true : false} //coba j
                    handleOnPress={() => this.filterData(400,7)}
                  />
                </View>
              </Body>
              <Body>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/p3k.png")}
                    name="P3K"
                    clicked={this.state.listClickedKatAPD[8] ? true : false} //coba j
                    handleOnPress={() => this.filterData(900,8)}
                  />
                </View>
              </Body>
              <Body>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/full.jpg")}
                    name="Full Body"
                    clicked={this.state.listClickedKatAPD[9] ? true : false} //coba j
                    handleOnPress={() => this.filterData(800,9)}
                  />
                </View>
              </Body>
              <Body>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/stock.png")}
                    name="Lain"
                    clicked={this.state.listClickedKatAPD[10] ? true : false} //coba j
                    handleOnPress={() => this.filterData(910,10)}
                  />
                </View>
              </Body>
            </ScrollView>
          </View>

          <View style={{ marginBottom: this.state.listApdChosen.length!=0?55:5,flex:1,flexDirection:'column'  }}>{list}</View>
        </View>

        <SnackBar ref="ReactNativeSnackBar" data={this.props.navigation} />
        {/* <CustomFooter navigation={this.props.navigation} menu="APD" /> */}
      </Container>
    );
  }
}
AppRegistry.registerComponent("OrderPeminjamanAPD", () => OrderPeminjamanAPD);
