import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  RefreshControl,
  Platform
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Card,
  CardItem,
  Item,
  Input,
  Icon,
  Thumbnail
} from "native-base";
// import Icon from "react-native-vector-icons/FontAwesome";
import LinearGradient from "react-native-linear-gradient";
import GlobalConfig from "../../components/GlobalConfig";
import CustomFooter from "../../components/CustomFooter";

import styles from "../styles/ApprovalAPD";
import colors from "../../../styles/colors";

const helmet = require("../../../assets/images/helmet.png");

var that;
class ListItem extends React.PureComponent {
  navigateToScreen(route, dataOrder) {
    // alert(route);
    AsyncStorage.setItem("idOrder", dataOrder).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    let title = "Status Order";
    let titlePinjam = "Status Pinjam";
    let status, approver1,rejecter, noteReject,statusPinjam,closer;
    if (this.props.data.DETAIL[0].REJECT_BY != null) {
      status = "Rejected";
      statusPinjam = "Order Closed"
      rejecter =
        this.props.data.DETAIL[0].REJECT_BY +
        " [" +
        this.props.data.DETAIL[0].REJECT_AT +
        "]";
      noteReject = this.props.data.DETAIL[0].NOTE_REJECT;
    } else {
      if (this.props.data.APPROVE_BY != null) {
        status="Diapprove K3"
        statusPinjam = "Belum Dikembalikan"
          approver1 =
            this.props.data.APPROVE_BY +
            " [" +
            this.props.data.APPROVE_AT +
            "]";
      } else {
        status = "Belum Diapprove";
        statusPinjam = "Belum Dikembalikan"
      }
    }

    if (this.props.data.DETAIL[0].CLOSED_BY != null) {
      status = "Order Closed";
      approver1=undefined;
      statusPinjam = "Telah Dikembalikan"
      closer =
        this.props.data.DETAIL[0].CLOSED_BY +
        " [" +
        this.props.data.DETAIL[0].CLOSED_AT +
        "]";
    }

    return (
      <View>
        <Card style={{ marginLeft: 10, marginRight: 10, borderRadius:10}}>
          <CardItem
            button
            onPress={() =>
              this.navigateToScreen(
                "DetailMyOrderPeminjamanAPD",
                this.props.data.ID
              )
            }
            style={{borderRadius:10}}
          >
            <View style={{ flexDirection: "column", flex: 1 }}>
              <View style={{ flexDirection: "row", flex: 1 }}>
                <View style={{ flex: 1 }}>
                  <Text
                    style={styles.cardtextKode}
                  >
                    {this.props.data.KODE_PINJAM}
                  </Text>
                </View>
                <View style={{ flex: 1 }}>
                  <Text note style={{ alignSelf: "flex-end", fontSize: 11 }}>
                    Tanggal Order: {this.props.data.CREATE_AT}
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: "row", flex: 1 }}>
                <View style={{ flex: 1 }}>
                  <Text
                    style={styles.cardtextKode}
                  >
                  </Text>
                </View>
                <View style={{ flex: 1 }}>
                  <Text note style={{ alignSelf: "flex-end", fontSize: 11 }}>
                    Tanggal Pinjam: {this.props.data.TANGGAL_PINJAM}
                  </Text>
                </View>
              </View>
              <View style={{ flex: 1 }}>
                <View style={{ flexDirection: "row" }}>
                  <Text note  style={{ fontSize:11}}>
                    {title} :
                  </Text>
                  {status == "Belum Diapprove" && (
                    <Text
                      note
                      style={{
                        alignSelf: "flex-end",
                        fontSize: 11,
                        color: "white",
                        backgroundColor: "#787878",
                        paddingRight: 4,
                        paddingLeft: 4,
                        paddingBottom: 2,
                        paddingTop: 2,
                        borderRadius: 6,

                      }}
                    >
                     {status}
                    </Text>
                  )}
                  {status == "Diapprove K3" && (
                    <Text
                      note
                      style={{
                        alignSelf: "flex-end",
                        fontSize: 11,
                        color: "white",
                        backgroundColor: "#1D84C6",
                        paddingRight: 4,
                        paddingLeft: 4,
                        paddingBottom: 2,
                        paddingTop: 2,
                        borderRadius: 6,

                      }}
                    >
                     {status}
                    </Text>
                  )}
                  {status == "Rejected" && (
                    <Text
                      note
                      style={{
                        alignSelf: "flex-end",
                        fontSize: 11,
                        color: "white",
                        backgroundColor: "#FF0101",
                        paddingRight: 4,
                        paddingLeft: 4,
                        paddingBottom: 2,
                        paddingTop: 2,
                        borderRadius: 6,

                      }}
                    >
                     {status}
                    </Text>
                  )}
                  {status == "Order Closed" && (
                    <Text
                      note
                      style={{
                        alignSelf: "flex-end",
                        fontSize: 11,
                        color: "white",
                        backgroundColor: "#1BB394",
                        paddingRight: 4,
                        paddingLeft: 4,
                        paddingBottom: 2,
                        paddingTop: 2,
                        borderRadius: 6
                      }}
                    >
                      {status}
                    </Text>
                  )}
                </View>


                {/* {rejecter != undefined && (
                  <View style={{ flexDirection: "column" }}>
                    <Text note style={styles.cardtext}>
                      Order telah direject oleh : {rejecter}
                    </Text>
                    <Text note style={styles.cardtext}>
                      Keterangan : {noteReject}
                    </Text>
                  </View>
                )}
                {(approver1 != undefined || approver2 != undefined) && (
                  <Text note style={styles.cardtext}>
                    Telah diapprove oleh :
                  </Text>
                )}
                {approver1 != undefined && (
                  <Text note style={styles.cardtext}>
                    - Atasan : {approver1}
                  </Text>
                )}
                {approver2 != undefined && (
                  <Text note style={styles.cardtext}>
                    - K3 : {approver2}
                  </Text>
                )} */}
              </View>


              {/*  */}
              <View style={{ flex: 1 }}>
                <View style={{ flexDirection: "row" }}>
                  <Text note  style={{ fontSize:11}}>
                    {titlePinjam} :
                  </Text>
                  {statusPinjam == "Belum Dikembalikan" && (
                    <Text
                      note
                      style={{
                        alignSelf: "flex-end",
                        fontSize: 11,
                        color: "#FF0101",

                      }}
                    >
                     {statusPinjam}
                    </Text>
                  )}
                  {statusPinjam == "Order Closed" && (
                    <Text
                      note
                      style={{
                        alignSelf: "flex-end",
                        fontSize: 11,
                        color: "#1BB394",

                      }}
                    >
                     {statusPinjam}
                    </Text>
                  )}
                  {statusPinjam == "Telah Dikembalikan" && (
                    <Text
                      note
                      style={{
                        alignSelf: "flex-end",
                        fontSize: 11,
                        color: "#1BB394",

                      }}
                    >
                     {statusPinjam}
                    </Text>
                  )}
                </View>
                </View>

                {/*  */}
            </View>

          </CardItem>

        </Card>
      </View>
    );
  }
}

export default class MyOrderPeminjamanAPD extends Component {
  constructor() {
    super();
    this.state = {
      active: "true",
      dataSource: [],
      isloading: true,
      // searchApproval: "",
      isEmpty: false
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} />;

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.setState({
          searchApproval: ""
        });
        this.loadData();
      }
    );
  }

  loadData() {
    this.setState({
      isloading: true
    });
    AsyncStorage.getItem("token").then(value => {
      // alert(JSON.stringify(value));
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/apd/order/list_view/peminjaman";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("my_order", null);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson));
          if (responseJson.status == 422) {
            this.setState({
              isloading: false
            });
            this.setState({
              isEmpty: true
            });
          } else {
            this.setState({
              dataSource: responseJson.data.peminjaman,
              isloading: false,
              isEmpty: false
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  onRefresh() {
    console.log('refreshing')
    this.setState({ isloading: true }, function() {
      this.loadData();
    });
  }

  render() {
    that = this;
    var list;
    if (this.state.isloading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.isEmpty) {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = (
          <ScrollView>
            <FlatList
              data={this.state.dataSource}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isloading}
                  onRefresh={this.onRefresh.bind(this)}
                />}
            />
          </ScrollView>
        );
      }
    }
    return (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
          <Left style={{flex:1}}>

          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>My Order</Title>
          </Body>

            <Right style={{flex:1}}/>

        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={styles.homeWrapper}>
          <Footer style={styles.tabHeight}>
            <FooterTab style={styles.tabfooter}>
              <Button
               onPress={() =>
                  this.props.navigation.navigate("MyOrderPersonalAPD")
                }
                >
                <Text style={{color:'white', fontWeight:'bold'}}>PERSONAL</Text>
              </Button>
              <Button
                onPress={() =>
                  this.props.navigation.navigate("MyOrderUnitKerjaAPD")
                }
              >
                <Text style={{color:'white', fontWeight:'bold'}}>UNIT KERJA</Text>
              </Button>
              <Button active style={styles.tabfooter}>
                <View style={{height:'40%'}}></View>
                <View style={{height:'50%'}}>
                <Text>PEMINJAMAN</Text>
                </View>
                <View style={{height:'20%'}}></View>
                <View style={{borderWidth:2, marginTop:2, height:0.5, width:'100%', borderColor:colors.white}}></View>
              </Button>
            </FooterTab>
          </Footer>

          <View style={{ flex: 1, flexDirection: "column" }}>
            {list}

          </View>
        </View>
        <CustomFooter navigation={this.props.navigation} menu="MyOrder" />
      </Container>
    );
  }
}
