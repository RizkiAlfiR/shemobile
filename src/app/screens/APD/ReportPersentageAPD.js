import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  AsyncStorage,
  FlatList,
  ActivityIndicator,
} from "react-native";
import {
  Container,
  Text,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Input,
  Item,
  Card,
  Form,
  Picker,
  CardItem,
  Icon
} from "native-base";
import LinearGradient from "react-native-linear-gradient";

import RoundedButtons from "../../components/RoundedButton";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/ReportAPD";
import colors from "../../../styles/colors";
import ChartView from "react-native-highcharts";
import CustomFooter from "../../components/CustomFooter";

class ListItem extends React.PureComponent{
  render(){
      return(
          <View style={{ marginLeft: 10, flex: 2, width: "37%",marginBottom:15 }}>
              <Text note style={{ fontSize: 10, fontWeight: "bold",paddingTop: 10 }}>{this.props.data.name} ({this.props.data.y})</Text>
              {/* <Text style={{ fontSize: 10, fontWeight: "bold" }}> {this.props.data.DATA_CHECKED.CHECK_VAL} {this.props.data.SATUAN}</Text> */}
          </View>
      )
  }
}

export default class ReportPersentageAPD extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      arrKodeApd: [
        "100",
        "200",
        "300",
        "400",
        "500",
        "600",
        "700",
        "800",
        "900",
        "910"
      ],
      arrNamaApd: [
        "P. Kepala",
        "P. Mata",
        "P. Telinga",
        "P. Hidung",
        "P. Tangan",
        "P. Badan",
        "P. Kaki",
        "Full Body",
        "Obat PPPK",
        "Lain-Lain"
      ],
      arrStokApd: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      isloading: true,
      dataPlant: "",
      plantText:'',
      data:[]
    };
  }

  componentDidMount() {
    this.loadPlant();
    //this.searchAPD();
  }

  loadPlant() {
    AsyncStorage.getItem("token").then(value => {
      const url = GlobalConfig.SERVERHOST + "api/Auth/user?token=" + value;

      fetch(url)
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson));
          this.setState({
            dataPlant: responseJson.data.data.plant
          },function(){
            switch(this.state.dataPlant){
            case "5001":
              this.setState({
                plantText: "Tuban"
              });
              break;
            case "5002":
              this.setState({
                plantText: "Gresik"
              });
              break;
            case "5003":
              this.setState({
                plantText: "Rembang"
              });
              break;
            case "5004":
              this.setState({
                plantText: "Cigading"
              });
              break;
          }
          this.loadData(this.state.dataPlant);
          });
          

          
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  _renderItem = ({ item }) => (
    <ListItem data={item}></ListItem>
)

  loadData(plant) {
    var tempArrKodeApd = this.state.arrKodeApd;
    console.log(tempArrKodeApd);
    var tempArrStokApd = this.state.arrStokApd;
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/apd/stock/reporting/percentage";
      // alert(url)
      var formData = new FormData();
      formData.append("token", value);
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson.data));
          for (let i = 0; i < tempArrKodeApd.length; i++) {
            var tempStok = 0;
            var tempItemApd = responseJson.data[plant][tempArrKodeApd[i]];
            for (let j = 0; j < tempItemApd.length; j++) {
              tempStok += parseInt(tempItemApd[j].STOK);
            }
            tempArrStokApd[i] = tempStok;
          }
          this.setState({
            arrStokApd: tempArrStokApd
          }, function() {
            var data = [];
            var tempArrKodeApd = this.state.arrNamaApd;
            // var tempArrStokApd = that.state.arrStokApd;
            for (let i = 0; i < this.state.arrStokApd.length; i++) {
              var name = tempArrKodeApd[i];
              var value = this.state.arrStokApd[i];
              data.push({
                name: name,
                y: value
              });
            }
            this.setState({
              data:data
            })
          this.setState({
            isloading: false
          });
          });

        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  render() {
    var that = this;
    var Highcharts = "Highcharts";
    var conf = {
      chart: {
        type: "pie",
        animation: Highcharts.svg, // don't animate in old IE
        marginRight: 10
        // events: {
        //     load: function () {

        //         // set up the updating of the chart each second
        //         var series = this.series[0];
        //         setInterval(function () {
        //             var x = (new Date()).getTime(), // current time
        //                 y = Math.random();
        //             series.addPoint([x, y], true, true);
        //         }, 1000);
        //     }
        // }
      },
      title: {
        text: "Plant: "+that.state.plantText
      },
      xAxis: {
        type: "datetime",
        tickPixelInterval: 150
      },
      yAxis: {
        title: {
          text: "Value"
        },
        plotLines: [
          {
            value: 0,
            width: 1,
            color: "#808080"
          }
        ]
      },
      tooltip: {
        formatter: function() {
          return (
            "<b>" +
            this.series.name +
            "</b>" +
            ": " +
            Highcharts.numberFormat(this.y, 0)
          );
        }
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      exporting: {
        enabled: false
      },
      series: [
        {
          name: "Jumlah Stok APD :",
          data: (function() {
            // generate an array of random data
            var data=that.state.data
            // that.setState({
            //   data:data
            // })
            // console.log(data);
            return data;
          })()
          // this.state.arrStokApd
        }
      ]
    };

    const options = {
      global: {
        useUTC: false
      },
      lang: {
        decimalPoint: ",",
        thousandsSep: "."
      }
    };

    return (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("LaporanAPD")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>Report Persentage APD</Title>
          </Body>

            <Right style={{flex:1}}/>

        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={styles.homeWrapper}>
          <ScrollView>
            {this.state.isloading ? (
              <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <ActivityIndicator size="large" color="#330066" animating />
              </View>
            ) : (



              <ChartView
                style={{ height: 300 }}
                config={conf}
                options={options}
                originWhitelist={['']}
              />

            )}
            <CardItem>
                            <FlatList
                                data={this.state.data}
                                renderItem={this._renderItem}
                                numColumns={3}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </CardItem>

          </ScrollView>
        </View>
        {/* <CustomFooter navigation={this.props.navigation} menu="APD" /> */}
      </Container>
    );
  }
}
