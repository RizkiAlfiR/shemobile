import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  RefreshControl,
  Platform,
  Dimensions,
  TouchableOpacity,
  Alert,   ImageBackground
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Card,
  CardItem,
  Item,
  Input,
  Icon,
  Thumbnail
} from "native-base";
// import Icon from "react-native-vector-icons/FontAwesome";
import LinearGradient from "react-native-linear-gradient";
import GlobalConfig from "../components/GlobalConfig";
import CustomFooter from "../components/CustomFooter";

import styles from "./styles/ApprovalAPD";
import colors from "../../styles/colors";


export default class LaporanAPD extends Component {
  constructor() {
    super();
    this.state={
        dataUser:null,
        isLoading:true
    }
    
  }

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadData();
      }
    );
    // this.setClickedKategori(0);
    // this.loadData();
  }

  static navigationOptions = {
    header: null
  };

  loadData(){
    this.setState({
        isLoading:true
    })
    AsyncStorage.getItem("token").then(value => {
        const url = GlobalConfig.SERVERHOST + "api/Auth/user?token=" + value;
  
        fetch(url)
          .then(response => response.json())
          .then(responseJson => {
            // alert(JSON.stringify(responseJson));
            this.setState({
              dataUser: responseJson.data.data
            });
            console.log(this.state.dataUser)
            this.setState({
                isLoading:false
            })
          })
          .catch(error => {
            console.log(error);
            this.setState({
                isLoading:false
            })
          });
      });
  }

  logOut(){
    Alert.alert(
      'Confirmation',
      'Logout SHE Mobile?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Yes', onPress: () => this.props.navigation.navigate("LoggedOut") },
      ],
      { cancelable: false }
    );
  }

  render() {
    var company,plant,role
    if (!this.state.isLoading){
      switch(this.state.dataUser.company){
        case 5000:
          company= "PT. Semen Gresik";
          break;
        case 7000:
          company= "PT. Semen Gresik";
          break;
        case 2000:
          company= "PT. Semen Gresik";
          break;
        case 3000:
          company= "PT. Semen Padang";
          break;
        case 4000:
          company= "PT. Semen Tonasa";
          break;
      }

      switch(this.state.dataUser.plant){
        case "5001":
          plant= 'Tuban'
          break;
        case "5002":
          plant= "Gresik";
          break;
        case "5003":
          plant= "Rembang";
          break;
        case "5004":
          plant= "Cigading";
          break;
      }

      switch(this.state.dataUser.role_id){
        case "1":
          role= 'Admin'
          break;
        case "22":
          role= "Tamu";
          break;
        case "18":
          role= "User";
          break;
        case "20":
          role= "Admin K3";
          break;
        case "21":
          role= "Manager K3";
          break;
        case "23":
          role= "Fire";
          break;
      }
    }
    
    
    return (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
          <Left style={{flex:1}}>
            
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
          <Title style={styles.textbody}>Profile</Title>
          </Body>
          
          <Right style={{flex:1}}>
            <Button
                  transparent
                  // style={{position:'absolute',top:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?40:20,right:20}}
                  onPress={()=>this.logOut()}
                >
                  <Icon
                    name='ios-exit'
                    size={20}
                    style={{color:colors.gray01}}
                  />
                </Button>
          </Right>

          
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <ImageBackground
                style={{
                  alignSelf: "stretch",
                  width: Dimensions.get("window").width,
                  height: (Dimensions.get("window").height) / 4
                }}
                source={require("../../assets/images/headprof-img.jpg")}>
              
              </ImageBackground>
        <View style={styles.homeWrapper}>
          <View style={{ flex: 1, flexDirection: "column" }}>
          <View>
          {this.state.isLoading ? (
              <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                  marginTop:30
                }}
              >
                <ActivityIndicator size="large" color="#330066" animating />
              </View>
            ) : (
                <Card style={{ marginLeft: 20, marginRight: 20, marginTop:20,borderRadius:5,}}>
                <CardItem
                style={{ borderRadius:5,}}
                    
                >
                    <View style={{flexDirection:"row"}}>
                      <View style={{ flexDirection: "column", flex: 1 }}>
                        <Text style={{ fontSize: 15 }}> {this.state.dataUser.name}</Text>
                        <Text style={{ fontSize: 11 }}> No Badge :{this.state.dataUser.no_badge} </Text>
                        <Text style={{ fontSize: 11 }}> Posisi :{this.state.dataUser.pos_text} </Text> 
                        <Text style={{ fontSize: 11 }}> {this.state.dataUser.unit_kerja} </Text>
                        <Text style={{ fontSize: 11 }}> Company :{company} </Text>
                        <Text style={{ fontSize: 11 }}> Plant :{plant} </Text>
                        <Text style={{ fontSize: 11 }}> Role :{role} </Text>
                      </View>
                      <Image
                source={require("../../assets/images/Profil-Color.png")}
                style={{ width: 50, height: 50, resizeMode: "contain" }}
              />
                    </View>
                    
                    
                </CardItem>
                    
                </Card>)}
            </View>
            <View style={{flex: 1,
  justifyContent: 'flex-end',
  }}>
          <Button
            block
            style={{
              bottom:0,
              width:'90%',
              height: 45,
              marginLeft: 20,
              marginRight: 20,
              marginBottom: 15,
              borderWidth: 1,
              backgroundColor: colors.red,
              borderColor: colors.red,
              borderRadius: 4
            }}
            onPress={() => this.logOut()}
          >
            <Text style={{color:colors.white}}>Logout</Text>
          </Button>
        </View>
            
         
         
          </View>
          
        </View>
        <CustomFooter navigation={this.props.navigation} menu="Profil" />
      </Container>
    );
  }
}
